Using Git Branches in this Project
----------------------------------



#### Summary

The "master" branch always reflects the state of production.
The "develop" branch collects all new developments.
"feature/*" branches are for developing bigger features in parallel.
"release/*" branches are for getting new developments production-ready.
"hotfix/*" branches are for urgent bug fixes.



#### Rationale

This project will transition to using the workflow suggested by 
[Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/) .

The "new" workflow is meant to keep the code clean, to separate features from 
each other and to always provide a production-ready, deployable "master" branch.

Once the workflow is established, we can start using the 
[Gitflow Git extension](https://github.com/nvie/gitflow) to help with operations
common to this workflow.




DESIRED WORKFLOW:
-----------------



#### Situation: There is a small bug that is not critical.

These changes can be made right to the "develop" branch.

    # DISCRETE GIT COMMANDS
    git checkout develop

    # GITFLOW COMMANDS
    n/a

Write, test and add your new code.

    # DISCRETE GIT COMMANDS
    git add path/to/your/changes
    git commit -m 'bugfix #<ticketnumber>'
    git push origin develop

    # GITFLOW COMMANDS
    n/a



#### Situation: Creating a new feature.

It's a bright new day and you need to start a brand new feature for the project. 
The gitflow mod has some commands to manage branching in this case. 
Replace <featurename> with the name of your new feature. ie: "granttracker" 
will become a branch named feature/granttracker.

    # DISCRETE GIT COMMANDS
    git checkout -b feature/<featurename> develop

    # GITFLOW COMMANDS
    git flow feature start <featurename>

Write code and commit as normal into this branch. By default the branch will be 
labeled "feature/featurename". When your feature is finished and ready, we add 
it back into the "develop" branch.

    # COMMON COMMANDS FOR WRAPPING IT UP
    git add path/to/your/changes
    git commit -m 'final commit for wrapping up this feature'

    # DISCRETE GIT COMMANDS
    git checkout develop
    git merge --no-ff feature/<featurename>
    git branch -d feature/<featurename>

    # GITFLOW COMMANDS
    git flow feature finish <featurename>

This feature gets merged back into the "develop" branch. Share this with your 
colleagues like this:

    # DISCRETE GIT COMMANDS
    git push origin develop

    # GITFLOW COMMANDS
    n/a # gitflow operates locally only, so you have to git-push "by hand"



#### Situation: Push to production

All your hard work has paid off and you want to push this code on to production!
We are going to create a release which will have a specified number. This 
release will eventually make it to the "master" branch as a tag. For releases, 
increment the version number part after the decimal point, i.e. "2.1" ==> "2.2".
Replace <release#> with your next release number.

    # DISCRETE GIT COMMANDS
    cat VERSION.txt # get an idea about the current version number
    git checkout -b release/<release#> develop
    ./bump-version.sh <release#> # at least, take note of the new release number
    git commit -a -m "Bumped version number to <release#>"

    # GITFLOW COMMANDS
    cat git-flow-version # get an idea about the current version number
    git flow release start <release#>

You will be able to make some changes and adjustments at this stage because 
it's a normal branch. When you have finished making adjustments to your release 
branch, make sure to pass on this "glue" wisdom to both "master" AND "develop" 
branches. Replace <release#> with your next release number.

    # COMMON COMMANDS FOR WRAPPING IT UP
    git add path/to/your/changes
    git commit -m 'final commit for wrapping up this release'

    # DISCRETE GIT COMMANDS
    git checkout master
    git merge --no-ff release/<release#>
    git tag -a <release#>
    git checkout develop
    git merge --no-ff release/<release#>
    git branch -d release/<release#>

    # GITFLOW COMMANDS
    git flow release finish <release#>

This will merge your code onto the "master" and "develop" branches with a 
tag corresponding to your release number. From here we push our new tag and code 
on to the remote repository. Please make sure you are on the master branch at 
this step.

    # DISCRETE GIT COMMANDS
    git checkout master; git push --tags origin master
    git checkout develop; git push --tags origin develop

    # GITFLOW COMMANDS
    n/a # gitflow operates locally only, so you have to git-push "by hand"



#### Situation: Hotfix

You've found a security flaw in the code which needs to be updated right away! 
This will add a new tag onto the master branch. For hotfixes, increment the
version number part after the decimal point, i.e. "2.9" ==> "2.10".
Replace <hotfix#> with your next hotfix number.

    # DISCRETE GIT COMMANDS
    cat VERSION.txt # get an idea about the current version number
    git checkout -b hotfix/<hotfix#> master
    ./bump-version.sh <hotfix#> # at least, take note of the new hotfix number
    git commit -a -m "Bumped version number to <hotfix#>"

    # GITFLOW COMMANDS
    cat git-flow-version # get an idea about the current version number
    git flow hotfix start <hotfix#>

Make your changes here like you would in a normal branch. When finished this 
branch will be merged straight into master so it should be used sparingly.

    # COMMON COMMANDS FOR WRAPPING IT UP
    git add path/to/your/changes
    git commit -m 'Fixed severe production problem.'

    # DISCRETE GIT COMMANDS
    git checkout master
    git merge --no-ff hotfix/<hotfix#>
    git tag -a <hotfix#>
    git checkout develop
    git merge --no-ff hotfix/<hotfix#>
    git branch -d hotfix/<hotfix#>

    # GITFLOW COMMANDS
    git flow hotfix finish <hotfix#>

Publish the hotfix:

    # DISCRETE GIT COMMANDS
    git checkout master; git push --tags origin master
    git checkout develop; git push --tags origin develop

    # GITFLOW COMMANDS
    n/a # gitflow operates locally only, so you have to git-push "by hand"



#### Credits

Original text (c) 2010 by Smith Milner


