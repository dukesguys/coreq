Tracking version for this project. 
1.0.0 will be first public launch. 
x.0.0 increments on changes to API that are not backwards compatible. 
0.x.0 increments on new features deployed.
0.0.x increments on hotfix deployments.

On each deployment to production environment will have a tagged commit message. The tag will match this version number.
The list of version changes below outlines the features and hotfixes that were deployed on this tag. 


USAEDU 0.1.0 
-------------- 
* Initial install profile development
