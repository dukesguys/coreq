<?php
global $levels, $vocabulary;

$levels = array('ERROR'); //, 'trace');
$vocabulary = taxonomy_vocabulary_machine_name_load('ccss');

$sources = array(
  // 'http://www.corestandards.org/Math.xml',
  // 'http://www.corestandards.org/ela-literacy.xml',
  'http://asn.jesandco.org/resources/D10003FC_manifest.json',
  'http://asn.jesandco.org/resources/D10003FB_manifest.json',
);

$data = array_filter(array_map('load', $sources));
array_walk($data, 'process');

function process($data) {

  $root_level_count = 0;
  $domain_level_count = 0;
  $grade_tids = array();
  $domain_tids = array();  
  $new_eng_anchor_standard = new stdClass(); // used in processing English Anchor Standards
  $domain_level_count_eng_anchor_standard = 0;
  foreach ($data as $domain) {
    
    // If we are processing the first term in the JSON object 
    // then we need to create the root term for the Subject first. 
    if ( $domain_level_count == 0) {
      $subject_root_term = new stdClass();
      switch ($domain->dcterms_subject->prefLabel) {
        case 'Math': // Process the CCSS Mathematics JSON
          $title_salt = "Mathematics" . " - " . rand(); 
          $title_salt = "Mathematics";           
          $subject_root_term->term_title = $title_salt;
          break;
        case 'English':
          $subject_root_term->term_title = "English Language Arts";
          break;          
        default: 
          $subject_root_term->term_title = "Root Term";
          break;
      }
      $subject_root_term->description = '';
      $subject_root_term->page_content = '';
      $subject_root_term->cluster_string = '';
      $subject_root_term->url = '';
      // $subject_root_term->grade_levels = _process_grades($domain); 
      $subject_root_term->weight = $root_level_count;
      $subject_root_term->parent_tid = 0;

      $new_subject_root_term = create_term($subject_root_term);      
      $root_level_count++;
    }
    
    
    // Manage rest of JSON object
    switch ($domain->dcterms_subject->prefLabel) {
    
      case 'Math': // Process the CCSS Mathematics JSON

        // handle special case for "Standards for Mathematical Practice"
        if( $domain_level_count == 0 ) {
          
          $grade_level_count = 0;
          
          // Build new top level term
          $root_term = new stdClass();
          $root_term->term_title = $domain->text;
          $root_term->description = '';
          $root_term->page_content = '';
          $root_term->cluster_string = '';
          $root_term->url = '';
          $root_term->grade_levels = _process_grades($domain); 
          $root_term->weight = $grade_level_count;
          $root_term->parent_tid = $new_subject_root_term->tid;
          
          // Build new leaf terms for the Standards of Math Practice section
          // $cluster_term = new stdClass();
          $new_root_term = create_term($root_term);
          
          // Created first term at grade level
          $grade_level_count++;
          
          $leaf_count = 0;
          foreach ($domain->children as $standards_practice) {
            // print print_r ($standards_practice, TRUE);
            $standard_term = new stdClass();
            $standard_term->term_title = $standards_practice->asn_statementNotation;
            $standard_term->description = $standards_practice->text;
            $standard_term->short_code = $standards_practice->asn_altStatementNotation;
            $standard_term->page_content = $standards_practice->asn_comment->literal;
            $standard_term->cluster_string = '';
            $standard_term->url = $standards_practice->skos_exactMatch[0]->uri;
            $standard_term->grade_levels = _process_grades($standards_practice); 
            $standard_term->weight = $leaf_count;
            $standard_term->parent_tid = $new_root_term->tid;
            
            $new_sec_level_term = create_term($standard_term);
            
            $leaf_count++;
          }
          
          // Create all the grades levels
          $grades = array(
              array("K", "Kindergarden", "http://www.corestandards.org/Math/Content/K/introduction", array("K")),
              array("1", "Grade 1", "http://www.corestandards.org/Math/Content/1/introduction", array("1")),
              array("2", "Grade 2", "http://www.corestandards.org/Math/Content/2/introduction", array("2")), 
              array("3", "Grade 3", "http://www.corestandards.org/Math/Content/3/introduction", array("3")), 
              array("4", "Grade 4", "http://www.corestandards.org/Math/Content/4/introduction", array("4")), 
              array("5", "Grade 5", "http://www.corestandards.org/Math/Content/5/introduction", array("5")), 
              array("6", "Grade 6", "http://www.corestandards.org/Math/Content/6/introduction", array("6")), 
              array("7", "Grade 7", "http://www.corestandards.org/Math/Content/7/introduction", array("7")), 
              array("8", "Grade 8", "http://www.corestandards.org/Math/Content/8/introduction", array("8")),
              array("High School — Number and Quantity", "High School — Number and Quantity", "http://www.corestandards.org/Math/Content/HSN/introduction", array(9, 10, 11, 12)),
              array("High School — Algebra", "High School — Algebra", "http://www.corestandards.org/Math/Content/HSA/introduction", array(9, 10, 11, 12)),
              array("High School — Functions", "High School — Functions", "http://www.corestandards.org/Math/Content/HSF/introduction", array(9, 10, 11, 12)),
              array("High School — Geometry", "High School — Geometry", "http://www.corestandards.org/Math/Content/HSG/introduction", array(9, 10, 11, 12)),
              array("High School — Statistics and Probability&lt;sup&gt;★&lt;/sup&gt;", "High School — Statistics and Probability", "http://www.corestandards.org/Math/Content/HSS/introduction", array(9, 10, 11, 12)),                                                                      
          );


          foreach ($grades as $grade) {
            $standard_term = new stdClass();
            $standard_term->term_title = $grade[1];
            $standard_term->description = '';
            $standard_term->page_content = '';
            $standard_term->url = $grade[2];
            $standard_term->grade_levels = $grade[3]; 
            $standard_term->weight = $grade_level_count;
            $standard_term->parent_tid = $new_subject_root_term->tid;
            $new_term = create_term($standard_term);
            $grade_tids[$grade[0]] = $new_term->tid;
            $grade_level_count++;
          }
          // print print_r($grade_tids, TRUE);
          
        } elseif ( $domain_level_count >= 1 && $domain_level_count < 12) { // Deal with the non-High School standards
          
          // print print_r($domain->dcterms_educationLevel, TRUE);
          
          if ( is_array($domain->dcterms_educationLevel) ) {
            foreach ($domain->dcterms_educationLevel as $standards_grade) {
              // print print_r($standards_grade, TRUE);

              // Create the domain under each of the grades
              $standard_term = new stdClass();
              $standard_term->term_title = $domain->text;
              $standard_term->description = "";
              $standard_term->page_content = "";
              $standard_term->url = "";
              $standard_term->grade_levels = array($standards_grade->prefLabel); 
              $standard_term->weight = $domain_level_count;
              $standard_term->parent_tid = $grade_tids[$standards_grade->prefLabel];
              $new_term = create_term($standard_term);
              $domain_tids[$domain->text . ":" . $standards_grade->prefLabel] = $new_term->tid;
            }
          } else { // Dealing with single education level

              // Create the domain under each of the grades
              $standard_term = new stdClass();
              $standard_term->term_title = $domain->text;
              $standard_term->description = "";
              $standard_term->page_content = "";
              $standard_term->url = "";
              $standard_term->grade_levels = array($domain->dcterms_educationLevel->prefLabel); 
              $standard_term->weight = $domain_level_count;
              $standard_term->parent_tid = $grade_tids[$domain->dcterms_educationLevel->prefLabel];
              $new_term = create_term($standard_term);
              $domain_tids[$domain->text . ":" . $domain->dcterms_educationLevel->prefLabel] = $new_term->tid;
          }
          
          
          // Create the Standards w/ Cluster under each domain
          $standards_count = 0;
          foreach ($domain->children as $cluster) {
            $cluster_name = $cluster->dcterms_description->literal;
            
            foreach( $cluster->children as $standard ) {
              
              // Create the standard
              $standard_term = new stdClass();
              $standard_term->term_title = $standard->asn_statementNotation;
              $standard_term->description = $standard->text;
              $standard_term->short_code = $standard->asn_altStatementNotation;
              $standard_term->page_content = $standard->asn_comment->literal;
              $standard_term->cluster_string = $cluster_name;
              $standard_term->url = $standard->skos_exactMatch[0]->uri;
              $standard_term->grade_levels = _process_grades($standard); 
              $standard_term->weight = $standards_count;
              $standard_term->parent_tid = $domain_tids[$domain->text . ":" . $standard_term->grade_levels[0]];
              
              // print print_r($standard_term, TRUE);
              create_term($standard_term);
              $standards_count++;
              
              // Process standard children
              if (is_array($standard->children)) {
                
                foreach( $standard->children as $standard_child ) {
                
                  // Create the standard
                  $standard_term = new stdClass();
                  $standard_term->term_title = $standard_child->asn_statementNotation;
                  $standard_term->description = $standard_child->text;
                  $standard_term->short_code = $standard_child->asn_altStatementNotation;
                  $standard_term->page_content = $standard_child->asn_comment->literal;
                  $standard_term->cluster_string = $cluster_name;
                  $standard_term->url = $standard_child->skos_exactMatch[0]->uri;
                  $standard_term->grade_levels = _process_grades($standard_child); 
                  $standard_term->weight = $standards_count;
                  $standard_term->parent_tid = $domain_tids[$domain->text . ":" . $standard_term->grade_levels[0]];
                
                  // print print_r($standard_term, TRUE);
                  create_term($standard_term);
                  $standards_count++;
                }
                
              }
              
            }
          
          }
          
        } elseif ( $domain_level_count >= 12 && $domain_level_count < 17) { // Dealing with the High School standards

          // Already created the High School Level, now need to create its domains
          
          // Create the Standards w/ Cluster under each domain
          $domain_hs_count = 1;
          foreach ($domain->children as $domain_real) {
            
            // Create the domain under each of the grades
            $standard_term = new stdClass();
            $standard_term->term_title = $domain_real->text;
            $standard_term->description = "";
            $standard_term->page_content = "";
            $standard_term->url = "";
            $standard_term->grade_levels = array("9", "10", "11", "12"); 
            $standard_term->weight = $domain_hs_count;
            $standard_term->parent_tid = $grade_tids[$domain->text];
            $new_term_hs_domain = create_term($standard_term);
            // print print_r($new_term_hs_domain, TRUE); 
            $domain_hs_count++;
            
            $standards_count = 1;
            foreach ($domain_real->children as $cluster) {
              $cluster_name = $cluster->dcterms_description->literal;

              foreach( $cluster->children as $standard ) {
                
                // Create the standard
                $standard_term = new stdClass();
                $standard_term->term_title = $standard->asn_statementNotation;
                $standard_term->description = $standard->text;
                $standard_term->short_code = $standard->asn_altStatementNotation;                 
                $standard_term->page_content = $standard->asn_comment->literal;
                $standard_term->cluster_string = $cluster_name;
                $standard_term->url = $standard->skos_exactMatch[0]->uri;
                $standard_term->grade_levels = _process_grades($standard); 
                $standard_term->weight = $standards_count;
                $standard_term->parent_tid = $new_term_hs_domain->tid;
                
                // print print_r($standard_term, TRUE);
                create_term($standard_term);
                $standards_count++;
                
                // Process standard children
                if (is_array($standard->children)) {
                  
                  foreach( $standard->children as $standard_child ) {
                  
                    // Create the standard
                    $standard_term = new stdClass();
                    $standard_term->term_title = $standard_child->asn_statementNotation;
                    $standard_term->description = $standard_child->text;
                    $standard_term->short_code = $standard_child->asn_altStatementNotation;                    
                    $standard_term->page_content = $standard_child->asn_comment->literal;
                    $standard_term->cluster_string = $cluster_name;
                    $standard_term->url = $standard_child->skos_exactMatch[0]->uri;
                    $standard_term->grade_levels = _process_grades($standard_child); 
                    $standard_term->weight = $standards_count;
                    $standard_term->parent_tid = $new_term_hs_domain->tid;
                  
                    // print print_r($standard_term, TRUE);
                    create_term($standard_term);
                    $standards_count++;
                  }
                  
                }
                
              }
              
            }
          
          } 


        }
        
        
        break; // End Math
      
      case 'English':
        
        
        if ( $domain_level_count == 0 || $domain_level_count == 6 || $domain_level_count == 9 || $domain_level_count == 11 ) { // Deal with the Anchor Standards 
          
          if ( $domain_level_count == 0) {
            // Build new top level term
            $root_term = new stdClass();
            $root_term->term_title = "Anchor Standards";
            $root_term->description = '';
            $root_term->page_content = '';
            $root_term->cluster_string = '';
            $root_term->url = '';
            $root_term->grade_levels = _process_grades($domain); 
            $root_term->weight = $domain_level_count;
            $root_term->parent_tid = $new_subject_root_term->tid;

            // Build new leaf terms for the Standards of Math Practice section
            // $cluster_term = new stdClass();
            $new_eng_anchor_standard = create_term($root_term);
            
          }
          
          $standard_term = new stdClass();
          $standard_term->term_title = $domain->text;
          $standard_term->description = "";
          $standard_term->page_content = "";
          $standard_term->url = "";
          $standard_term->grade_levels = _process_grades($domain); 
          $standard_term->weight = $domain_level_count_eng_anchor_standard;
          $standard_term->parent_tid = $new_eng_anchor_standard->tid;
           if ( !isset($standard_term->parent_tid) ) {
              print print_r($standard_term, TRUE);
              exit();
            }
          $new_term_eng_domain = create_term($standard_term);
          $domain_level_count_eng_anchor_standard++;
          
          $standards_count = 1;
          foreach ($domain->children as $cluster) {
            $cluster_name = $cluster->dcterms_description->literal;
          
            foreach( $cluster->children as $standard ) {
              
              // Create the standard
              $standard_term = new stdClass();
              $standard_term->term_title = $standard->asn_statementNotation;
              $standard_term->description = $standard->text;
              $standard_term->short_code = $standard->asn_altStatementNotation;              
              $standard_term->page_content = $standard->asn_comment->literal;
              $standard_term->cluster_string = $cluster_name;
              $standard_term->url = $standard->skos_exactMatch[0]->uri;
              $standard_term->grade_levels = _process_grades($standard); 
              $standard_term->weight = $standards_count;
              $standard_term->parent_tid = $new_term_eng_domain->tid;
               if ( !isset($standard_term->parent_tid) ) {
                  print print_r($standard_term, TRUE);
                  exit();
                }
              // print print_r($standard_term, TRUE);
              create_term($standard_term);
              $standards_count++;
              
              // Process standard children
              if (is_array($standard->children)) {
                
                foreach( $standard->children as $standard_child ) {
                
                  // Create the standard
                  $standard_term = new stdClass();
                  $standard_term->term_title = $standard_child->asn_statementNotation;
                  $standard_term->description = $standard_child->text;
                  $standard_term->short_code = $standard_child->asn_altStatementNotation;                  
                  $standard_term->page_content = $standard_child->asn_comment->literal;
                  $standard_term->cluster_string = $cluster_name;
                  $standard_term->url = $standard_child->skos_exactMatch[0]->uri;
                  $standard_term->grade_levels = _process_grades($standard_child); 
                  $standard_term->weight = $standards_count;
                  $standard_term->parent_tid = $new_term_eng_domain->tid;
                   if ( !isset($standard_term->parent_tid) ) {
                      print print_r($standard_term, TRUE);
                      exit();
                    }
                  // print print_r($standard_term, TRUE);
                  create_term($standard_term);
                  $standards_count++;
                }
                
              }
              
            }
            
          }
          
          
        } else {
          
          if ($domain_level_count == 4 || $domain_level_count == 5 || $domain_level_count == 8) {
            $grades = array(
              array("6-8", "Grade 6-8", "", array(6,7,8)),
              array("9-10", "Grade 9-10", "", array(9,10)),
              array("11-12", "Grade 11-12", "", array(11,12)),
            );
          } else {
            $grades = array(
              array("K", "Kindergarden", "", array("K")),
              array("1", "Grade 1", "", array("1")),
              array("2", "Grade 2", "", array("2")), 
              array("3", "Grade 3", "", array("3")), 
              array("4", "Grade 4", "", array("4")), 
              array("5", "Grade 5", "", array("5")), 
              array("6", "Grade 6", "", array("6")), 
              array("7", "Grade 7", "", array("7")), 
              array("8", "Grade 8", "", array("8")),
              array("9-10", "Grade 9-10", "", array(9,10)),
              array("11-12", "Grade 11-12", "", array(11,12)),
            );

          }
          
          // Create the domain term
          
          $standard_term = new stdClass();
          $standard_term->term_title = $domain->text;
          $standard_term->description = "";
          $standard_term->page_content = "";
          $standard_term->url = "";
          $standard_term->grade_levels = _process_grades($domain); 
          $standard_term->weight = $domain_level_count;
          $standard_term->parent_tid = $new_subject_root_term->tid;
           if ( !isset($standard_term->parent_tid) ) {
              print print_r($standard_term, TRUE);
              exit();
            }
          $new_term_eng_domain = create_term($standard_term);          
          
          $grade_level_count = 0;
          $grade_tids = array(); 
          // Create all the grade levels 
          foreach ($grades as $grade) {
            $standard_term = new stdClass();
            $standard_term->term_title = $grade[1];
            $standard_term->description = '';
            $standard_term->page_content = '';
            $standard_term->url = $grade[2];
            $standard_term->grade_levels = $grade[3]; 
            $standard_term->weight = $grade_level_count;
            $standard_term->parent_tid = $new_term_eng_domain->tid;
            $new_term = create_term($standard_term);
            $grade_tids[$grade[0]] = $new_term->tid;
            // print print_r($grade_tids, TRUE);
            $grade_level_count++;
          }
          
          // Create all the standards within the grade levels. 
          // Create the Standards w/ Cluster under each domain
          $standards_count = 0;
          foreach ($domain->children as $cluster) {
            $cluster_name = $cluster->dcterms_description->literal;
            
            foreach( $cluster->children as $standard ) {
              
              // Create the standard
              $standard_term = new stdClass();
              $standard_term->term_title = $standard->asn_statementNotation;
              $standard_term->description = $standard->text;
              $standard_term->short_code = $standard->asn_altStatementNotation;
              $standard_term->page_content = $standard->asn_comment->literal;
              $standard_term->cluster_string = $cluster_name;
              $standard_term->url = $standard->skos_exactMatch[0]->uri;
              $standard_term->grade_levels = _process_grades($standard); 
              $standard_term->weight = $standards_count;
              // Determine parent of this standard
              if ( count($standard_term->grade_levels) == 1 ) {
                // Then this is a single grade item and should be added under the single grade
                $grade_level_str = (string) $standard_term->grade_levels[0];
                $standard_term->parent_tid = $grade_tids[$grade_level_str];
              } else {
                // If it has more than one grade level then we need to determine which range it is. 
                if ( $standard_term->grade_levels[0] == 6 ) {
                  $standard_term->parent_tid = $grade_tids["6-8"];
                } elseif ( $standard_term->grade_levels[0] == 9 ) {
                  $standard_term->parent_tid = $grade_tids["9-10"];
                } elseif ( $standard_term->grade_levels[0] == 11 ) {
                  $standard_term->parent_tid = $grade_tids["11-12"];
                } else {
                  $standard_term->parent_tid = $grade_tids["11-12"];
                }
              }
              // print print_r($standard_term, TRUE);
               if ( !isset($standard_term->parent_tid) ) {
                  print "domain_level_count:" . $domain_level_count . "\n";
                  print print_r($grade_tids, TRUE);
                  print print_r($standard_term, TRUE);
                  exit();
                }
              create_term($standard_term);
              $standards_count++;
              
              // Process standard children
              if (is_array($standard->children)) {
                
                foreach( $standard->children as $standard_child ) {
                
                  // Create the standard
                  $standard_term = new stdClass();
                  $standard_term->term_title = $standard_child->asn_statementNotation;
                  $standard_term->description = $standard_child->text;
                  $standard_term->short_code = $standard_child->asn_altStatementNotation;
                  $standard_term->page_content = $standard_child->asn_comment->literal;
                  $standard_term->cluster_string = $cluster_name;
                  $standard_term->url = $standard_child->skos_exactMatch[0]->uri;
                  $standard_term->grade_levels = _process_grades($standard_child); 
                  $standard_term->weight = $standards_count;
                  // Determine parent of this standard
                  if ( count($standard_term->grade_levels) == 1 ) {
                    // Then this is a single grade item and should be added under the single grade
                    $grade_level_str = (string) $standard_term->grade_levels[0];
                    $standard_term->parent_tid = $grade_tids[$grade_level_str];
                  } else {
                    // If it has more than one grade level then we need to determine which range it is. 
                    if ( $standard_term->grade_levels[0] == 6 ) {
                      $standard_term->parent_tid = $grade_tids["6-8"];
                    } elseif ( $standard_term->grade_levels[0] == 9 ) {
                      $standard_term->parent_tid = $grade_tids["9-10"];
                    } elseif ( $standard_term->grade_levels[0] == 11 ) {
                      $standard_term->parent_tid = $grade_tids["11-12"];
                    }
                  }
                  // print print_r($standard_term, TRUE);
                  if ( !isset($standard_term->parent_tid) ) {
                    print "domain_level_count:" . $domain_level_count . "\n";
                    print print_r($grade_tids, TRUE);
                    print print_r($standard_term, TRUE);
                    exit();
                  }
                  create_term($standard_term);
                  $standards_count++;
                }
                
              }
              
            }
          }
          
        } 
        
        
        break;  // End English
    }
    
    
    // print print_r($root_term, TRUE);
    // break;
    // 
    // print '.';
    $domain_level_count++;
  }
  // print PHP_EOL;
}




function create_term($standard = NULL) {
  global $vocabulary;
  
  if ($standard == NULL) {
    return FALSE; 
  }
  
  print print_r($standard->term_title, TRUE) . "\r\n";
  
  $entity = entity_create('taxonomy_term', array(
    'vid' => $vocabulary->vid,
    'vocabulary_machine_name' => 'ccss',
  ));
  $tmp_str_code = (string) $standard->StatementCodes->StatementCode;
  $tmp_str_grade = (string) $standard->GradeLevels->GradeLevel;
  if (empty($tmp_str_code)) {
    mylog("Missing StatementCode: {$standard->RefURI}");
  }
  else if (empty($tmp_str_grade)) {
    mylog("Missing GradeLevel: {$standard->RefURI}");
  }

  $wrapper = entity_metadata_wrapper('taxonomy_term', $entity);
  $str_replace_find_array = array(
      "&lt;sup&gt;★&lt;/sup&gt;", 
      "<sup>★</sup>", 
  ); 
  $standard->term_title = str_replace($str_replace_find_array, "", $standard->term_title);
  $wrapper->name = (string) $standard->term_title;
  $wrapper->description = (string) $standard->description;
  $wrapper->field_source_url = array('url' => (string) $standard->url);

  $wrapper->field_standard_short_code = (string) $standard->short_code;  

  $wrapper->field_term_page_content = array('value' => $standard->page_content, 'format' => 'plain_text');
  
  // Try to load cluster string or else create new cluster string
  if ( !empty($standard->cluster_string) ) {

    $cluster_term = _get_cluster_tid($standard->cluster_string); 
    if ( empty($cluster_term) ) {
      // Create new term
      $cluster_term = new stdClass();
      $cluster_term->name = $standard->cluster_string;
      $cluster_vocab = 'standards_cluster';
      $vocabularies = taxonomy_vocabulary_get_names();
      if (isset($vocabularies[$cluster_vocab])) {
        $cluster_term->vid = $vocabularies[$cluster_vocab]->vid;
        taxonomy_term_save($cluster_term);
      }
    } else {
      $cluster_term = array_shift($cluster_term);
    }
    // print print_r($cluster_term, TRUE);
    $wrapper->field_standards_cluster = $cluster_term->tid;
  }
  
  // Prep grade levels for this term
  $grades = (array) $standard->grade_levels;
  $wrapper->field_grade_level = $grades;
  $wrapper->weight = $standard->weight;
  $wrapper->parent = array($standard->parent_tid);
  // print print_r($wrapper->value(), 1);
  $wrapper->save();
  
  return $wrapper->value();
}

function _process_grades($standard = NULL) {
  if ($standard == NULL) {
    return FALSE; 
  }
  $grades = array();
  if ( is_array($standard->dcterms_educationLevel) ) {
    foreach($standard->dcterms_educationLevel as $grade_level) {
      $grades[] = $grade_level->prefLabel;
    }
  } else {
    $grades[] = $standard->dcterms_educationLevel->prefLabel;
  }
  return $grades;
}

function _get_cluster_tid($name) {
  
  $cluster_vocab = 'standards_cluster';
  $conditions = array('name' => trim($name));
  if (isset($cluster_vocab)) {
    $vocabularies = taxonomy_vocabulary_get_names();
    if (isset($vocabularies[$cluster_vocab])) {
      $conditions['vid'] = $vocabularies[$cluster_vocab]->vid;
    }
    else {
      // Return an empty array when filtering by a non-existing vocabulary.
      return array();
    }
  }
  return taxonomy_term_load_multiple(array(), $conditions);
}



function load($url) {

  // Parsing JSON
  if (($json = file_get_contents($url)) === FALSE) {
    mylog("Error: could not load $url", 'ERROR');
    return FALSE;
  }
  return json_decode($json); 

  // Parsing XML
  // if (($xml = file_get_contents($url)) === FALSE) {
  //   mylog("Error: could not load $url", 'ERROR');
  //   return FALSE;
  // }
  // return new SimpleXMLElement($xml);
}







function mylog($message, $status = 'trace') {
  global $levels;
  if (in_array($status, $levels)) {
    error_log("[{$status}] $message");
  }
}
