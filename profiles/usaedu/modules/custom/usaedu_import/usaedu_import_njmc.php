<?php
global $levels, $vocabulary;

$levels = array('ERROR'); //, 'trace');
$vocabulary = taxonomy_vocabulary_machine_name_load('curriculum');

$sources = array(
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJMC_Math.csv',
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJMC_ELA.csv',
);

$data = array_filter(array_map('load', $sources));
array_walk($data, 'process');

function process($data) {

  // print print_r($data, TRUE);

  // Setup the general counts. 
  $root_level_count = 0;
  $grade_level_count = 0;
  $unit_level_count = 0;  
  $slo_level_count = 0;
  
  $current_root_level_title = '';
  $current_grade_level_title= '';
  $current_unit_level_title= '';
    
  $content_area_tids = array();
  $grade_tids = array();
  $unit_tids = array();  
  
  // Remove csv header row from giant array. 
  array_shift($data);
  
  // $domain array contents looks like this: 
  // $domain[0] == Standard name
  // $domain[1] == Content Area  
  // $domain[2] == Grade
  // $domain[3] == Unit
  // $domain[4] == Unit name
  // $domain[5] == SLO Code
  // $domain[6] == SLO Text
  // $domain[7] == Alignment to CCSS
    
  foreach ($data as $domain) {
    
    // Process the CSV string into an array.
    $domain = str_getcsv($domain);
    
    // If we have a new Content Area then create the term.
    if ( $domain[1] != $current_root_level_title) {
      $subject_root_term = new stdClass();
      $subject_root_term->term_title = $domain[1];      
      $subject_root_term->description = '';
      $subject_root_term->map_label = '';
      $subject_root_term->page_content = '';
      $subject_root_term->weight = $root_level_count;
      $subject_root_term->parent_tid = 0;
      $new_subject_root_term = create_term($subject_root_term);     
      
      // Set new root level title string
      $current_root_level_title = $domain[1];
      $content_area_tids[$domain[1]] = $new_subject_root_term->tid; 
      $root_level_count++;
    }
    
    
    // If we have a new Grade then we create the term
    if ( $domain[2] != $current_grade_level_title) {
      $subject_root_term = new stdClass();
      if ( is_numeric ($domain[2] ) ) {
        $subject_root_term->term_title = "Grade " . $domain[2];      
      } else {
        if ( $domain[2] == "K" ) {
          $subject_root_term->term_title = "Kindergarten";      
        } else {
          $subject_root_term->term_title = $domain[2];      
        }
      }
      $subject_root_term->description = $subject_root_term->term_title;
      $subject_root_term->map_label = $domain[2];
      $subject_root_term->page_content = '';
      $subject_root_term->weight = $grade_level_count;
      $subject_root_term->parent_tid = $content_area_tids[$domain[1]];
      $new_subject_term = create_term($subject_root_term);     
      
      // Set new grade level title string
      $current_grade_level_title = $domain[2];
      $grade_tids[$current_grade_level_title] = $new_subject_term->tid; 
      $grade_level_count++;
      
      // Since we are adding a new grade we should reset the unit level count.
      $unit_level_count = 0;
    }
    
    // If we have a new Unit then we create the term
    if ( is_numeric($current_grade_level_title) ) {
      $tmp_grade_level_title = "Grade " . $current_grade_level_title;      
    } else {
      if ( $domain[2] == "K" ) {
        $tmp_grade_level_title = "Kindergarten";      
      } else {
        $tmp_grade_level_title = $current_grade_level_title;      
      }
    }
    if ( trim($domain[4]) == "<NONE>" ) {
      $this_unit_name = $tmp_grade_level_title . " Unit " . $domain[3] ;
    } else {
      $this_unit_name = $tmp_grade_level_title . " Unit " . $domain[3] . ": " . $domain[4];
    }
    if ( $this_unit_name  != $current_unit_level_title) {
      $subject_root_term = new stdClass();
      $subject_root_term->term_title = $this_unit_name;      
      $subject_root_term->description = $domain[4];
      $subject_root_term->map_label = "Unit " . $domain[3];
      $subject_root_term->grade_levels = array($domain[2]);      
      $subject_root_term->page_content = '';
      $subject_root_term->weight = $unit_level_count;
      $subject_root_term->parent_tid = $grade_tids[$domain[2]];
      $new_subject_term = create_term($subject_root_term);     
      
      // Set new grade level title string
      $current_unit_level_title = $this_unit_name;
      $unit_tids[$this_unit_name] = $new_subject_term->tid; 
      $unit_level_count++;
      
      // Since we are starting a new unit we should reset the SLO count. 
      $slo_level_count = 0;
    }
    
    // Each row contains an SLO so create that here.
    if ( is_numeric($current_grade_level_title) ) {
      $tmp_grade_level_title = "Grade " . $current_grade_level_title;      
    } else {
      if ( $domain[2] == "K" ) {
        $tmp_grade_level_title = "Kindergarten";      
      } else {
        $tmp_grade_level_title = $current_grade_level_title;      
      }
    }
    $this_slo_name = $tmp_grade_level_title . " Unit " . $domain[3] ." SLO " . $domain[5] ;
    $subject_root_term = new stdClass();
    $subject_root_term->term_title = $this_slo_name;      
    $subject_root_term->description = $domain[6];
    $subject_root_term->map_label = "SLO " . $domain[5];
    $subject_root_term->grade_levels = array($domain[2]);          
    $subject_root_term->page_content = '';
    $subject_root_term->weight = $slo_level_count;
    $subject_root_term->parent_tid = $unit_tids[$current_unit_level_title];
    $subject_root_term->ccss_tids = (array) _get_ccss_tids_for_slo( $domain[7] );
    $new_subject_term = create_term($subject_root_term);     
    
    $slo_level_count++;
    
    // if ($slo_level_count == 3) {
    //   exit(0);
    // }
    
    // print '.';

    // exit();

  }
  
}


function create_term($standard = NULL) {
  global $vocabulary;
  
  if ($standard == NULL) {
    return FALSE; 
  }
  
  print print_r($standard->term_title, TRUE) . "\r\n";
  
  $entity = entity_create('taxonomy_term', array(
    'vid' => $vocabulary->vid,
    'vocabulary_machine_name' => 'curriculum',
  ));
  
  $wrapper = entity_metadata_wrapper('taxonomy_term', $entity);
  $wrapper->name = (string) substr($standard->term_title, 0, 254);
  $wrapper->description = (string) $standard->description;
  
  // $wrapper->field_map_label = $standard->map_label;
  if ( $standard->map_label != '' ) {
    // $wrapper->field_map_label = array('value' => (string) $standard->map_label);
    // $wrapper->field_map_label = array('value' => $standard->map_label, 'format' => 'plain_text');
    $wrapper->field_map_label = (string) $standard->map_label;
  }
  
  if ( $standard->page_content != '' ) {
    $wrapper->field_term_page_content = array('value' => $standard->page_content, 'format' => 'plain_text');
  }
  
  // Prep grade levels for this term
  if ( isset($standard->ccss_tids)) {
    $ccss_tids = (array) $standard->ccss_tids;
    $wrapper->field_related_ccss = $ccss_tids;
  }
  
  // Prep grade levels for this term
  $grades = (array) $standard->grade_levels;
  $wrapper->field_grade_level = $grades;
  
  $wrapper->weight = $standard->weight;
  $wrapper->parent = array($standard->parent_tid);
  // print print_r($wrapper->value(), 1);
  
  $wrapper->save();
  
  return $wrapper->value();
}

function _process_grades($standard = NULL) {
  if ($standard == NULL) {
    return FALSE; 
  }
  $grades = array();
  if ( is_array($standard->dcterms_educationLevel) ) {
    foreach($standard->dcterms_educationLevel as $grade_level) {
      $grades[] = $grade_level->prefLabel;
    }
  } else {
    $grades[] = $standard->dcterms_educationLevel->prefLabel;
  }
  return $grades;
}



function _get_ccss_tids_for_slo( $ccss_codes_str ) {
  
  $ccss_tids_return = array();
  $ccss_shortcode_array = explode(",", $ccss_codes_str);
  foreach ($ccss_shortcode_array as $shortcode ) {
    $vocab = 'ccss';
    $vocabularies = taxonomy_vocabulary_get_names();
    $vocab_vid = $vocabularies[$vocab]->vid;
    
    $taxonomnyQuery = new EntityFieldQuery();

    $terms = $taxonomnyQuery->entityCondition('entity_type', 'taxonomy_term')
       ->propertyCondition('vid', $vocab_vid)
       ->fieldCondition('field_standard_short_code', 'value', trim($shortcode), "=")
       ->execute();
    
    if (isset($terms['taxonomy_term'])) {
      $tids = array_keys($terms['taxonomy_term']);
      foreach ($tids as $tid) {
        $ccss_tids_return[] = taxonomy_term_load($tid);
      }
    }
    
  }
  
  return $ccss_tids_return;
  
}




function load($url) {
  // Parsing CSV
  if (($csv = file_get_contents($url)) === FALSE) {
    mylog("Error: could not load $url", 'ERROR');
    return FALSE;
  }
  $csv_array = array();
  // $csv_array = explode("\r", $csv);
  $csv_array = explode("EOL\n", $csv);
  return $csv_array; 
}







function mylog($message, $status = 'trace') {
  global $levels;
  if (in_array($status, $levels)) {
    error_log("[{$status}] $message");
  }
}
