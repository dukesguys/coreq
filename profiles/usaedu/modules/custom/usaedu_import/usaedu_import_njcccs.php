<?php
global $levels, $vocabulary;

$levels = array('ERROR'); //, 'trace');
$vocabulary = taxonomy_vocabulary_machine_name_load('statestand');

$sources = array(
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_Science.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_H&PE.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_SS.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_VPA.csv',
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_WL.csv',    
);

print print_r($sources, TRUE). "\r\n";

$data = array_filter(array_map('load', $sources));

// print print_r($data, TRUE). "\r\n";

array_walk($data, 'process');

function process($data) {

  // print print_r($data, TRUE);

  // Setup the general counts. 
  $root_level_count = 0;
  $standard_level_count = 0;
  $strand_level_count = 0;
  $grade_level_count = 0;
  $content_statement_level_count = 0;  
  $cpi_level_count = 0;
  
  $current_root_level_title = '';
  $current_standard_level_title= '';
  $current_strand_level_title= '';  
  $current_grade_level_title= '';
  $current_content_statement_level_title= '';
    
  $content_area_tids = array();
  $standard_tids = array();
  $strand_tids = array();  
  $grade_tids = array();
  $content_statement_tids = array();  
  
  // Special for Social Studies we have an "Era" column
  $era_level_count = 0;
  $current_era_level_title = '';
  $era_tids = array();
  
  // Prepared proficiency level lookup array
  $proficiency_level_lookup_array = array(
    "NM" => "Proficiency Level: Novice-Mid",
    "NH" => "Proficiency Level: Novice-High",
    "IL" => "Proficiency Level: Intermediate-Low",
    "IM" => "Proficiency Level: Intermediate-Mid",
    "IH" => "Proficiency Level: Intermediate-High",
    "AL" => "Proficiency Level: Advanced-Low",
    "AM" => "Proficiency Level: Advanced-Mid",
    "AH" => "Proficiency Level: Advanced-High",
  );
  
  // Remove csv header row from giant array. 
  array_shift($data);
  
  // $domain array contents looks like this: 
  // $domain[0] == Standard name
  // $domain[1] == Content Area  
  // $domain[2] == Standard Code
  // $domain[3] == Standard Title
  // $domain[4] == Standard Text
  // $domain[5] == Strand Code
  // $domain[6] == Strand Title
  // $domain[7] == Grade
  // $domain[8] == Grade Levels
  // $domain[9] == Content Statement
  // $domain[10] == CPI #
  // $domain[11] == CPI Text    
  // $domain[12] == Misc (Strand Text (Science), Content Statement Text (WL) or Era title (SS)) 
  foreach ($data as $domain) {
    
    // Process the CSV string into an array.
    $domain = str_getcsv($domain);
    
    // If we have a new Content Area then create the term.
    if ( $domain[1] != $current_root_level_title) {
      $subject_root_term = new stdClass();
      $subject_root_term->term_title = $domain[1];      
      $subject_root_term->description = '';
      $subject_root_term->page_content = '';
      $subject_root_term->weight = $root_level_count;
      $subject_root_term->parent_tid = 0;
      $new_subject_root_term = create_term($subject_root_term);     
      
      // Set new root level title string
      $current_root_level_title = $domain[1];
      $content_area_tids[$domain[1]] = $new_subject_root_term->tid; 
      $root_level_count++;
    }
    
    // If we have a new Standard then we create the term
    $this_standard_name = $domain[2] . " " . $domain[3];
    if ( $this_standard_name != $current_standard_level_title) {
      $subject_root_term = new stdClass();
      $subject_root_term->term_title = $this_standard_name;      
      $subject_root_term->description = $domain[4];
      $subject_root_term->page_content = '';
      $subject_root_term->weight = $standard_level_count;
      $subject_root_term->parent_tid = $content_area_tids[$domain[1]];
      $new_subject_term = create_term($subject_root_term);     
      
      // Set new standard level title string
      $current_standard_level_title = $this_standard_name;
      $standard_tids[$this_standard_name] = $new_subject_term->tid; 
      $standard_level_count++;
      
      // Since we are adding a new Standard we should reset the strand level count.
      $strand_level_count = 0;
      $era_level_count = 0;
    }
    
    //  Processing the Era level for Social Studies
    if ( $domain[1] == 'Social Studies' && $domain[12] != "<NONE>" ) {  // Then process and use the Era column in $domain[12]
      // $era_level_count = 0;
      // $current_era_level_title= '';
      // $era_tids = array();
      
      // print print_r($domain, TRUE). "\r\n";
      //print "here - " . $domain[12] . "\r\n";
      $this_era_name = $domain[12];
      if ( $this_era_name != $current_era_level_title) {
        
        $subject_root_term = new stdClass();
        $subject_root_term->term_title = $this_era_name;      
        $subject_root_term->description = '';
        $subject_root_term->page_content = '';
        $subject_root_term->weight = $era_level_count;
        $subject_root_term->parent_tid = $standard_tids[$current_standard_level_title];
        $new_subject_term = create_term($subject_root_term);     
        
        // Set new standard level title string
        $current_era_level_title = $this_era_name;
        $era_tids[$this_era_name] = $new_subject_term->tid; 
        $era_level_count++;
        
        // Since we are adding a new Standard we should reset the strand level count.
        $strand_level_count = 0;
      }
    
    }
    
    
    // If we have a new Strand then we create the term
    $this_strand_name = $domain[5] . ". " . $domain[6];
    //print "\r\nThis Strand Name - " . $this_strand_name . "";
    //print "\r\nCurr Strand Name - " . $current_strand_level_title . "\r\n";
    if ( $this_strand_name != $current_strand_level_title) {
      $subject_root_term = new stdClass();
      $subject_root_term->term_title = $this_strand_name;      
      if ( $domain[1] == 'Science' ) {
        $subject_root_term->description = $domain[12];
      } else {
        $subject_root_term->description = '';
      }
      $subject_root_term->page_content = '';
      $subject_root_term->weight = $strand_level_count;
      // if ( $domain[1] == 'Social Studies' && $domain[12] != "<NONE>" ) {
      //   //print "\r\nThis Era Name - " . $this_era_name . "\r\n";
      //   $subject_root_term->parent_tid = $era_tids[$current_era_level_title];
      //   // print "\r\nThis Era Name - " . $this_era_name . "\r\n";
      // } else {
        $subject_root_term->parent_tid = $standard_tids[$current_standard_level_title];
      // }
      $new_subject_term = create_term($subject_root_term);     
      
      // Set new strand level title string
      $current_strand_level_title = $this_strand_name;
      $strand_tids[$this_strand_name] = $new_subject_term->tid; 
      $strand_level_count++;
      
      // Since we are adding a new Strand we should reset the grade level count.
      $grade_level_count = 0;
    }
    
    // If we have a new Grade then we create the term
    if ( $domain[7] != $current_grade_level_title) {
      $subject_root_term = new stdClass();
      if ( is_numeric ($domain[7] ) ) {
        $subject_root_term->term_title = "By the end of grade " . $domain[7];      
      } else {
        if ( $domain[7] == "P" ) {
          $subject_root_term->term_title = "By the end of Preschool";      
        } elseif ( $domain[7] == "K" ) {
            $subject_root_term->term_title = "Kindergarten";      
        } elseif ($domain[1] == "World Languages") { // Then we are dealing with proficiency levels and not grades
          $subject_root_term->term_title = $proficiency_level_lookup_array[$domain[7]];
        } else {
          $subject_root_term->term_title = $domain[7];      
        }
      }
      $subject_root_term->description = $subject_root_term->term_title;
      $subject_root_term->page_content = '';
      $subject_root_term->grade_levels = explode(",", $domain[8]);
      $subject_root_term->weight = $grade_level_count;
      $subject_root_term->parent_tid = $strand_tids[$current_strand_level_title];
      $new_subject_term = create_term($subject_root_term);     
      
      // Set new grade level title string
      $current_grade_level_title = $domain[7];
      $grade_tids[$current_grade_level_title] = $new_subject_term->tid; 
      $grade_level_count++;
      
      // Since we are adding a new grade we should reset the unit level count.
      $content_statement_level_count = 0;
    }
    
    // If we have a new Content Statement then we create the term
    if ( $domain[1] == "World Languages") {
      $this_content_statement_name = "CS " . $domain[9];
    } else {
      $tmp_array = explode(".", $domain[10]);
      array_pop($tmp_array);
      $tmp_cs_cpi_tag = implode(".", $tmp_array); // Remove the last digit from the CPI # to get the CS #
      $tmp_cs_small_heading = substr($domain[9], 0, (strpos($domain[9], " ", 20) + 1) );
      $this_content_statement_name = "CS " . $tmp_cs_cpi_tag . ": " . $tmp_cs_small_heading . "...";
    }
    if ( $this_content_statement_name  != $current_content_statement_level_title) {
      $subject_root_term = new stdClass();
      $subject_root_term->term_title = $this_content_statement_name;      
      if ( $domain[1] == "World Languages") {
        $subject_root_term->description = $domain[12];
      } else {
        $subject_root_term->description = $domain[9];
      }
      if ( $domain[1] == 'Science' ) {
        $subject_root_term->page_content = $domain[12];
      } else {
        $subject_root_term->page_content = "";
      }
      $subject_root_term->grade_levels = explode(",", $domain[8]);           
      $subject_root_term->weight = $content_statement_level_count;
      $subject_root_term->parent_tid = $grade_tids[$current_grade_level_title];
      $new_subject_term = create_term($subject_root_term);     
      
      // Set new grade level title string
      $current_content_statement_level_title = $this_content_statement_name;
      $content_statement_tids[$this_content_statement_name] = $new_subject_term->tid; 
      $content_statement_level_count++;
      
      // Since we are starting a new unit we should reset the SLO count. 
      $cpi_level_count = 0;
    }
    
    // Each row contains an CPI so create that here.
    $this_cpi_name = $domain[10];
    $subject_root_term = new stdClass();
    $subject_root_term->term_title = $this_cpi_name;      
    $subject_root_term->description = $domain[11];
    $subject_root_term->page_content = '';
    $subject_root_term->grade_levels = explode(",", $domain[8]);
    $subject_root_term->weight = $cpi_level_count;
    $subject_root_term->parent_tid = $content_statement_tids[$current_content_statement_level_title];
    // $subject_root_term->ccss_tids = (array) _get_ccss_tids_for_slo( $domain[7] );
    $new_subject_term = create_term($subject_root_term);     
    
    $cpi_level_count++;
    
    // print '.';

    // exit();

  }
  
}


function create_term($standard = NULL) {
  global $vocabulary;
  
  if ($standard == NULL) {
    return FALSE; 
  }
  
  $entity = entity_create('taxonomy_term', array(
    'vid' => $vocabulary->vid,
    'vocabulary_machine_name' => 'statestand',
  ));
  
  print print_r($standard->term_title, TRUE) . "\r\n";
  
  $wrapper = entity_metadata_wrapper('taxonomy_term', $entity);
  $wrapper->name = (string) substr($standard->term_title, 0, 254);
  $wrapper->description = (string) $standard->description;
  
  // $wrapper->field_map_label = $standard->map_label;
  if ( $standard->map_label != '' ) {
    // $wrapper->field_map_label = array('value' => (string) $standard->map_label);
    // $wrapper->field_map_label = array('value' => $standard->map_label, 'format' => 'plain_text');
    $wrapper->field_map_label = (string) $standard->map_label;
  }
  
  if ( $standard->page_content != '' ) {
    // $wrapper->field_term_page_content = array('value' => (string) $standard->page_content, 'format' => 'plain_text');
    $wrapper->field_term_page_content = (string) $standard->page_content;
  }
  
  // Prep grade levels for this term
  // if ( isset($standard->ccss_tids)) {
  //   $ccss_tids = (array) $standard->ccss_tids;
  //   $wrapper->field_related_ccss = $ccss_tids;
  // }
  
  // Prep grade levels for this term
  $grades = (array) $standard->grade_levels;
  $wrapper->field_grade_level = $grades;
  
  $wrapper->weight = $standard->weight;
  $wrapper->parent = array($standard->parent_tid);
  
  print print_r($wrapper->value(), 1);
  
  $wrapper->save();
  
  return $wrapper->value();
}

function _process_grades($standard = NULL) {
  if ($standard == NULL) {
    return FALSE; 
  }
  $grades = array();
  if ( is_array($standard->dcterms_educationLevel) ) {
    foreach($standard->dcterms_educationLevel as $grade_level) {
      $grades[] = $grade_level->prefLabel;
    }
  } else {
    $grades[] = $standard->dcterms_educationLevel->prefLabel;
  }
  return $grades;
}



function _get_ccss_tids_for_slo( $ccss_codes_str ) {
  
  
  $cluster_vocab = 'ccss';
  $conditions = array('name' => trim($name));
  if (isset($cluster_vocab)) {
    $vocabularies = taxonomy_vocabulary_get_names();
    if (isset($vocabularies[$cluster_vocab])) {
      $conditions['vid'] = $vocabularies[$cluster_vocab]->vid;
    }
    else {
      // Return an empty array when filtering by a non-existing vocabulary.
      return array();
    }
  }
  return taxonomy_term_load_multiple(array(), $conditions);
  
}




function _get_cluster_tid($name) {
  
  $cluster_vocab = 'standards_cluster';
  $conditions = array('name' => trim($name));
  if (isset($cluster_vocab)) {
    $vocabularies = taxonomy_vocabulary_get_names();
    if (isset($vocabularies[$cluster_vocab])) {
      $conditions['vid'] = $vocabularies[$cluster_vocab]->vid;
    }
    else {
      // Return an empty array when filtering by a non-existing vocabulary.
      return array();
    }
  }
  return taxonomy_term_load_multiple(array(), $conditions);
}



function load($url) {

   

  // Parsing CSV
  if (($csv = file_get_contents($url)) === FALSE) {
    mylog("Error: could not load $url", 'ERROR');
    return FALSE;
  }
  $csv_array = array();
  $csv_array = explode("EOL\r", $csv);
  return $csv_array; 
}







function mylog($message, $status = 'trace') {
  global $levels;
  if (in_array($status, $levels)) {
    error_log("[{$status}] $message");
  }
}
