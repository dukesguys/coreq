<?php

include_once(dirname(__FILE__) . '/lib/UsaeduImport/Exception.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Factory/Interface.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Factory.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Importer/Interface.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Importer/Njcccs.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Interface.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/Abstract.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/Default.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/Science.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/SocialStudies/Abstract.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/SocialStudies/EraAbstract.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/SocialStudies/61.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/SocialStudies/61WithEra.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/SocialStudies/62.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/SocialStudies/63.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/WorldLanguages.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Strategy/Njcccs/21C.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/State/Interface.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/State.php');

$source_file_names = array(
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_Science.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_H&PE.csv',
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_SS_61.csv',
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_SS_62.csv',
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_SS_63.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_VPA.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_WL.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_21C.csv',
  // './profiles/usaedu/modules/custom/usaedu_import/import_data/NJCCCS_Technology.csv',
);

// my little DI container ;-)
$factory = new UsaeduImport_Factory(
  'UsaeduImport_Importer_Njcccs', // the importer class
  'UsaeduImport_State', // holds the state of the current import
  array( // some specialized row handling classes
    'UsaeduImport_Strategy_Njcccs_Science',
    'UsaeduImport_Strategy_Njcccs_SocialStudies_61',
    'UsaeduImport_Strategy_Njcccs_SocialStudies_61WithEra',
    'UsaeduImport_Strategy_Njcccs_SocialStudies_62',
    'UsaeduImport_Strategy_Njcccs_SocialStudies_63',
    'UsaeduImport_Strategy_Njcccs_WorldLanguages',
    'UsaeduImport_Strategy_Njcccs_21C',
  ),
  'UsaeduImport_Strategy_Njcccs_Default' // the default row handling class
);

$importer = $factory->getNewImporter($source_file_names, array('error', 'trace', 'debug'));
$importer->init();
$importer->run();
