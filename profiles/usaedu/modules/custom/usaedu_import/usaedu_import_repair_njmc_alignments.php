<?php

include_once(dirname(__FILE__) . '/lib/UsaeduImport/Schema.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Logger/Interface.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Logger/Abstract.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Logger/Drush.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Csv/Loader.php');
include_once(dirname(__FILE__) . '/lib/UsaeduImport/Repair.php');

$import_filenames = array(
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJMC_ELA.csv',
  './profiles/usaedu/modules/custom/usaedu_import/import_data/NJMC_Math.csv',
);

$logger = new UsaeduImport_Logger_Drush();
$loader = new UsaeduImport_Csv_Loader($logger);

$repair = new UsaeduImport_Repair($logger, $loader);
$repair->run($import_filenames);
