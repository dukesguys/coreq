<?php

interface UsaeduImport_Factory_Interface {
  public function __construct($importerClassName = 'UsaeduImport_Njcccs', $stateClassName = 'UsaeduImport_State', $specializedStrategyClassNames = array(), $defaultStrategyClassName = 'UsaeduImport_Strategy_Njcccs_Default');
  public function getNewImporter(array $source_file_names, array $log_levels = array('error'));
  public function getNewState();
  public function getAllNewStrategies(UsaeduImport_Importer_Interface $importer, UsaeduImport_State_Interface $state);
}
