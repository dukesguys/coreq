<?php

class UsaeduImport_Repair {
  protected $logger = NULL;
  protected $loader = NULL;

  public function __construct(UsaeduImport_Logger_Interface $logger, UsaeduImport_Csv_Loader $loader) {
    $this->logger = $logger;
    $this->loader = $loader;
  }

  public function run(array $filenames = array()) {
    $this->prepare();
    $this->loadFiles($filenames);
    $this->matchSlosWithCsv();
  }

  protected function prepare() {
    db_query("TRUNCATE usaedu_import_log");
    db_query("TRUNCATE usaedu_import_njmc");
    db_query("TRUNCATE usaedu_import_slo_align");
    db_query("TRUNCATE usaedu_import_slo_align_search");
  }

  protected function loadFiles(array $filenames = array()) {
    foreach ($filenames as $filename) {
      if (FALSE !== ($csv = $this->loader->loadCsv($filename))) {
        if (! $this->isApplicableForHeader($csv->header)) {
          $this->logger->error('Unable to find mandatory CSV columns in file: ' . $filename);
          continue;
        }
        $headerMappingResult = $this->makeDbFieldNameToCsvFieldIndexMap($csv->header);
        $this->mapping = $headerMappingResult->map;
        foreach ($csv->csv as $csvRowIndex => $csvRow) {
          $row = $this->map($csvRow);
//           $this->logger->debug('SLO: ' . $row['slo_text']);
          $row['import_file'] = basename($filename);
          $row['import_file_csv_row'] = $csvRowIndex + 2; // add 2 to account for header and 0-based counting
          db_insert('usaedu_import_njmc')->fields($row)->execute();
        }
      }
    }
  }

  protected function matchSlosWithCsv() {
    $stats = array(
      'not_found' => 0,
      'found_too_many' => 0,
      'found' => 0,
      'found_ccss' => 0,
      'saved_slo_with_new_ccss' => 0,
    );
    $unfound = array();
    foreach ($this->getAllSloTermData() as $termData) {
      $foundImports = $this->findImportRow($termData);
      if (0 === count($foundImports)) {
        $stats['not_found']++;
        $this->logger->warn('Unable to find import data for term tid=' . $termData->tid . ' (' . $termData->name . ')');
        continue;
      }
      if (1 < count($foundImports)) {
        $stats['found_too_many']++;
        $this->logger->warn('Found too many (' . count($foundImports) . ') import data rows for term tid=' . $termData->tid . ' (' . $termData->name . ')');
        continue;
      }
      $stats['found']++;
      $found = $foundImports[0];
      db_insert('usaedu_import_slo_align')->fields(array(
        'import_db_table' => 'usaedu_import_njmc',
        'import_id' => $found->import_id,
        'slo_tid' => $termData->tid,
      ))->execute();

      $shortCodes = UsaeduMigrateToolbox::splitShortcodes($found->ccss_shortcodes);
      $ccssTids = array();
      foreach ($shortCodes as $code) {
        $canonical = $this->canonicalize($code);
        db_insert('usaedu_import_slo_align_search')->fields(array(
          'slo_tid' => $termData->tid,
          'search_term_canonical' => $canonical,
          'search_term_orig' => $code,
        ))->execute();
        $matchingCcssTids = $this->getMatchingCcssTids($canonical);
        if (0 === count($matchingCcssTids)) {
          $this->logger->warn('Unable to find CCSS term for: "' . $code . '" found in "' . $found->ccss_shortcodes . '"');
          $unfound[$code] = $code;
        }
        $ccssTids = array_merge($ccssTids, $matchingCcssTids);
      }
      if (0 < count($ccssTids)) {
        $stats['found_ccss']++;
        $stats['saved_slo_with_new_ccss'] += $this->saveRelatedCcssTids($termData->tid, $ccssTids) ? 1 : 0;
      }
    }
    ksort($unfound);
    $this->logger->notice(array_keys($unfound));
    $this->logger->notice($stats);
  }

  protected function getMatchingCcssTids($search) {
    $query = db_select('usaedu_migrate_ccss_njcccs_keywords', 'ccss_njcccs_keywords');
    $query->join('taxonomy_term_data', 'ccss_term', 'ccss_term.tid = ccss_njcccs_keywords.ccss_njcccs_tid');
    $query->join('taxonomy_vocabulary', 'ccss_vocabulary', 'ccss_term.vid = ccss_vocabulary.vid');
    $query->condition('ccss_njcccs_keywords.keyword', $search);
    $query->condition('ccss_vocabulary.machine_name', 'ccss');
    $query->fields('ccss_njcccs_keywords', array('ccss_njcccs_tid'));
    return $query->execute()->fetchCol();
  }

  protected function saveRelatedCcssTids($sloTid, array $ccssTids) {
    if (0 < count($ccssTids)) {
      $sloTerm = taxonomy_term_load($sloTid);
      if (isset($sloTerm->field_related_ccss[LANGUAGE_NONE][0]['tid'])) {
        $alreadyThere = array();
        foreach ($sloTerm->field_related_ccss[LANGUAGE_NONE] as $singleValue) {
          $alreadyThere[] = $singleValue['tid'];
        }
        $together = array_merge($ccssTids, $alreadyThere);
        if (count(array_unique($together)) <= count(array_unique($alreadyThere))) {
          // If we don't really have something to add, don't save.
          return FALSE;
        }
        $ccssTids = $together;
      }
      $ccssTids = array_unique($ccssTids);
      $newMultiValue = array();
      foreach ($ccssTids as $ccssTid) {
        $newMultiValue[] = array('tid' => $ccssTid);
      }
      $sloTerm->field_related_ccss = array(LANGUAGE_NONE => $newMultiValue);
      if (module_exists('taxonomy_revision')) {
        $sloTerm->log = 'Saved related CCSS terms automatically with UsaeduImport_Repair.';
        $sloTerm->revision = TRUE;
      }
      taxonomy_term_save($sloTerm);
      $this->logger->debug('Saved SLO term with ' . count($ccssTids) . ' CCSS alignment(s). term tid=' . $sloTerm->tid . ' (' . $sloTerm->name . ')');
      return TRUE;
    }
  }

  protected function getRelatedCcss($sloTid) {
    $query = db_select('field_data_field_related_ccss', 'related_ccss');
    $query->join('taxonomy_term_data', 'slo_term', 'slo_term.tid = related_ccss.entity_id AND slo_term.revision_id = related_ccss.revision_id');
    $query->condition('related_ccss.entity_type', 'taxonomy_term');
    $query->condition('related_ccss.bundle', 'curriculum');
    $query->condition('related_ccss.deleted', '0');
    $query->condition('related_ccss.entity_id', $sloTid);
    $query->fields('related_ccss', array('delta', 'field_related_ccss_tid'));
    return $query->execute()->fetchAll();
  }

  protected function generalizeShortCodeForGrade9to12($shortCode) {
    // Is this a short code which is for one grade only (out of grade 9, 10, 11 or 12)?
    if (preg_match('{^([A-Z]+)\.(9|10|11|12)\.(\d+[a-z]*)$}', $shortCode, $matches)) {
      // Re-map the grade 9, 10, 11 or 12 to the "grade groups" 9-10 or 11-12.
      // These "grade groups" can be found in the current CCSS (2010 edition).
      $remap = array(
        '9' => '9-10',
        '10' => '9-10',
        '11' => '11-12',
        '12' => '11-12',
      );
      // Re-assemble the generalized short code.
      $general = $matches[1] . '.' . $remap[$matches[2]] . '.' . $matches[3];
      $this->logger->debug('Created more general short code "' . $general . '" from "' . $shortCode . '"');
      return $general;
    }
    return $shortCode;
  }

  protected function canonicalize($search) {
    $search = $this->generalizeShortCodeForGrade9to12($search);
    $processed = drupal_strtolower($search);
  
    // Remove all non-alphanumeric characters.
    $processed = preg_replace('/[^a-z0-9]/', '', $processed);
    return $processed;
  }

  protected function getAllSloTermData() {
    $query = db_select('taxonomy_term_data', 'term');
    $query->join('taxonomy_vocabulary', 'vocabulary', 'term.vid = vocabulary.vid');
    $query->join('field_data_field_map_label', 'label', 'term.tid = label.entity_id');
//     $query->leftJoin('field_data_field_related_ccss', 'related_ccss', 'term.tid = related_ccss.entity_id');
    $query->condition('vocabulary.machine_name', 'curriculum');
    $query->condition('label.field_map_label_value', 'SLO%', 'LIKE');
//     $query->isNull('related_ccss.field_related_ccss_tid');
    $query->fields('term', array('tid', 'name', 'description'));
    return $query->execute()->fetchAll();
  }

  protected function findImportRow($termData) {
    $content_area_map = array(
      'ELA' => 'English Language Arts', 
      'Math' => 'Mathematics', 
    );

    $grade_map = array(
      'Kindergarten' => 'K',
      'Algebra 1' => 'Algebra 1',
      'Algebra 2' => 'Algebra 2',
      'Geometry' => 'Geometry',
    );

    $term_name_pattern = '{
      (?P<content_area_short>
        ELA
        |
        Math
      )
      \s
      (?P<grade_long>
        Kindergarten
        |
        Algebra\s1
        |
        Algebra\s2
        |
        Geometry
        |
          Grade
          \s
          (?P<grade_number>\d+)
      )
      \s
      Unit
      \s
      (?P<unit_number>\d+)
      \s
      SLO
      \s
      (?P<slo_code>\d+)
      $
    }x';
    preg_match($term_name_pattern, $termData->name, $matches);

    $content_area = $content_area_map[$matches['content_area_short']];
    $grade = isset($grade_map[$matches['grade_long']]) ? $grade_map[$matches['grade_long']] : $matches['grade_number'];

    $query = db_select('usaedu_import_njmc', 'import');
    $query->condition('import.content_area', $content_area);
    $query->condition('import.grade', $grade);
    $query->condition('import.unit_number', $matches['unit_number']);
    $query->condition('import.slo_code', $matches['slo_code']);
    $query->condition('import.slo_text', $termData->description);
    $query->fields('import', array('import_id', 'ccss_shortcodes'));
    return $query->execute()->fetchAll();
  }

  protected function map(array $csvRow) {
    $mapped = array();
    foreach ($this->mapping as $fieldName => $csvIndex) {
      $mapped[$fieldName] = $csvRow[$csvIndex];
    }
    return $mapped;
  }

  public function isApplicableForHeader(array $header) {
    $result = $this->makeDbFieldNameToCsvFieldIndexMap($header);
    return isset($result->map) && ! isset($result->notFound);
  }

  protected function makeDbFieldNameToCsvFieldIndexMap(array $csvFileHeader) {
    $map = array();
    $notFound = array();
    foreach ($this->getFieldNameToFileHeaderMap() as $fieldName => $potentialHeaders) {
      foreach ($potentialHeaders as $potentialHeader) {
        if (FALSE !== ($foundIndex = array_search($potentialHeader, $csvFileHeader))) {
          $map[$fieldName] = $foundIndex;
          break;
        }
      }
      if (! isset($map[$fieldName])) {
        $notFound = array_merge($notFound, $potentialHeaders);
      }
    }
    $return = new stdClass;
    if (0 < count($notFound)) {
      $return->notFound = $notFound;
    }
    else {
      $return->map = $map;
    }
    return $return;
  }

  /**
   * Map our consistent code-internal "field names" to (varying) CSV file header column names.
   */
  protected function getFieldNameToFileHeaderMap() {
    // This here is aliased because UsaeduImport_Schema is better useable in usaedu_import.install
    return UsaeduImport_Schema::getNjmcDbFieldNameToCsvFileHeaderMap();
  }
  
}
