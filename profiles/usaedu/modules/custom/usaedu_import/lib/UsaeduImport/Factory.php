<?php

class UsaeduImport_Factory implements UsaeduImport_Factory_Interface {
  protected $importerClassName = 'UsaeduImport_Njcccs';
  protected $stateClassName = 'UsaeduImport_State';
  protected $specializedStrategyClassNames = array(
    'UsaeduImport_Strategy_Njcccs_Science',
    'UsaeduImport_Strategy_Njcccs_SocialStudies',
    'UsaeduImport_Strategy_Njcccs_WorldLanguages',
    'UsaeduImport_Strategy_Njcccs_21C',
  );
  protected $defaultStrategyClassName = 'UsaeduImport_Strategy_Njcccs_Default';
  
  public function __construct($importerClassName = 'UsaeduImport_Njcccs', $stateClassName = 'UsaeduImport_State', $specializedStrategyClassNames = array(), $defaultStrategyClassName = 'UsaeduImport_Strategy_Njcccs_Default') {
    $this->importerClassName = $importerClassName;
    $this->stateClassName = $stateClassName;
    $this->specializedStrategyClassNames = $specializedStrategyClassNames;
    $this->defaultStrategyClassName = $defaultStrategyClassName;
  }

  public function getNewImporter(array $source_file_names, array $log_levels = array('error')) {
    return new $this->importerClassName($source_file_names, $this, $log_levels);
  }

  public function getNewState() {
    return new $this->stateClassName();
  }

  public function getAllNewStrategies(UsaeduImport_Importer_Interface $importer, UsaeduImport_State_Interface $state) {
    $strategies = array();
    foreach ($this->specializedStrategyClassNames as $strategyClassName) {
      $strategies[] = new $strategyClassName($importer, $state);
    }
    // Make sure default strategy comes last.
    $strategies[] = new $this->defaultStrategyClassName($importer, $state);
    return $strategies;
  }
}
