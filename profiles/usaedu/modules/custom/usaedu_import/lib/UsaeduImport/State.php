<?php

class UsaeduImport_State implements UsaeduImport_State_Interface {
  protected $weight = array();
  protected $currentUniqueTitle = array();
  protected $currentTid = array();

  public function __construct() {
  }

  public function getAndIncrementWeight($level = 0) {
    return isset($this->weight[$level]) ? $this->weight[$level]++ : ($this->weight[$level] = 0);
  }

  public function setWeight($level = 0, $newWeight) {
    $this->weight[$level] = $newWeight;
    return $this;
  }

  public function resetChildWeight($level = 0) {
    return $this->setWeight($level + 1, 0);
  }

  public function getCurrentUniqueTitle($level = 0) {
    return $this->currentUniqueTitle[$level];
  }

  public function equalsCurrentUniqueTitle($level = 0, $compareTitle) {
    return ($this->currentUniqueTitle[$level] == $compareTitle);
  }

  public function setCurrentUniqueTitle($level = 0, $newUniqueTitle) {
    $this->currentUniqueTitle[$level] = $newUniqueTitle;
    return $this;
  }

  public function resetChildUniqueTitle($level = 0) {
    return $this->setCurrentUniqueTitle($level + 1, '');
  }

  public function getCurrentTid($level = 0) {
    return $this->currentTid[$level];
  }

  public function getCurrentParentTid($level = 0) {
    return (0 == $level) ? 0 : $this->getCurrentTid($level - 1);
  }

  public function setCurrentTid($level = 0, $newTid) {
    $this->currentTid[$level] = $newTid;
    return $this;
  }
}