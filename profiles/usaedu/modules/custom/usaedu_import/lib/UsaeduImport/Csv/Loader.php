<?php

class UsaeduImport_Csv_Loader {
  protected $logger = NULL;

  public function __construct(UsaeduImport_Logger_Interface $logger) {
    $this->logger = $logger;
  }

  public function loadCsv($url) {
    if (($file_content = file_get_contents($url)) === FALSE) {
      $this->logger->error("Could not load $url");
      return FALSE;
    }

    if (! usaedu_csv_is_utf8($file_content)) {
      $this->logger->error("File is NOT utf8: $url");
      return FALSE;
    }

    $line_ending = usaedu_csv_count_line_endings_heuristics($file_content);

    if (is_null($line_ending)) {
      $this->logger->error("File has ambiguous line endings, i.e. a combination of CR, CRLF and/or LF: $url");
      $this->logger->error('Line ending statistics:');
      $stats = usaedu_csv_count_line_endings_per_kind($file_content);
      $this->logger->error($stats);
      return FALSE;
    }

    $csv = usaedu_csv_parse_string($file_content, ',', '"', $line_ending);
    $csv = usaedu_csv_trim($csv);
    $header = array_shift($csv);
    $return = new stdClass();
    $return->csv = $csv;
    $return->header = $header;
    return $return;
  }
}
