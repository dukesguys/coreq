<?php

class UsaeduImport_Schema {
  /**
   * @return array Keys are DB column names, values are arrays of CSV column names.
   */
  static public function getNjmcDbFieldNameToCsvFileHeaderMap() {
    return array(
      'standard_name' => array('Standard', 'Standard name', 'Standard Name', ), // allow for different header spellings
      'content_area' => array('Content Area'),
      'grade' => array('Grade'),
      'unit_number' => array('Unit'),
      'unit_name' => array('Unit name', 'Unit Name'),
      'slo_code' => array('SLO Code'),
      'slo_text' => array('SLO Text'),
      'ccss_shortcodes' => array('Alignment'),
    );
  }
}
