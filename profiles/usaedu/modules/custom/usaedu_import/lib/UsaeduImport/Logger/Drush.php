<?php

class UsaeduImport_Logger_Drush extends UsaeduImport_Logger_Abstract {
  protected $drushLevelMap = array(
    0 => 'error',
    1 => 'error',
    2 => 'error',
    3 => 'error',
    4 => 'warning',
    5 => 'status',
    6 => 'info',
    7 => 'debug',
  );

  protected function log($level, $message) {
    $numLevel = $this->levelMap[$level];
    if ($numLevel <= $this->maxNumLevel) {
      if (! is_string($message)) {
        $message = var_export($message, TRUE);
      }
      drush_log($message, $this->drushLevelMap[$numLevel]);
    }
  }
}
