<?php

interface UsaeduImport_Logger_Interface {
  public function __construct($maxLevel = 'debug');

  // @see http://en.wikipedia.org/wiki/Syslog
  // @see http://tools.ietf.org/html/rfc5424
  public function emergency($message);     // Level 0
  public function emerg($message);         // Level 0
  public function panic($message);         // Level 0

  public function alert($message);         // Level 1

  public function critical($message);      // Level 2
  public function crit($message);          // Level 2

  public function error($message);         // Level 3
  public function err($message);           // Level 3

  public function warning($message);       // Level 4
  public function warn($message);          // Level 4

  public function notice($message);        // Level 5

  public function informational($message); // Level 6
  public function info($message);          // Level 6

  public function debug($message);         // Level 7
}
