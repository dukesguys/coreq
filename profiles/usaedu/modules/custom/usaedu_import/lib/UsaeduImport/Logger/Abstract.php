<?php

abstract class UsaeduImport_Logger_Abstract implements UsaeduImport_Logger_Interface {
  protected $maxNumLevel;

  protected $levelMap = array(
    'emergency' => 0,
    'emerg' => 0,
    'panic' => 0,
    'alert' => 1,
    'critical' => 2,
    'crit' => 2,
    'error' => 3,
    'err' => 3,
    'warning' => 4,
    'warn' => 4,
    'notice' => 5,
    'informational' => 6,
    'info' => 6,
    'debug' => 7,
  );

  public function __construct($maxLevel = 'debug') {
    $this->maxNumLevel = $this->levelMap[$maxLevel];
  }

  public function emergency($message) {
    return $this->log('emergency', $message);
  }
  public function emerg($message) {
    return $this->log('emergency', $message);
  }
  public function panic($message) {
    return $this->log('emergency', $message);
  }

  public function alert($message) {
    return $this->log('alert', $message);
  }

  public function critical($message) {
    return $this->log('critical', $message);
  }
  public function crit($message) {
    return $this->log('critical', $message);
  }

  public function error($message) {
    return $this->log('error', $message);
  }
  public function err($message) {
    return $this->log('error', $message);
  }

  public function warning($message) {
    return $this->log('warning', $message);
  }
  public function warn($message) {
    return $this->log('warning', $message);
  }

  public function notice($message) {
    return $this->log('notice', $message);
  }

  public function informational($message) {
    return $this->log('informational', $message);
  }
  public function info($message) {
    return $this->log('informational', $message);
  }

  public function debug($message) {
    return $this->log('debug', $message);
  }
}
