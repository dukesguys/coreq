<?php

interface UsaeduImport_Importer_Interface {
  public function __construct(array $file_names = array(), UsaeduImport_Factory $factory, array $log_levels = array('error'));
  public function init();
  public function run();  
  public function createTerm($standard = array());
}
