<?php

class UsaeduImport_Importer_Njcccs implements UsaeduImport_Importer_Interface {
  protected $factory = NULL;
  protected $state = NULL;
  protected $strategies = array();
  
  protected $log_levels = array(
    'error',
    'debug',
    'trace',
  );

  // Taxonomy vocabulary to import into.
  protected $vocabulary = NULL;

  // File names of CSV files to import.
  protected $file_names = array();

  // Arrays with the content the loaded CSV files, parsed into lines and columns.
  protected $files = array();

  public function __construct(array $file_names = array(), UsaeduImport_Factory $factory, array $log_levels = array('error')) {
    if (array() != $log_levels) {
      $this->log_levels = array_intersect($this->log_levels, $log_levels);
    }

    $this->file_names = $file_names;
    $this->log($this->file_names, 'debug');

    $this->factory = $factory;
  }

  public function init() {
    $this->vocabulary = taxonomy_vocabulary_machine_name_load('statestand');
    $this->state = $this->factory->getNewState();
    $this->strategies = $this->factory->getAllNewStrategies($this, $this->state);
  }

  public function run() {
    foreach ($this->file_names as $csvFileName) {
      if (FALSE !== ($csvFile = $this->loadCsv($csvFileName))) {
        $applicableStrategies = $this->getApplicableStrategiesForHeader($csvFile->header);
        if (0 === count($applicableStrategies)) {
          $this->helpUserFindOutWhyAllStrategiesFailThisHeader($csvFileName, $csvFile->header);
        }
        else {
          foreach ($csvFile->csv as $csvFileLineNumber => $csvFileLine) {
            foreach ($applicableStrategies as $potentialStrategy) {
              if ($potentialStrategy->isApplicableForRow($csvFileLine)) {
                try {
                  $potentialStrategy->processRow($csvFileLine);
                }
                catch (UsaeduImport_Exception $e) {
                  $this->log('Unable to process row ' . ($csvFileLineNumber+2) . ' in file ' . $csvFileName . ' reason: ' . $e->getMessage(), 'error');
                }
                continue 2; // next line (outer loop)
              }
            }
          }
        }
      }
    }
  }

  protected function helpUserFindOutWhyAllStrategiesFailThisHeader($csvFileName, $header) {
    $this->log('No applicable import strategies (header mismatch) found for file: ' . $csvFileName, 'error');
    $this->log('Used headers were: ' . implode(',', $header), 'error');
    foreach ($this->strategies as $failedStrategy) {
      try {
        $failedStrategy->setHeader($header);
      }
      catch (UsaeduImport_Exception $e) {
        $this->log('Strategy "' . get_class($failedStrategy) . '" can not use headers because: ' . $e->getMessage(), 'error');
      }
    }
  }

  protected function getApplicableStrategiesForHeader($header) {
    $applicableStrategies = array();
    foreach ($this->strategies as $potentialStrategy) {
      if ($potentialStrategy->isApplicableForHeader($header)) {
        $potentialStrategy->setHeader($header);
        $applicableStrategies[] = $potentialStrategy;
      }
    }
    return $applicableStrategies;
  }

  /**
   * Create Drupal taxonomy term from a data array.
   * 
   * @param array $standard term data array with these fields:
   *   - term_title
   *   - description
   *   - map_label
   *   - page_content
   *   - grade_levels
   *   - weight
   *   - parent_tid
   * @return stdClass The taxonomy term object.
   */
  public function createTerm($standard = array()) {
    if (array() === $standard) {
      return FALSE; 
    }

    $standard = (object) $standard;

    $entity = entity_create('taxonomy_term', array(
      'vid' => $this->vocabulary->vid,
      'vocabulary_machine_name' => 'statestand',
    ));

    $this->log($standard->term_title, 'debug');
    
    $wrapper = entity_metadata_wrapper('taxonomy_term', $entity);
    $wrapper->name = (string) substr($standard->term_title, 0, 254);
    $wrapper->description = (string) $standard->description;
    
    // $wrapper->field_map_label = $standard->map_label;
    if (! empty($standard->map_label)) {
      // $wrapper->field_map_label = array('value' => (string) $standard->map_label);
      // $wrapper->field_map_label = array('value' => $standard->map_label, 'format' => 'plain_text');
      $wrapper->field_map_label = (string) $standard->map_label;
    }
    
    if (! empty($standard->page_content)) {
      // $wrapper->field_term_page_content = array('value' => (string) $standard->page_content, 'format' => 'plain_text');
      $wrapper->field_term_page_content = (string) $standard->page_content;
    }
    
    // Prep grade levels for this term
    $grades = (array) $standard->grade_levels;
    $wrapper->field_grade_level = $grades;
    
    $wrapper->weight = $standard->weight;
    $wrapper->parent = array($standard->parent_tid);

    $this->log($wrapper->value(), 'debug');

    $wrapper->save();

    return $wrapper->value();
  }

  protected function loadCsv($url) {
    if (($file_content = file_get_contents($url)) === FALSE) {
      $this->log("Error: could not load $url", 'error');
      return FALSE;
    }

    if (! usaedu_csv_is_utf8($file_content)) {
      $this->log("Error: File is NOT utf8: $url", 'error');
      return FALSE;
    }

    $line_ending = usaedu_csv_count_line_endings_heuristics($file_content);

    if (is_null($line_ending)) {
      $this->log("Error: File has ambiguous line endings, i.e. a combination of CR, CRLF and/or LF: $url", 'error');
      $this->log('Error: Line ending statistics:', 'error');
      $stats = usaedu_csv_count_line_endings_per_kind($file_content);
      $this->log($stats, 'error');
      return FALSE;
    }

    $csv = usaedu_csv_parse_string($file_content, ',', '"', $line_ending);
    $csv = usaedu_csv_trim($csv);
    $header = array_shift($csv);
    $return = new stdClass();
    $return->csv = $csv;
    $return->header = $header;
    return $return;
  }

  public function log($message, $status = 'trace') {
    if (in_array($status, $this->log_levels)) {
      $exported = var_export($message, TRUE);
      drush_log($exported, $status);
//       var_dump("[{$status}] $message");
    }
  }
}
