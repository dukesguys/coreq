<?php

interface UsaeduImport_State_Interface {
  public function __construct();
  public function getAndIncrementWeight($level = 0);
  public function setWeight($level = 0, $newWeight);
  public function resetChildWeight($level = 0);
  public function getCurrentUniqueTitle($level = 0);
  public function equalsCurrentUniqueTitle($level = 0, $compareTitle);
  public function setCurrentUniqueTitle($level = 0, $newUniqueTitle);
  public function resetChildUniqueTitle($level = 0);
  public function getCurrentTid($level = 0);
  public function getCurrentParentTid($level = 0);
  public function setCurrentTid($level = 0, $newTid);
}
