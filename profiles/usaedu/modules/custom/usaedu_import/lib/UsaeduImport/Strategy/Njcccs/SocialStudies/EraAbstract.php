<?php

abstract class UsaeduImport_Strategy_Njcccs_SocialStudies_EraAbstract extends UsaeduImport_Strategy_Njcccs_SocialStudies_Abstract {
  public function isApplicableForRow(array $row) {
    return
      parent::isApplicableForRow($row)
      &&
      ($this->hasEra($row))
    ;
  }

  protected function getLevels() {
    return array(
      'ContentArea',
      'Standard',
      'Grade',
      'Era',
      'CpiOrSlo',
    );
  }

  protected function getUniqueTitle($levelName) {
    switch ($levelName) {
      case 'Era': return $this->getUniqueTitleEra();
      default: return parent::getUniqueTitle($levelName);
    }
  }

  protected function getUniqueTitleEra() {
    $parts = $this->getContentStatementParts();
    return $parts['title'];
  }

  protected function getContentStatementParts() {
    $content_statement = $this->getField('content_statement');
    $content_statement = trim($content_statement);
    list($title, $body) = preg_split('{[\r\n]+}', $content_statement, 2);
    return array(
      'title' => trim($title),
      'body' => trim($body),
    );
  }

  protected function getDescription($levelName) {
    switch ($levelName) {
      case 'Era': return $this->getDescriptionEra();
      default: return parent::getDescription($levelName);
    }
  }

  protected function getDescriptionEra() {
    $parts = $this->getContentStatementParts();
    return $parts['body'];
  }

  protected function getGradeLevels($levelName) {
    switch ($levelName) {
      case 'Era': return parent::getGradeLevels('ContentStatement');
      default: return parent::getGradeLevels($levelName);
    }
  }
}
