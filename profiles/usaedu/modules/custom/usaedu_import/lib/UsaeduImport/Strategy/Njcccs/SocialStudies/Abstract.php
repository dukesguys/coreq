<?php

abstract class UsaeduImport_Strategy_Njcccs_SocialStudies_Abstract extends UsaeduImport_Strategy_Njcccs_Abstract {
  public function isApplicableForRow(array $row) {
    return
      parent::isApplicableForRow($row)
      &&
      ('Social Studies' == $this->getFieldFromIndexedRow('content_area', $row))
    ;
  }

  public function hasEra(array $row) {
    return
      ('Social Studies' == $this->getFieldFromIndexedRow('content_area', $row))
      &&
      (1 === preg_match('{^\d+\.}', $this->getFieldFromIndexedRow('content_statement', $row)))
    ;
  }
}
