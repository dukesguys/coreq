<?php

class UsaeduImport_Strategy_Njcccs_SocialStudies_62 extends UsaeduImport_Strategy_Njcccs_SocialStudies_EraAbstract {
  public function isApplicableForRow(array $row) {
    return
      parent::isApplicableForRow($row)
      &&
      ('6.2' == trim($this->getFieldFromIndexedRow('standard_code', $row)))
    ;
  }
}
