<?php

class UsaeduImport_Strategy_Njcccs_WorldLanguages extends UsaeduImport_Strategy_Njcccs_Abstract {
  // Prepared proficiency level lookup array
  protected $proficiency_level_lookup_array = array(
    "NM" => "Proficiency Level: Novice-Mid",
    "NH" => "Proficiency Level: Novice-High",
    "IL" => "Proficiency Level: Intermediate-Low",
    "IM" => "Proficiency Level: Intermediate-Mid",
    "IH" => "Proficiency Level: Intermediate-High",
    "AL" => "Proficiency Level: Advanced-Low",
    "AM" => "Proficiency Level: Advanced-Mid",
    "AH" => "Proficiency Level: Advanced-High",
  );

  public function isApplicableForRow(array $row) {
    $parent = parent::isApplicableForRow($row);
    return $parent && ('World Languages' == $this->getFieldFromIndexedRow('content_area', $row));
  }

  protected function getFieldNameToFileHeaderMap() {
    $map = parent::getFieldNameToFileHeaderMap();
    $map['content_statement_text'] = array('Content Statement Text');
    return $map;
  }

  protected function getUniqueTitle($levelName) {
    switch ($levelName) {
      case 'ContentStatement': return "CS " . $this->getField('content_statement');
      default: return parent::getUniqueTitle($levelName);
    }
  }

  protected function getDescription($levelName) {
    switch ($levelName) {
      case 'ContentStatement': return $this->getField('content_statement_text');
      default: return parent::getDescription($levelName);
    }
  }

  protected function getTermTitleGrade() {
    if (isset($this->proficiency_level_lookup_array[$this->getField('grade')])) {
      return $this->proficiency_level_lookup_array[$this->getField('grade')];
    }
    else {
      return parent::getTermTitleGrade();
    }
  }
}
