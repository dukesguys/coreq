<?php

abstract class UsaeduImport_Strategy_Njcccs_Abstract implements UsaeduImport_Strategy_Interface {
  protected $fieldNameToFieldIndexMap = array();
  protected $importer = NULL;
  protected $state = NULL;
  protected $row = array();

  public function __construct(UsaeduImport_Importer_Interface $importer, UsaeduImport_State_Interface $state) {
    $this->importer = $importer;
    $this->state = $state;
  }
  
  public function isApplicableForHeader(array $header) {
    $result = $this->makeFieldNameToFieldIndexMap($header);
    return isset($result->map) && ! isset($result->notFound);
  }
  
  public function setHeader(array $header) {
    $result = $this->makeFieldNameToFieldIndexMap($header);
    if (isset($result->map)) {
      $this->fieldNameToFieldIndexMap = $result->map;
    }
    else {
      throw new UsaeduImport_Exception('Mandatory headers missing: ' . implode(',', $result->notFound));
    }
  }
  
  public function isApplicableForRow(array $row) {
    $notFound = array();
    foreach ($this->getFieldNameToFileHeaderMap() as $fieldName => $potentialHeaders) {
      $index = $this->fieldNameToFieldIndexMap[$fieldName];
      if (! isset($row[$index])) {
        $notFound[] = $fieldName;
      }
    }
    return 0 === count($notFound);
  }
  
  public function processRow(array $row) {
    $this->row = $this->mapRow($row);
    foreach ($this->getLevels() as $level => $levelName) {
      $uniqueTitle = $this->getUniqueTitle($levelName);
      if (! $this->state->equalsCurrentUniqueTitle($level, $uniqueTitle)) {
        $new_subject_term = $this->importer->createTerm(array(
          'term_title' => $this->getTermTitle($levelName),
          'description' => $this->getDescription($levelName),
          'page_content' => $this->getPageContent($levelName),
          'grade_levels' => $this->getGradeLevels($levelName),
          'weight' => $this->state->getAndIncrementWeight($level),
          'parent_tid' => $this->state->getCurrentParentTid($level),
        ));
        $this->state->setCurrentUniqueTitle($level, $uniqueTitle)
          ->setCurrentTid($level, $new_subject_term->tid)
          ->resetChildUniqueTitle($level)
          ->resetChildWeight($level);
      }
    }
    $this->row = array(); // Clean up after this row. Avoid confusing isApplicableForRow() on next row.
  }

  // $row array contents looks like this:
  // $row[0] == $this->getField('standard_name')     // Standard name
  // $row[1] == $this->getField('content_area')      // Content Area
  // $row[2] == $this->getField('standard_code')     // Standard Code
  // $row[3] == $this->getField('standard_title')    // Standard Title
  // $row[4] == $this->getField('standard_text')     // Standard Text
  // $row[5] == $this->getField('strand_code')       // Strand Code
  // $row[6] == $this->getField('strand_title')      // Strand Title
  // $row[7] == $this->getField('grade')             // Grade
  // $row[8] == $this->getField('grade_levels')      // Grade Levels
  // $row[9] == $this->getField('content_statement') // Content Statement
  // $row[10] == $this->getField('cpi_number')       // CPI #
  // $row[11] == $this->getField('cpi_text')         // CPI Text
  // $row[12] == $this->getField('XYZ')              // Misc (Strand Text (Science), Content Statement Text (WL) or Era title (SS))

  /**
   * Map our consistent code-internal "field names" to (varying) CSV file header column names. 
   */
  protected function getFieldNameToFileHeaderMap() {
    return array(
      'standard_name' => array('Standard', 'Standard name', 'Standard Name', ), // allow for different header spellings
      'content_area' => array('Content Area'),
      'standard_code' => array('Standard Code'),
      'standard_title' => array('Standard Title'),
      'standard_text' => array('Standard Text'),
      'strand_code' => array('Strand Code'),
      'strand_title' => array('Strand Title'),
      'grade' => array('Grade'),
      'grade_levels' => array('Grade Levels'),
      'content_statement' => array('Content Statement', 'Content Statement ID'),
      'cpi_number' => array('CPI #'),
      'cpi_text' => array('CPI Text'),
    );
  }

  /**
   * Enlist all hierarchy levels.
   */
  protected function getLevels() {
    return array(
      'ContentArea',
      'Standard',
      'Strand',
      'Grade',
      'ContentStatement',
      'CpiOrSlo',
    );
  }

  protected function makeFieldNameToFieldIndexMap(array $fileHeader) {
    $map = array();
    $notFound = array();
    foreach ($this->getFieldNameToFileHeaderMap() as $fieldName => $potentialHeaders) {
      foreach ($potentialHeaders as $potentialHeader) {
        if (FALSE !== ($foundIndex = array_search($potentialHeader, $fileHeader))) {
          $map[$fieldName] = $foundIndex;
          break;
        }
      }
      if (! isset($map[$fieldName])) {
        $notFound = array_merge($notFound, $potentialHeaders);
      }
    }
    $return = new stdClass;
    if (0 < count($notFound)) {
      $return->notFound = $notFound;
    }
    else {
      $return->map = $map;
    }
    return $return;
  }

  /**
   * Remap CSV row from integer indexes to fieldNames.
   * @param array $row
   * @throws UsaeduImport_Exception
   * @return array
   */
  protected function mapRow(array $row) {
    $mapped = array();
    foreach ($this->fieldNameToFieldIndexMap as $fieldName => $fieldIndex) {
      if (! isset($row[$fieldIndex])) {
        throw new UsaeduImport_Exception('Unable to access CSV field ' . $fieldIndex . ' (' . $fieldName . ')');
      }
      $mapped[$fieldName] = $row[$fieldIndex];
    }
    return $mapped;
  }

  protected function getField($fieldName) {
    if (isset($this->row[$fieldName])) {
      return $this->row[$fieldName];
    }
    $index = $this->fieldNameToFieldIndexMap[$fieldName];
    throw new UsaeduImport_Exception('Unable to find data for CSV column ' . $index . ': ' . $fieldName);
  }

  protected function getFieldFromIndexedRow($fieldName, array $indexedRow) {
    $index = $this->fieldNameToFieldIndexMap[$fieldName];
    if (isset($indexedRow[$index])) {
      return $indexedRow[$index];
    }
    throw new UsaeduImport_Exception('Unable to find data for raw CSV column ' . $index . ': ' . $fieldName);
  }

  protected function getUniqueTitle($levelName) {
    switch ($levelName) {
      case 'ContentArea': return $this->getField('content_area');
      case 'Standard': return $this->getField('standard_code') . " " . $this->getField('standard_title');
      case 'Strand': return $this->getField('strand_code') . ". " . $this->getField('strand_title');
      case 'Grade': return $this->getField('grade');
      case 'ContentStatement': return $this->getUniqueTitleContentStatement();
      case 'CpiOrSlo': return $this->getField('cpi_number');
      default: throw new UsaeduImport_Exception('Unable to create unique title for unknown level name: ' . (string) $levelName);
    }
  }

  protected function getTermTitle($levelName) {
    switch ($levelName) {
      case 'Grade': return $this->getTermTitleGrade();
      case 'ContentStatement': return $this->getTermTitleContentStatement();
      default: return $this->getUniqueTitle($levelName);
    }
  }

  protected function getDescription($levelName) {
    switch ($levelName) {
      case 'Standard': return $this->getField('standard_text');
      case 'Grade': return $this->getTermTitle($levelName);
      case 'ContentStatement': return $this->getField('content_statement');
      case 'CpiOrSlo': return $this->getField('cpi_text');
      default: return '';
    }
  }

  protected function getPageContent($levelName) {
    return '';
  }

  protected function getGradeLevels($levelName) {
    switch ($levelName) {
      case 'Grade': // fall-through intended
      case 'ContentStatement': // fall-through intended
      case 'CpiOrSlo':
        return explode(",", $this->getField('grade_levels'));
      default: return array();
    }
  }

  //////////////////////////////////////////////////////////////////////////////

  protected function getUniqueTitleContentStatement() {
    $cs_cpi_tag = $this->getShortenedCpiNumber();
    $content_statement = $this->getField('content_statement');
    $content_statement = trim(preg_replace('{\s+}', ' ', $content_statement)); // Normalize whitespace
    $uniqueTitle = "CS " . $cs_cpi_tag . ": " . $this->getField('content_statement');
    return $uniqueTitle;
  }

  protected function getTermTitleGrade() {
    if (is_numeric($this->getField('grade'))) {
      $term_title = "By the end of grade " . $this->getField('grade');
    }
    else if ($this->getField('grade') == "P") {
      $term_title = "By the end of Preschool";
    }
    else if ($this->getField('grade') == "K") {
      $term_title = "Kindergarten";
    }
    else {
      $term_title = $this->getField('grade'); // Fallback
    }
    return $term_title;
  }

  protected function getTermTitleContentStatement() {
    $cs_cpi_tag = $this->getShortenedCpiNumber();
    $content_statement = $this->getField('content_statement');
    $cs_shortened_heading = substr($content_statement, 0, (strpos($content_statement, ' ', 20) + 1));
    $uniqueTitle = "CS " . $cs_cpi_tag . ": " . $cs_shortened_heading . "...";
    return $uniqueTitle;
  }

  protected function getShortenedCpiNumber() {
    $cpi_number_parts = explode(".", $this->getField('cpi_number'));
    array_pop($cpi_number_parts); // Remove the last digit from the CPI # to get the CS #
    return implode(".", $cpi_number_parts);
  }
}
