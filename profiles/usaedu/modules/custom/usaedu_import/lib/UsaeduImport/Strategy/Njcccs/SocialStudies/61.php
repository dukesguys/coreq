<?php

class UsaeduImport_Strategy_Njcccs_SocialStudies_61 extends UsaeduImport_Strategy_Njcccs_SocialStudies_Abstract {
  public function isApplicableForRow(array $row) {
    return
      parent::isApplicableForRow($row)
      &&
      ('6.1' == trim($this->getFieldFromIndexedRow('standard_code', $row)))
      &&
      (! $this->hasEra($row))
    ;
  }

  protected function getLevels() {
    return array(
      'ContentArea',
      'Standard',
      'Grade',
      'Strand',
      'ContentStatement',
      'CpiOrSlo',
    );
  }
}
