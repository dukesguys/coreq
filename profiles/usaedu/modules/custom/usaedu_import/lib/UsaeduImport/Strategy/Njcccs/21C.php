<?php

class UsaeduImport_Strategy_Njcccs_21C extends UsaeduImport_Strategy_Njcccs_Abstract {
  public function isApplicableForRow(array $row) {
    $parent = parent::isApplicableForRow($row);
    return $parent && ('21st-Century Life and Careers' == $this->getFieldFromIndexedRow('content_area', $row));
  }

  protected function getLevels() {
    $parentLevels = parent::getLevels();
    $gradePos = array_search('Grade', $parentLevels);
    $levels = array_merge(
      array_slice($parentLevels, 0, $gradePos), // everything up to 'Grade'
      array('Pathway'), // insert 'Pathway' before 'Grade' (after 'Strand')
      array_slice($parentLevels, $gradePos) // 'Grade' and everything after
    );
    return $levels;
  }

  protected function getFieldNameToFileHeaderMap() {
    $map = parent::getFieldNameToFileHeaderMap();
    $map['pathway_code'] = array('Pathway Code');
    $map['pathway_text'] = array('Pathway Text');
    return $map;
  }

  protected function getUniqueTitle($levelName) {
    switch ($levelName) {
      case 'Pathway': return $this->getUniqueTitlePathway($levelName);
      default: return parent::getUniqueTitle($levelName);
    }
  }

  protected function getUniqueTitlePathway($levelName) {
    $code = $this->getField('pathway_code');
    $text = $this->getField('pathway_text');
    if (in_array($code, array('', '<NONE>')) && in_array($text, array('', '<NONE>'))) {
      return 'General Cluster Standards';
    }
    else {
      return $code . '. ' . $text;
    }
  }

  protected function getDescription($levelName) {
    switch ($levelName) {
      case 'Pathway': return $this->getTermTitle($levelName);
      default: return parent::getDescription($levelName);
    }
  }
}
