<?php

class UsaeduImport_Strategy_Njcccs_Science extends UsaeduImport_Strategy_Njcccs_Abstract {
  public function isApplicableForRow(array $row) {
    $parent = parent::isApplicableForRow($row);
    return $parent && ('Science' == $this->getFieldFromIndexedRow('content_area', $row));
  }

  protected function getFieldNameToFileHeaderMap() {
    $map = parent::getFieldNameToFileHeaderMap();
    $map['strand_text'] = array('Strand Text');
    return $map;
  }

  protected function getDescription($levelName) {
    switch ($levelName) {
      case 'Strand': return $this->getField('strand_text');
      default: return parent::getDescription($levelName);
    }
  }

  protected function getPageContent($levelName) {
    switch ($levelName) {
      case 'ContentStatement': return $this->getField('strand_text');
      default: return parent::getPageContent($levelName);
    }
  }
}
