<?php

interface UsaeduImport_Strategy_Interface {
  public function __construct(UsaeduImport_Importer_Interface $importer, UsaeduImport_State_Interface $state);
  public function isApplicableForHeader(array $header);
  public function setHeader(array $header);
  public function isApplicableForRow(array $row);
  public function processRow(array $row);
}
