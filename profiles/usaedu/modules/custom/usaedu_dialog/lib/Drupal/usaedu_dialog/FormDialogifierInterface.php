<?php

// namespace Drupal\usaedu_dialog;

interface FormDialogifierInterface {
  public function __construct($form_id, array $options = array());
  public function getFormId();
  public function formAlter(&$form, &$form_state);
  public function ajaxCallback(&$form, &$form_state);
}
