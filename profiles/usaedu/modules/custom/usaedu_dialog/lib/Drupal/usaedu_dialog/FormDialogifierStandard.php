<?php

// namespace Drupal\usaedu_dialog;

// class FormDialogifierStandard implements Drupal\usaedu_dialog\FormDialogifierInterface {
class FormDialogifierStandard implements FormDialogifierInterface {
  protected $formId = '';
  protected $options = array();
  
  public function __construct($form_id, array $options = array()) {
    $this->formId = $form_id;
    $this->options = $options;
    if (!isset($this->options['wrapper'])) {
      $this->options['wrapper'] = 'dialog-form-wrapper-' . usaedu_dialog_unique_id();
    }
    // TODO validate $this->options['ajax callback'] here in constructor.
  }

  public function getFormId() {
    return $this->formId;
  }

  public function formAlter(&$form, &$form_state) {
    $form['#dialogifier'] = $this;
    usaedu_dialog_dialogify_form($form, $form_state, TRUE, 'usaedu_dialog_form_ajax_callback', $this->options['wrapper']);
  }

  public function ajaxCallback(&$form, &$form_state) {
    if (isset($this->options['ajax callback']) && is_callable($this->options['ajax callback'])) {
      $ajax_callback = $this->options['ajax callback'];
      return $ajax_callback($form, $form_state);
    }
    else {
      return $form;
    }
  }
}
