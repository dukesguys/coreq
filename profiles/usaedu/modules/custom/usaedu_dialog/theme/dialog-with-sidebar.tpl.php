<?php
/**
 * $title - renderable (array or string)
 * $sidebar - render array
 * $symbol - string CSS class
 * $main - render array
 */
?>
<div class="usaedu-dialog-left">
  <div class="usaedu-dialog-symbol <?php print check_plain($symbol); ?>"></div>
  <h2 class="usaedu-dialog-title"><?php print render($title); ?></h2>
  <div class="usaedu-dialog-main">
    <?php print render($main); ?>
  </div>
</div>
<div class="usaedu-dialog-sidebar">
  <div class="usaedu-dialog-close cancel"></div>
  <div class="usaedu-dialog-sidebar-content">
    <?php print render($sidebar); ?>
  </div>
</div>
