/**
 * @file
 * A JavaScript file for the "collect_resource_form" block of the "usaedu_collections" module.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  $(document).ready(function () {
    $('body').delegate('.usaedu-dialog .cancel', 'click', function (event) {
      event.preventDefault();
      $('#dialog').dialog('close');
    });

    // If user choose to create a new collection then remove possible previous selections
    $('#dialog').on( "dialogopen", function( event, ui ) {
      $("#edit-title-dialog").focus(function(){
        $("#edit-my-collections-dialog option:selected").removeAttr("selected");
      });
    } );
  }); // document#ready()

})(jQuery, Drupal, this, this.document); // "anonymous closure"
