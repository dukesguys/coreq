//var $ = jQuery;
Drupal.behaviors.njcoreterms = {
    attach: function(context, settings) {
        $('.njcore-terms-vid', context).live('click', function() {
            var vid = $(this).data('vid');
            var parent = $(this).data('parent');
            var level = $(this).data('level');
            if (vid) {
                get_ajax_response(vid, parent, level);
                var wrapp = get_wrapper(level);
                $("#" + wrapp[0] + " li").each(function(index) {
                    $(this).removeClass('active');
                });
                $("#" + wrapp[1] + " select").val(vid);
                $(this).addClass('active');
            }
        });

        $('.njcore-terms-tid', context).live('click', function() {
            var vid = $(this).data('vid');
            var parent = $(this).data('parent');
            var level = $(this).data('level');
            if (vid) {
                get_ajax_response(vid, parent, level);
                var wrapp = get_wrapper(level);
                $("#" + wrapp[0] + " li").each(function(index) {
                    $(this).removeClass('active');
                });
                $("#" + wrapp[1] + " select").val(parent);
                $(this).addClass('active');
            }
        });

        $('.sel-vid', context).live('change', function() {
            var vid = $(this).val();
            var parent = $(this).data('parent');
            var level = $(this).data('level');
            if (vid) {
                var wrapp = get_wrapper(level);
                $("#" + wrapp[0] + " li").each(function(index) {
                    $(this).removeClass('active');
                });
                $(".vid-" + vid).addClass('active');
                get_ajax_response(vid, parent, level);
            }
        });

        $('.sel-tid', context).live('change', function() {
            var vid = $(this).data('vid');
            var parent = $(this).val();
            var level = $(this).data('level');
            if (vid) {
                var wrapp = get_wrapper(level);
                $("#" + wrapp[0] + " li").each(function(index) {
                    $(this).removeClass('active');
                });
                $(".tid-" + parent).addClass('active');
                get_ajax_response(vid, parent, level);
            }
        });

        function get_ajax_response(vid, parent, level) {
            var div = '<div style="display: none;" id="global-throbber"><div class="ajax-progress-throbber"><div class="throbber"></div></div></div>';
            $.ajax({
                type: "POST",
                url: Drupal.settings.basePath + "get/terms/ajax",
                data: 'vid=' + vid + "&parent=" + parent + "&level=" + level,
                success: function(msg) {
                    if (msg.status == '1') {
                        for (var i = 10; i >= msg.level; i--) {
                            var wrapper = get_wrapper(i);
                            $("#" + wrapper[0]).remove();
                            $("#" + wrapper[1]).remove();
                        }
                        var title = $(".standard-title").html();
                        $("#njcore_terms").append(msg.output1);
                        $("#njcore_terms_select").append(msg.output2);
                        $("#term-title").html(msg.name);
                        if (msg.description != '') {
                            $("#term-desc1").html(msg.description);
                            $("#term-desc2").html("<h3>Description</h3><p>" + msg.description + "</p>");
                        }

                        $("#count_nodes").html(msg.count_nodes);
                        $("#unit-view-list-info").html(msg.output3);
                        //$(".standard-title").html(title + " " + msg.name);
                        $(".standard-title").html(msg.name);
                        document.getElementById("view-resource").href = msg.path;
                        jcf.replaceAll();
                    }
                },
                beforeSend: function() {
                    // Add throbber image before send ajax request.
                    if ($('#global-throbber').length == 0) {
                        $('body').append(div);
                        $('#global-throbber').fadeTo("fast", 0.5);
                    }
                },
                complete: function() {
                    // Remove throbber image ajax request complete.
                    $('#global-throbber').remove();
                }
            });
        }

    }
};

$("#standart-unit").hide();
$("#standart-regular-view").hide();

$(document).ready(function() {
    // Show regular view on regular view button click function.
    $("#standart-regular-view").click(function() {
        $("#standart-unit").hide();
        $("#standart-regular-view").hide();
        $("#standart-regular").show();
        $("#standart-unit-view").show();
    });

    // Show unit view on uni view button click function.
    $("#standart-unit-view").click(function() {
        $("#standart-unit").show();
        $("#standart-regular-view").show();
        $("#standart-regular").hide();
        $("#standart-unit-view").hide();
    });

    // Search button  submit function.
    $("#search-submit").click(function() {
        var search_string = jQuery('#search').val();
        var url = jQuery('#view-resource').attr('href');
        window.location = "" + url + "?search_api_views_fulltext= " + search_string + "&sort_by=created";
        return false;
    });
});

/**
 * Get wrapper level for the selected content.
 * @param {type} level
 * @returns {Array}
 */
function get_wrapper(level) {
    var wrapper = new Array();
    switch (level) {
        case 0:
            wrapper.push('level-zero');
            wrapper.push('select-level-zero');
            break;
        case 1:
            wrapper.push('level-one');
            wrapper.push('select-level-one');
            break;
        case 2:
            wrapper.push('level-two');
            wrapper.push('select-level-two');
            break;
        case 3:
            wrapper.push('level-three');
            wrapper.push('select-level-three');
            break;
        case 4:
            wrapper.push('level-four');
            wrapper.push('select-level-four');
            break;
        case 5:
            wrapper.push('level-five');
            wrapper.push('select-level-five');
            break;
        case 6:
            wrapper.push('level-six');
            wrapper.push('select-level-six');
            break;
        case 7:
            wrapper.push('level-seven');
            wrapper.push('select-level-seven');
            break;
        case 8:
            wrapper.push('level-eight');
            wrapper.push('select-level-eight');
            break;
        case 9:
            wrapper.push('level-nine');
            wrapper.push('select-level-nine');
            break;
        case 10:
            wrapper.push('level-ten');
            wrapper.push('select-level-ten');
            break;
    }
    return wrapper;
}