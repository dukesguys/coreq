<div class="block-standard">
    <div class="row">
        <div class="col-sm-4">
            <!-- column of the page -->
            <div class="col">
                <h2>Start Here:</h2>
                <div action="#" class="form-standard">
                    <fieldset>
                        <ol class="list" id="njcore_terms_select">
                            <li id="select-level-zero">
                                <select class="sel-vid sel" data-parent="0" data-level="0">
                                    <option value="">--Select Value--</option>
                                    <?php foreach ($results as $value) { ?>
                                        <?php echo '<option value="' . $value->vid . '">' . $value->name . '</option>'; ?>
                                    <?php } ?>
                                </select>
                            </li>
                        </ol>
                    </fieldset>
                </div>
                <!-- list button of the page -->
                <div class="list-btn">
                    <form action="#" class="about-form">
                        <fieldset>
                            <input type="search" placeholder="Search within this Standard">
                            <button id="search-submit" type="submit"><i class="icon-search"></i></button>
                        </fieldset>
                    </form>
                    <span id="standart-unit-view" class="btn btn-success"><i class="icon-nav"></i><span>Go to Standards Units</span></span>
                    <span id="standart-regular-view" class="btn btn-success"><i class="icon-nav"></i><span>View Curriculum Map</span></span>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div id="standart-regular">
                <!-- introductory content of the page -->
                <div class="col content ">
                    <h2 class="standard-title"></h2>
                    <ol class="list-content" id="njcore_terms">
                        <li id="level-zero">
                            <ul>
                                <?php foreach ($results as $value) { ?>
                                    <li class="vid-<?php echo $value->vid; ?> njcore-terms-vid" data-vid = "<?php echo $value->vid; ?>" data-parent="0" data-level="0">
                                        <a href="#" class="box">
                                            <span><?php echo $value->name; ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ol>
                    <div class="info">
                        <div class="link">
                            <a href="#">Print this Document</a>
                        </div>
                        <h2 id="term-title"></h2>
                        <p id="term-desc1"></p>
                        <a id="view-resource" target="_blank" class="btn btn-primary" href="#">View Resources (<span id="count_nodes">0</span>)</a>
                    </div>
                </div>
            </div>
            <div id="standart-unit">
                <!-- introductory content of the page -->
                <div class="col content ">
                    <h2 class="standard-title"></h2>
                    <div class="hold">
                        <div class="txt-inf" id="term-desc2">
                        </div>
                        <ul id="unit-view-list-info" class="list-info">
                            <?php foreach ($results as $value) { ?>
                                <li>
                                    <a href="#" class="btn btn-default">
                  <span class="page">
                    <span><?php echo count_nodes_from_vid($value->vid, 'resource'); ?> <i class="icon-arrow-right"></i></span>
                  </span>
                  <span class="txt">
                    <span><?php echo $value->name; ?></span>
                  </span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
