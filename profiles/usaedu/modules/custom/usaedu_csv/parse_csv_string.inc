<?php

/**
 * Parse a CSV string
 * 
 * @param string $csv_string
 * @param string $field_separator
 * @param string $enclosure
 * @param string $record_separator
 * @param string|null $enclosure_escape
 * @return (string[])[] contents of CSV string, separated into lines and cells
 */
function parse_csv_string($csv_string, $field_separator = ',', $enclosure = '"', $record_separator = "\n", $enclosure_escape = NULL) {
  $enclosure_escape = is_null($enclosure_escape) ? $enclosure : $enclosure_escape;
  $escaped_enclosure = $enclosure_escape . $enclosure;

  $preg_csv_field_separator = preg_quote($field_separator);
  $preg_enclosure = preg_quote($enclosure);
  $preg_enclosure_escape = preg_quote($enclosure_escape);
  $preg_escaped_enclosure = $preg_enclosure_escape . $preg_enclosure;
  $preg_csv_record_separator = addcslashes($record_separator, "\r\n");
  $field_pattern = "{
    (?>
      (?P<separator_match>
        ^|$preg_csv_field_separator|$preg_csv_record_separator
      )
      (?:
        $preg_enclosure
        (?P<enclosed_match>
          (?:
            [^$preg_enclosure]
            |
            $preg_escaped_enclosure
          )*
        )
        $preg_enclosure
        |
        (?P<simple_match>
          [^$preg_csv_field_separator$preg_csv_record_separator]*
        )
      )
    )
  }x";
  $offset = 0;
  $matches = array();
  $output = array();
  $fields = array();
  while (1 === preg_match($field_pattern, $csv_string, $matches, 0, $offset)) {
    $full_match = $matches[0];
    $separator_match = $matches['separator_match'];
    $enclosed_match = isset($matches['enclosed_match']) ? $matches['enclosed_match'] : '';
    $simple_match = isset($matches['simple_match']) ? $matches['simple_match'] : '';
    if (FALSE !== strpos($enclosed_match, $escaped_enclosure)) {
      $enclosed_match = str_replace($escaped_enclosure, $enclosure, $enclosed_match);
    }
    if ($record_separator == $separator_match) {
      $output[] = $fields;
      $fields = array();
    }
    $fields[] = (strlen($simple_match) > strlen($enclosed_match)) ? $simple_match : $enclosed_match;
    $offset += strlen($full_match);
  }
  if (0 < count($fields)) {
    $output[] = $fields;
  }
  while (end($output) == array(0 => '')) {
    array_pop($output);
  }
  return $output;
}
