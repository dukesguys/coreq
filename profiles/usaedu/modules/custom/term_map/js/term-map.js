/**
 * USAEDU Taxonomy Terms Graph jQuery plugin
 *
 * Usage:
 *  var myTermsGraph = $("#my-selector").termsgraph({
 *   foo: bar
 * });
 *
 * myTermsGraph.termsgraph("methodA");
 *
 * The HTML structure for this plugin must look to something like this
 *   <div class="the_container">
 *     <ul>
 *       <li>
 *         <span class="title">Taxonomy Term Title</span>
 *         <ul>...</ul>
 *       </li>
 *       ...
 *       <li>
 *         <span class="title">Taxonomy Term Title</span>
 *         <ul>...</ul>
 *       </li>
 *     </ul>
 */

/*! Template credits.
 * jQuery UI Widget-factory plugin boilerplate (for 1.8/9+)
 * Author: @addyosmani
 * Further changes: @peolanha
 * Licensed under the MIT license
 */


;(function ( $, window, document, undefined ) {

  // define our widget under a namespace of your choice
  // with additional parameters e.g.
  // $.widget( "namespace.widgetname", (optional) - an
  // existing widget prototype to inherit from, an object
  // literal to become the widget's prototype );

  $.widget( "usaedu.termsgraph" , {

    //Options to be used as defaults
    options: {
      //container: $(document.body),
      item: 'li',                 // the CSS selector for an item on the graph
      item_title: '.title',       // item's title
      item_desc: '.description',  // item's description
      group: 'ul',                // the CSS selector for the HTML tag which encapsulate a group of 'items'
      scrollOffset: 100,
      speed: 800                  // animation's speed in ms
    },

    //Setup widget (e.g. element creation, apply theming
    // , bind events etc.)
    _create: function () {
      var group = this.options.group,
          title = this.options.item_title;

      this._container = this.options.container || this.element;

      // Identify the container
      this.element.addClass('terms-graph');

      // Display only the first level
      this.element.children(group).each(function() {
        $(this).show();
        $(group, this).hide();
      });

      // Click event on Item
      $(title, this.element).bind("click touchend", $.proxy(this._toggleItemView, this));

      if (window.location.hash) {
        this.open(window.location.hash);
      }
    },

    // Destroy an instantiated plugin and clean up
    // modifications the widget has made to the DOM
    destroy: function () {

      // this.element.removeStuff();
      // For UI 1.8, destroy must be invoked from the
      // base widget
      $.Widget.prototype.destroy.call( this );
      // For UI 1.9, define _destroy instead and don't
      // worry about
      // calling the base widget
    },

    open: function (id) {
        var $item = id[0] === '#' ? $(id) : $('#' + id),
            containerTop = this._container.offset().top;
        this.toggleItemView($item);
        // go to the top of the container
        setTimeout(function() {
          window.scrollTo(0, containerTop - 100);
        }, 1);
    },

    // Toggle the view of an item in the graph
    toggleItemView: function ($item) {
        // Item on active branch
        if (this._onActiveBranch($item)) {
            // Item is active leaf
            if (this._isActive($item)) {
                this._hideBranch($item);
                $item = this._activateParentItem($item);
            }
            else {
                // Item expanded
                if ($item.children(this.options.group).is(":visible")) {
                    this._hideBranch($item);
                }
                // Active sibling branch
                var $sibling = $item.siblings('.active-trail');
                if ($sibling.length) {
                    this._hideBranch($sibling)
                }
                this._showGroup($item);
                this._activateItem($item);
            }
        }
        else {
            this._hideBranch();
            this._showGroup($item);
            this._activateItem($item);
            this._ensureActiveTrail($item);
        }

        this._setPath($item);

        if ($item.length > 0) {
            this._scrollTo($item);
        }

        this._trigger("ItemChanged", null, $item);
    },

    _ensureActiveTrail: function ($item) {
        var $current = this._getParentItem($item);
        while ($current.length) {
            this._showGroup($current);
            $current = this._getParentItem($current);
        }
        return this;
    },

    // return true if the "item" is on active branch
    _onActiveBranch: function($item) {
      var active = $item.hasClass('active-trail');
      return !active ? this._getParentItem($item).hasClass('active-trail') : active;
    },

    _isActive: function ($item) {
        return $item.hasClass('active');
    },

    // Get the parent item of "item"
    _getParentItem: function($item) {
      return $item.parent(this.options.group).parent(this.options.item);
    },

    _activateItem: function ($item) {
        var $active_item = $('.active', this.element);

        $active_item.removeClass('active')
        $active_item.prevAll().removeClass('active-before');
        $active_item.nextAll().removeClass('active-after');

        $('.leaf-parent', this.element).removeClass('leaf-parent');

        if ($item.length) {
            $item.addClass('active');
            $item.prevAll().addClass('active-before');
            $item.nextAll().addClass('active-after');
            if ($item.hasClass('leaf')) {
              $item.parents('li:first').addClass('leaf-parent');
            }
        }
    },

    _setPath: function ($item) {
        var id = this.active_id = $item.length ? $item.get(0).id : '';
        this._setHashPreservePosition(id);
    },

    _scrollTo: function ($item) {
       this._container.stop().scrollTo($item, this.options.speed);
    },

    // Set parent of "item" as active
    _activateParentItem: function($item) {
      var $parent = this._getParentItem($item);
      this._activateItem($parent);
      return $parent;
    },

    // Hide a branch
    _hideBranch: function ($stop, $current) {
      var $stop = $stop || this.element,
          $branch = $('.active', $stop.parent());

      if ($current && $current.parent().get(0) === $stop.parent().get(0)) {
        if ($stop.hasClass('active')) {
          this._hideGroup($stop);
          this._activateParentItem($stop);
        }
      }
      else if ($branch.length) {
        this._hideGroup($branch);
        this._hideBranch($stop, this._activateParentItem($branch));
      }
    },

    // Hide the group contained into "item"
    _hideGroup: function ($item) {
      $item
        .removeClass('active-trail')
        .children(this.options.group).hide();

      $item.prevAll().removeClass('active-trail-before');
      $item.nextAll().removeClass('active-trail-after');
    },

    // Show the group contain into "item"
    _showGroup: function ($item) {
      $item
        .addClass('active-trail')
        .children(this.options.group).show();

      $item.prevAll().addClass('active-trail-before');
      $item.nextAll().addClass('active-trail-after');
    },

    /**
     * Event handler.
     */
    _toggleItemView: function (e) {
      var $item = $(e.currentTarget),
        doToggle = true;

      // This method is also called by the event click on the item's title
      if (e.type === 'click' || e.type === 'touchend') {
        $item = $item.parent(this.options.item);

        if ($(e.target).hasClass('term-count')/* || $(e.target).parent().hasClass('leaf')*/) {
          doToggle = false;
        }
      }

      if (doToggle === true) {
        this.toggleItemView($item);

        return false;
      }
    },

    /**
     * Set current hash while preserving the browser window scroll position.
     */
    _setHashPreservePosition: function (hash) {
        // Solution proposed by Lea Verou: http://lea.verou.me/2011/05/change-url-hash-without-page-jump/
        //if(history.pushState) {
        //  history.pushState(null, null, hash);
        //}
        //else {
        //  location.hash = hash;
        //}

        // Our solution... after many tries :(
        var doc = document.documentElement, body = document.body,
            left = (doc && doc.scrollLeft || body && body.scrollLeft || 0),
            top = (doc && doc.scrollTop  || body && body.scrollTop  || 0),
            containerTop = this._container.scrollTop(),
            containerLeft = this._container.scrollLeft();

        window.location.hash = hash;

        // Restore the scroll offset, should be flicker free
        window.scrollTo(left, top);
        this._container.scrollTop(containerTop);
        this._container.scrollLeft(containerLeft);
    },

    // Respond to any changes the user makes to the
    // option method
    _setOption: function ( key, value ) {
      switch ( key ) {
      case "someValue":
        // this.options.someValue = doSomethingWith( value );
        break;
      default:
        // this.options[ key ] = value;
        break;
      }

      // For UI 1.8, _setOption must be manually invoked
      // from the base widget
      $.Widget.prototype._setOption.apply( this, arguments );
      // For UI 1.9 the _super method can be used instead
      // this._super( "_setOption", key, value );
    }
  });

})(jQuery, window, document);
