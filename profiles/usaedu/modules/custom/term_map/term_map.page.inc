<?php

/**
 * Page callback for standards landing page.
 */
function term_map_standards_page_callback($vocabulary) {
  if (!_term_map_is_standards_vocabulary($vocabulary)) {
    return MENU_NOT_FOUND;
  }

  // We shouldn't have to do this here, but for some reason the page title callback doesn't work.
  drupal_set_title($vocabulary->name);

  $elements = _term_map_standards_navbar($vocabulary);

  $tree = _term_map_get_tree_by_parent($vocabulary->vid);
  foreach ($tree[0] as $term) {
    $path = "standards/{$vocabulary->machine_name}/$term->tid";
    $links[] = l(entity_label('taxonomy_term', $term), $path, array('attributes' => array('class' => array('title'))));
  }

  if (!empty($vocabulary->description)) {
    $path = drupal_get_path('module', 'term_map');
    drupal_add_js($path . '/js/term-map-ui.js');
    $elements['standards_description'] = array(
      '#type' => 'container',
      '#children' => '<div class="standards-description-close"></div>' . $vocabulary->description,
      '#attributes' => array('class' => array('standards-description')),
    );
  }

  $elements['standards_list'] = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array('class' => array('terms-wrapper', 'terms-graph', 'standards-curriculum-list')),
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $links,
      '#title' => t('Select a Curriculum from the list below to get started.'),
      '#attributes' => array(
        'id' => 'term-tree', // needed to properly style the graph
        'class' => array('standards-curriculum-selector')
      ),
    ),
  );

  return $elements;
}

/**
 * Page callback for rendering a taxonomy vocabulary.
 */
function term_map_standards_map_page_callback($vocabulary, $term = NULL) {
  if (!_term_map_is_standards_vocabulary($vocabulary) || $term->vid !== $vocabulary->vid) {
    return MENU_NOT_FOUND;
  }

  $elements = _term_map_standards_navbar($vocabulary, $term->tid);

  $id = 'term-tree';

  $root = _term_map_get_tree_by_parent($vocabulary->vid, $term->tid);
  $elements['tree'] = _term_map_vocabulary_tree($root, $term->tid, array('id' => $id));
  $elements['tree']['#theme_wrappers'] = array('container');
  $elements['tree']['#attributes'] = array('class' => array('terms-wrapper'));
  $elements['tree']['children']['#attached'] = array(
    'library' => array(
      array('term_map', 'term-map'),
    ),
    'js' => array(
      array(
        'type' => 'inline',
        'data' => _term_map_init_map_js($id),
        'scope' => 'footer',
      ),
    ),
  );

  return $elements;
}
