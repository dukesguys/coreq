<?php
    /**
     * Step 9 in the multistep form upload process
     */

kpr($page);

?>

<div class="col-sm-6">
    <?php print $page['title']; ?>
</div>

<div class="col-sm-12">

  <?php print $page['field_date_created']; ?>
  <?php print $page['field_copyright_holder']; ?>
  <?php print $page['field_copyright_year']; ?>
  <?php print $page['field_age_range']; ?>
  <?php print $page['field_keywords']; ?>
  <?php print $page['field_revised_blooms']; ?>
  <?php print $page['field_language']; ?>
  <?php print $page['field_interactivity_type']; ?>
  <?php print $page['field_usage_rights_url']; ?>
  <?php print $page['field_publisher_provider']; ?>
  <?php print $page['field_educational_alignment']; ?>
  <?php print $page['field_is_based_on_url']; ?>

</div>
<div class="col-sm-12">

    <?php //print $page['field_standards_ccss']; ?>

</div>
<div class="col-sm-12">

    <?php //print $page['field_standards_state']; ?>

</div>