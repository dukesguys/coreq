<?php
    /**
     * Step 1 in the multistep form upload process
     * Data is set and handled within the $page variable -- obtained from upload-resource-form.tpl.php
     */

$block = usaedu_upload_resource_block_render('block', 3);
?>


<div class="col-sm-6">
    <?php print $page['title']; ?>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <select name="uselect">
            <option>-Resource Type-</option>
            <option>Option-1</option>
            <option>Option-2</option>
            <option>Option-3</option>
            <option>Option-4</option>
        </select>
    </div>
</div>
<div class="col-sm-12">
    <p>Choose a file to upload or add a link</p>
</div>
<div class="col-sm-12">
    <div class="col-sm-6">
        <?php print $page['field_resource_url']; ?>
    </div>
    <div class="col-sm-6">
        <?php print $page['field_attached_files']; ?>
    </div>
</div>
<div class="col-sm-12">

    <?php print $page['body']; ?>

    <?php print $block; ?>
</div>