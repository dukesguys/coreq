<?php

/**
 * Base layout that is loaded on upload resource
 * The step is used to include .tpl file for the specific step
 */


$page = $form['#rendered'];
$step = $form['#current_step'];
$sidebar = $form['#sidebar_data'];
// $messages is turned off in preprocess_page for this module to allow specific placement in the form
$messages = theme('status_messages');

?>

<div class="block-standard">
    <div class="row">
        <div class="col-sm-4">
            <div class="col">
                <!-- steps list -->
                <ol class="step-list">
                    <?php
                        foreach($sidebar['step_names'] as $step_num => $step_name) {
                            $class = $sidebar['active_step'] == $step_num ? 'active' : '';
                            $item = '<li class="'.$class.'">'.$step_name.'</li>';

                            print($item);
                        }
                    ?>
                </ol>
            </div>
        </div>


        <div class="col-sm-8">
            <div class="col content">
                <h2><?php print $page['page_title']; ?></h2>
                <!-- steps form -->

                    <fieldset>
                        <div class="row steps-form">
                            <?php if(!empty($messages)) : ?>
                                <div class="col-sm-12 form-errors">
                                    <?php print $messages; ?>
                                </div>
                            <?php endif; ?>

                            <?php require(drupal_get_path('module', 'usaedu_upload_resource') . '/templates/form_step/step_'.$step.'.tpl.php'); ?>

                            <?php print drupal_render_children($form); // Renders action buttons ?>

                        </div>
                    </fieldset>

            </div>
        </div>


    </div>
</div>

