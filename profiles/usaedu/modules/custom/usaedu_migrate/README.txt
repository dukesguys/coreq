If you use drush for starting the migration, do it as user 1, like so:

drush mi Resource -d -v --user=1

This is necessary so that workflow.module doesn't reject the workflow state we
want these nodes to be in after import.

And here is a more complete list of commands necessary to do a full migration:

drush migrate-stop Resource --user=1
drush migrate-reset-status Resource --user=1

# Forgot in which order they have to be called, so here it is again
drush migrate-stop Resource --user=1
drush migrate-reset-status Resource --user=1

drush migrate-rollback Resource --user=1
drush pm-disable usaedu_migrate -y
drush pm-uninstall usaedu_migrate -y
drush pm-enable usaedu_migrate -y
drush usaedu-resources-scan-files ~/resource_files
drush usaedu-resources-csv-to-db  ~/resource_csv
drush sql-query "TRUNCATE usaedu_migrate_resource_x_file"
drush usaedu-resources-connect-files
drush sql-query "TRUNCATE usaedu_migrate_resource_x_ccss_njcccs_search"
drush sql-query "TRUNCATE usaedu_migrate_ccss_njcccs_keywords"
drush usaedu-resources-match
drush ms
drush eval 'search_api_index_load("resources_search")->update(array("enabled" => 0));'
drush eval 'search_api_index_load("standards")->update(array("enabled" => 0));'
drush migrate-import Resource --user=1 --uri=njcore.org
drush eval 'search_api_index_load("resources_search")->update(array("enabled" => 1));'
drush eval 'search_api_index_load("standards")->update(array("enabled" => 1));'
drush search-api-reindex resources_search
drush search-api-reindex standards
drush search-api-index resources_search --uri=njcore.org
drush search-api-index standards --uri=njcore.org
