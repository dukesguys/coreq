<?php

/**
 * Implements hook_drush_command().
 */
function usaedu_migrate_drush_command() {
  $items = array();
  $items['usaedu-resources-csv-to-db'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Scan a directory for CSV files and import them into the DB.'),
  );
  $items['usaedu-resources-scan-db-values'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Scan which (multi-)values exist in certain columns after importing CSV files into the DB.'),
  );
  $items['usaedu-resources-scan-files'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Scan resource files and create DB table to connect files with resource records.'),
  );
  $items['usaedu-resources-connect-files'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Connect scanned files with resource records.'),
  );
  $items['usaedu-resources-match'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Match several data in preparation of import.'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function usaedu_migrate_drush_help($section) {
  switch ($section) {
    case 'drush:usaedu-resources-csv-to-db':
      return dt('Scan a directory for CSV files and import them into the DB.');
    case 'drush:usaedu-resources-scan-db-values':
      return dt('Scan which (multi-)values exist in certain columns after importing CSV files into the DB.');
    case 'drush:usaedu-resources-scan-files':
      return dt('Scan resource files and create DB table to connect files with resource records.');
    case 'drush:usaedu-resources-connect-files':
      return dt('Connect scanned files with resource records.');
  }
}

function drush_usaedu_migrate_usaedu_resources_csv_to_db_validate($csv_dir) {
  if (! is_dir($csv_dir)) {
    return drush_set_error('RESOURCES_CSV_TO_DB', dt('Please provide the directory path in which to scan for CSV files.'));
  }
  if (! usaedu_migrate_check_csv_headers($csv_dir)) {
    return FALSE;
  }
}

function usaedu_migrate_check_csv_headers($csv_dir) {
  $expected_header = array_keys(UsaeduMigrateConfig::getCsvFields());
  $expected_header_original = array_values(UsaeduMigrateConfig::getCsvFields());
  drush_log(dt('Start checking CSV file headers.'), 'status');
  $files = file_scan_directory($csv_dir, '/\.csv/i');
  foreach ($files as $file) {
    $filename = $file->uri;
    drush_log(dt('Checking headers on file "@file".', array('@file' => $filename)), 'status');
    $csv_data = usaedu_migrate_get_csv($filename);
    $header = $csv_data->header;
    $header_original = $csv_data->header_original;
    if (! usaedu_migrate_are_csv_headers_equal_sets($expected_header, $header)) {
      $diff_expected = array_diff($expected_header_original, $header_original);
      $diff_found = array_diff($header_original, $expected_header_original);
      drush_log(dt('Expected header(s): !expected.', array('!expected' => '"' . implode('", "', $diff_expected) . '"')), 'error');
      drush_log(dt('Found header(s): !found.', array('!found' => '"' . implode('", "', $diff_found) . '"')), 'error');
      return drush_set_error('RESOURCES_CSV_TO_DB', dt('Headers different from previous files in file "@file".', array('@file' => $filename)));
    }
  }
  drush_log(dt('Done checking CSV file headers.'), 'status');
  return TRUE;
}

function drush_usaedu_migrate_usaedu_resources_csv_to_db($csv_dir) {
  drush_log(dt('Start importing resource CSV files into DB.'), 'status');
  $files = file_scan_directory($csv_dir, '/\.csv/i');
  foreach ($files as $file) {
    $filename = $file->uri;
    $short_filename = $file->filename;
    drush_log(dt('Importing resource CSV file "@file" into DB.', array('@file' => $filename)), 'status');
    $csv_data = usaedu_migrate_get_csv($filename);
    $csv = $csv_data->csv;
    $header = $csv_data->header;
    array_push($header, 'migrate_file', 'migrate_table_row');
    foreach ($csv as $csv_index => $line) {
      array_push($line, $short_filename, $csv_index+2);
      try {
        db_insert('usaedu_migrate_raw_resource_data')
          ->fields($header, $line)
          ->execute();
      }
      catch (PDOException $e) {
        return drush_set_error('RESOURCES_CSV_TO_DB', dt('DB error: @msg.', array('@msg' => $e->getMessage())));
      }
    }
  }
  drush_log(dt('Done importing resource CSV files into DB.'), 'status');
}

function usaedu_migrate_get_supported_encodings() {
  $list = `iconv -l`;
  $list = preg_replace('{//(?=\W)}', '', $list); // Linux (or Debian) iconv appends "//" when outputting to a pipe. Reason unknown.
  $encodings = preg_split("/\s/", $list);
  return $encodings;
}

function usaedu_migrate_choose_encoding() {
  static $chosen_encoding = NULL;
  if (is_null($chosen_encoding)) {
    $encs = array_intersect(array('MACROMAN', 'MAC', 'MACINTOSH'), usaedu_migrate_get_supported_encodings());
    $chosen_encoding = reset($encs);
  }
  return $chosen_encoding;
}

function usaedu_migrate_get_csv($filename) {
  $file_content = file_get_contents($filename);
  $chosen_encoding = usaedu_migrate_choose_encoding();
  $file_content = iconv($chosen_encoding, "UTF-8", $file_content);
  if (FALSE === $file_content) {
    drush_log(dt('iconv does not understand the character encoding "@enc"', array('@enc' => $chosen_encoding)), 'error');
    exit;
  }
  $csv = usaedu_csv_parse_string($file_content, ',', '"', "\r");
  $csv = usaedu_csv_trim($csv);
  $header = array_shift($csv);
  $header_original = $header;
  $header = usaedu_migrate_normalize_csv_header($header);
  $return = new stdClass();
  $return->csv = $csv;
  $return->header = $header;
  $return->header_original = $header_original;
  return $return;
}

function usaedu_migrate_normalize_csv_header(array $csv_header) {
  $normalized_header = array();
  foreach ($csv_header as $csv_column_name_index => $csv_column_name) {
    $csv_column_name = usaedu_migrate_fix_typo_in_header($csv_column_name);
    $csv_column_name = usaedu_migrate_make_db_column_name($csv_column_name);
    $normalized_header[$csv_column_name_index] = $csv_column_name;
  }
  return $normalized_header;
}

function usaedu_migrate_make_db_column_name($string) {
  $db_columns = array_flip(UsaeduMigrateConfig::getCsvFields());
  if (! isset($db_columns[$string])) {
    throw new Exception('Unknown CSV column name: ' . $string);
  }
  return $db_columns[$string];
}

// This is only a comparison of sets, element order is irrelevant.
function usaedu_migrate_are_csv_headers_equal_sets(array $csv_header_a, array $csv_header_b) {
  sort($csv_header_a);
  sort($csv_header_b);
  return $csv_header_a == $csv_header_b;
}

function usaedu_migrate_fix_typo_in_header($csv_column_name) {
  $typos = UsaeduMigrateConfig::getTyposInHeader();
  return isset($typos[$csv_column_name]) ? $typos[$csv_column_name] : $csv_column_name;
}

function drush_usaedu_migrate_usaedu_resources_scan_db_values() {
  $subjects = array();
  $grades = array();
  $units = array();
  $slos = array();
  $raw_data = db_select('usaedu_migrate_raw_resource_data', 'raw')
    ->fields('raw', array('subject', 'grade', 'unit', 'slo'))
    ->execute()->fetchAll();
  foreach ($raw_data as $row) {
    $row->subject = UsaeduMigrateToolbox::fixTyposInSubject($row->subject);
    $row->grade = UsaeduMigrateToolbox::fixTyposInGrade($row->grade);
    $row->unit = UsaeduMigrateToolbox::fixTyposInUnit($row->unit);
    $row->slo = UsaeduMigrateToolbox::fixTyposInSlo($row->slo);

    $subjects = array_unique(array_merge($subjects, array($row->subject)));
    $grades = array_unique(array_merge($grades, UsaeduMigrateToolbox::splitShortcodes($row->grade)));
    $units = array_unique(array_merge($units, UsaeduMigrateToolbox::splitShortcodes($row->unit)));
    $slos = array_unique(array_merge($slos, UsaeduMigrateToolbox::splitShortcodes($row->slo)));
  }
  sort($subjects);
  sort($grades);
  sort($units);
  sort($slos);

  drush_log(dt('Subjects:'), 'status');
  var_dump($subjects);

  drush_log(dt('Grades:'), 'status');
  var_dump($grades);

  drush_log(dt('Units:'), 'status');
  var_dump($units);

  drush_log(dt('SLOs:'), 'status');
  var_dump($slos);
}

function drush_usaedu_migrate_usaedu_resources_scan_files_validate($files_dir) {
  if (! is_dir($files_dir)) {
    return drush_set_error('RESOURCE_FILES', dt('Please provide the directory path in which to scan for resource files.'));
  }
}

function drush_usaedu_migrate_usaedu_resources_scan_files($files_dir) {
  drush_log(dt('Start scanning files.'), 'status');
  $files = file_scan_directory(rtrim($files_dir, '/'), '/.*/');
  foreach ($files as $file) {
    $filename = $file->uri;
    $short_filename = $file->filename;
    drush_log(dt('Entering file "@file" into DB.', array('@file' => $filename)), 'status');
    try {
      db_insert('usaedu_migrate_files')
      ->fields(array('absolute_file_name', 'base_name'), array($file->uri, $file->filename))
      ->execute();
    }
    catch (PDOException $e) {
      return drush_set_error('RESOURCES_FILES', dt('DB error: @msg.', array('@msg' => $e->getMessage())));
    }
  }
  drush_log(dt('Done scanning files.'), 'status');
}

function drush_usaedu_migrate_usaedu_resources_connect_files() {
  $stats = array(
    'total' => 0,
    'both_empty' => 0,
    'valid_url' => 0,
    'found_filename_in_filename_field' => 0,
    'found_filename_in_url_field' => 0,
    'found_file_and_url' => 0,
    'found_file_only' => 0,
    'found_url_only' => 0,
    'not_found_neither_file_nor_url' => 0,
  );
  drush_log(dt('Start connecting files.'), 'status');
  $raw_data = db_select('usaedu_migrate_raw_resource_data', 'raw')
    ->fields('raw', array('migrate_id', 'file_name_location', 'resource_url'))
    ->execute()->fetchAll();
  foreach ($raw_data as $row) {
    $stats['total']++;

    if ('' == $row->file_name_location && '' == $row->resource_url) {
      $stats['both_empty']++;
      continue;
    }

    $valid_url = valid_url($row->resource_url, TRUE);
    if ($valid_url) {
      $stats['valid_url']++;
    }

    $file_id = usaedu_migrate_find_file($row->file_name_location);
    if (is_numeric($file_id)) {
      $stats['found_filename_in_filename_field']++;
    }
    else if (! $valid_url) {
      // try again in the resource_url field, some CSVs have a file name there.
      $file_id = usaedu_migrate_find_file($row->resource_url);
      if (is_numeric($file_id)) {
        $stats['found_filename_in_url_field']++;
      }
    }

    if (is_numeric($file_id)) {
      if ($valid_url) {
        $stats['found_file_and_url']++;
      }
      else {
        $stats['found_file_only']++;
      }
      drush_log(dt('Did find file_id "!file_id".', array('!file_id' => $file_id)), 'status');
      db_insert('usaedu_migrate_resource_x_file')
        ->fields(array('migrate_id1' => $row->migrate_id, 'file_id' => $file_id))
        ->execute();
    }
    else {
      if ($valid_url) {
        $stats['found_url_only']++;
      }
      else {
        $stats['not_found_neither_file_nor_url']++;
      }
      drush_log(dt('Did not find "!file" or "!url".', array('!file' => $row->file_name_location, '!url' => $row->resource_url)), 'warning');
    }
  }
  drush_print_table(UsaeduMigrateToolbox::arrayTranspose2d(array(array_keys($stats), array_values($stats))));
  drush_log(dt('Done connecting files.'), 'status');
}

function usaedu_migrate_find_file($search) {
  $delimiter = (FALSE !== strpos($search, '\\')) ? '\\' : '/';
  $search_parts = explode($delimiter, $search);
  for ($i = -1; $i >= - count($search_parts); $i--) {
    $current_search_parts = array_slice($search_parts, $i);
    $current_search = implode('/', $current_search_parts);
    $query = db_select('usaedu_migrate_files', 'files');
    $query->fields('files', array('file_id'));
    if (1 == count($current_search_parts)) {
      $query->condition('base_name', $current_search);
    }
    else {
      $query->condition('absolute_file_name', '%' . $current_search, 'LIKE');
    }
    $file_ids = $query->execute()->fetchCol();
    if (1 == count($file_ids)) {
      return $file_ids[0];
    }
  }
  return NULL;
}

function drush_usaedu_migrate_usaedu_resources_match($what = 'all') {
  if ('all' == $what || 'ccss_njcccs' == $what) {
    $matcher = new UsaeduMigrateMatcherCcssNjcccs('drush_log');
    $matcher->run();
  }
}
