<?php

class UsaeduMigrateToolbox {
  const LOG_TABLE = 'usaedu_migrate_log';

  public static function getWorkflow() {
    static $workflow_states = NULL;
    if (! isset($workflow_states->draft, $workflow_states->pending, $workflow_states->returned)) {
      $workflow_states = new stdClass();
      $workflow_state_data = workflow_get_workflow_states_by_type('resource');
      $of_interest = array_flip(UsaeduMigrateConfig::getWorkflowStates());
      foreach ($workflow_state_data as $workflow_state) {
        if (isset($of_interest[$workflow_state->state])) {
          $propertyname = $of_interest[$workflow_state->state];
          $workflow_states->$propertyname = $workflow_state->sid;
        }
      }
    }
    return $workflow_states;
  }

  public static function meansNotAvailable($string) {
    $boiled_down_to_letters = strtoupper(preg_replace('/[^a-zA-Z0-9]/', '', $string));
    return in_array($boiled_down_to_letters, array('NA', ''));
  }

  public static function splitShortcodes($string) {
    $shortcodes = array();
    $values = preg_split('/[,;&\s]|and/i', $string, NULL, PREG_SPLIT_NO_EMPTY);
    foreach ($values as $value) {
      $value = trim($value);

      // Remove non-alphanumeric chars from front and end.
      $value = preg_replace('/(^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$)/', '', $value);
      $shortcodes[] = $value;
    }
    return $shortcodes;
  }

  public static function findTidsByVocabsParentsAndNames(array $vocabNames, array $parentTids, array $termNames) {
    $query = db_select('taxonomy_term_data', 'term');
    $query->join('taxonomy_term_hierarchy', 'hierarchy', 'term.tid = hierarchy.tid');
    $query->join('taxonomy_vocabulary', 'vocabulary', 'term.vid = vocabulary.vid');
    $query->condition('vocabulary.machine_name', $vocabNames, 'IN');
    $query->condition('hierarchy.parent', $parentTids, 'IN');
    $query->condition('term.name', $termNames, 'IN');
    $query->fields('term', array('tid'));
    return $query->execute()->fetchCol();
  }

  public static function findCcssTidsByShortcode($shortcode) {
    return db_select('field_data_field_standard_short_code', 'short_code')
      ->fields('short_code', array('entity_id'))
      ->condition('entity_type', 'taxonomy_term')
      ->condition('bundle', 'ccss')
      ->condition('field_standard_short_code_value', $shortcode)
      ->execute()
      ->fetchCol();
  }

  public static function findCcssTidsWithTermNameLike($termName) {
    $likeTermName = '%' . $termName . '%';
    $query = db_select('taxonomy_term_data', 'term');
    $query->join('taxonomy_vocabulary', 'vocabulary', 'term.vid = vocabulary.vid');
    $query->condition('vocabulary.machine_name', 'ccss');
    $query->condition('term.name', $likeTermName, 'LIKE');
    $query->fields('term', array('tid'));
    return $query->execute()->fetchCol();
  }

  public static function findCcssTermsByName(array $parentTids, array $termNames) {
    return self::findTidsByVocabsParentsAndNames(array('ccss'), $parentTids, $termNames);
  }

  public static function findCcssSubjectTids(array $subjectNames) {
    return self::findCcssTermsByName(array('0'), $subjectNames);
  }

  public static function findCcssOverviewTids(array $subjectNames) {
    $subjectTids = self::findCcssSubjectTids($subjectNames);
    $intermediateTids = self::findCcssTermsByName($subjectTids, array('Math Overview', 'ELA Overview'));
    return self::findCcssTermsByName($intermediateTids, array('Mathematics Overview', 'English Language Arts Overview'));
  }

  public static function breakIntoMonotonousSequences(array $input) {
    $sequences = array();
    $current_sequence_index = 0;
    $maximum_so_far = - PHP_INT_MAX;
    foreach ($input as $input_value) {
      $input_number = (int) preg_replace('/^SLO\s+/i', '', $input_value);
      if ($input_number > $maximum_so_far) {
        $maximum_so_far = $input_number;
      }
      else {
        $current_sequence_index++;
        $maximum_so_far = - PHP_INT_MAX;
      }
      $sequences[$current_sequence_index][] = $input_value;
    }
    return $sequences;
  }

  public static function findMcTermsByMapLabel(array $parentTids, array $mapLabels) {
    $query = db_select('taxonomy_term_data', 'term');
    $query->join('taxonomy_term_hierarchy', 'hierarchy', 'term.tid = hierarchy.tid');
    $query->join('taxonomy_vocabulary', 'vocabulary', 'term.vid = vocabulary.vid');
    $query->join('field_data_field_map_label', 'label', 'term.tid = label.entity_id');
    $query->condition('vocabulary.machine_name', 'curriculum');
    $query->condition('hierarchy.parent', $parentTids, 'IN');
    $query->condition('label.field_map_label_value', $mapLabels, 'IN');
    $query->fields('term', array('tid'));
    return $query->execute()->fetchCol();
  }
  
  public static function findMcTermsByName(array $parentTids, array $termNames) {
    return self::findTidsByVocabsParentsAndNames(array('curriculum'), $parentTids, $termNames);
  }

  public static function findMcSubjectTids(array $subjectNames) {
    return self::findMcTermsByName(array('0'), $subjectNames);
  }
  
  public static function findMcGradeTids(array $subjectNames, array $gradeMapLabels) {
    $subjectTids = self::findMcSubjectTids($subjectNames);
    return self::findMcTermsByMapLabel($subjectTids, $gradeMapLabels);
  }

  public static function findMcUnitTids(array $subjectNames, array $gradeMapLabels, array $unitMapLabels) {
    $gradeTids = self::findMcGradeTids($subjectNames, $gradeMapLabels);
    return self::findMcTermsByMapLabel($gradeTids, $unitMapLabels);
  }

  public static function findMcSloTids(array $subjectNames, array $gradeMapLabels, array $unitMapLabels, array $sloSequences) {
    $sloTids = array();
    foreach ($unitMapLabels as $unitIndex => $unitMapLabel) {
      $unitTids = self::findMcUnitTids($subjectNames, $gradeMapLabels, array($unitMapLabel));
      foreach ($unitTids as $unitTid) {
        $sloTids = array_merge($sloTids, self::findMcTermsByMapLabel(array($unitTid), $sloSequences[$unitIndex]));
      } 
    }
    return $sloTids;
  }

  public static function findMcOverviewTids(array $subjectNames) {
    $subjectTids = self::findMcSubjectTids($subjectNames);
    $intermediateTids = self::findMcTermsByName($subjectTids, array('Math Overview', 'ELA Overview'));
    return self::findMcTermsByName($intermediateTids, array('Mathematics Overview', 'English Language Arts Overview'));
  }

  public static function fixTyposInSubject($subjectValue) {
    $typos = UsaeduMigrateConfig::getTyposInSubject();
    return isset($typos[$subjectValue]) ? $typos[$subjectValue] : $subjectValue;
  }

  public static function fixTyposInGrade($gradeValue) {
    $gradeValue = preg_replace('/(^|\W)k(\W|$)/i', 'K', $gradeValue); // uppercase Kindergarten
    $gradeValue = preg_replace('/Algebra\s*/i', 'Algebra_', $gradeValue); // avoid splitting at space between Algebra and number
    $typos = UsaeduMigrateConfig::getTyposInGrade();
    return isset($typos[$gradeValue]) ? $typos[$gradeValue] : $gradeValue;
  }

  public static function fixTyposInUnit($unitValue) {
    $typos = UsaeduMigrateConfig::getTyposInUnit();
    return isset($typos[$unitValue]) ? $typos[$unitValue] : $unitValue;
  }

  public static function fixTyposInSlo($sloValue) {
    $typos = UsaeduMigrateConfig::getTyposInSlo();
    return isset($typos[$sloValue]) ? $typos[$sloValue] : $sloValue;
  }

  public static function fixTyposInCcssNjcccs($ccssNjcccsValue) {
    $typos = UsaeduMigrateConfig::getTyposInCcssNjcccs();
    return isset($typos[$ccssNjcccsValue]) ? $typos[$ccssNjcccsValue] : $ccssNjcccsValue;
  }

  public static function fixTyposInDigitalMediaType($digitalMediaTypeValue) {
    $typos = UsaeduMigrateConfig::getTyposInDigitalMediaType();
    return isset($typos[$digitalMediaTypeValue]) ? $typos[$digitalMediaTypeValue] : $digitalMediaTypeValue;
  }

  public static function fixTyposInLearningResourceType($learningResourceTypeValue) {
    $typos = UsaeduMigrateConfig::getTyposInLearningResourceType();
    return isset($typos[$learningResourceTypeValue]) ? $typos[$learningResourceTypeValue] : $learningResourceTypeValue;
  }

  public static function fixTyposInEducationalUseValue($educationalUseValue) {
    $typos = UsaeduMigrateConfig::getTyposInEducationalUseValue();
    return isset($typos[$educationalUseValue]) ? $typos[$educationalUseValue] : $educationalUseValue;
  }

  public static function singleMapLabelGrade($gradeValue) {
    // @see fixTyposInGrade() why the mapping back is necessary
    // Also, the import data has roman numerals, but our taxonomy has arabic ones (in it's map labels).
    $back_map = array(
      'Algebra_I' => 'Algebra 1',
      'Algebra_II' => 'Algebra 2',
    );
    return isset($back_map[$gradeValue]) ? $back_map[$gradeValue] : $gradeValue;
  }
  
  public static function singleMapLabelUnit($unitValue) {
    return 'Unit ' . $unitValue;
  }
  
  public static function singleMapLabelSlo($sloValue) {
    return 'SLO ' . $sloValue;
  }

  public static function mapLabelGrade(array $grades) {
    $mapLabels = array();
    foreach ($grades as $grade) {
      $mapLabels[] = self::singleMapLabelGrade($grade);
    }
    return $mapLabels;
  }

  public static function mapLabelUnit(array $units) {
    $mapLabels = array();
    foreach ($units as $unit) {
      $mapLabels[] = self::singleMapLabelUnit($unit);
    }
    return $mapLabels;
  }

  public static function mapLabelSlo(array $slos) {
    $mapLabels = array();
    foreach ($slos as $slo) {
      $mapLabels[] = self::singleMapLabelSlo($slo);
    }
    return $mapLabels;
  }

  public static function areCsvFieldsInRowEmpty(stdClass $row, array $exclude_fields = array()) {
    $test_fields = array_diff(array_keys(UsaeduMigrateConfig::getCsvFields()), $exclude_fields);
    foreach ($test_fields as $fieldname) {
      if (! empty($row->$fieldname)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  public static function writeLog(stdClass $row, $message = '', $link = '') {
    if (('' == $message) && isset($row->revision_log_entry)) {
      $message = $row->revision_log_entry;
    }
    db_insert(self::LOG_TABLE)
      ->fields(array(
        'migrate_id' => isset($row->migrate_id) ? (int) $row->migrate_id : 0,
        'migrate_file' => isset($row->migrate_file) ? $row->migrate_file : '[unknown]',
        'migrate_table_row' => isset($row->migrate_table_row) ? $row->migrate_table_row : '[unknown]',
        'data' => serialize($row),
        'message' => $message,
        'link' => $link,
      ))
      ->execute();
  }

  public static function arrayTranspose2d($arr) {
    $out = array();
    foreach ($arr as $key => $subarr) {
      foreach ($subarr as $subkey => $subvalue) {
        $out[$subkey][$key] = $subvalue;
      }
    }
    return $out;
  }
}
