<?php

class ResourceMigration extends Migration {

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('usaedu_resources'));
    $this->setupSource();
    $this->setupDestination();
    $this->setupMapping();
    $this->setupFieldMappings();
    $this->setupUnmigratedSources();
    $this->setupUnmigratedDestinations();
  }

  protected function setupSource() {
    // Define our source data.
    $source_fields = array_merge(array(
      'migrate_id',
      'migrate_file',
      'migrate_table_row',
    ), array_keys(UsaeduMigrateConfig::getCsvFields()));

    // These fields get generated in prepareRow() et al.
    $known_virtual_source_fields = array(
      'revision_log_entry', // goes into $node->log
      'alignment_type_other',
      'digital_media_type_other',
      'educational_use_other',
      'enduser_other',
      'grade_grade_level_other',
      'item_type_other',
      'learning_resource_type_other',
      'subject_other',
      'usage_rights_url_na',

      'ccss_tids',
      'njcccs_tids',
      'njmc_tids',
    );
    
    $query = db_select('usaedu_migrate_raw_resource_data', 'res');
    $query->fields('res', $source_fields);
    foreach ($known_virtual_source_fields as $virtual_field) {
      $query->addExpression("''", $virtual_field);
    }

    $query->leftJoin('usaedu_migrate_resource_x_file', 'x', 'x.migrate_id1 = res.migrate_id');
    $query->leftJoin('usaedu_migrate_files', 'files', 'files.file_id = x.file_id');
    $query->fields('files', array('absolute_file_name'));

    $this->source = new MigrateSourceSQL($query);
  }

  protected function setupDestination() {
    // Define what type of data that migration will create
    $this->destination = new MigrateDestinationNode('resource');
  }

  protected function setupMapping() {
    // Map the keys for the source and the destination to allow rollbacks.
    $source_key = array(
      'migrate_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    );
    $this->map = new MigrateSQLMap(
      $this->machineName,
      $source_key,
      MigrateDestinationNode::getKeySchema()
    );
  }

  protected function setupFieldMappings() {
    $this->setupNodeSpecificFieldMappings();
    $this->setupTextFieldMappings();
    $this->setupDecimalFieldMappings();
    $this->setupTermRefWithOtherFieldMappings();
    $this->setupSimpleTermRefFieldMappings();
    $this->setupComplexTermRefFieldMappings();
    $this->setupDateFieldMappings();
    $this->setupFileFieldMappings();
    $this->setupLinkFieldMappings();
  }

  protected function setupNodeSpecificFieldMappings() {
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('uid')->defaultValue(1);
    $this->addFieldMapping('status')->defaultValue(1);
    $this->addFieldMapping('revision')->defaultValue(1);
    $this->addFieldMapping('log', 'revision_log_entry')->defaultValue('Imported with usaedu_migrate.module');
  }

  protected function setupTextFieldMappings() {
    $this->addFieldMapping('body', 'description');
    $this->addFieldMapping('field_age_range', 'age_range');
    $this->addFieldMapping('field_author_creator', 'author_creator');
    $this->addFieldMapping('field_copyright_holder', 'copyrightholder');
    $this->addFieldMapping('field_educational_alignment', 'educational_alignment');
    $this->addFieldMapping('field_publisher_provider', 'publisher_provider');
    $this->addFieldMapping('field_skill_micro_standard_perfo', 'skill_micro_standard_performances');
  }

  protected function setupDecimalFieldMappings() {
    $this->addFieldMapping('field_peer_rating', 'peer_rating');
  }

  protected function setupTermRefWithOtherFieldMappings() {
    $this->addFieldMapping('field_alignment_type', 'alignment_type');
    $this->addFieldMapping('field_alignment_type_other', 'alignment_type_other'); // @see prepareAllMultiTermRefFields()

    $this->addFieldMapping('field_digital_media_type', 'digital_media_type');
    $this->addFieldMapping('field_digital_media_type_other', 'digital_media_type_other'); // @see prepareAllMultiTermRefFields()

    $this->addFieldMapping('field_educational_use', 'educational_use');
    $this->addFieldMapping('field_educational_use_other', 'educational_use_other'); // @see prepareAllMultiTermRefFields()

    $this->addFieldMapping('field_end_user', 'enduser');
    $this->addFieldMapping('field_end_user_other', 'enduser_other'); // @see prepareAllMultiTermRefFields()

    $this->addFieldMapping('field_grade_level_term', 'grade_grade_level');
    $this->addFieldMapping('field_grade_level_other', 'grade_grade_level_other'); // @see prepareField_grade_grade_level()

    $this->addFieldMapping('field_item_type', 'item_type');
    $this->addFieldMapping('field_item_type_other', 'item_type_other'); // @see prepareAllMultiTermRefFields()

    $this->addFieldMapping('field_learning_resource_type', 'learning_resource_type');
    $this->addFieldMapping('field_resource_type_other', 'learning_resource_type_other'); // @see prepareAllMultiTermRefFields()

    $this->addFieldMapping('field_subject_areas', 'subject');
    $this->addFieldMapping('field_subject_area_other', 'subject_other'); // @see prepareAllMultiTermRefFields()
  }

  protected function setupSimpleTermRefFieldMappings() {
    $this->addFieldMapping('field_interactivity_type', 'interactivity_type');

    $this->addFieldMapping('field_revised_blooms', 'taxonomy_depth_of_knowledge');

    $this->addFieldMapping('field_keywords', 'keywords');
    $this->addFieldMapping('field_keywords:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_keywords:ignore_case')->defaultValue(TRUE);

    $this->addFieldMapping('field_language', 'language');
    $this->addFieldMapping('field_language:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_language:ignore_case')->defaultValue(TRUE);
  }

  protected function setupComplexTermRefFieldMappings() {
    $this->addFieldMapping('field_standards_ccss', 'ccss_tids'); // @see prepareField_ccss_njcccs()
    $this->addFieldMapping('field_standards_ccss:source_type')->defaultValue('tid'); // @see prepareField_ccss_njcccs()

    $this->addFieldMapping('field_standards_state', 'njcccs_tids'); // @see prepareField_ccss_njcccs()
    $this->addFieldMapping('field_standards_state:source_type')->defaultValue('tid'); // @see prepareField_ccss_njcccs()

    $this->addFieldMapping('field_standard_curriculum', 'njmc_tids'); // @see prepareField_njmc()
    $this->addFieldMapping('field_standard_curriculum:source_type')->defaultValue('tid'); // @see prepareField_njmc()
  }

  protected function setupDateFieldMappings() {
    $date_arguments = DateMigrateFieldHandler::arguments('UTC', 'UTC', NULL, 'en');
    $this->addFieldMapping('field_copyright_year', 'copyrightyear')->arguments($date_arguments);
    $this->addFieldMapping('field_date_created', 'date_created')->arguments($date_arguments);
  }

  protected function setupFileFieldMappings() {
    $this->addFieldMapping('field_attached_files', 'absolute_file_name');
  }

  protected function setupLinkFieldMappings() {
    $link_arguments = MigrateLinkFieldHandler::arguments(NULL, NULL, 'en');
    $this->addFieldMapping('field_is_based_on_url', 'is_based_on_url')->arguments($link_arguments);
    $this->addFieldMapping('field_resource_url', 'resource_url')->arguments($link_arguments);
    $this->addFieldMapping('field_usage_rights_url', 'usage_rights_url')->arguments($link_arguments);
    $this->addFieldMapping('field_usage_rights_url_na', 'usage_rights_url_na'); // @see prepareField_usage_rights_url()
  }

  protected function setupUnmigratedSources() {
    $this->addUnmigratedSources(array(
      // Administrative data about import source
      'migrate_id',
      'migrate_file',
      'migrate_table_row',

      // Model Curriculum stuff
      'grade',
      'unit',
      'slo',

      // This get's sorted into the 2 virtual fields 'ccss_tids' and 'njcccs_tids'
      'ccss_njcccs', // @see prepareField_ccss_njcccs()
    ));
  }

  protected function setupUnmigratedDestinations() {
    $this->addUnmigratedDestinations(array(
      // Stuff from node.module
      'is_new',
      'promote',
      'language',
      'sticky',
      'created',
      'changed',
      'revision_uid',
      'path',
      'comment',

      // Evaluations are only possible with new code base, there is no old data.
      'field_aggregate_alignment',
      'field_aggregate_assessment',
      'field_aggregate_key_shifts',
      'field_aggregate_supports',

      // Endorsements are only possible with new code base, there is no old data.
      'field_endorsed_resource',
      'field_endorsement_comments',
      'field_endorsement_date',
      'field_endorsement_file',

      // These features are only possible with new code base, there is no old data for them.
      'field_thumbnail',
      'field_restricted_to_educators',
    ));
  }

  public function prepare($node, stdClass $row) {
    // workflow_node_insert() complains that it can't find a valid state, so we have to force the node to have one it here.
    // Also, if you use drush to run the migration, use --user=1 for this to work.

    // As per emails by Evan Linhardt and Allen Continanza on Thu, Aug 29, 2013 at 9:07 PM, import into "public access" state.
    $node->workflow = UsaeduMigrateToolbox::getWorkflow()->approved_public;
    // $node->workflow = $row->reject ? UsaeduMigrateToolbox::getWorkflow()->returned : UsaeduMigrateToolbox::getWorkflow()->pending;

    // this doesn't really work. @see complete()
    $node->workflow_comment = $row->revision_log_entry;
  }

  public function complete($node, stdClass $row) {
    // Since writing the revision_log_entry into the workflow_comment in
    // prepare() seems to happen too early, try updating it here again.
    $history_id = workflow_get_recent_node_history($node->nid)->hid;
    db_update('workflow_node_history')
      ->fields(array('comment' => $row->revision_log_entry))
      ->condition('hid', $history_id)
      ->execute();

    $uri = entity_uri('node', $node);
    $url = url($uri['path'], array('absolute' => TRUE));
    $message = $row->reject ?
      ('Node has been created, but with problems:' . PHP_EOL . $row->revision_log_entry)
      :
      'Node has been created.'
    ;
    UsaeduMigrateToolbox::writeLog($row, $message, $url);
  }

  protected function log(&$row, $message, $alsoDrupalMessage = TRUE) {
    $row->revision_log_entry .= $message . PHP_EOL;
    if ($alsoDrupalMessage) {
      drupal_set_message($message);
    }
  }

  protected function reject(&$row, $message, $alsoDrupalMessage = TRUE) {
    $this->log($row, $message, $alsoDrupalMessage);
    $row->reject = TRUE;
    return FALSE;
  }

  /**
   * Iterate over rows and clean them up before mapping values
   */
  public function prepareRow($row) {
    // Emptiness check has to come first, before populating all the extra fields.
    if (UsaeduMigrateToolbox::areCsvFieldsInRowEmpty($row, array('language'))) {
      UsaeduMigrateToolbox::writeLog($row, 'This row is empty.');
      return FALSE;
    }

    $row->reject = FALSE;
    $this->log($row, t('Imported from row @row in file "@file".', array('@row' => $row->migrate_table_row, '@file' => $row->migrate_file)), FALSE);
    $row->subject = UsaeduMigrateToolbox::fixTyposInSubject($row->subject);
    $row->grade = UsaeduMigrateToolbox::fixTyposInGrade($row->grade);
    $row->grade_grade_level = UsaeduMigrateToolbox::fixTyposInGrade($row->grade_grade_level);
    $row->unit = UsaeduMigrateToolbox::fixTyposInUnit($row->unit);
    $row->slo = UsaeduMigrateToolbox::fixTyposInSlo($row->slo);

    $row->ccss_njcccs = UsaeduMigrateToolbox::fixTyposInCcssNjcccs($row->ccss_njcccs);
    $row->digital_media_type = UsaeduMigrateToolbox::fixTyposInDigitalMediaType($row->digital_media_type);
    $row->learning_resource_type = UsaeduMigrateToolbox::fixTyposInLearningResourceType($row->learning_resource_type);
    $row->educational_use = UsaeduMigrateToolbox::fixTyposInEducationalUseValue($row->educational_use);

    $this->prepareAllMultiTermRefFields($row);
    $this->prepareField_grade_grade_level($row);
    $this->prepareField_usage_rights_url($row);
    $this->prepareField_ccss_njcccs($row);
    $this->prepareField_njmc_tids($row);
    $this->prepareField_title($row);
    
    // Skip row if any preprocess deems it necessary.
    if ($row->reject == TRUE) {
      // As per emails by Evan Linhardt and Allen Continanza on Thu, Aug 29, 2013 at 9:07 PM, import ALL nodes, even with faulty data.
      //UsaeduMigrateToolbox::writeLog($row, 'This row has been rejected.');
      //return FALSE;
    }
    return TRUE;
  }

  protected function prepareField_grade_grade_level(&$row) {
    $found_values = array();
    $not_found_values = array();
    $grades = array_unique(array_merge(
      UsaeduMigrateToolbox::splitShortcodes($row->grade),
      UsaeduMigrateToolbox::splitShortcodes($row->grade_grade_level)
    ));
    foreach ($grades as $grade) {
      $grade = str_replace('Algebra_', 'Algebra ', $grade); // "Un-hack" UsaeduMigrateToolbox::fixTyposInGrade(), but here, keep roman numerals, b/c 'grade_level' vocab has them.
      $found_terms = taxonomy_get_term_by_name($grade, 'grade_level');
      if (0 == count($found_terms)) {
        $this->reject($row, t('Did not find "@not_found" in "@vocab".', array('@vocab' => 'grade_level', '@not_found' => $grade)));
        $not_found_values[] = $grade;
      }
      else {
        $found_values[] = $grade;
      }
    }
    $row->grade_grade_level = $found_values;
    $row->grade_grade_level_other = implode(', ', $not_found_values);
  }

  protected function prepareField_title(&$row) {
    if ('' == $row->title) {
      $row->title = truncate_utf8($row->description, 72, TRUE, TRUE, 50);
    }
    if ('' == $row->title) {
      // still empty ---> flag that in log.
      $this->reject($row, t('Empty title.'));
    }
  }

  protected function prepareField_usage_rights_url(&$row) {
    $row->usage_rights_url_na = 0;
    if (UsaeduMigrateToolbox::meansNotAvailable($row->usage_rights_url)) {
      $row->usage_rights_url_na = 1;
      $row->usage_rights_url = '';
    }
  }

  protected function prepareField_ccss_njcccs(&$row) {
    $found_njcccs_tids = array();
    $found_ccss_tids = array();
    $not_found = array();

    $matches = UsaeduMigrateMatcherCcssNjcccs::getMatches($row->migrate_id);
    foreach ($matches as $match) {
      if (is_null($match->tid)) {
        $not_found[] = $match->search_orig;
      }
      else {
        if ('ccss' == $match->vocab_name) {
          $found_ccss_tids[] = $match->tid;
        }
        else if ('statestand' == $match->vocab_name) {
          $found_njcccs_tids[] = $match->tid;
        }
        else {
          $this->reject($row, t('This error should NOT happen: Matched term @tid, supposedly from vocabulary "@vocab_name".', array('@tid' => $match->tid, '@vocab_name' => $match->vocab_name)));
          $not_found[] = $match->search_orig;
        }
      }
    }

    if (0 < count($not_found)) {
      $this->reject($row, t('Could not find "!not_found", not in CCSS nor in NJCCCS.', array('!not_found' => implode('", "', $not_found))));
    }

    $row->ccss_tids = (0 < count($matches)) ? array_unique($found_ccss_tids) : UsaeduMigrateToolbox::findCcssOverviewTids($row->subject) ;
    $row->njcccs_tids = array_unique($found_njcccs_tids);
  }

  protected function prepareField_njmc_tids(&$row) {
    $row->njmc_tids = array();

    $subjects = $row->subject; // subjects already got split in prepareAllMultiTermRefFields()
    $grades = UsaeduMigrateToolbox::splitShortcodes($row->grade);
    $units = UsaeduMigrateToolbox::splitShortcodes($row->unit);
    $slos = UsaeduMigrateToolbox::splitShortcodes($row->slo);

    $grades = UsaeduMigrateToolbox::mapLabelGrade($grades);
    $units = UsaeduMigrateToolbox::mapLabelUnit($units);
    $slos = UsaeduMigrateToolbox::mapLabelSlo($slos);

    $subject_count = count($subjects);
    $grade_count = count($grades);
    $unit_count = count($units);
    $slo_count = count($slos);

    $has_subjects = (0 < $subject_count);
    $has_grades = (0 < $grade_count);
    $has_units = (0 < $unit_count);
    $has_slos = (0 < $slo_count);

    if ($has_subjects && $has_grades && $has_units && $has_slos) {
      if (1 < $subject_count) {
        return $this->reject($row, t('More than 1 subject together with grades, units and SLOs.'));
      }
      if (1 < $grade_count) {
        return $this->reject($row, t('More than 1 grade together with units and SLOs.'));
      }

      $slo_sequences = UsaeduMigrateToolbox::breakIntoMonotonousSequences($slos);
      if ($unit_count != count($slo_sequences)) {
        return $this->reject($row, t('The number of monotonous SLO sequences does not match the number of units.'));
      }

      $row->njmc_tids = UsaeduMigrateToolbox::findMcSloTids($subjects, $grades, $units, $slo_sequences);
    }
    else if ($has_subjects && $has_grades && $has_units && ! $has_slos) {
      if (1 < $subject_count) {
        return $this->reject($row, t('More than 1 subject together with grades and units.'));
      }
      if (1 < $grade_count) {
        return $this->reject($row, t('More than 1 grade together with units.'));
      }
      $row->njmc_tids = UsaeduMigrateToolbox::findMcUnitTids($subjects, $grades, $units);
    }
    else if ($has_subjects && $has_grades && ! $has_units && ! $has_slos) {
      //$row->njmc_tids = UsaeduMigrateToolbox::findMcGradeTids($subjects, $grades);
      $row->njmc_tids = UsaeduMigrateToolbox::findMcOverviewTids($subjects);
    }
    else if ($has_subjects && ! $has_grades && ! $has_units && ! $has_slos) {
      //$row->njmc_tids = UsaeduMigrateToolbox::findMcSubjectTids($subjects);
      $row->njmc_tids = UsaeduMigrateToolbox::findMcOverviewTids($subjects);
    }
    else {
      return $this->reject($row, t('Unable to understand the combination (or lack thereof) of SLO, Unit, Grade and Subject.'));
    }
  }

  protected function prepareAllMultiTermRefFields(&$row) {
    $fields = array(
      'alignment_type'         => array('alignment_type_other',         'alignment_type'),
      'digital_media_type'     => array('digital_media_type_other',     'digital_media_type'),
      'educational_use'        => array('educational_use_other',        'educational_use'),
      'enduser'                => array('enduser_other',                'end_user'),
      'item_type'              => array('item_type_other',              'item_type'),
      'learning_resource_type' => array('learning_resource_type_other', 'learning_resource_type'),
      'subject'                => array('subject_other',                'subject'),
    );
    foreach ($fields as $name => $info) {
      $this->prepareMultiTermRefFieldAndOtherField($row, $name, $info[0], $info[1]);
    }
  }

  protected function prepareMultiTermRefFieldAndOtherField(&$row, $csv_field_name, $csv_field_name_other, $vocab_name, $delimiter = ',') {
    $found_values = array();
    $not_found_values = array();
    if ('' != $row->$csv_field_name) {
      $values = explode($delimiter, $row->$csv_field_name);
      foreach ($values as $value) {
        $value = trim($value);
        $found_terms = taxonomy_get_term_by_name($value, $vocab_name);
        if (0 == count($found_terms)) {
          $this->reject($row, t('Did not find "@not_found" in "@vocab".', array('@vocab' => $vocab_name, '@not_found' => $value)));
          $not_found_values[] = $value;
        }
        else {
          $found_values[] = $value;
        }
      }
    }

    $row->$csv_field_name = $found_values;
    if (! is_null($csv_field_name_other)) {
      $row->$csv_field_name_other = implode($delimiter . ' ', $not_found_values);
    }
  }
}
