<?php

class TomsExampleMigrationCode {
  
  /**
   * Parser for event locations. Searchs the value string for integers and fetch
   * fetches results that match from the field_data_field_building_number table.
   * In some edge cases it will return multiple locations. At this point we need
   * to choose which location to use so we compare the location name with our
   * value. lowest in levenshtein score (most similar label) wins and becomes the
   * event location.
   */
  public function eventmigration_preprocess_on_campus_location($row, $key, $value) {
    preg_match_all('{(\d+)}', $value, $matches);
    $return = array();
    if (!empty($matches[1])) {
      $query = db_select('field_data_field_building_number', 'building_num');
      $query->fields('building_num', array('field_building_number_value'))
            ->fields('td', array('tid', 'name'))
            ->condition('building_num.field_building_number_value', $matches[1], 'IN')
            ->condition('building_num.bundle', 'campus_location', '=')
            ->join('taxonomy_term_data', 'td', 'building_num.entity_id = td.tid');
      $results = $query->execute();
      // In the result of multiple matches in the database.
      // We're going to select the one with the closest name from the source
      // field.

      // no shortest distance found, yet
      $shortest = -1;
      foreach($results as $result) {
        // calculate the distance between the input word,
        // and the current word
        $lev = levenshtein($value, $result->name);

        // check for an exact match
        if ($lev == 0) {

          // closest word is this one (exact match)
          $closest = $result;
          $shortest = 0;

          // break out of the loop; we've found an exact match
          break;
        }

        // if this distance is less than the next found shortest
        // distance, OR if a next shortest word has not yet been found
        if ($lev <= $shortest || $shortest < 0) {
          // set the closest match, and shortest distance
          $closest  = $result;
          $shortest = $lev;
        }
      }
      return $closest->name;
    }
    // No building numbers found in value.
    // watchdog('Event Migration', 'the imported event @name could not be matched with an existing location entity.', array('@name' => $row->event_title));
    return '';
  }
  
  /**
   * skip the node if the title is missing
   */
  public function eventmigration_preprocess_event_title($row, $key, $value) {
    if (empty($value)) {
      $row->skip = TRUE;
    }
    return $value;
  }

  /**
   * Appending the Last name into the same field as the first name
   */
  public function eventmigration_preprocess_first_name($row, $key, $value) {
    return $value . " " . $row->last_name;
  }
  /**
   * preprocess functions which can be declared for the source objects fields.
   * Use the format mirgation_source_preprocess_"sourcefield". Where
   * "sourcefield" is the source object's field without the quotes.
   */
  public function eventmigration_preprocess_intended_audience($row, $key, $value) {
    // clean data
    $value = $this->eventmigration_value_trim($value);
    $value = $this->eventmigration_check_tax_value($value, "event_audience", "Open to all");
    return $value;
  }
  
  public function eventmigration_preprocess_event_type_type_devenement($row, $key, $value) {
    // clean data
    $value = $this->eventmigration_value_trim($value);
    $value = $this->eventmigration_check_tax_value($value, "event_types", "TBD");
    return $value;
  }

  public function eventmigration_preprocess_event_organizer_organisateur_de_levenement($row, $key, $value) {
    $value = $this->eventmigration_value_trim($value);
    $value = $this->eventmigration_check_tax_value($value, "organization", "Administration");
    if (empty($value)) {
      watchdog('Event Migration', 'the imported event @name has no event organizer value.', array('@name' => $row->event_title), WATCHDOG_WARNING);
    }
    return $value;
  }

  /**
   * Grabbing the To date data and mapping it as the second value for the drupal
   * date field.
   */
  public function eventmigration_preprocess_event_start_date_time_date__heure_de_debut_de_levenement($row, $key, $value) {
    $date_data = array(
      'from' => (string) $row->event_start_date_time_date__heure_de_debut_de_levenement,
      'to' => (string) $row->event_end_date_time_date__heure_de_fin_de_levenement,
    );
    return array(drupal_json_encode($date_data));
  }

  /**
   * checkbox rewrite
   */
  public function eventmigration_preprocess_cost_to_attend_frais_de_participation($row, $key, $value) {
    return $this->migration_checkbox_value_rewrite($value);
  }

  /**
   * checkbox rewrite
   */
  public function eventmigration_preprocess_submission_request_to_be_featured_on_front_page($row, $key, $value) {
    return $this->migration_checkbox_value_rewrite($value);
  }

  /**
   * checkbox rewrite
   */
  public function eventmigration_preprocess_registration_required_inscription_requise($row, $key, $value) {
    return $this->migration_checkbox_value_rewrite($value);
  }

  /**
   * Utility functions which multiple fields may need for mappping. Stuff like
   * checkbox value rewriting.
   */
  /**
   * References value against available taxonomy terms.
   */
  public function eventmigration_check_tax_value($value, $vocab_name, $default) {
    // compare it with available event types
    static $terms = array();
    if (empty($terms[$vocab_name])) {
      $query = db_select('taxonomy_term_data', 'td');
      $query
        ->fields('td', array('name'))
        ->join('taxonomy_vocabulary', 'vocab', 'vocab.vid = td.vid');
      $query->condition('vocab.machine_name', $vocab_name, 'LIKE');
      $results = $query->execute();
      $results->fetchAssoc();
      foreach ($results as $result) {
        $terms[$vocab_name][] = $result->name;
      }
    }
    if (!in_array($value, $terms[$vocab_name])) {
      $value = $default;
    }
    return $value;
  }

  public function eventmigration_value_trim($value) {
    // convert text into friendly formatting
    $value = $this->migration_convert_to_utf8($value);
    // Once the strings are cleaned we need to prune them
    // Remove leading -
    $dash_pos = strpos($value, "-");
    if ($dash_pos === 0) {
      $dash_pos++;
      $value = substr($value, $dash_pos);
    }
    // Clean white space
    $value = trim($value);
    return $value;
  }

  public function migration_checkbox_value_rewrite($value) {
    switch ($value) {
      case 'oui':
      case 'Oui':
      case 'yes':
      case 'Yes':
      case 'To be posted on front page':
        $return = 1;
        break;
      case 'pas':
      case 'no':
      case 'non':
      case 'Free':
      case 'Gratuit/Free':
      case '':
      default:
        $return = 0;
        break;
    }
    return $return;
  }

  /**
   * swaps out unusual characters from the source data with friendly normal ones
   */
  public function migration_convert_to_utf8($string) {
    // First, replace UTF-8 characters.
    $string = str_replace(
     array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6", "\xC2\xA0"),
     array("'", "'", '"', '"', '-', '--', '...', ' '),
     $string);
    return $string;
  }
}
