<?php

class UsaeduMigrateConfig {
  public static function getWorkflowStates() {
    // These are only the workflow states we are interested in.
    return array(
      'draft' => 'Draft',
      'pending' => 'Pending',
      'returned' => 'Returned',
      'approved_public' => 'Approved (public)',
    );
  }

  public static function getCsvFields() {
    return array(
      'subject'                           => 'Subject',
      'grade'                             => 'Grade',
      'educational_use'                   => 'Educational Use',
      'ccss_njcccs'                       => 'CCSS / NJCCCS',
      'unit'                              => 'Unit',
      'slo'                               => 'SLO',
      'title'                             => 'Title',
      'description'                       => 'Description',
      'file_name_location'                => 'File Name/Location',
      'resource_url'                      => 'Resource URL',
      'usage_rights_url'                  => 'Usage Rights URL',
      'publisher_provider'                => 'Publisher / Provider',
      'enduser'                           => 'EndUser',
      'alignment_type'                    => 'Alignment Type',
      'educational_alignment'             => 'Educational Alignment',
      'learning_resource_type'            => 'Learning Resource Type',
      'item_type'                         => 'Item type',
      'digital_media_type'                => 'Digital Media Type',
      'author_creator'                    => 'Author/Creator',
      'date_created'                      => 'Date Created',
      'copyrightholder'                   => 'CopyrightHolder',
      'copyrightyear'                     => 'CopyrightYear',
      'grade_grade_level'                 => 'Grade / Grade Level',
      'age_range'                         => 'Age Range',
      'skill_micro_standard_performances' => 'Skill, Micro Standard, Performances',
      'taxonomy_depth_of_knowledge'       => 'Taxonomy, Depth of Knowledge',
      'keywords'                          => 'Keywords',
      'language'                          => 'Language',
      'is_based_on_url'                   => 'Is Based On URL',
      'interactivity_type'                => 'Interactivity Type',
      'peer_rating'                       => 'Peer rating',
    );
  }

  public static function getTyposInHeader() {
    return array(
      'Learning Resource Type, Learning Resource Type' => 'Learning Resource Type',
      '0' => 'Publisher / Provider',
    );
  }

  public static function getTyposInSubject() {
    return array(
      'English Language Arts' => 'English Language Arts',
      'English Language Arts, K, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12' => 'English Language Arts',
      'English Language Arts, English Language Arts' => 'English Language Arts',
      '' => '',
      'Mathematics' => 'Mathematics',
    );
  }

  public static function getTyposInGrade() {
    return array(
      'All' => '', // Without any grades, the resource gets assigned to the subject(s), which is equivalent to assign it to all grades.
    );
  }

  public static function getTyposInUnit() {
    return array(
      'All' => '', // Without any units, the resource gets assigned to the grade(s), which is equivalent to assign it to all units.
    );
  }

  public static function getTyposInSlo() {
    return array(
      '3,4,51,2' => '3,4,5,1,2',
      'All' => '', // Without any SLOs, the resource gets assigned to the unit(s), which is equivalent to assign it to all SLOs.
    );
  }

  public static function getTyposInCcssNjcccs() {
    return array(
      'All' => '',
      'ALL' => '',
    );
  }

  public static function getTyposInDigitalMediaType() {
    return array(
      'Other (comma separated for multiple values)' => 'Other',
    );
  }

  public static function getTyposInLearningResourceType() {
    return array(
      'Other (comma separated for multiple values)' => 'Other',
      'Text (needs to be defined)' => 'Text',
      'lesson' => 'Lesson',
    );
  }

  public static function getTyposInEducationalUseValue() {
    return array(
      'Other (comma separated for multiple values)' => 'Other',
    );
  }
}
