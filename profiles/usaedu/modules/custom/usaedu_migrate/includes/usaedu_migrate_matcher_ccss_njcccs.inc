<?php

class UsaeduMigrateMatcherCcssNjcccs {
  
  public function __construct($log_function) {
    $this->log_function = $log_function;
  }

  protected function log($message, $severity) {
    call_user_func($this->log_function, $message, $severity);
  }

  public function run() {
    $this->processAllSearches();
    $this->processAllTermNames();
    $this->processAllShortCodes();
  }

  protected function processAllSearches() {
    $raw_data = db_select('usaedu_migrate_raw_resource_data', 'raw')
      ->fields('raw', array('migrate_id', 'ccss_njcccs'))
      ->execute()->fetchAll();
    foreach ($raw_data as $row) {
      $row->ccss_njcccs = UsaeduMigrateToolbox::fixTyposInCcssNjcccs($row->ccss_njcccs);
      $values = UsaeduMigrateToolbox::splitShortcodes($row->ccss_njcccs);
      foreach ($values as $value) {
        $canonical = $this->canonicalize($value);
        if ('' == $canonical) {
          continue;
        }
        db_insert('usaedu_migrate_resource_x_ccss_njcccs_search')
          ->fields(array('migrate_id1' => $row->migrate_id, 'search' => $canonical, 'search_orig' => $value))
          ->execute();
        $this->log(t('Import row !migrate_id searches for !search (!search_orig)', array('!migrate_id' => $row->migrate_id, '!search' => $canonical, '!search_orig' => $value)), 'status');
      }
    }
  }

  protected function canonicalize($search) {
    $processed = drupal_strtolower($search);

    // Remove all non-alphanumeric characters.
    $processed = preg_replace('/[^a-z0-9]/', '', $processed);
    return $processed;
  }

  protected function processAllTermNames() {
    $query = db_select('taxonomy_term_data', 'term');
    $query->join('taxonomy_vocabulary', 'vocabulary', 'term.vid = vocabulary.vid');
    $query->condition('vocabulary.machine_name', array('ccss', 'statestand'));
    $query->fields('term', array('tid', 'name'));
    $term_name_data = $query->execute()->fetchAll();
    foreach ($term_name_data as $row) {
      $processed = $row->name;
      $processed = preg_replace('/^CCSS\.(:?ELA-Literacy|Math\.Content|Math\.Practice)\./', '', $processed);
      $canonical = $this->canonicalize($processed);
      if ('' == $canonical) {
        continue;
      }
      db_insert('usaedu_migrate_ccss_njcccs_keywords')
        ->fields(array('ccss_njcccs_tid' => $row->tid, 'keyword' => $canonical))
        ->execute();
      $this->log(t('Term !tid term name keyword !keyword', array('!tid' => $row->tid, '!keyword' => $canonical)), 'status');
    }
  }

  protected function processAllShortCodes() {
    $query = db_select('field_data_field_standard_short_code', 'short_code');
    $query->addField('short_code', 'entity_id', 'tid');
    $query->addField('short_code', 'field_standard_short_code_value', 'short_code');
    $query->condition('entity_type', 'taxonomy_term');
    $query->condition('bundle', 'ccss');
    $short_code_data = $query->execute()->fetchAll();
    foreach ($short_code_data as $row) {
      $canonical = $this->canonicalize($row->short_code);
      if ('' == $canonical) {
        continue;
      }
      db_insert('usaedu_migrate_ccss_njcccs_keywords')
        ->fields(array('ccss_njcccs_tid' => $row->tid, 'keyword' => $canonical))
        ->execute();
      $this->log(t('Term !tid short code keyword !keyword', array('!tid' => $row->tid, '!keyword' => $canonical)), 'status');
    }
  }

  public static function getMatches($migrate_id) {
    return db_query("
      SELECT
        search.search AS search
      , search.search_orig AS search_orig
      , term.tid AS tid
      , vocabulary.machine_name AS vocab_name
      FROM {usaedu_migrate_resource_x_ccss_njcccs_search} AS search
      LEFT JOIN {usaedu_migrate_ccss_njcccs_keywords} AS keywords ON search.search = keywords.keyword
      LEFT JOIN {taxonomy_term_data} AS term ON keywords.ccss_njcccs_tid = term.tid
      LEFT JOIN {taxonomy_vocabulary} AS vocabulary ON term.vid = vocabulary.vid
      WHERE
        search.migrate_id1 = :migrate_id
    ", array(':migrate_id' => $migrate_id))->fetchAll();
  }
}
