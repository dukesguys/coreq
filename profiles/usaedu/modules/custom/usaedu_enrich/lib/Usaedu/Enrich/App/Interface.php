<?php

interface Usaedu_Enrich_App_Interface {
  const KIND_DESCRIPTION = 'description';
  const KIND_THUMBNAIL = 'thumbnail';

  public function getUrlParts($urlString);
  public function getAttachedFile(stdClass $node);
  public function getResourceUrl(stdClass $node);
  public function hasThumbnail(stdClass $node);

  /**
   * @param stdClass $node
   * @param string $fileContents
   * @param string $thumbnailId The part of the thumbnail file name which should
   *   be different for each thumbnail. Can contain slashes to make subdirs.
   */
  public function setThumbnail(stdClass & $node, $fileContents, $thumbnailId);
  public function hasDescription(stdClass $node);
  public function setDescription(stdClass & $node, $description);
}
