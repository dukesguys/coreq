<?php

class Usaedu_Enrich_Processor_Simple extends Usaedu_Enrich_Processor_Abstract implements Usaedu_Enrich_Processor_Interface {

  public function enrich(stdClass & $node) {
    $wasChanged = FALSE;
    if (! $this->app()->hasThumbnail($node)) {
      $wasChanged = $wasChanged || $this->enrichThumbnail($node);
    }
    if (! $this->app()->hasDescription($node)) {
      $wasChanged = $wasChanged || $this->enrichDescription($node);
    }
    if ($wasChanged) {
      node_save($node);
    }
  }

  public function enrichThumbnail(stdClass & $node) {
    foreach ($this->getDi('strategies') as $strategy) {
      if ($strategy->isApplicable($node)) {
        $thumbnailId = $strategy->getThumbnailId($node);
        $thumbnail = $strategy->fetchThumbnail($node);
        $this->app()->setThumbnail($node, $thumbnail, $thumbnailId);
        return TRUE;
      }
    }
    return FALSE;
  }

  public function enrichDescription(stdClass & $node) {
    foreach ($this->getDi('strategies') as $strategy) {
      if ($strategy->isApplicable($node)) {
        $description = $strategy->fetchDescription($node);
        $this->app()->setDescription($node, $description);
        return TRUE;
      }
    }
    return FALSE;
  }
}
