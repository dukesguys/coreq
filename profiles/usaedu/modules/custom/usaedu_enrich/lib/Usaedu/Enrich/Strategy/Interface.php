<?php

interface Usaedu_Enrich_Strategy_Interface {
  public function isApplicable(stdClass $node);
  public function getThumbnailId(stdClass $node);
  public function fetchThumbnail(stdClass $node);
  public function fetchDescription(stdClass $node);
}
