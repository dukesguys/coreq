<?php

class Usaedu_Enrich_Strategy_Vimeo extends Usaedu_Enrich_Strategy_Abstract implements Usaedu_Enrich_Strategy_Interface {
  protected $vimeoUrlPathPregPatterns = [
    '{^/(\d+)$}' => 1,
    '{video/(\d+)}' => 1,
  ];
  
  protected $api = []; // Cache Vimeo API call results.

  public function isApplicable(stdClass $node) {
    $file = $this->app()->getAttachedFile($node);
    if ($this->isVimeoMediaFile($file)) {
      return TRUE;
    }

    $url = $this->app()->getResourceUrl($node);
    if ($this->isVimeoUrl($url)) {
      return TRUE;
    }

    return FALSE;
  }

  public function getThumbnailId(stdClass $node) {
    return 'vimeo/' . $this->getVimeoId($node) . '-large';
  }

  public function fetchThumbnail(stdClass $node) {
    $api = $this->fetchApi($this->getVimeoId($node));
    return file_get_contents($api[0]->thumbnail_large);
  }

  public function fetchDescription(stdClass $node) {
    $api = $this->fetchApi($this->getVimeoId($node));
    return $api[0]->description;
  }

  protected function fetchApi($vimeoId) {
    if (! isset($this->api[$vimeoId])) {
      $this->api[$vimeoId] = json_decode(file_get_contents('http://vimeo.com/api/v2/video/' . $vimeoId . '.json'));
    }
    return $this->api[$vimeoId];
  }

  protected function getVimeoId(stdClass $node) {
    $file = $this->app()->getAttachedFile($node);
    if ($this->isVimeoMediaFile($file)) {
      return $this->getVimeoIdFromFile($file);
    }

    $url = $this->app()->getResourceUrl($node);
    if ($this->isVimeoUrl($url)) {
      return $this->getVimeoIdFromUrl($url);
    }

    return NULL;
  }

  protected function isVimeoMediaFile(stdClass $file) {
    if (! isset($file->uri)) {
      return FALSE;
    }
  
    $url = $this->app()->getUrlParts($file->uri);
    return 'vimeo' == $url->scheme;
  }

  protected function getVimeoIdFromFile(stdClass $file) {
    return ltrim($this->app()->getUrlParts($file->uri)->path, '/');
  }

  protected function isVimeoUrl($urlString) {
    if (is_null($urlString)) {
      return FALSE;
    }

    $url = $this->app()->getUrlParts($urlString);
    
    if ('vimeo.com' == $url->host) {
      foreach ($this->vimeoUrlPathPregPatterns as $pattern => $matchIndex) {
        if (1 === preg_match($pattern, $url->path, $matches)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  protected function getVimeoIdFromUrl($urlString) {
    $url = $this->app()->getUrlParts($urlString);
    
    if ('vimeo.com' == $url->host) {
      foreach ($this->vimeoUrlPathPregPatterns as $pattern => $matchIndex) {
        if (1 === preg_match($pattern, $url->path, $matches)) {
          return $matches[$matchIndex];
        }
      }
    }
    return NULL;
  }
}
