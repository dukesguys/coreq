<?php
trait Usaedu_Enrich_Injectable {
  private $_dependencyContainer = NULL; 

  public function __construct(Usaedu_Enrich_Di_Interface $di = NULL) {
    if ($di instanceof Usaedu_Enrich_Di_Interface) {
      $this->setDi($di);
    }
  }

  public function setDi(Usaedu_Enrich_Di_Interface $di) {
    $this->_dependencyContainer = $di;
  }

  public function getDi($dependencyName = NULL) {
    if (is_null($dependencyName)) {
      return $this->_dependencyContainer;
    }
    else {
      return $this->_dependencyContainer->{$dependencyName};
    }
  }

  protected function app() {
    if ($this->getDi('app') instanceof Usaedu_Enrich_App_Interface) {
      return $this->getDi('app');
    }
    else {
      throw new Exception('Could not find "app" in DI container.');
    }
  }
}