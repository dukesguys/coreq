<?php

class Usaedu_Enrich_Di_Simple implements Usaedu_Enrich_Di_Interface {
  protected $things = [];

  public function register($name, $thing) {
    $this->things[$name] = $thing;
  }

  public function __get($name) {
    if (isset($this->things[$name])) {
      return $this->things[$name];
    }
    else {
      throw new Exception('Could not find "' . $name . '"');
    }
  }
}
