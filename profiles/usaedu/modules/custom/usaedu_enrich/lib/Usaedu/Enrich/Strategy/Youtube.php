<?php

class Usaedu_Enrich_Strategy_Youtube extends Usaedu_Enrich_Strategy_Abstract implements Usaedu_Enrich_Strategy_Interface {

  protected $api = []; // Cache Youtube API call results.

  public function isApplicable(stdClass $node) {
    $file = $this->app()->getAttachedFile($node);
    if ($this->isYoutubeMediaFile($file)) {
      return TRUE;
    }

    $url = $this->app()->getResourceUrl($node);
    if ($this->isYoutubeUrl($url)) {
      return TRUE;
    }

    return FALSE;
  }

  public function getThumbnailId(stdClass $node) {
    return 'youtube/' . $this->getYoutubeId($node) . '-default';
  }

  public function fetchThumbnail(stdClass $node) {
    $youtubeId = $this->getYoutubeId($node);
    return file_get_contents('http://i1.ytimg.com/vi/' . $youtubeId . '/default.jpg');
  }

  public function fetchDescription(stdClass $node) {
    $youtubeId = $this->getYoutubeId($node);
    return $this->getYoutubeDescription($youtubeId);
  }

  protected function fetchApi($youtubeId) {
    if (! isset($this->api[$youtubeId])) {
      $this->api[$youtubeId] = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/videos/' . $youtubeId . '?v=2&prettyprint=true&alt=json'));
    }
    return $this->api[$youtubeId];
  }

  protected function getYoutubeDescription($youtubeId) {
    $api = $this->fetchApi($youtubeId);
    return $api->{'entry'}->{'media$group'}->{'media$description'}->{'$t'};
  }

  protected function getYoutubeId(stdClass $node) {
    $file = $this->app()->getAttachedFile($node);
    if ($this->isYoutubeMediaFile($file)) {
      return $this->getYoutubeIdFromFile($file);
    }

    $url = $this->app()->getResourceUrl($node);
    if ($this->isYoutubeUrl($url)) {
      return $this->getYoutubeIdFromUrl($url);
    }

    return NULL;
  }

  protected function isYoutubeMediaFile(stdClass $file) {
    if (! isset($file->uri)) {
      return FALSE;
    }
  
    $url = $this->app()->getUrlParts($file->uri);
    return 'youtube' == $url->scheme;
  }

  protected function getYoutubeIdFromFile(stdClass $file) {
    return ltrim($this->app()->getUrlParts($file->uri)->path, '/');
  }

  protected function isYoutubeUrl($urlString) {
    if (is_null($urlString)) {
      return FALSE;
    }

    $url = $this->app()->getUrlParts($urlString);

    return (
      ('youtube.com' == $url->host)
      &&
      ('/watch' == $url->path)
      &&
      isset($url->query['v'])
    );
  }

  protected function getYoutubeIdFromUrl($urlString) {
    return $this->app()->getUrlParts($urlString)->query['v'];
  }
}
