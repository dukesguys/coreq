<?php

class Usaedu_Enrich_Strategy_Learnzillion extends Usaedu_Enrich_Strategy_Abstract implements Usaedu_Enrich_Strategy_Interface {

  protected $cache = []; // Cache Learnzillion HTML pages.

  public function isApplicable(stdClass $node) {
    $url = $this->app()->getResourceUrl($node);
    if ($this->isLearnzillionUrl($url)) {
      return TRUE;
    }

    return FALSE;
  }

  public function getThumbnailId(stdClass $node) {
    return 'learnzillion/' . sha1($this->getLearnzillionId($node));
  }

  public function fetchThumbnail(stdClass $node) {
    $rawResourceHtml = $this->fetchRawResourceHtml($this->getLearnzillionId($node));
    $doc = new DOMDocument();
    $doc->loadHTML($rawResourceHtml, LIBXML_NOERROR);
    $xpath = new DOMXpath($doc);
    $elements = $xpath->query("//div[@class='showcase-video-player']//div[@itemtype='http://schema.org/VideoObject']/meta[@itemprop='thumbnail']/@content");
    foreach ($elements as $element) {
      if (valid_url($element->nodeValue)) {
        $thumbnailUrl = $element->nodeValue;
        return file_get_contents($thumbnailUrl);
      }
    }
    return NULL;
  }

  public function fetchDescription(stdClass $node) {
    $rawResourceHtml = $this->fetchRawResourceHtml($this->getLearnzillionId($node));
    $doc = new DOMDocument();
    $doc->loadHTML($rawResourceHtml, LIBXML_NOERROR);
    $xpath = new DOMXpath($doc);
    $elements = $xpath->query("//div[contains(@class,'showcase-video-details')]//div[@class='showcase-description']");
    foreach ($elements as $element) {
      if ('' != $element->textContent) {
        $description = $element->textContent;
        return $description;
      }
    }
    return NULL;
  }

  protected function fetchRawResourceHtml($learnzillionId) {
    if (! isset($this->cache[$learnzillionId])) {
      $this->cache[$learnzillionId] = file_get_contents($learnzillionId);
    }
    return $this->cache[$learnzillionId];
  }

  protected function getLearnzillionId(stdClass $node) {
    $url = $this->app()->getResourceUrl($node);
    if ($this->isLearnzillionUrl($url)) {
      return $url;
    }

    return NULL;
  }

  protected function isLearnzillionUrl($urlString) {
    if (is_null($urlString)) {
      return FALSE;
    }

    return 'learnzillion.com' == $this->app()->getUrlParts($urlString)->host;
  }
}
