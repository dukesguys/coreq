<?php

interface Usaedu_Enrich_Processor_Interface {
  public function enrich(stdClass & $node);
}
