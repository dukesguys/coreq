<?php

interface Usaedu_Enrich_Di_Interface {
  public function register($name, $thing);
  public function __get($name);
}
