<?php

class Usaedu_Enrich_Loop_Simple extends Usaedu_Enrich_Loop_Abstract implements Usaedu_Enrich_Loop_Interface {
  public function run() {
    foreach ($this->getNids() as $nid) {
      $node = node_load($nid);
      $this->getDi('processor')->enrich($node);
    }
  }

  protected function getNids() {
    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_thumbnail', 'thumb', 'n.nid = thumb.entity_id');
    $query->condition('n.type', 'resource');
    $query->isNull('thumb.entity_id');
    $query->fields('n', ['nid']);
    $nids_without_thumbnails = $query->execute()->fetchCol();

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_body', 'body', 'n.nid = body.entity_id');
    $query->condition('n.type', 'resource');
    $query->isNull('body.entity_id');
    $query->fields('n', ['nid']);
    $nids_without_body = $query->execute()->fetchCol();

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_body', 'body', 'n.nid = body.entity_id');
    $query->condition('n.type', 'resource');
    $query->where("TRIM(body.body_value) = ''");
    $query->fields('n', ['nid']);
    $nids_with_empty_body = $query->execute()->fetchCol();

    $nids = array_unique(array_merge($nids_without_thumbnails, $nids_without_body, $nids_with_empty_body));

    return $nids;
  }
}
