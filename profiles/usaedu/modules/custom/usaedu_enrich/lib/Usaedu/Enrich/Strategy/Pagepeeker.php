<?php

class Usaedu_Enrich_Strategy_Pagepeeker extends Usaedu_Enrich_Strategy_Abstract implements Usaedu_Enrich_Strategy_Interface {

  public function isApplicable(stdClass $node) {
    $url = $this->app()->getResourceUrl($node);
    if (valid_url($url, TRUE)) {
      return TRUE;
    }

    return FALSE;
  }

  public function getThumbnailId(stdClass $node) {
    return 'pagepeeker/' . sha1($this->app()->getResourceUrl($node));
  }

  public function fetchThumbnail(stdClass $node) {
    $url = $this->app()->getResourceUrl($node);
    $this->requestRemoteThumbnail($url);
    if ($this->waitForRemoteThumbnail($url)) {
      $fileContents = $this->getRemoteThumbnail($url);
      if (FALSE !== $fileContents) {
        return $fileContents;
      }
    }
    return NULL;
  }

  public function fetchDescription(stdClass $node) {
    return NULL;
  }

  protected function requestRemoteThumbnail($url) {
    // With Pagepeeker, requesting and getting a thumbnail uses the same API call.
    return $this->getRemoteThumbnail($url);
  }

  protected function waitForRemoteThumbnail($url, $retryCountLimit = 10) {
    $retryCount = 0;
    $isReady = FALSE;
    $isError = FALSE;
    do {
      $status = json_decode(file_get_contents($this->getRemoteWaitUrl($url)));
//       var_dump('status', $status, 'retryCount', $retryCount);
      $isReady = (isset($status->IsReady) && (1 == $status->IsReady));
      $isError = (isset($status->Error) && (1 == $status->Error));
      if (! $isReady) {
        sleep(5 + $retryCount);
      }
      $retryCount++;
    } while ((! $isReady) && (! $isError) && ($retryCount < $retryCountLimit));
    return (! $isError) && ($retryCount < $retryCountLimit);
  }

  protected function getRemoteThumbnail($url) {
    return file_get_contents($this->getRemoteThumbnailUrl($url));
  }

  protected function getRemoteThumbnailUrl($websiteUrl) {
    return 'http://free.pagepeeker.com/v2/thumbs.php?size=m&url=' . urlencode($websiteUrl);
  }

  protected function getRemoteWaitUrl($websiteUrl) {
    return 'http://free.pagepeeker.com/v2/thumbs_ready.php?size=m&url=' . urlencode($websiteUrl);
  }
}
