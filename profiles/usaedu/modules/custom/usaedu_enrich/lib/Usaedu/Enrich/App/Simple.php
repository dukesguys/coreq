<?php

class Usaedu_Enrich_App_Simple implements Usaedu_Enrich_App_Interface {
  use Usaedu_Enrich_Injectable;

  public function getUrlParts($urlString) {
    $url = new stdClass;

    if (FALSE === ($urlParts = parse_url($urlString))) {
      $urlParts = [];
    }

    $url->scheme = isset($urlParts['scheme']) ? $urlParts['scheme'] : '';
    $url->host = isset($urlParts['host']) ? $this->canonicalUrlHost($urlParts['host']) : '';
    $url->path = isset($urlParts['path']) ? $urlParts['path'] : '';

    if (isset($urlParts['query'])) {
      parse_str($urlParts['query'], $query);
    }
    else {
      $query = [];
    }

    $url->query = $query;

    return $url;
  }

  protected function canonicalUrlHost($host) {
    $host = preg_replace('{^www\.}', '', $host);
    return $host;
  }

  public function getAttachedFile(stdClass $node) {
    if ('resource' == $node->type) {
      $file_items = field_get_items('node', $node, 'field_attached_files');
      if (is_array($file_items) && (0 < count($file_items))) {
        // There should be only 1 item. Also convert to stdClass.
        $file = (object) reset($file_items);
        if (isset($file->fid)) {
          return $file;
        }
      }
    
    }
    return NULL;
  }

  public function getResourceUrl(stdClass $node) {
    if ('resource' == $node->type) {
      $url_items = field_get_items('node', $node, 'field_resource_url');
      if (is_array($url_items) && (0 < count($url_items))) {
        // There should be only 1 item.
        $url_item = reset($url_items);
        if (isset($url_item['url'])) {
          return $url_item['url'];
        }
      }
    }
    return NULL;
  }

  public function hasThumbnail(stdClass $node) {
    return isset($node->field_thumbnail[LANGUAGE_NONE][0]['fid']);
  }

  public function setThumbnail(stdClass & $node, $fileContents, $thumbnailId) {
    if (! is_null($fileContents)) {
      $file = $this->saveAsDrupalFile($fileContents, $thumbnailId);
      $node->field_thumbnail[LANGUAGE_NONE][0] = (array) $file;
    }
  }

  protected function saveAsDrupalFile($fileContents, $thumbnailId) {
    $fileUri = $this->makeThumbnailUri($thumbnailId);
    $dirUri = drupal_dirname($fileUri);
    file_prepare_directory($dirUri, FILE_CREATE_DIRECTORY);
    return file_save_data($fileContents, $fileUri, FILE_EXISTS_REPLACE);
  }

  protected function makeThumbnailUri($thumbnailId) {
    return 'public://resource-thumbnails/' . $thumbnailId . '.jpg';
  }

  public function hasDescription(stdClass $node) {
    return (
      isset($node->body[LANGUAGE_NONE][0]['value'])
      &&
      ('' != trim($node->body[LANGUAGE_NONE][0]['value']))
    );
  }

  public function setDescription(stdClass & $node, $description) {
    $description = check_plain($description);
    $node->body[LANGUAGE_NONE][0] = [
      'value' => $description,
      'summary' => '',
      'format' => 'plain_text',
      'safe_value' => $description,
      'safe_summary' => '',
    ];
  }
}
