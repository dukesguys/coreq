<?php

$di = new Usaedu_Enrich_Di_Simple();

$di->register('app', new Usaedu_Enrich_App_Simple($di));

$di->register('strategies', [
  new Usaedu_Enrich_Strategy_Youtube($di),
  new Usaedu_Enrich_Strategy_Vimeo($di),
  new Usaedu_Enrich_Strategy_Learnzillion($di),
  new Usaedu_Enrich_Strategy_Pagepeeker($di),
]);

$di->register('processor', new Usaedu_Enrich_Processor_Simple($di));
$di->register('loop', new Usaedu_Enrich_Loop_Simple($di));

$di->loop->run();
