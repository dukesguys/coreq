<?php

class Histogram {
  protected $counts = [];

  public function add($what) {
    $what = (string) $what;
    if (isset($this->counts[$what])) {
      $this->counts[$what]++;
    }
    else {
      $this->counts[$what] = 1;
    }
  }

  public function getCounts() {
    arsort($this->counts);
    return $this->counts;
  }

  public function getOrderedListAsText() {
    $output = '';
    foreach ($this->getCounts() as $what => $count) {
      $output .= $count . "\t" . $what . PHP_EOL;
    }
    return $output;
  }
}

function usaedu_import_canonical_host($host) {
  $host = preg_replace('{^www\.}', '', $host);
  return $host;
}

function usaedu_import_count_host_names() {
  $hosts = new Histogram();
  $urls = db_select('field_data_field_resource_url', 'u')
    ->fields('u', ['field_resource_url_url'])
    ->execute()
    ->fetchCol();
  foreach ($urls as $url) {
    if (FALSE === ($url_parts = parse_url($url))) {
      $hosts->add('malformed_url');
    }
    else {
      $host = usaedu_import_canonical_host($url_parts['host']);
      if ('' == $host) {
//         var_dump($url);
        continue;
      }
//       if ('illustrativemathematics.org' == $host) {
//       if ('learnnc.org' == $host) {
//       if ('learnzillion.com' == $host) {
//       if ('readwritethink.org' == $host) {
      if ('youtube.com' == $host) {
        var_dump($url);
      }
      $hosts->add($host);
    }
  }
  var_dump(PHP_EOL . $hosts->getOrderedListAsText());
}

// usaedu_import_count_host_names();

function usaedu_import_count_file_types() {
  $mimes = new Histogram();
  $schemes = new Histogram();
  $extensions = new Histogram();
  $fids = db_select('field_data_field_attached_files', 'f')
    ->fields('f', ['field_attached_files_fid'])
    ->execute()
    ->fetchCol();
  foreach ($fids as $fid) {
    $file = file_load($fid);
    $mimes->add($file->filemime);
    $name = $file->filename;
    $name_pieces = explode('.', $file->filename);
    $extension = array_pop($name_pieces);
    $extensions->add('.' . $extension);
    list($scheme, $rest) = explode('://', $file->uri, 2);
    $schemes->add($scheme);
  }
  var_dump('$mimes' . PHP_EOL . $mimes->getOrderedListAsText());
  var_dump('$schemes' . PHP_EOL . $schemes->getOrderedListAsText());
  var_dump('$extensions' . PHP_EOL . $extensions->getOrderedListAsText());
}

usaedu_import_count_file_types();
