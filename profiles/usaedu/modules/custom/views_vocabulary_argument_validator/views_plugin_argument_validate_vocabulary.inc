<?php

/**
 * @file
 * Contains the 'taxonomy term' argument validator plugin.
 */

/**
 * Validate whether an argument is an acceptable node.
 */
class views_plugin_argument_validate_vocabulary extends views_plugin_argument_validate {
  function option_definition() {
    $options = parent::option_definition();
    $options['vocabularies'] = array('default' => array());

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $vocabularies = taxonomy_get_vocabularies();
    $options = array();
    foreach ($vocabularies as $voc) {
      $options[$voc->machine_name] = check_plain($voc->name);
    }

    $form['vocabularies'] = array(
      '#type' => 'checkboxes',
      '#prefix' => '<div id="edit-options-validate-argument-vocabulary-wrapper">',
      '#suffix' => '</div>',
      '#title' => t('Vocabularies'),
      '#options' => $options,
      '#default_value' => $this->options['vocabularies'],
      '#description' => t('If you wish to validate for specific vocabularies, check them; if none are checked, all vocabularies will pass.'),
    );
  }

  function options_submit(&$form, &$form_state, &$options = array()) {
    // Filter unselected items so we don't unnecessarily store giant arrays.
    $options['vocabularies'] = array_filter($options['vocabularies']);
  }

  function convert_options(&$options) {
    if (!isset($options['vocabularies']) && !empty($this->argument->options['validate_argument_vocabulary'])) {
      $options['vocabularies'] = $this->argument->options['validate_argument_vocabulary'];
    }
  }

  function validate_argument($argument) {
    $vocabularies = taxonomy_vocabulary_get_names();

    $allowed = array();
    foreach (array_filter($this->options['vocabularies']) as $option) {
      if (isset($vocabularies[$option])) {
        $allowed[$vocabularies[$option]->vid] = $vocabularies[$option];
      }
    }

    // Setting a title value for use if the argument is valid.
    if (isset($allowed[$argument])) {
      $vocab = $allowed[$argument];
      $this->argument->validated_title = entity_label('taxonomy_vocabulary', $vocab);
      return TRUE;
    }

    return FALSE;;
  }
}
