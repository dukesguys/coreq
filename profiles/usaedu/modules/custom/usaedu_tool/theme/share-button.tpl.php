<?php
/**
 * @param array $config The local addthis_config data.
 * @param array $share The local addthis_share data.
 */
?>

<a
  class="addthis_button_email"
  addthis:url="<?php print $share['url']; ?>"
  addthis:ui_use_css="false"
><span><?php print t('Share'); ?></span></a>
