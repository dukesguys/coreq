<?php

function usaedu_tool_tool_info() {
  return array(
    'share' => array(
      'title' => t('Share'),
      'weight' => 300,
    ),
  );
}

function usaedu_tool_tool_available($tool_id, array $tool_context) {
  if ('share' == $tool_id) {
    if ('view' == $tool_context['context type'] && 'resources_search' == $tool_context['id'] && $tool_context['is_start']) {
      // No sharing of the resource search start page.
      return FALSE;
    }

    if ('view' == $tool_context['context type'] && 'my_collections' == $tool_context['id']) {
      // No sharing of the "my collections" page.
      return FALSE;
    }

    if ('node' == $tool_context['context type'] && 'collection' == $tool_context['node type']) {
      // No sharing of collection node pages.
      return FALSE;
    }

    // "Share" everything else.
    return TRUE;
  }
  return FALSE;
}

function usaedu_tool_tool_view($tool_id, array $tool_context) {
  if ('share' == $tool_id) {
    drupal_add_library('usaedu_tool', 'addthis');
    $config = array();
    $share = array(
      // TODO Do this in JS to also capture #-fragments.
      'url' => usaedu_tool_get_absolute_page_url(),
    );
    return theme('share_button', array('config' => $config, 'share' => $share));
  }
}
