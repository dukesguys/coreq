<?php
/**
 * @file
 * site_links.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function site_links_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about-us:node/51
  $menu_links['main-menu_about-us:node/51'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/51',
    'router_path' => 'node/%',
    'link_title' => 'About Us',
    'options' => array(
      'identifier' => 'main-menu_about-us:node/51',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'green',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_administrator:resources/portal/18240
  $menu_links['main-menu_administrator:resources/portal/18240'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/portal/18240',
    'router_path' => 'resources/portal/%',
    'link_title' => 'Administrator',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_administrator:resources/portal/18240',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_role:<nolink>',
  );
  // Exported menu link: main-menu_assessment:resources/educational_use/assessment
  $menu_links['main-menu_assessment:resources/educational_use/assessment'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/educational_use/assessment',
    'router_path' => 'resources',
    'link_title' => 'Assessment',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_assessment:resources/educational_use/assessment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_category:<nolink>',
  );
  // Exported menu link: main-menu_category:<nolink>
  $menu_links['main-menu_category:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Category',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'no-link',
        'style' => '',
      ),
      'identifier' => 'main-menu_category:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_search-resources-by:<nolink>',
  );
  // Exported menu link: main-menu_ccss:standards/ccss
  $menu_links['main-menu_ccss:standards/ccss'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'standards/ccss',
    'router_path' => 'standards/%',
    'link_title' => 'CCSS',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'section-1',
        'style' => '',
      ),
      'identifier' => 'main-menu_ccss:standards/ccss',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_state-standards:resources',
  );
  // Exported menu link: main-menu_curriculuminstruction:resources/educational_use/curriculuminstruction
  $menu_links['main-menu_curriculuminstruction:resources/educational_use/curriculuminstruction'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/educational_use/curriculuminstruction',
    'router_path' => 'resources',
    'link_title' => 'Curriculum/Instruction',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_curriculuminstruction:resources/educational_use/curriculuminstruction',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_category:<nolink>',
  );
  // Exported menu link: main-menu_dashboard:user/edit
  $menu_links['main-menu_dashboard:user/edit'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'user/edit',
    'router_path' => 'user/edit',
    'link_title' => 'Dashboard',
    'options' => array(
      'identifier' => 'main-menu_dashboard:user/edit',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'purple',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_family-member:resources/portal/18242
  $menu_links['main-menu_family-member:resources/portal/18242'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/portal/18242',
    'router_path' => 'resources/portal/%',
    'link_title' => 'Family Member',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_family-member:resources/portal/18242',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_role:<nolink>',
  );
  // Exported menu link: main-menu_family-resources:resources/end_user/parent
  $menu_links['main-menu_family-resources:resources/end_user/parent'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/end_user/parent',
    'router_path' => 'resources',
    'link_title' => 'Family Resources',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_family-resources:resources/end_user/parent',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_role:<nolink>',
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'menu-item-home',
        'style' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_keyword--filter:resources
  $menu_links['main-menu_keyword--filter:resources'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources',
    'router_path' => 'resources',
    'link_title' => 'Keyword / Filter',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_keyword--filter:resources',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'main-menu_search-resources-by:<nolink>',
  );
  // Exported menu link: main-menu_loginsign-up:user/login
  $menu_links['main-menu_loginsign-up:user/login'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Login/Sign Up',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_loginsign-up:user/login',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_nj-model-curriculum:standards/curriculum
  $menu_links['main-menu_nj-model-curriculum:standards/curriculum'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'standards/curriculum',
    'router_path' => 'standards/%',
    'link_title' => 'NJ Model Curriculum',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'section-3',
        'style' => '',
      ),
      'identifier' => 'main-menu_nj-model-curriculum:standards/curriculum',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_state-standards:resources',
  );
  // Exported menu link: main-menu_njcccs:standards/statestand
  $menu_links['main-menu_njcccs:standards/statestand'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'standards/statestand',
    'router_path' => 'standards/%',
    'link_title' => 'NJCCCS',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'section-2',
        'style' => '',
      ),
      'identifier' => 'main-menu_njcccs:standards/statestand',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_state-standards:resources',
  );
  // Exported menu link: main-menu_professional-development:resources/educational_use/professional-development
  $menu_links['main-menu_professional-development:resources/educational_use/professional-development'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/educational_use/professional-development',
    'router_path' => 'resources',
    'link_title' => 'Professional Development',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_professional-development:resources/educational_use/professional-development',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_category:<nolink>',
  );
  // Exported menu link: main-menu_role:<nolink>
  $menu_links['main-menu_role:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Role',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'no-link',
        'style' => '',
      ),
      'identifier' => 'main-menu_role:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_search-resources-by:<nolink>',
  );
  // Exported menu link: main-menu_search-resources-by:<nolink>
  $menu_links['main-menu_search-resources-by:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Search Resources By:',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'label',
        'style' => '',
      ),
      'identifier' => 'main-menu_search-resources-by:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_state-standards:resources
  $menu_links['main-menu_state-standards:resources'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources',
    'router_path' => 'resources',
    'link_title' => 'State Standards',
    'options' => array(
      'identifier' => 'main-menu_state-standards:resources',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'dkgreen no-link',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_teacher:resources/portal/18241
  $menu_links['main-menu_teacher:resources/portal/18241'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/portal/18241',
    'router_path' => 'resources/portal/%',
    'link_title' => 'Teacher',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_teacher:resources/portal/18241',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_role:<nolink>',
  );
  // Exported menu link: menu-footer-menu_about-us:node/51
  $menu_links['menu-footer-menu_about-us:node/51'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/51',
    'router_path' => 'node/%',
    'link_title' => 'About Us',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_about-us:node/51',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_about:node/51
  $menu_links['menu-footer-menu_about:node/51'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/51',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_about:node/51',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_dashboard:dashboard
  $menu_links['menu-footer-menu_dashboard:dashboard'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'dashboard',
    'router_path' => 'dashboard',
    'link_title' => 'Dashboard',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_dashboard:dashboard',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_help:node/56
  $menu_links['menu-footer-menu_help:node/56'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/56',
    'router_path' => 'node/%',
    'link_title' => 'Help',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_help:node/56',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_home:<front>
  $menu_links['menu-footer-menu_home:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_state-standards:standards/statestand
  $menu_links['menu-footer-menu_state-standards:standards/statestand'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'standards/statestand',
    'router_path' => 'standards/%',
    'link_title' => 'State Standards',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_state-standards:standards/statestand',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_terms-of-use:legal
  $menu_links['menu-footer-menu_terms-of-use:legal'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'legal',
    'router_path' => 'legal',
    'link_title' => 'Terms of Use',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_terms-of-use:legal',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_toolkits:legal
  $menu_links['menu-footer-menu_toolkits:legal'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'legal',
    'router_path' => 'legal',
    'link_title' => 'Toolkits',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu_toolkits:legal',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: user-menu_dashboard:dashboard
  $menu_links['user-menu_dashboard:dashboard'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'dashboard',
    'router_path' => 'dashboard',
    'link_title' => 'Dashboard',
    'options' => array(
      'identifier' => 'user-menu_dashboard:dashboard',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'purple',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: user-menu_log-out:user/logout
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 1,
  );
  // Exported menu link: user-menu_user-account:user
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -10,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('About Us');
  t('Administrator');
  t('Assessment');
  t('CCSS');
  t('Category');
  t('Curriculum/Instruction');
  t('Dashboard');
  t('Family Member');
  t('Family Resources');
  t('Help');
  t('Home');
  t('Keyword / Filter');
  t('Log out');
  t('Login/Sign Up');
  t('NJ Model Curriculum');
  t('NJCCCS');
  t('Professional Development');
  t('Role');
  t('Search Resources By:');
  t('State Standards');
  t('Teacher');
  t('Terms of Use');
  t('Toolkits');
  t('User account');


  return $menu_links;
}
