<?php
/**
 * @file
 * usaedu_statestand.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function usaedu_statestand_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_curriculum_level'
  $field_bases['field_curriculum_level'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_curriculum_level',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Subject Area' => 'Subject Area',
        'State Standard' => 'State Standard',
        'Strand' => 'Strand',
        'Grade Band' => 'Grade Band',
        'Content Statement' => 'Content Statement',
        'Cumulative Progress Indicator' => 'Cumulative Progress Indicator',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
