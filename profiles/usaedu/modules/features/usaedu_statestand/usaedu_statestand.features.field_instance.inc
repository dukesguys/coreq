<?php
/**
 * @file
 * usaedu_statestand.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function usaedu_statestand_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-statestand-field_curriculum_level'
  $field_instances['taxonomy_term-statestand-field_curriculum_level'] = array(
    'bundle' => 'statestand',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the level of this term within the structure of the NJCCCS data model.',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_curriculum_level',
    'label' => 'Curriculum level',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 0,
      ),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'taxonomy_term-statestand-field_file_attachments'
  $field_instances['taxonomy_term-statestand-field_file_attachments'] = array(
    'bundle' => 'statestand',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_file_attachments',
    'label' => 'File attachments',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'pdf doc docx xls xlsx odt ods odp rtf txt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'taxonomy_term-statestand-field_grade_level'
  $field_instances['taxonomy_term-statestand-field_grade_level'] = array(
    'bundle' => 'statestand',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select all grades relevant to this item. When entering World Languages choose proficiency level from end of list. ',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_grade_level',
    'label' => 'Grade level',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-statestand-field_term_page_content'
  $field_instances['taxonomy_term-statestand-field_term_page_content'] = array(
    'bundle' => 'statestand',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_page_content',
    'label' => 'Term page content',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Curriculum level');
  t('File attachments');
  t('Grade level');
  t('Select all grades relevant to this item. When entering World Languages choose proficiency level from end of list. ');
  t('Select the level of this term within the structure of the NJCCCS data model.');
  t('Term page content');

  return $field_instances;
}
