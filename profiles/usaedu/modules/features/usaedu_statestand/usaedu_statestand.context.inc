<?php
/**
 * @file
 * usaedu_statestand.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_statestand_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'statestand_term_page';
  $context->description = 'Term page or map page in state standard';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'standards/statestand/*' => 'standards/statestand/*',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'statestand' => 'statestand',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'standards/statestand',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Term page or map page in state standard');
  $export['statestand_term_page'] = $context;

  return $export;
}
