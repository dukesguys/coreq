<?php
/**
 * @file
 * usaedu_curriculum.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function usaedu_curriculum_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-curriculum-field_academic_benchmark_guid'
  $field_instances['taxonomy_term-curriculum-field_academic_benchmark_guid'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The Academic Bechmark GUIDs which correspond to this standard.',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'narrow_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_academic_benchmark_guid',
    'label' => 'Academic Benchmark GUID',
    'placeholder' => '',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'taxonomy_term-curriculum-field_file_attachments'
  $field_instances['taxonomy_term-curriculum-field_file_attachments'] = array(
    'bundle' => 'curriculum',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'narrow_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_file_attachments',
    'label' => 'File attachments',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'pdf doc docx xls xlsx odt ods odp rtf txt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-curriculum-field_grade_level'
  $field_instances['taxonomy_term-curriculum-field_grade_level'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select all grades relevant to this item.',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'narrow_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_grade_level',
    'label' => 'Grade level',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'taxonomy_term-curriculum-field_map_label'
  $field_instances['taxonomy_term-curriculum-field_map_label'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'narrow_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_map_label',
    'label' => 'Map label',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-curriculum-field_related_ccss'
  $field_instances['taxonomy_term-curriculum-field_related_ccss'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Do not forget to click the "Save" button at the end of the form after you added or changed the Related CCSS here!',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 4,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'narrow_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_related_ccss',
    'label' => 'Related CCSS',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'hs_taxonomy',
      'settings' => array(),
      'type' => 'taxonomy_hs',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'taxonomy_term-curriculum-field_term_page_content'
  $field_instances['taxonomy_term-curriculum-field_term_page_content'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'narrow_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_page_content',
    'label' => 'Term page content',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Academic Benchmark GUID');
  t('Do not forget to click the "Save" button at the end of the form after you added or changed the Related CCSS here!');
  t('File attachments');
  t('Grade level');
  t('Map label');
  t('Related CCSS');
  t('Select all grades relevant to this item.');
  t('Term page content');
  t('The Academic Bechmark GUIDs which correspond to this standard.');

  return $field_instances;
}
