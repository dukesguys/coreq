<?php
/**
 * @file
 * usaedu_curriculum.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_curriculum_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'njmc_term_page';
  $context->description = 'Term page or map page in NJMC standard';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'standards/curriculum/*' => 'standards/curriculum/*',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'curriculum' => 'curriculum',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'standards/curriculum',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Term page or map page in NJMC standard');
  $export['njmc_term_page'] = $context;

  return $export;
}
