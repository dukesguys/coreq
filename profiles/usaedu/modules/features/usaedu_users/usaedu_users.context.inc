<?php
/**
 * @file
 * usaedu_users.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_users_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'myfeed';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'user_feed:page' => 'user_feed:page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;
  $export['myfeed'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'registration';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'register' => 'register',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'content_preface',
          'weight' => '-10',
        ),
        'usaedu_users-why_register' => array(
          'module' => 'usaedu_users',
          'delta' => 'why_register',
          'region' => 'content_preface',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['registration'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide_anonymous';
  $context->description = 'Sitewide context for anonymous users';
  $context->tag = '';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'anonymous user' => 'anonymous user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-anonymous-user-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-anonymous-user-menu',
          'region' => 'user_navigation',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sitewide context for anonymous users');
  $export['sitewide_anonymous'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_dashboard';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'dashboard' => 'dashboard',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'usaedu_users-dashboard_announcement' => array(
          'module' => 'usaedu_users',
          'delta' => 'dashboard_announcement',
          'region' => 'header',
          'weight' => '-11',
        ),
        'usaedu_search-find_resources_facets' => array(
          'module' => 'usaedu_search',
          'delta' => 'find_resources_facets',
          'region' => 'header',
          'weight' => '-10',
        ),
        'usaedu_users-dashboard_standards_links' => array(
          'module' => 'usaedu_users',
          'delta' => 'dashboard_standards_links',
          'region' => 'header',
          'weight' => '-9',
        ),
        'usaedu_users-dashboard_badges' => array(
          'module' => 'usaedu_users',
          'delta' => 'dashboard_badges',
          'region' => 'content_preface',
          'weight' => '-10',
        ),
        'views-my_collections-block' => array(
          'module' => 'views',
          'delta' => 'my_collections-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-recent_resources-block_1' => array(
          'module' => 'views',
          'delta' => 'recent_resources-block_1',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-recent_resources-block_3' => array(
          'module' => 'views',
          'delta' => 'recent_resources-block_3',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-user_feed-block_1' => array(
          'module' => 'views',
          'delta' => 'user_feed-block_1',
          'region' => 'content',
          'weight' => '-7',
        ),
        'views-recent_video-block' => array(
          'module' => 'views',
          'delta' => 'recent_video-block',
          'region' => 'content',
          'weight' => '3',
        ),
        'views-recent_video-block_1' => array(
          'module' => 'views',
          'delta' => 'recent_video-block_1',
          'region' => 'content',
          'weight' => '4',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['user_dashboard'] = $context;

  return $export;
}
