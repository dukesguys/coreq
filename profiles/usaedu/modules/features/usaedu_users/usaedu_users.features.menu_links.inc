<?php
/**
 * @file
 * usaedu_users.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function usaedu_users_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-anonymous-user-menu:node/56
  $menu_links['menu-anonymous-user-menu:node/56'] = array(
    'menu_name' => 'menu-anonymous-user-menu',
    'link_path' => 'node/56',
    'router_path' => 'node/%',
    'link_title' => 'Help',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-anonymous-user-menu:user/login
  $menu_links['menu-anonymous-user-menu:user/login'] = array(
    'menu_name' => 'menu-anonymous-user-menu',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Login',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-anonymous-user-menu:user/register
  $menu_links['menu-anonymous-user-menu:user/register'] = array(
    'menu_name' => 'menu-anonymous-user-menu',
    'link_path' => 'user/register',
    'router_path' => 'user/register',
    'link_title' => 'Register',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Help');
  t('Login');
  t('Register');


  return $menu_links;
}
