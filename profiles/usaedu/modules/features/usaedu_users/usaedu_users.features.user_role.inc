<?php
/**
 * @file
 * usaedu_users.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function usaedu_users_user_default_roles() {
  $roles = array();

  // Exported role: Academic Leader.
  $roles['Academic Leader'] = array(
    'name' => 'Academic Leader',
    'weight' => 3,
  );

  // Exported role: Educator.
  $roles['Educator'] = array(
    'name' => 'Educator',
    'weight' => 5,
  );

  // Exported role: Manager.
  $roles['Manager'] = array(
    'name' => 'Manager',
    'weight' => 2,
  );

  // Exported role: Super Educator.
  $roles['Super Educator'] = array(
    'name' => 'Super Educator',
    'weight' => 4,
  );

  // Exported role: non-authenticated.
  $roles['non-authenticated'] = array(
    'name' => 'non-authenticated',
    'weight' => 6,
  );

  return $roles;
}
