<?php

/**
 * Implements hook_drush_command().
 */
function usaedu_users_drush_command() {
  $items = array();
  $items['usaedu-users-import-smid'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Import a CSV file with SMID data into the DB.'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function usaedu_users_drush_help($section) {
  switch ($section) {
    case 'drush:usaedu-users-import-smid':
      return dt('Import a CSV file with SMID data into the DB.');
  }
}

function drush_usaedu_users_import_smid_validate($csv_filename) {
  if (! is_file($csv_filename)) {
    return drush_set_error('SMID_CSV_TO_DB', dt('Please provide a CSV file name.'));
  }
  if (! usaedu_users_check_csv_headers($csv_filename)) {
    return FALSE;
  }
}

function usaedu_users_check_csv_headers($csv_filename) {
  $expected = array(
    'StaffMemberIdentifier' => 'StaffMemberIdentifier',
    'FirstName' => 'FirstName',
    'LastName' => 'LastName',
    'DateOfBirth' => 'DateOfBirth',
  );
  $expected_header = array_keys($expected);
  $expected_header_original = array_values($expected);
  drush_log(dt('Start checking CSV file headers.'), 'status');
  $filename = $csv_filename;
  drush_log(dt('Checking headers on file "@file".', array('@file' => $filename)), 'status');
  $csv_data = usaedu_users_get_csv($filename);
  $header = $csv_data->header;
  $header_original = $csv_data->header_original;
  if (! usaedu_users_are_csv_headers_equal_sets($expected_header, $header)) {
    $diff_expected = array_diff($expected_header_original, $header_original);
    $diff_found = array_diff($header_original, $expected_header_original);
    drush_log(dt('Expected header(s): !expected.', array('!expected' => '"' . implode('", "', $diff_expected) . '"')), 'error');
    drush_log(dt('Found header(s): !found.', array('!found' => '"' . implode('", "', $diff_found) . '"')), 'error');
    return drush_set_error('SMID_CSV_TO_DB', dt('Headers different from previous files in file "@file".', array('@file' => $filename)));
  }
  drush_log(dt('Done checking CSV file headers.'), 'status');
  return TRUE;
}

function drush_usaedu_users_import_smid($csv_filename) {
  drush_log(dt('Start importing SMID CSV file into DB.'), 'status');
  $csv_data = usaedu_users_get_csv($csv_filename);
  if (FALSE === $csv_data) {
    return FALSE;
  }
  $csv = $csv_data->csv;
  $header = $csv_data->header;
  $csv_cols = array_flip($header);
  $stats = array(
    'already_there' => 0,
    'insert' => 0,
  );
  try {
    foreach ($csv as $csv_index => $line) {
      $smid = $line[$csv_cols['StaffMemberIdentifier']];
      $already_there = db_select('SMIDs', 's')
        ->condition('s.StaffMemberIdentifier', $smid, '=')
        ->fields('s', array('StaffMemberIdentifier'))
        ->execute()->fetchCol();
      if (0 == count($already_there)) {
        db_insert('SMIDs')
          ->fields($header, $line)
          ->execute();
        $stats['insert']++;
      }
      else {
        $stats['already_there']++;
      }
    }
  }
  catch (PDOException $e) {
    drush_log(dt('Tried to insert this data:'), 'error');
    drush_print_table(UsaeduMigrateToolbox::arrayTranspose2d(array($header, $line)));
    return drush_set_error('SMID_CSV_TO_DB', dt('DB error: @msg.', array('@msg' => $e->getMessage())));
  }
  
  drush_print_table(UsaeduMigrateToolbox::arrayTranspose2d(array(array_keys($stats), array_values($stats))));
  drush_log(dt('Done importing SMID CSV file into DB.'), 'status');
}

function usaedu_users_get_supported_encodings() {
  $list = `iconv -l`;
  $list = preg_replace('{//(?=\W)}', '', $list); // Linux (or Debian) iconv appends "//" when outputting to a pipe. Reason unknown.
  $encodings = preg_split("/\s/", $list);
  return $encodings;
}

function usaedu_users_choose_encoding() {
  static $chosen_encoding = NULL;
  if (is_null($chosen_encoding)) {
    $encs = array_intersect(array('MACROMAN', 'MAC', 'MACINTOSH'), usaedu_users_get_supported_encodings());
    $chosen_encoding = reset($encs);
  }
  return $chosen_encoding;
}

function usaedu_users_get_csv($filename) {
  $file_content = file_get_contents($filename);
  $chosen_encoding = usaedu_users_choose_encoding();
  $file_content = iconv($chosen_encoding, "UTF-8", $file_content);
  if (FALSE === $file_content) {
    return drush_set_error(dt('iconv does not understand the character encoding "@enc"', array('@enc' => $chosen_encoding)));
  }

  $line_ending = usaedu_csv_count_line_endings_heuristics($file_content);

  if (is_null($line_ending)) {
    drush_log("Error: File has ambiguous line endings, i.e. a combination of CR, CRLF and/or LF: $url", 'error');
    drush_log('Error: Line ending statistics:', 'error');
    $stats = usaedu_csv_count_line_endings_per_kind($file_content);
    drush_log(var_export($stats, TRUE), 'error');
    return drush_set_error('SMID_CSV_MIXED_LINE_ENDINGS', dt('Mixed line endings.'));
  }

  $csv = usaedu_csv_parse_string($file_content, ',', '"', $line_ending);
  usaedu_csv_trim_in_place($csv);
  $header = array_shift($csv);
  $header_original = $header;
  $header = usaedu_users_normalize_csv_header($header);
  $return = new stdClass();
  $return->csv = $csv;
  $return->header = $header;
  $return->header_original = $header_original;
  return $return;
}

function usaedu_users_normalize_csv_header(array $csv_header) {
  $normalized_header = array();
  foreach ($csv_header as $csv_column_name_index => $csv_column_name) {
    $csv_column_name = usaedu_users_fix_typo_in_header($csv_column_name);
    $normalized_header[$csv_column_name_index] = $csv_column_name;
  }
  return $normalized_header;
}

// This is only a comparison of sets, element order is irrelevant.
function usaedu_users_are_csv_headers_equal_sets(array $csv_header_a, array $csv_header_b) {
  sort($csv_header_a);
  sort($csv_header_b);
  return $csv_header_a == $csv_header_b;
}

function usaedu_users_fix_typo_in_header($csv_column_name) {
  $typos = array(
    'MistypedColumnHeader' => 'CorrectedColumnHEader',
  );
  return isset($typos[$csv_column_name]) ? $typos[$csv_column_name] : $csv_column_name;
}
