/**
 * @file
 * JavaScript to display resource upload feedback as modal dialog.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  $(document).ready(function () {
    var dialogElement = $('.usaedu-users-upload-feedback');

    dialogElement.once('dialog', function() {
      dialogElement.dialog({
        dialogClass: 'usaedu-users-upload-feedback-dialog',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false,
        width: '70%', // Undocumented, but works.
        height: 'auto',
        show: 750,
        hide: 1000,
        close: function (event, ui) {
          Drupal.detachBehaviors(dialogElement);
          $(this).hide();
        }
      });

      $('.usaedu-users-upload-feedback-dialog-close').click(function (event) {
        event.preventDefault();
        dialogElement.dialog('close');
      });

    });
  }); // document#ready()

})(jQuery, Drupal, this, this.document); // "anonymous closure"
