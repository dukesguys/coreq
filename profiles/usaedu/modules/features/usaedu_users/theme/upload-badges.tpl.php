<?php
/**
 * $upload_count - integer  number of uploads
 * $size_suffix - string  qualify CSS classes with this
 */
?>
<div class="usaedu-users-upload-feedback-badges usaedu-users-upload-feedback-badges<?php print $size_suffix; ?>">
  <?php foreach ($badges as $badge) : ?>
    <div class="<?php print $badge['div_class']; ?>"><img src="<?php print $badge['src']; ?>" class="<?php print $badge['img_class']; ?>" /></div>
  <?php endforeach; ?>
</div>
