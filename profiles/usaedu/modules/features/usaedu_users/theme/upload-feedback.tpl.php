<?php
/**
 * $upload_count - integer number of uploads
 */
?>
<div class="usaedu-users-upload-feedback" style="display:none;">
  <div class="usaedu-users-upload-feedback-dialog-close"></div>

  <h1><?php print t("Congratulations, you've earned your @upload_count upload badge!", array('@upload_count' => $upload_count)); ?></h1>

  <p>
    <?php print t('Thanks for contributing to the Resource Exchange.'); ?>
    Lorem ipsum, etc.
  </p>

  <?php print render($badges); ?>

  <a class="usaedu-users-upload-feedback-button button" href="<?php print url('node/add/resource'); ?>"><?php print t('Upload another resource'); ?></a>
</div>
