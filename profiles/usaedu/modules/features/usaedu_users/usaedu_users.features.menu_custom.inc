<?php
/**
 * @file
 * usaedu_users.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function usaedu_users_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-anonymous-user-menu.
  $menus['menu-anonymous-user-menu'] = array(
    'menu_name' => 'menu-anonymous-user-menu',
    'title' => 'Anonymous user menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Anonymous user menu');


  return $menus;
}
