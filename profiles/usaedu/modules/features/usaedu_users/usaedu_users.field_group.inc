<?php
/**
 * @file
 * usaedu_users.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function usaedu_users_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_myaccount|user|user|form';
  $field_group->group_name = 'group_myaccount';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'My Account',
    'weight' => '0',
    'children' => array(
      0 => 'group_personal_info',
      1 => 'group_smid',
    ),
    'format_type' => 'profile-tab',
    'format_settings' => array(
      'label' => 'My Account',
      'instance_settings' => array(
        'path' => 'edit',
        'description' => '',
        'classes' => 'group-myaccount field-group-profile-tab',
      ),
    ),
  );
  $export['group_myaccount|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_myfeeds|user|user|form';
  $field_group->group_name = 'group_myfeeds';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Configure my feed',
    'weight' => '2',
    'children' => array(
      0 => 'field_subject_areas',
      1 => 'field_grade_level_term',
    ),
    'format_type' => 'profile-tab',
    'format_settings' => array(
      'label' => 'Configure my feed',
      'instance_settings' => array(
        'path' => 'edit-my-feed',
        'description' => '',
        'classes' => 'group-myfeeds field-group-profile-tab',
      ),
    ),
  );
  $export['group_myfeeds|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_personal_info|user|user|form';
  $field_group->group_name = 'group_personal_info';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_myaccount';
  $field_group->data = array(
    'label' => 'Personal information',
    'weight' => '11',
    'children' => array(
      0 => 'field_birth_date',
      1 => 'field_first_name',
      2 => 'field_last_name',
      3 => 'account',
      4 => 'timezone',
      5 => 'locale',
      6 => 'wysiwyg',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Personal information',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => ' group-personal-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'user_user_form_group_personal_info',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_personal_info|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_smid|user|user|form';
  $field_group->group_name = 'group_smid';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_myaccount';
  $field_group->data = array(
    'label' => 'Professional information',
    'weight' => '12',
    'children' => array(
      0 => 'field_position_title',
      1 => 'field_schools',
      2 => 'field_smid',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Professional information',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => ' group-smid field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'user_user_form_group_smid',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_smid|user|user|form'] = $field_group;

  return $export;
}
