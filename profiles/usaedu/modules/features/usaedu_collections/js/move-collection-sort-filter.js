(function ($) {

  Drupal.behaviors.usaeduMoveCollectionSortFilter = {
    attach: function (context, settings) {
      var view_selector = ".view-collection-nodes",
          filter_selector = "#edit-sort-simple",
          sort_by_selector = "#edit-sort-by",
          sort_order_selector = "#edit-sort-order",
          fake_filter = 'fake-edit-sort-simple',
          btn_submit = '#edit-submit-collection-nodes';
      
      if ($(".view-header", view_selector).length && $(filter_selector, view_selector).length && $('#' + fake_filter).length == 0) {
        $(filter_selector, view_selector)
          .clone()
          .attr('id', fake_filter)
          .on('change', function(e) {
            $(filter_selector, view_selector).val(this.value);
            var values = this.value.split('--');
            $(sort_by_selector, view_selector).val(values[0]);
            $(sort_order_selector, view_selector).val(values[1]);
            $(btn_submit).trigger('click');
          })
          .prependTo(".view-header", view_selector)
          .wrap('<form id="fake-sort-filter" />')
          .chosen();
      }
    }
  };

})(jQuery);