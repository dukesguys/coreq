<?php
/**
 * @file
 * usaedu_collections.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function usaedu_collections_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function usaedu_collections_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function usaedu_collections_node_info() {
  $items = array(
    'collection' => array(
      'name' => t('Collection'),
      'base' => 'node_content',
      'description' => t('A collection of resources created by an Educator.'),
      'has_title' => '1',
      'title_label' => t('Collection label'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
