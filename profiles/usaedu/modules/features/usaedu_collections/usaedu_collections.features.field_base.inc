<?php
/**
 * @file
 * usaedu_collections.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function usaedu_collections_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_resources'
  $field_bases['field_resources'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resources',
    'indexes' => array(
      'nid' => array(
        0 => 'nid',
      ),
    ),
    'locked' => 0,
    'module' => 'node_reference',
    'settings' => array(
      'referenceable_types' => array(
        'assessment' => 0,
        'assessment_item' => 0,
        'collection' => 0,
        'page' => 0,
        'resource' => 'resource',
      ),
      'view' => array(
        'args' => array(),
        'display_name' => '',
        'view_name' => '',
      ),
    ),
    'translatable' => 0,
    'type' => 'node_reference',
  );

  return $field_bases;
}
