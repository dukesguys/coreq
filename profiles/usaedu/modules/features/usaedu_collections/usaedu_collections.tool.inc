<?php

function usaedu_collections_tool_info() {
  return array(
    'collect' => array(
      'title' => t('Collect'),
      'weight' => 200,  
    ),
  );
}

function usaedu_collections_tool_available($tool_id, array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    if ('collect' == $tool_id) {
      $uid = $tool_context['uid'];
      $user = user_load($uid);
      return user_access('edit own collection content', $user);
    }
  }
  return FALSE;
}

function usaedu_collections_tool_view($tool_id, array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $resource_nid = $tool_context['id'];
    return usaedu_tool_link(t('Collect'), "collect-resource/nojs/$resource_nid", TRUE);
  }
}
