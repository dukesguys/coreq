<?php
/**
 * @file
 * usaedu_collections.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_collections_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'collection_node';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'collection' => 'collection',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-collection_nodes-block_1' => array(
          'module' => 'views',
          'delta' => 'collection_nodes-block_1',
          'region' => 'content',
          'weight' => '10',
        ),
        'usaedu_tool-tools' => array(
          'module' => 'usaedu_tool',
          'delta' => 'tools',
          'region' => 'sidebar_second',
          'weight' => '-11',
        ),
        'views-recent_resources-block' => array(
          'module' => 'views',
          'delta' => 'recent_resources-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['collection_node'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'my_collections';
  $context->description = 'The "My Collections" view page display.';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'my_collections:page' => 'my_collections:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'usaedu_tool-tools' => array(
          'module' => 'usaedu_tool',
          'delta' => 'tools',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('The "My Collections" view page display.');
  $export['my_collections'] = $context;

  return $export;
}
