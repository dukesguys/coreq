<?php
/**
 * @file
 * usaedu_featured_content.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function usaedu_featured_content_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'featured_content';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Featured Content';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Featured Content';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Feature Type */
  $handler->display->display_options['fields']['field_fc_type']['id'] = 'field_fc_type';
  $handler->display->display_options['fields']['field_fc_type']['table'] = 'field_data_field_fc_type';
  $handler->display->display_options['fields']['field_fc_type']['field'] = 'field_fc_type';
  $handler->display->display_options['fields']['field_fc_type']['label'] = '';
  $handler->display->display_options['fields']['field_fc_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_fc_type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_fc_type']['alter']['text'] = '[field_fc_type-value]';
  $handler->display->display_options['fields']['field_fc_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_fc_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_fc_type']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_fc_type']['element_default_classes'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_fc_link']['id'] = 'field_fc_link';
  $handler->display->display_options['fields']['field_fc_link']['table'] = 'field_data_field_fc_link';
  $handler->display->display_options['fields']['field_fc_link']['field'] = 'field_fc_link';
  $handler->display->display_options['fields']['field_fc_link']['label'] = '';
  $handler->display->display_options['fields']['field_fc_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_fc_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_fc_link']['alter']['text'] = '[field_fc_link-url]';
  $handler->display->display_options['fields']['field_fc_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_fc_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_fc_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_fc_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_fc_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_fc_link']['type'] = 'link_plain';
  /* Field: Content: Thumbnail */
  $handler->display->display_options['fields']['field_thumbnail']['id'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['field'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail']['element_type'] = '0';
  $handler->display->display_options['fields']['field_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_thumbnail']['element_wrapper_class'] = 'featured-content-image';
  $handler->display->display_options['fields']['field_thumbnail']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail']['settings'] = array(
    'image_style' => 'featured_content',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'featured-content-title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_fc_description']['id'] = 'field_fc_description';
  $handler->display->display_options['fields']['field_fc_description']['table'] = 'field_data_field_fc_description';
  $handler->display->display_options['fields']['field_fc_description']['field'] = 'field_fc_description';
  $handler->display->display_options['fields']['field_fc_description']['label'] = '';
  $handler->display->display_options['fields']['field_fc_description']['alter']['max_length'] = '350';
  $handler->display->display_options['fields']['field_fc_description']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_fc_description']['element_type'] = '0';
  $handler->display->display_options['fields']['field_fc_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_fc_description']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_fc_description']['element_wrapper_class'] = 'featured-content-description';
  $handler->display->display_options['fields']['field_fc_description']['element_default_classes'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'featured_content' => 'featured_content',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['block_description'] = 'Featured Content Slideshow';
  $translatables['featured_content'] = array(
    t('Master'),
    t('Featured Content'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('[field_fc_type-value]'),
    t('[field_fc_link-url]'),
    t('Block'),
    t('Featured Content Slideshow'),
  );
  $export['featured_content'] = $view;

  return $export;
}
