(function ($) {

  Drupal.behaviors.usaeduMoveSortFilter = {
    attach: function (context, settings) {
      var view_selector = ".view-usaedu-taxonomy-term",
          filter_selector = "#edit-sort-order"
          fake_filter = 'fake-edit-sort-order',
          btn_submit = '#edit-submit-usaedu-taxonomy-term';
      
      if ($(".view-header", view_selector).length && $(filter_selector, view_selector).length && $('#' + fake_filter).length == 0) {
        $(filter_selector, view_selector)
          .clone()
          .attr('id', fake_filter)
          .on('change', function(e) {
            $(filter_selector, view_selector).val(this.value);
            $(btn_submit).trigger('click');
          })
          .prependTo(".view-header", view_selector)
          .wrap('<form id="fake-sort-filter" />')
          .chosen();
      }
    }
  };

})(jQuery);