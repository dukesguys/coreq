/* Add view toogle behaviour to the related SLO */
(function ($) {
  Drupal.behaviors.usaeduRelatedSLO = {
    attach: function (context, settings) {
      $('.view-standards-term > .attachment', context).once('usaeduRelatedSLO', function () {
        var top_parent = this,
            slo_label = 'h3';
        
        this.style.height = 0;
        
        $(slo_label, this).each(function() {
          var pos = $(this).position(),
              next = $(this).next(),
              this_group = $(this).parent();
          
          $(this)
            .css({
              'left': pos.left + 'px'/*, >:( Thanks the comment of the top position to IE 
              'top': pos.top + 'px'*/
            })
            .click(function() {
              $(this_group)
                .toggleClass('expanded')
                .siblings('.expanded') // hide expanded siblings
                  .each(function() {
                    $(this).removeClass('expanded');
                    $('ul', this).hide();
                });
              
              $(next).slideToggle({
                progress: function() {
                  top_parent.style.height = $(this).outerHeight(true) + 'px';
                }
              });
            });
        });
        // Set position absoute after we have set the right coordinates
        $(slo_label, this).css('position', 'absolute');
      });
    }
  };
})(jQuery);