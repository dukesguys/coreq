/**
 * Javascript behaviours for the Evaluation node
 */
(function ($) {
  Drupal.behaviors.usaeduEvaluationNode = {
    attach: function (context, settings) {
      // Count characters in textarea >> http://stackoverflow.com/questions/5371089/count-characters-in-textarea
      function countChar(elem, counter, maxcount) {
        var len = elem.value.length;
        if (len >= maxcount) {
          elem.value = elem.value.substring(0, maxcount);
        } else {
          $(counter).text(maxcount - len);
        }
      }

      $('#evaluation-node-form-dialog', context).once('usaedu-evaluation-node', function() {

        /* On the resource evaluation form, check the 'N/A' radios by default. */
        $('div.form-radios', this).each(function(){
          var has_radio_set = $('input[type=radio]:checked', this).length;
          if (!has_radio_set) {
            $('input:radio[value=_none]', this).prop('checked', true);
          }
        });

        var bodyField = $("#edit-body-dialog", this);
        if (bodyField.length) {
          var commentBody = $('.form-textarea-wrapper', bodyField),
              commentBodyLength = $('textarea', commentBody).val().length * 1,
              counter = 250 - commentBodyLength;

          if (commentBodyLength === 0) {
            $(commentBody).hide(function() {
              $(bodyField).addClass('collapsed');
            });
          }

          $('label', bodyField)
            .click(function() {
              $(commentBody).slideToggle(function(){
                $(bodyField).toggleClass('collapsed');
              });
            })
            .before('<div id="rubric-body-counter">' + counter +'</div>');
          $('textarea', bodyField)
            .attr('maxlength', counter)
            .keyup(function() {
              countChar(this, '#rubric-body-counter', counter);
            });
        }
      });
    }
  };
})(jQuery);
