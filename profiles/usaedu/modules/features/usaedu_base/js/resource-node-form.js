(function (globalContext) {
  "use strict";
  // Local dependencies.
  var $ = globalContext.jQuery
    , Drupal = globalContext.Drupal
    , document = globalContext.document;

  /**
   * Show "*_other" fields already on page load if the "Other" taxo term is chosen.
   */
  function initOtherField() {
    $('.node-resource-form select').each(function (selectIndex) {
      var $chosenContainer = $(this).next('.chosen-container');
      var isOtherChosen = false;
      $('ul.chosen-choices li.search-choice span', $chosenContainer).each(function (choiceIndex) {
        if ('Other' == $(this).text()) {
          isOtherChosen = true;
          return false;
        }
      });
      if (isOtherChosen) {
        showOtherForSelect(this);
      }
    });
  }

  /**
   * Display the "other" field when the selected field has an "other" value
   */
  function showOtherField() {
    $('select').once().bind('change', function(event, params) {
      if (params) {
        var value;
        if (params.selected) {
          value = params.selected;
        }
        else if (params.deselected) {
          value = params.deselected;
        }
        value = $('option[value=' + value + ']', this).text();

        if ('other' == value.toLowerCase()) {
          if (params.selected) {
            showOtherForSelect(this);
          }
          else if (params.deselected) {
            hideOtherForSelect(this);
          }
        }
      }
    });
  }

  function markChosenFieldsWithCustomValidationAsRequired() {
    var fields = [
      'subject-areas'
     ,'grade-level-term'
     ,'educational-use'
     ,'end-user'
     ,'alignment-type'
     ,'learning-resource-type'
     ,'digital-media-type'
    ];
    for (var i = 0; i < fields.length; i++) {
      var selector = '.form-field-name-field-' + fields[i] + ' .chosen-container';
      addRequiredMarker($(selector));
    }
  }

  function showOtherForSelect(selectElement) {
    var $otherField = $(selectElement).parents('.form-wrapper').next('.form-wrapper[class*=other]');
    $otherField.slideDown();
    $otherField.find('label').each(function () {
      addRequiredMarker(this, 'append');
    });
    $otherField.find('input[type=text]:first').each(function () {
      addRequiredMarker(this);
    });
  }

  function addRequiredMarker(element, placementMethod) {
    var requiredHTML = '<span class="field-suffix"><span class="form-required" title="This field is required.">*</span></span>';
    var $element = $(element);
    if (! $element.hasClass('has-required-appended')) {
      placementMethod = ('append' == placementMethod) ? 'append' : 'after';
      $element.addClass('has-required-appended')[placementMethod](requiredHTML);
    }
  }

  function hideOtherForSelect(selectElement) {
    $(selectElement).parents('.form-wrapper').next('.form-wrapper[class*=other]').slideUp();
  }

  Drupal.behaviors.usaeduResourceNodeForm = {
    attach: function (context, settings) {
      initOtherField();
      showOtherField();
      markChosenFieldsWithCustomValidationAsRequired();
    }
  };
})(window);
