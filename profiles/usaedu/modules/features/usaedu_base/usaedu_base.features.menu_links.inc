<?php
/**
 * @file
 * usaedu_base.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function usaedu_base_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:<front>
  $menu_links['main-menu:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'menu-item-home',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: main-menu:<nolink>
  $menu_links['main-menu:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Search Resources By:',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'label',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '1',
    'weight' => '-49',
  );
  // Exported menu link: main-menu:resources
  $menu_links['main-menu:resources'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources',
    'router_path' => 'resources',
    'link_title' => 'Keyword / Filter',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-45',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:resources/educational_use/assessment
  $menu_links['main-menu:resources/educational_use/assessment'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/educational_use/assessment',
    'router_path' => 'resources',
    'link_title' => 'Assessment',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:resources/educational_use/curriculuminstruction
  $menu_links['main-menu:resources/educational_use/curriculuminstruction'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/educational_use/curriculuminstruction',
    'router_path' => 'resources',
    'link_title' => 'Curriculum/Instruction',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:resources/educational_use/professional-development
  $menu_links['main-menu:resources/educational_use/professional-development'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/educational_use/professional-development',
    'router_path' => 'resources',
    'link_title' => 'Professional Development',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:resources/portal/18240
  $menu_links['main-menu:resources/portal/18240'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/portal/18240',
    'router_path' => 'resources/portal/%',
    'link_title' => 'Administrator',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:resources/portal/18241
  $menu_links['main-menu:resources/portal/18241'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/portal/18241',
    'router_path' => 'resources/portal/%',
    'link_title' => 'Teacher',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:resources/portal/18242
  $menu_links['main-menu:resources/portal/18242'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources/portal/18242',
    'router_path' => 'resources/portal/%',
    'link_title' => 'Family Member',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:standards/ccss
  $menu_links['main-menu:standards/ccss'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'standards/ccss',
    'router_path' => 'standards/%',
    'link_title' => 'CCSS',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'section-1',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:standards/curriculum
  $menu_links['main-menu:standards/curriculum'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'standards/curriculum',
    'router_path' => 'standards/%',
    'link_title' => 'NJ Model Curriculum',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'section-3',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: main-menu:standards/statestand
  $menu_links['main-menu:standards/statestand'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'standards/statestand',
    'router_path' => 'standards/%',
    'link_title' => 'NJCCCS',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'section-2',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => '<nolink>',
  );
  // Exported menu link: menu-footer-menu:<front>
  $menu_links['menu-footer-menu:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Administrator');
  t('Assessment');
  t('CCSS');
  t('Curriculum/Instruction');
  t('Family Member');
  t('Home');
  t('Keyword / Filter');
  t('NJ Model Curriculum');
  t('NJCCCS');
  t('Professional Development');
  t('Search Resources By:');
  t('Teacher');


  return $menu_links;
}
