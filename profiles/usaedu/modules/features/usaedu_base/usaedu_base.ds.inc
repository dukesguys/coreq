<?php
/**
 * @file
 * usaedu_base.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function usaedu_base_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|evaluation|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'evaluation';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'author' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_post_date_long',
    ),
  );
  $export['node|evaluation|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|evaluation|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'evaluation';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'author' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'author',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_post_date_long',
    ),
  );
  $export['node|evaluation|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|admin_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'admin_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
  );
  $export['node|resource|admin_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|author_narrow_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'author_narrow_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'inline',
      'format' => 'ds_post_date_short',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => 'Uploaded',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'uploaded-resource-post-date',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|resource|author_narrow_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|author_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'author_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'post_date' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'ds_post_date_short',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'uploaded-resource-post-date',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|resource|author_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'upload_feedback' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'evaluation_body_count' => array(
      'weight' => '25',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_endorsement_comments' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'endorsement-comments',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_peer_rating' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => '',
          'ow-def-cl' => TRUE,
          'ow-at' => 'id="peer-rating"',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_endorsement_date' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => 'Endorsed on ',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'endorsement-date',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|resource|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|mini_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'mini_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => '',
        'class' => '',
      ),
    ),
  );
  $export['node|resource|mini_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|narrow_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'narrow_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|resource|narrow_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|resource|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|resource|video_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'resource';
  $ds_fieldsetting->view_mode = 'video_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|resource|video_teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function usaedu_base_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|evaluation|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'evaluation';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'post_date',
        1 => 'author',
        2 => 'body',
      ),
    ),
    'fields' => array(
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|evaluation|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|evaluation|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'evaluation';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'author',
        1 => 'post_date',
        2 => 'body',
      ),
    ),
    'fields' => array(
      'author' => 'ds_content',
      'post_date' => 'ds_content',
      'body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|evaluation|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|admin_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'admin_teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'regions' => array(
      'left' => array(
        0 => 'field_thumbnail',
        1 => 'field_endorsed_resource',
        2 => 'field_peer_rating',
        3 => 'field_grade_level_term',
        4 => 'title',
        5 => 'field_standards_state',
        6 => 'field_standards_ccss',
      ),
      'right' => array(
        0 => 'usaedu_workflow_summary',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'left',
      'field_endorsed_resource' => 'left',
      'field_peer_rating' => 'left',
      'field_grade_level_term' => 'left',
      'title' => 'left',
      'field_standards_state' => 'left',
      'field_standards_ccss' => 'left',
      'usaedu_workflow_summary' => 'right',
    ),
  );
  $export['node|resource|admin_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|author_narrow_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'author_narrow_teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_thumbnail',
        1 => 'field_endorsed_resource',
      ),
      'right' => array(
        2 => 'post_date',
        3 => 'usaedu_workflow_state',
        4 => 'field_peer_rating',
        5 => 'title',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'left',
      'field_endorsed_resource' => 'left',
      'post_date' => 'right',
      'usaedu_workflow_state' => 'right',
      'field_peer_rating' => 'right',
      'title' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|resource|author_narrow_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|author_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'author_teaser';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_thumbnail',
        1 => 'field_endorsed_resource',
      ),
      'right' => array(
        2 => 'field_grade_level_term',
        3 => 'post_date',
        4 => 'usaedu_workflow_state',
        5 => 'field_peer_rating',
        6 => 'title',
        7 => 'field_standards_state',
        8 => 'field_standards_ccss',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'left',
      'field_endorsed_resource' => 'left',
      'field_grade_level_term' => 'right',
      'post_date' => 'right',
      'usaedu_workflow_state' => 'right',
      'field_peer_rating' => 'right',
      'title' => 'right',
      'field_standards_state' => 'right',
      'field_standards_ccss' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|resource|author_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'upload_feedback',
        1 => 'group_resource_info',
        2 => 'field_endorsed_resource',
        3 => 'field_peer_rating',
        4 => 'field_learning_resource_type',
        5 => 'field_grade_level_term',
        6 => 'group_evaluation_results',
        7 => 'body',
        8 => 'group_main_content',
        9 => 'group_tags',
        10 => 'field_attached_files',
        11 => 'field_resource_url',
        12 => 'field_alignment_type',
        13 => 'field_digital_media_type',
        14 => 'field_educational_use',
        15 => 'field_end_user',
        16 => 'field_interactivity_type',
        17 => 'field_endorsement_date',
        18 => 'field_item_type',
        19 => 'field_endorsement_comments',
        20 => 'field_keywords',
        21 => 'group_rubric',
        22 => 'field_aggregate_alignment',
        23 => 'field_language',
        24 => 'evaluation_body_count',
        25 => 'field_revised_blooms',
        26 => 'field_aggregate_key_shifts',
        27 => 'field_subject_areas',
        28 => 'field_aggregate_supports',
        29 => 'field_aggregate_assessment',
      ),
    ),
    'fields' => array(
      'upload_feedback' => 'ds_content',
      'group_resource_info' => 'ds_content',
      'field_endorsed_resource' => 'ds_content',
      'field_peer_rating' => 'ds_content',
      'field_learning_resource_type' => 'ds_content',
      'field_grade_level_term' => 'ds_content',
      'group_evaluation_results' => 'ds_content',
      'body' => 'ds_content',
      'group_main_content' => 'ds_content',
      'group_tags' => 'ds_content',
      'field_attached_files' => 'ds_content',
      'field_resource_url' => 'ds_content',
      'field_alignment_type' => 'ds_content',
      'field_digital_media_type' => 'ds_content',
      'field_educational_use' => 'ds_content',
      'field_end_user' => 'ds_content',
      'field_interactivity_type' => 'ds_content',
      'field_endorsement_date' => 'ds_content',
      'field_item_type' => 'ds_content',
      'field_endorsement_comments' => 'ds_content',
      'field_keywords' => 'ds_content',
      'group_rubric' => 'ds_content',
      'field_aggregate_alignment' => 'ds_content',
      'field_language' => 'ds_content',
      'evaluation_body_count' => 'ds_content',
      'field_revised_blooms' => 'ds_content',
      'field_aggregate_key_shifts' => 'ds_content',
      'field_subject_areas' => 'ds_content',
      'field_aggregate_supports' => 'ds_content',
      'field_aggregate_assessment' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|resource|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|mini_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'mini_teaser';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_thumbnail',
        1 => 'field_endorsed_resource',
      ),
      'right' => array(
        2 => 'field_peer_rating',
        3 => 'title',
        4 => 'body',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'left',
      'field_endorsed_resource' => 'left',
      'field_peer_rating' => 'right',
      'title' => 'right',
      'body' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|resource|mini_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|narrow_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'narrow_teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_thumbnail',
        1 => 'field_endorsed_resource',
      ),
      'right' => array(
        2 => 'field_peer_rating',
        3 => 'title',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'left',
      'field_endorsed_resource' => 'left',
      'field_peer_rating' => 'right',
      'title' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|resource|narrow_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_thumbnail',
        1 => 'field_endorsed_resource',
      ),
      'right' => array(
        2 => 'field_peer_rating',
        3 => 'field_learning_resource_type',
        4 => 'field_grade_level_term',
        5 => 'title',
        6 => 'field_standards_state',
        7 => 'field_standards_ccss',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'left',
      'field_endorsed_resource' => 'left',
      'field_peer_rating' => 'right',
      'field_learning_resource_type' => 'right',
      'field_grade_level_term' => 'right',
      'title' => 'right',
      'field_standards_state' => 'right',
      'field_standards_ccss' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|resource|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|resource|video_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'resource';
  $ds_layout->view_mode = 'video_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_attached_files',
        1 => 'field_endorsed_resource',
        2 => 'field_peer_rating',
        3 => 'title',
      ),
    ),
    'fields' => array(
      'field_attached_files' => 'ds_content',
      'field_endorsed_resource' => 'ds_content',
      'field_peer_rating' => 'ds_content',
      'title' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|resource|video_teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function usaedu_base_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'admin_teaser';
  $ds_view_mode->label = 'Admin teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['admin_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'author_narrow_teaser';
  $ds_view_mode->label = 'Author narrow teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['author_narrow_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'author_teaser';
  $ds_view_mode->label = 'Author teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['author_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'child_taxonomy_term_page';
  $ds_view_mode->label = 'Child taxonomy term page';
  $ds_view_mode->entities = array(
    'taxonomy_term' => 'taxonomy_term',
  );
  $export['child_taxonomy_term_page'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'mini_teaser';
  $ds_view_mode->label = 'Mini-teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'taxonomy_term' => 'taxonomy_term',
  );
  $export['mini_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'narrow_teaser';
  $ds_view_mode->label = 'Narrow teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'taxonomy_term' => 'taxonomy_term',
  );
  $export['narrow_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'video_teaser';
  $ds_view_mode->label = 'Video-teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['video_teaser'] = $ds_view_mode;

  return $export;
}
