<?php

/**
 * Evaluation form ajax callback.
 */
function usaedu_base_evaluate_resource_callback($is_dialog, $resource, $evaluation = NULL) {
  global $user;

  $evaluations_by_current_user = usaedu_base_resource_evaluation($resource, $user);
  $already_evaluated = !empty($evaluations_by_current_user);

  if ($already_evaluated && empty($evaluation)) {
    // Only show a message that the current user has already evaluated this resource.
    drupal_set_message(_usaedu_base_resource_evaluated(), 'warning');

    if ($is_dialog) {
      // Actually, no dialog in this case, only show our message.
      $commands[] = ajax_command_remove('div.messages'); // clear other/old messages
      $commands[] = ajax_command_after('nav.breadcrumb', trim(theme('status_messages'))); // display our message
      ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
    }
    else {
      // TODO: Figure out best redirect location.
      return drupal_goto("node/{$resource->nid}");
    }
  }
  else {
    $elements['form'] = usaedu_base_evaluation_form($resource, $is_dialog, $evaluation);

    if ($is_dialog) {
      // Add the standards viewer element to form.
      if ($resource) {
        $standards = _usaedu_base_resource_standards($resource);
        if (!empty($standards)) {
          $standards_viewer = drupal_get_form('usaedu_base_resource_standards_viewer', $standards);
        }
      }
      $sidebar = array(
        '#type' => 'markup',
        '#markup' => render($standards_viewer),
      );
      $dialog = array(
        '#theme' => 'usaedu_dialog_with_sidebar',
        '#title' => menu_get_active_title(),
        '#symbol' => 'usaedu-dialog-symbol-evaluate-resource',
        '#main' => $elements,
        '#sidebar' => $sidebar,
      );
      render($dialog);
    }
    else {
      return $elements;
    }
  }
}

/**
 * List of evaluation comments
 */
function usaedu_base_resource_evaluation_comments_callback($resource_node) {
  $title = $resource_node->title;
  $content = '';

  $rating = field_view_field('node', $resource_node, 'field_peer_rating', 'full');
  $endorsed_resource = field_view_field('node', $resource_node, 'field_endorsed_resource', 'full');
  $content .=  '<div class="endorsed-rating">' . render($endorsed_resource) . render($rating) . '</div>';
  $content .=  views_embed_view('evaluations', 'block_1');
  $content .=  views_embed_view('evaluations', 'page_1');
  $sidebar = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t("If you are an educator registered with the Resource Exchange you may add a suggested enhancements of your own. To do so please !rate and add details about the alignment of the resource to the applicable rubrics.", array('!rate' => l(t('rate the rubric'), "evaluate-resource/nojs/$resource_node->nid"))) . '</p>',
  );

  $dialog = array(
    '#theme' => 'usaedu_dialog_with_sidebar',
    '#title' => $title,
    '#symbol' => 'usaedu-dialog-symbol-evaluate-resource',
    '#main' => $content,
    '#sidebar' => $sidebar,
    '#options' => array (
      'dialogClass' => 'resource-suggested-enhancements',
      'hide' => FALSE,
    ),
  );
  render($dialog);
}

/**
 * Evaluation form.
 */
function usaedu_base_evaluation_form($resource, $is_dialog = FALSE, $evaluation = NULL) {
  global $user;

  $type = 'evaluation';
  
  if (!empty($evaluation)) {
    $node = $evaluation;
  }
  else {
    $node = (object) array(
      'uid' => $user->uid,
      'name' => (isset($user->name) ? $user->name : ''),
      'type' => $type,
      'language' => LANGUAGE_NONE,
    );
  }

  module_load_include('inc', 'node', 'node.pages');

  $form_id = "{$type}_node_form";
  if ($is_dialog) {
    $dialogifier = new FormDialogifierStandard($form_id, array('ajax callback' => 'usaedu_base_evaluation_form_ajax_callback'));
    usaedu_dialog_set_dialogifier($dialogifier);
  }

  $form = drupal_get_form($form_id, $node, $resource);
  $form['#prefix'] = '<p class="usaedu-dialog-intro">' .$resource->title . '</p>';
  $form['actions']['submit']['#value'] = t('Submit rating');
  $form['#attached']['js'][] = drupal_get_path('module', 'usaedu_base') . '/js/evaluation-node.js';

  // Hide unused elements.
  hide($form['title']);
  hide($form['field_resource_id']);

  // Add map legend next to the submit buttons
  $form['actions']['legend'] = array(
    '#type' => 'markup',
    '#markup' => _usaedu_base_evaluation_legend(),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Legend for the Resource evaluation form.
 */
function _usaedu_base_evaluation_legend() {
  $output = '<div id="evaluation-legend">';
  $output .=   'N/A – Does not apply to this resource<br />';
  $output .=   '1 – Strongly Disagree<br />';
  $output .=   '2 – Disagree<br />';
  $output .=   '3 – Agree<br />';
  $output .=   '4 – Strongly Agree<br />';
  $output .= '</div>';

  return $output;
}

/**
 * Resource node edit form in dialog.
 */
function usaedu_base_resource_edit_callback($is_dialog, $resource) {
  if ($resource->type !== 'resource') {
    return MENU_NOT_FOUND;
  }

  $elements['form'] = usaedu_base_resource_edit_form($resource, $is_dialog);

  if ($is_dialog) {
    // Only some placeholder text here for the time being.
    $sidebar = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t('Edit this resource. There are no comps for editing a resource in a dialog, so it is still unclear what exactly would be shown in the sidebar in that case.') . '</p>',
    );
    $dialog = array(
      '#theme' => 'usaedu_dialog_with_sidebar',
      '#title' => menu_get_active_title(),
      '#symbol' => 'usaedu-dialog-symbol-edit-resource',
      '#main' => $elements,
      '#sidebar' => $sidebar,
    );
    render($dialog);
  }
  else {
    return $elements;
  }
}

/**
 * Callback for dialog-ified resource node edit form.
 */
function usaedu_base_resource_edit_form($resource, $is_dialog = FALSE) {
  if ($is_dialog) {
    $dialogifier = new FormDialogifierStandard('resource_node_form', array('ajax callback' => 'usaedu_dialog_form_ajax_callback_redirect'));
    usaedu_dialog_set_dialogifier($dialogifier);
  }
  module_load_include('inc', 'node', 'node.pages');
  return node_page_edit($resource);
}
/**
 * Custom taxonomy term page callback. Doesn't actually render a page but
 * instead invokes a hook to allow other modules to register for the task.
 *
 * Delegation is handled on a first-come first-served basis; the first callback
 * to return a non-null value is used.
 */
function usaedu_base_taxonomy_term_callback($term) {
  $callbacks = module_invoke_all('usaedu_base_term_callback_register', $term);

  foreach ($callbacks as $callback) {
    $response = $callback($term);
    if ($response) {
      return $response;
    }
  }

  return MENU_NOT_FOUND;
}

/**
 * Callback for taxonomy terms belonging to standards vocabularies.
 * If this term has child terms, display an overview and tagged resources in Quicktabs.
 */
function _usaedu_base_standards_term_callback($term) {

  drupal_add_js(
    drupal_get_path('module', 'usaedu_base') .'/js/related-slo.js',
    array('type' => 'file', 'scope' => 'footer', 'weight' => 5)
  );
  // Elements which are displayed on both leaves and branches
  $elements['map_link'] = array('#markup' => term_map_generate_link($term));

  // TODO: This is hard-coded for the NJMC, maybe it should be configurable.
  if ($term->vocabulary_machine_name === 'curriculum') {
    $elements['nav_links'] = term_map_generate_standards_nav($term);
  }

  // Check if this term has child terms below it in the hierarchy.
  $children = taxonomy_get_children($term->tid, $term->vid);

  // If no children, this term is a leaf; display its overview and tagged resources in the page content.
  if (!count($children)) {

    // Display the Overview and Resources.
    $elements['overview'] = array(
      '#markup' => views_embed_view('standards_term', 'standards_term', $term->tid),
    );
    $elements['resources'] = array(
      '#markup' => views_embed_view('usaedu_taxonomy_term', 'resources_by_standards', $term->tid),
    );

  } else { 
    // This term has children, so render quicktabs.
    $settings = array('renderer' => 'ui_tabs', 'options' => array('history' => TRUE));

    $tabs = quicktabs_build_quicktabs('standards', $settings, array(
      array(
        'title' => 'Overview',
        'type' => 'view',
        'vid' => 'standards_term',
        'display' => 'standards_term_children',
        'args' => "%2",
        'weight' => 0,
      ),
      array(
        'title' => 'Resources',
        'type' => 'view',
        'vid' => 'usaedu_taxonomy_term',
        'display' => 'resources_by_standards',
        'args' => "%2",
        'weight' => 1,
      ),
    ));

    // Get the downstream Resources count.
    if ($count = _usaedu_base_node_count_by_tid($term->tid)) {
      // If the count is greater than zero, override the markup of the Resources tab title to show it. 
      foreach ($tabs['content']['content']['tabs']['tablinks'] as $delta => $tablink) {
        if (strip_tags($tablink['#markup']) == 'Resources') {
          $linkdelta = $delta + 1;
          $markup = '<a href="#qt-standards-ui-tabs' . $linkdelta . '" class="qt-resources"><span class="qt-count">' . $count . '</span> Resources</a>';
          $tabs['content']['content']['tabs']['tablinks'][$delta]['#markup'] = $markup;
        }
      }
    }

    // Display the Overview above the tabs
    $elements['overview'] = array(
      '#markup' => views_embed_view('standards_term', 'standards_term', $term->tid),
    );
    $elements['tabs'] = $tabs;
  }
  
  // Move sort filter
  drupal_add_js(drupal_get_path('module', 'usaedu_base') .'/js/move-sort-filter.js', 'file');
  
  return $elements;
}

/**
 * Report a resource callback
 */
function usaedu_base_resource_report_callback($resource_node) {
  $title = $resource_node->title;

  $form_id = 'usaedu_base_resource_report';
  $dialogifier = new FormDialogifierStandard($form_id, array('ajax callback' => 'usaedu_base_report_form_ajax_callback'));
  usaedu_dialog_set_dialogifier($dialogifier);

  $content['form'] = drupal_get_form($form_id, $resource_node);

  $sidebar = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t("Thank you so much for your collaboration in keeping our site healthy.") . '</p>',
  );

  $dialog = array(
    '#theme' => 'usaedu_dialog_with_sidebar',
    '#title' => $title,
    '#main' => $content,
    '#sidebar' => $sidebar,
  );
  render($dialog);
}
