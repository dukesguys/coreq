<?php
/**
 * @file
 * usaedu_base.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_base_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = 'Just the home page';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'usaedu_search-find_resources_facets' => array(
          'module' => 'usaedu_search',
          'delta' => 'find_resources_facets',
          'region' => 'header',
          'weight' => '-9',
        ),
        'views-front_page_video-block' => array(
          'module' => 'views',
          'delta' => 'front_page_video-block',
          'region' => 'content_preface',
          'weight' => '-10',
        ),
        'views-tweet_feed-block' => array(
          'module' => 'views',
          'delta' => 'tweet_feed-block',
          'region' => 'content_preface',
          'weight' => '-9',
        ),
        'views-front_page_block-block' => array(
          'module' => 'views',
          'delta' => 'front_page_block-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Just the home page');
  $export['frontpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'resource_node';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'resource' => 'resource',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-related_resources-block' => array(
          'module' => 'views',
          'delta' => 'related_resources-block',
          'region' => 'content',
          'weight' => '25',
        ),
        'usaedu_base-resource_tools' => array(
          'module' => 'usaedu_base',
          'delta' => 'resource_tools',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-resource_term_blocks-block' => array(
          'module' => 'views',
          'delta' => 'resource_term_blocks-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-resource_term_blocks-block_1' => array(
          'module' => 'views',
          'delta' => 'resource_term_blocks-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-related_resources-block_1' => array(
          'module' => 'views',
          'delta' => 'related_resources-block_1',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
    'breadcrumb' => 'resources',
    'menu' => 'resources',
  );
  $context->condition_mode = 0;
  $export['resource_node'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_wide';
  $context->description = 'Site wide reactions';
  $context->tag = 'By path';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'user-login' => array(
          'module' => 'user',
          'delta' => 'login',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'usaedu_search-find_resources_search' => array(
          'module' => 'usaedu_search',
          'delta' => 'find_resources_search',
          'region' => 'navigation',
          'weight' => '-9',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'usaedu_base-in_collaboration' => array(
          'module' => 'usaedu_base',
          'delta' => 'in_collaboration',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'footer',
          'weight' => '-9',
        ),
        'usaedu_base-copyright' => array(
          'module' => 'usaedu_base',
          'delta' => 'copyright',
          'region' => 'footer',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('By path');
  t('Site wide reactions');
  $export['site_wide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'standards_term_pages';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'ccss' => 'ccss',
        'curriculum' => 'curriculum',
        'statestand' => 'statestand',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'usaedu_tool-tools' => array(
          'module' => 'usaedu_tool',
          'delta' => 'tools',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-recent_resources-block_2' => array(
          'module' => 'views',
          'delta' => 'recent_resources-block_2',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['standards_term_pages'] = $context;

  return $export;
}
