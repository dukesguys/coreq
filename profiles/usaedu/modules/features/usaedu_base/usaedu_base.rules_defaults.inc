<?php
/**
 * @file
 * usaedu_base.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function usaedu_base_default_rules_configuration() {
  $items = array();
  $items['rules_email_managers'] = entity_import('rules_config', '{ "rules_email_managers" : {
      "LABEL" : "Email Managers",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules", "usaedu_base" ],
      "ON" : { "submit_resource_report" : [] },
      "IF" : [ { "php_eval" : { "code" : "return TRUE;" } } ],
      "DO" : [
        { "drupal_message" : { "message" : "[site:current-user] your report on \\u003Cem\\u003E[resource:title]\\u003C\\/em\\u003E has been sent." } },
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "5" : "5" } },
            "subject" : "A report on [resource:title] has been filed",
            "message" : "\\u003Ca href=\\u0022[current-user:url]\\u0022\\u003E[current-user:name]\\u003C\\/a\\u003E has filed a report on \\u003Ca href=\\u0022[resource:url]\\u0022\\u003E[resource:title]\\u003C\\/a\\u003E\\r\\n\\r\\n\\u003Ch3\\u003EReport Description\\u003C\\/h3\\u003E\\r\\n[report-body:value]"
          }
        }
      ]
    }
  }');
  $items['rules_resource_set_to_approved'] = entity_import('rules_config', '{ "rules_resource_set_to_approved" : {
      "LABEL" : "Resource set to Approved (public)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "USAEDU resource workflow" ],
      "REQUIRES" : [ "workflow_rules", "rules" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "2" : "2", "3" : "3", "4" : "4", "6" : "6" } },
            "new_state" : { "value" : { "7" : "7" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "The resource you submitted to [site:name] has been approved and published ",
            "message" : "Hi [node:author:name],\\r\\n\\r\\nWe thought we\\u0027d let you know that the resource you submitted titled \\u0022[node:title]\\u0022 has been approved and published by our moderators. \\r\\n\\r\\n[node:workflow-current-state-log-entry]\\r\\n\\r\\nIt\\u0027s now visible to all visitors to our website. You can view the resource here: [node:url]\\r\\n\\r\\n-- [site:name] team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_resource_set_to_approved_educators_only'] = entity_import('rules_config', '{ "rules_resource_set_to_approved_educators_only" : {
      "LABEL" : "Resource set to Approved (Educators only)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "USAEDU resource workflow" ],
      "REQUIRES" : [ "workflow_rules", "rules" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "2" : "2", "3" : "3", "4" : "4", "7" : "7" } },
            "new_state" : { "value" : { "6" : "6" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "The resource you submitted to [site:name] has been approved",
            "message" : "Hi [node:author:name],\\r\\n\\r\\nWe thought we\\u0027d let you know that the resource you submitted titled \\u0022\\u0022[node:title]\\u0022\\u0022 has been approved and published by our moderators. \\u00a0It\\u0027s now visible to other Educators on our website.\\r\\n[node:workflow-current-state-log-entry]\\r\\n\\r\\nYou can view the resource here: [node:url]\\u00a0\\r\\n\\r\\n(Note that it is not accessible to everyone - only to members of the site who have Educator-level accounts.)\\r\\n\\r\\n-- [site:name] team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_resource_set_to_pending'] = entity_import('rules_config', '{ "rules_resource_set_to_pending" : {
      "LABEL" : "Resource submitted by Educator",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "USAEDU resource workflow" ],
      "REQUIRES" : [ "workflow_rules", "rules" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "ANY" : "ANY" } },
            "new_state" : { "value" : { "3" : "3" } }
          }
        },
        { "user_has_role" : { "account" : [ "node:author" ], "roles" : { "value" : { "4" : "4" } } } }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Your resource has been submitted to our moderators, and can no longer be edited.  You\\u0027ll be notified by email when they\\u0027ve reviewed it." } },
        { "mail" : {
            "to" : "zoe+njdoemoderators@affinitybridge.com",
            "subject" : "A resource has been submitted for approval",
            "message" : "The following resource has been submitted for approval:\\r\\n\\r\\nAuthor: [node:author:name]\\r\\nTitle: [node:title]\\r\\nLink: [node:url]\\r\\n\\r\\n-- [site:name] team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_resource_set_to_returned'] = entity_import('rules_config', '{ "rules_resource_set_to_returned" : {
      "LABEL" : "Resource set to Returned",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "USAEDU resource workflow" ],
      "REQUIRES" : [ "workflow_rules", "rules" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "ANY" : "ANY" } },
            "new_state" : { "value" : { "4" : "4" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "The resource you submitted to [site:name] has been returned with comments",
            "message" : "Hi [node:author:name],\\r\\n\\r\\nYour resource titled \\u0022[node:title]\\u0022 has been reviewed by our moderators, and they found they can\\u0027t publish it yet. \\u00a0\\r\\n\\r\\nThey provided the following comments:\\r\\n\\u0022[node:workflow-current-state-log-entry]\\u0022\\r\\n\\r\\nHere\\u0027s a link to the resource so you can make the suggested changes and re-submit for approval: [node:url]\\r\\n\\r\\nThanks.\\r\\n\\r\\n-- [site:name] team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
