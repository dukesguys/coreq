<?php
/**
 * @file
 * usaedu_base.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function usaedu_base_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_endorsement|node|resource|form';
  $field_group->group_name = 'group_endorsement';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Endorse this Resource',
    'weight' => '7',
    'children' => array(
      0 => 'field_endorsed_resource',
      1 => 'field_endorsement_comments',
      2 => 'field_endorsement_file',
      3 => 'field_endorsement_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Endorse this Resource',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-endorsement field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_resource_form_group_endorsement',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_endorsement|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_evaluation_results|node|resource|full';
  $field_group->group_name = 'group_evaluation_results';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Evaluation results',
    'weight' => '5',
    'children' => array(
      0 => 'field_endorsement_comments',
      1 => 'field_endorsement_date',
      2 => 'evaluation_body_count',
      3 => 'group_rubric',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Evaluation results',
      'instance_settings' => array(
        'classes' => ' group-evaluation-results field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_resource_full_group_evaluation_results',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_evaluation_results|node|resource|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header|node|resource|form';
  $field_group->group_name = 'group_header';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_attached_files',
      2 => 'field_resource_url',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Header',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-header field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_resource_form_group_header',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_header|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_content|node|resource|full';
  $field_group->group_name = 'group_main_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Main Content of Resource',
    'weight' => '6',
    'children' => array(
      0 => 'body',
      1 => 'field_attached_files',
      2 => 'field_resource_url',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Main Content of Resource',
      'instance_settings' => array(
        'classes' => ' group-main-content field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_resource_full_group_main_content',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_main_content|node|resource|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mc|node|resource|full';
  $field_group->group_name = 'group_mc';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Model Curriculum',
    'weight' => '27',
    'children' => array(
      0 => 'field_standard_curriculum',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Model Curriculum',
      'instance_settings' => array(
        'classes' => ' group-mc field-group-div',
        'description' => '',
        'id' => 'node_resource_full_group_mc',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_mc|node|resource|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_media|node|resource|form';
  $field_group->group_name = 'group_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Media',
    'weight' => '1',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => ' group-media field-group-div',
        'required_fields' => 1,
        'id' => 'node_resource_form_group_media',
      ),
    ),
  );
  $export['group_media|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_peer_ratings|node|resource|form';
  $field_group->group_name = 'group_peer_ratings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Peer ratings',
    'weight' => '2',
    'children' => array(
      0 => 'field_aggregate_alignment',
      1 => 'field_aggregate_assessment',
      2 => 'field_aggregate_key_shifts',
      3 => 'field_aggregate_supports',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => ' group-peer-ratings field-group-div',
        'required_fields' => 1,
        'id' => 'node_resource_form_group_peer_ratings',
      ),
    ),
  );
  $export['group_peer_ratings|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publish_state|node|resource|form';
  $field_group->group_name = 'group_publish_state';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Publish state',
    'weight' => '9',
    'children' => array(
      0 => 'workflow',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-publish-state field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publish_state|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_info|node|resource|full';
  $field_group->group_name = 'group_resource_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Resource info',
    'weight' => '0',
    'children' => array(
      0 => 'field_endorsed_resource',
      1 => 'field_grade_level_term',
      2 => 'field_learning_resource_type',
      3 => 'field_peer_rating',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Resource info',
      'instance_settings' => array(
        'classes' => ' group-resource-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_resource_full_group_resource_info',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_resource_info|node|resource|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rttt_optional|node|resource|form';
  $field_group->group_name = 'group_rttt_optional';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Optional Tags',
    'weight' => '6',
    'children' => array(
      0 => 'field_age_range',
      1 => 'field_author_creator',
      2 => 'field_copyright_holder',
      3 => 'field_copyright_year',
      4 => 'field_date_created',
      5 => 'field_interactivity_type',
      6 => 'field_keywords',
      7 => 'field_language',
      8 => 'field_peer_rating',
      9 => 'field_revised_blooms',
      10 => 'field_educational_alignment',
      11 => 'field_is_based_on_url',
      12 => 'field_publisher_provider',
      13 => 'field_featured_portal_content',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => ' group-rttt-optional field-group-div',
        'required_fields' => 1,
        'id' => 'node_resource_form_group_rttt_optional',
      ),
    ),
  );
  $export['group_rttt_optional|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rttt_required|node|resource|form';
  $field_group->group_name = 'group_rttt_required';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Required Tags',
    'weight' => '5',
    'children' => array(
      0 => 'field_alignment_type',
      1 => 'field_digital_media_type',
      2 => 'field_educational_use',
      3 => 'field_end_user',
      4 => 'field_grade_level_term',
      5 => 'field_item_type',
      6 => 'field_learning_resource_type',
      7 => 'field_subject_areas',
      8 => 'field_alignment_type_other',
      9 => 'field_digital_media_type_other',
      10 => 'field_educational_use_other',
      11 => 'field_end_user_other',
      12 => 'field_grade_level_other',
      13 => 'field_item_type_other',
      14 => 'field_resource_type_other',
      15 => 'field_subject_area_other',
      16 => 'field_usage_rights_url',
      17 => 'field_usage_rights_url_na',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => ' group-rttt-required field-group-div',
        'required_fields' => 1,
        'id' => 'node_resource_form_group_rttt_required',
      ),
    ),
  );
  $export['group_rttt_required|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rubric|node|evaluation|form';
  $field_group->group_name = 'group_rubric';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'evaluation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Rubric',
    'weight' => '3',
    'children' => array(
      0 => 'field_rubric_alignment',
      1 => 'field_rubric_assessment',
      2 => 'field_rubric_key_shifts',
      3 => 'field_rubric_supports',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Rubric',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-rubric field-group-div',
        'description' => '<p>Possible Score Range:  1-4</p>
<ul>
<li>N/A – Does not apply to this resource</li>
<li>1 – Strongly Disagree</li>
<li>2 – Disagree</li>
<li>3 – Agree</li>
<li>4 – Strongly Agree</li>
</ul>',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_evaluation_form_group_rubric',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_rubric|node|evaluation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rubric|node|resource|full';
  $field_group->group_name = 'group_rubric';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_evaluation_results';
  $field_group->data = array(
    'label' => 'Rubric',
    'weight' => '23',
    'children' => array(
      0 => 'field_aggregate_alignment',
      1 => 'field_aggregate_assessment',
      2 => 'field_aggregate_key_shifts',
      3 => 'field_aggregate_supports',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Rubric',
      'instance_settings' => array(
        'classes' => ' group-rubric field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_resource_full_group_rubric',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_rubric|node|resource|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standards|node|resource|form';
  $field_group->group_name = 'group_standards';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Related standards',
    'weight' => '3',
    'children' => array(
      0 => 'field_standard_curriculum',
      1 => 'field_standards_ccss',
      2 => 'field_standards_state',
      3 => 'field_standards_skip',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => ' group-standards field-group-div',
        'required_fields' => 1,
        'id' => 'node_resource_form_group_standards',
      ),
    ),
  );
  $export['group_standards|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standards|node|resource|full';
  $field_group->group_name = 'group_standards';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Related Standards',
    'weight' => '28',
    'children' => array(
      0 => 'field_standards_ccss',
      1 => 'field_standards_state',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Related Standards',
      'instance_settings' => array(
        'classes' => ' group-standards field-group-div',
        'description' => '',
        'id' => 'node_resource_full_group_standards',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standards|node|resource|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tags|node|resource|full';
  $field_group->group_name = 'group_tags';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Tags:',
    'weight' => '7',
    'children' => array(
      0 => 'field_alignment_type',
      1 => 'field_digital_media_type',
      2 => 'field_educational_use',
      3 => 'field_end_user',
      4 => 'field_interactivity_type',
      5 => 'field_item_type',
      6 => 'field_keywords',
      7 => 'field_language',
      8 => 'field_revised_blooms',
      9 => 'field_subject_areas',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Tags:',
      'instance_settings' => array(
        'classes' => ' group-tags field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_resource_full_group_tags',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_tags|node|resource|full'] = $field_group;

  return $export;
}
