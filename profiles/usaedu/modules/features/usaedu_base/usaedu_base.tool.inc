<?php

function usaedu_base_tool_info() {
  return array(
//    'rate' => array(
//      'title' => t('Rate'),
//      'weight' => 100,  
//    ),

    'endorse' => array(
      'title' => t('Endorse'),
      'weight' => 400,  
    ),

    'download' => array(
      'title' => t('Download'),
      'weight' => 500,  
    ),

    'upload' => array(
      'title' => t('Upload'),
      'weight' => 600,  
    ),

    'edit' => array(
      'title' => t('Edit'),
      'weight' => 700,  
    ),

    'workflow' => array(
      'title' => t('Workflow'),
      'weight' => 800,  
    ),

    'report' => array(
      'title' => t('Report'),
      'weight' => 900,
    ),
  );
}

function usaedu_base_tool_available($tool_id, array $tool_context) {
  $function_name = '_usaedu_base_tool_available_' . $tool_id;
  if (function_exists($function_name)) {
    return $function_name($tool_context);
  }
  else {
    return FALSE;
  }
}

function _usaedu_base_tool_available_rate(array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $uid = $tool_context['uid'];
    $node_nid = $tool_context['id'];
    $user = user_load($uid);
    // TODO replace with permission check and give these roles those permissions 
    return 0 < count(array_intersect(array_values($user->roles), array('Academic Leader', 'Educator', 'Super Educator')));
  }
  return FALSE;
}

function _usaedu_base_tool_available_endorse(array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $uid = $tool_context['uid'];
    $node_nid = $tool_context['id'];
    $user = user_load($uid);
    // TODO replace with permission check and give these roles those permissions 
    return 0 < count(array_intersect(array_values($user->roles), array('Academic Leader')));
  }
  return FALSE;
}

function _usaedu_base_tool_available_download(array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $uid = $tool_context['uid'];
    $node_nid = $tool_context['id'];
    $user = user_load($uid);

    $resource_node = node_load($node_nid);

    return (
      usaedu_base_resource_has_download($resource_node)
      &&
      // TODO replace with permission check and give these roles those permissions 
      (0 < count(array_intersect(array_values($user->roles), array('Academic Leader', 'Educator', 'Super Educator', 'authenticated user'))))
    );
  }
  return FALSE;
}

function _usaedu_base_tool_available_upload(array $tool_context) {
  if (in_array($tool_context['context type'], array('node', 'view', 'term'))) {
    return user_access('create resource content');
  }
  return FALSE;
}

function _usaedu_base_tool_available_edit(array $tool_context) {
  if ('node' == $tool_context['context type']) {
    if ('resource' == $tool_context['node type'] || 'collection' == $tool_context['node type']) {
      $this_node = node_load($tool_context['id']);
      // Show "Edit" button for everyone who has access to edit. Access is configured through Workflow modules.
      if (node_access('update', $this_node)) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

function _usaedu_base_tool_available_workflow(array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $uid = $tool_context['uid'];
    $node_nid = $tool_context['id'];
    // Only for admin, Academic Leaders, and Managers, add a "Workflow" button.
    return workflow_node_tab_access(node_load($node_nid));
  }
  return FALSE;
}

function _usaedu_base_tool_available_report(array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $uid = $tool_context['uid'];
    $node_nid = $tool_context['id'];
    $user = user_load($uid);
    // TODO replace with permission check and give these roles those permissions
    return 0 < count(array_intersect(array_values($user->roles), array('authenticated user')));
  }
  return FALSE;
}


function usaedu_base_tool_view($tool_id, array $tool_context) {
  $tool = NULL;

  switch ($tool_id) {
    case 'upload':
      $tool = usaedu_base_tool_view_upload($tool_context);
      break;
    case 'download':
      $tool = usaedu_base_tool_view_download($tool_context);
      break;
    default:
      if ($tool_context['context type'] === 'node' && in_array($tool_context['node type'], array('collection', 'resource'))) {
        $node_nid = $tool_context['id'];
        $tools = usaedu_base_tool_config($node_nid);
        if (isset($tools[$tool_id]) && !$tools[$tool_id]['disabled']) {
          $tool = usaedu_tool_link($tools[$tool_id]['title'], $tools[$tool_id]['path'], $tools[$tool_id]['dialog'], $tools[$tool_id]['query'], $tools[$tool_id]['fragment']);
        }
      }
  }

  return $tool;
}

function usaedu_base_tool_view_upload(array $tool_context) {
  $query = ($tool_context['context type'] == 'term') ? array($tool_context['vocab'] => $tool_context['id']) : array();
  return usaedu_tool_link(t('Upload'), "node/add/resource", FALSE, $query);
}

function usaedu_base_tool_view_download(array $tool_context) {
  if ('node' == $tool_context['context type'] && 'resource' == $tool_context['node type']) {
    $node_nid = $tool_context['id'];
    $resource_node = node_load($node_nid);
    if (usaedu_base_resource_has_download($resource_node)) {
      $options = array('external' => TRUE, 'html' => TRUE);
      $file = $resource_node->field_attached_files[LANGUAGE_NONE][0];
      $uri = file_create_url($file['uri']);
      if (usaedu_base_resource_download_in_new_tab($file['filemime'])) {
        $options['attributes']['target'] = '_blank';
      }
      return l('<span>' . t('Download') . '</span>', $uri, $options);
    }
  }
}

function usaedu_base_tool_config($node_nid) {
  return array(
    'rate' => array(
      'disabled' => FALSE,
      'title' => t('Rate'),
      'path' => "evaluate-resource/nojs/$node_nid",
      'dialog' => TRUE,
      'query' => array(),
      'fragment' => '',
    ),

    'endorse' => array(
      'disabled' => FALSE,
      'title' => t('Endorse'),
      'path' => "node/$node_nid/edit/endorse",
      'dialog' => FALSE,
      'query' => array(),
      'fragment' => '',
    ),

    'download' => array(
      'disabled' => TRUE,
      'title' => t('Download'),
      'path' => "download-resource/nojs/$node_nid",
      'dialog' => TRUE,
      'query' => array(),
      'fragment' => '',
    ),

    'edit' => array(
      'disabled' => FALSE,
      'title' => t('Edit'),
      'path' => "node/$node_nid/edit",
      'dialog' => FALSE,
      'query' => array(),
      'fragment' => '',
    ),

    'workflow' => array(
      'disabled' => FALSE,
      'title' => t('Approve'),
      'path' => "node/$node_nid/workflow",
      'dialog' => FALSE,
      'query' => array('destination' => current_path()),
      'fragment' => '',
    ),

    'report' => array(
      'disabled' => FALSE,
      'title' => t('Something wrong?'),
      'path' => "resource/$node_nid/report",
      'dialog' => TRUE,
      'query' => array('destination' => current_path()),
      'fragment' => '',
    ),
  );
}
