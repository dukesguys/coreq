<?php
/**
 * @file
 * usaedu_base.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function usaedu_base_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer blocks'.
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(
      'Manager' => 'Manager',
    ),
    'module' => 'block',
  );

  // Exported permission: 'administer workflow'.
  $permissions['administer workflow'] = array(
    'name' => 'administer workflow',
    'roles' => array(),
    'module' => 'workflow_admin_ui',
  );

  // Exported permission: 'create resource content'.
  $permissions['create resource content'] = array(
    'name' => 'create resource content',
    'roles' => array(
      'Academic Leader' => 'Academic Leader',
      'Educator' => 'Educator',
      'Manager' => 'Manager',
      'Super Educator' => 'Super Educator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any resource content'.
  $permissions['delete any resource content'] = array(
    'name' => 'delete any resource content',
    'roles' => array(
      'Academic Leader' => 'Academic Leader',
      'Manager' => 'Manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own resource content'.
  $permissions['delete own resource content'] = array(
    'name' => 'delete own resource content',
    'roles' => array(
      'Manager' => 'Manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any resource content'.
  $permissions['edit any resource content'] = array(
    'name' => 'edit any resource content',
    'roles' => array(
      'Academic Leader' => 'Academic Leader',
      'Manager' => 'Manager',
      'Super Educator' => 'Super Educator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own resource content'.
  $permissions['edit own resource content'] = array(
    'name' => 'edit own resource content',
    'roles' => array(
      'Manager' => 'Manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'participate in workflow'.
  $permissions['participate in workflow'] = array(
    'name' => 'participate in workflow',
    'roles' => array(
      'Academic Leader' => 'Academic Leader',
      'Educator' => 'Educator',
      'Manager' => 'Manager',
      'Super Educator' => 'Super Educator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'non-authenticated' => 'non-authenticated',
    ),
    'module' => 'workflow_admin_ui',
  );

  // Exported permission: 'save resources with workflow transition'.
  $permissions['save resources with workflow transition'] = array(
    'name' => 'save resources with workflow transition',
    'roles' => array(
      'Academic Leader' => 'Academic Leader',
      'Manager' => 'Manager',
    ),
    'module' => 'usaedu_base',
  );

  // Exported permission: 'schedule workflow transitions'.
  $permissions['schedule workflow transitions'] = array(
    'name' => 'schedule workflow transitions',
    'roles' => array(),
    'module' => 'workflow',
  );

  // Exported permission: 'show workflow state form'.
  $permissions['show workflow state form'] = array(
    'name' => 'show workflow state form',
    'roles' => array(),
    'module' => 'workflow',
  );

  // Exported permission: 'submit resources for moderation'.
  $permissions['submit resources for moderation'] = array(
    'name' => 'submit resources for moderation',
    'roles' => array(
      'Educator' => 'Educator',
    ),
    'module' => 'usaedu_base',
  );

  // Exported permission: 'view resources restricted to educators'.
  $permissions['view resources restricted to educators'] = array(
    'name' => 'view resources restricted to educators',
    'roles' => array(
      'Academic Leader' => 'Academic Leader',
      'Educator' => 'Educator',
      'Manager' => 'Manager',
      'Super Educator' => 'Super Educator',
    ),
    'module' => 'usaedu_base',
  );

  return $permissions;
}
