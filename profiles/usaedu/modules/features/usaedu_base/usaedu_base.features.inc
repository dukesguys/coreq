<?php
/**
 * @file
 * usaedu_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function usaedu_base_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function usaedu_base_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function usaedu_base_image_default_styles() {
  $styles = array();

  // Exported image style: narrow_teaser.
  $styles['narrow_teaser'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 75,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'narrow_teaser',
  );

  // Exported image style: teaser.
  $styles['teaser'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 140,
          'height' => 106,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'teaser',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function usaedu_base_node_info() {
  $items = array(
    'evaluation' => array(
      'name' => t('Evaluation'),
      'base' => 'node_content',
      'description' => t('Evaluate resources according to Tri-state Rubrics.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Basic page.  Can be added to the menu.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'resource' => array(
      'name' => t('Instructional Resource'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title of Resource'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_workflow_default_workflows().
 */
function usaedu_base_workflow_default_workflows() {
  $workflows = array();

  // Exported workflow: Resource publish state
  $workflows['Resource publish state'] = array(
    'name' => 'Resource publish state',
    'tab_roles' => '5,3',
    'options' => 'a:4:{s:16:"comment_log_node";s:1:"1";s:15:"comment_log_tab";s:1:"1";s:13:"name_as_title";s:1:"0";s:12:"watchdog_log";s:1:"0";}',
    'states' => array(
      0 => array(
        'state' => '(creation)',
        'weight' => -50,
        'sysid' => 1,
        'status' => 1,
        'name' => 'Resource publish state',
      ),
      1 => array(
        'state' => 'Draft',
        'weight' => -19,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Resource publish state',
      ),
      2 => array(
        'state' => 'Pending',
        'weight' => -18,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Resource publish state',
      ),
      3 => array(
        'state' => 'Returned',
        'weight' => -17,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Resource publish state',
      ),
      4 => array(
        'state' => 'Approved (Educators only)',
        'weight' => -16,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Resource publish state',
      ),
      5 => array(
        'state' => 'Approved (public)',
        'weight' => -15,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Resource publish state',
      ),
    ),
    'transitions' => array(
      0 => array(
        'roles' => 'workflow_features_author_name,Manager,Academic Leader',
        'state' => '(creation)',
        'target_state' => 'Draft',
      ),
      1 => array(
        'roles' => 'workflow_features_author_name,Manager,Academic Leader',
        'state' => '(creation)',
        'target_state' => 'Pending',
      ),
      2 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => '(creation)',
        'target_state' => 'Returned',
      ),
      3 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => '(creation)',
        'target_state' => 'Approved (Educators only)',
      ),
      4 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => '(creation)',
        'target_state' => 'Approved (public)',
      ),
      5 => array(
        'roles' => 'workflow_features_author_name,Manager,Academic Leader',
        'state' => 'Draft',
        'target_state' => 'Pending',
      ),
      6 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Draft',
        'target_state' => 'Returned',
      ),
      7 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Draft',
        'target_state' => 'Approved (public)',
      ),
      8 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Draft',
        'target_state' => 'Approved (Educators only)',
      ),
      9 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Pending',
        'target_state' => 'Returned',
      ),
      10 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Pending',
        'target_state' => 'Draft',
      ),
      11 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Pending',
        'target_state' => 'Approved (public)',
      ),
      12 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Pending',
        'target_state' => 'Approved (Educators only)',
      ),
      13 => array(
        'roles' => 'workflow_features_author_name,Manager,Academic Leader',
        'state' => 'Returned',
        'target_state' => 'Draft',
      ),
      14 => array(
        'roles' => 'workflow_features_author_name,Manager,Academic Leader',
        'state' => 'Returned',
        'target_state' => 'Pending',
      ),
      15 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Returned',
        'target_state' => 'Approved (public)',
      ),
      16 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Returned',
        'target_state' => 'Approved (Educators only)',
      ),
      17 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (Educators only)',
        'target_state' => 'Draft',
      ),
      18 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (Educators only)',
        'target_state' => 'Pending',
      ),
      19 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (Educators only)',
        'target_state' => 'Returned',
      ),
      20 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (Educators only)',
        'target_state' => 'Approved (public)',
      ),
      21 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (public)',
        'target_state' => 'Pending',
      ),
      22 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (public)',
        'target_state' => 'Returned',
      ),
      23 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (public)',
        'target_state' => 'Draft',
      ),
      24 => array(
        'roles' => 'Manager,Academic Leader',
        'state' => 'Approved (public)',
        'target_state' => 'Approved (Educators only)',
      ),
    ),
    'node_types' => array(
      0 => 'resource',
    ),
  );

  return $workflows;
}
