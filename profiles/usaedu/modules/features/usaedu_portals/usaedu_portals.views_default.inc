<?php
/**
 * @file
 * usaedu_portals.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function usaedu_portals_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'portal_topics';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Portal Topics';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Portal Topics';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = '';
  $handler->display->display_options['fields']['tid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['tid']['separator'] = '';
  /* Field: Taxonomy term: Portal block background image */
  $handler->display->display_options['fields']['field_portal_bg_img_block']['id'] = 'field_portal_bg_img_block';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['table'] = 'field_data_field_portal_bg_img_block';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['field'] = 'field_portal_bg_img_block';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['label'] = '';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portal_bg_img_block']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Taxonomy term: Portal Short Title */
  $handler->display->display_options['fields']['field_portal_short_title']['id'] = 'field_portal_short_title';
  $handler->display->display_options['fields']['field_portal_short_title']['table'] = 'field_data_field_portal_short_title';
  $handler->display->display_options['fields']['field_portal_short_title']['field'] = 'field_portal_short_title';
  $handler->display->display_options['fields']['field_portal_short_title']['label'] = '';
  $handler->display->display_options['fields']['field_portal_short_title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_portal_short_title']['alter']['path'] = 'resources/portal/[tid]';
  $handler->display->display_options['fields']['field_portal_short_title']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Portal Short Description */
  $handler->display->display_options['fields']['field_portal_short_description']['id'] = 'field_portal_short_description';
  $handler->display->display_options['fields']['field_portal_short_description']['table'] = 'field_data_field_portal_short_description';
  $handler->display->display_options['fields']['field_portal_short_description']['field'] = 'field_portal_short_description';
  $handler->display->display_options['fields']['field_portal_short_description']['label'] = '';
  $handler->display->display_options['fields']['field_portal_short_description']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Portal page background image */
  $handler->display->display_options['fields']['field_portal_bg_img_page']['id'] = 'field_portal_bg_img_page';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['table'] = 'field_data_field_portal_bg_img_page';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['field'] = 'field_portal_bg_img_page';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['label'] = '';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portal_bg_img_page']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Term description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'portal_topics' => 'portal_topics',
  );
  /* Filter criterion: Taxonomy term: This portal is featured on the frontpage. (field_portal_frontpage_featured) */
  $handler->display->display_options['filters']['field_portal_frontpage_featured_value']['id'] = 'field_portal_frontpage_featured_value';
  $handler->display->display_options['filters']['field_portal_frontpage_featured_value']['table'] = 'field_data_field_portal_frontpage_featured';
  $handler->display->display_options['filters']['field_portal_frontpage_featured_value']['field'] = 'field_portal_frontpage_featured_value';
  $handler->display->display_options['filters']['field_portal_frontpage_featured_value']['value'] = array(
    1 => '1',
  );

  /* Display: Frontpage Block */
  $handler = $view->new_display('block', 'Frontpage Block', 'frontpage_block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Model materials just for you';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = '';
  $handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['tid']['separator'] = '';
  /* Field: Taxonomy term: Portal block background image */
  $handler->display->display_options['fields']['field_portal_bg_img_block']['id'] = 'field_portal_bg_img_block';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['table'] = 'field_data_field_portal_bg_img_block';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['field'] = 'field_portal_bg_img_block';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['label'] = '';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portal_bg_img_block']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portal_bg_img_block']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Taxonomy term: Portal Short Title */
  $handler->display->display_options['fields']['field_portal_short_title']['id'] = 'field_portal_short_title';
  $handler->display->display_options['fields']['field_portal_short_title']['table'] = 'field_data_field_portal_short_title';
  $handler->display->display_options['fields']['field_portal_short_title']['field'] = 'field_portal_short_title';
  $handler->display->display_options['fields']['field_portal_short_title']['label'] = '';
  $handler->display->display_options['fields']['field_portal_short_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_portal_short_title']['alter']['path'] = 'resources/portal/[tid]';
  $handler->display->display_options['fields']['field_portal_short_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portal_short_title']['element_default_classes'] = FALSE;
  /* Field: Taxonomy term: Portal Short Description */
  $handler->display->display_options['fields']['field_portal_short_description']['id'] = 'field_portal_short_description';
  $handler->display->display_options['fields']['field_portal_short_description']['table'] = 'field_data_field_portal_short_description';
  $handler->display->display_options['fields']['field_portal_short_description']['field'] = 'field_portal_short_description';
  $handler->display->display_options['fields']['field_portal_short_description']['label'] = '';
  $handler->display->display_options['fields']['field_portal_short_description']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_portal_short_description']['alter']['text'] = '<h3 class="portal-term-info-title">[field_portal_short_title]</h3>
<div class="portal-term-info-description">[field_portal_short_description]</div>';
  $handler->display->display_options['fields']['field_portal_short_description']['alter']['max_length'] = '220';
  $handler->display->display_options['fields']['field_portal_short_description']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_portal_short_description']['alter']['html'] = TRUE;
  $handler->display->display_options['fields']['field_portal_short_description']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_portal_short_description']['element_class'] = 'portal-term-info-description';
  $handler->display->display_options['fields']['field_portal_short_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portal_short_description']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_portal_short_description']['element_wrapper_class'] = 'portal-term-info';
  $handler->display->display_options['fields']['field_portal_short_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['block_description'] = 'Portal Topics Frontpage';

  /* Display: Portal Page Header Block */
  $handler = $view->new_display('block', 'Portal Page Header Block', 'portal_page_header_block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = '';
  $handler->display->display_options['fields']['tid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['tid']['separator'] = '';
  /* Field: Taxonomy term: Portal page background image */
  $handler->display->display_options['fields']['field_portal_bg_img_page']['id'] = 'field_portal_bg_img_page';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['table'] = 'field_data_field_portal_bg_img_page';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['field'] = 'field_portal_bg_img_page';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['label'] = '';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portal_bg_img_page']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portal_bg_img_page']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Term description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['tid']['default_argument_options']['index'] = '2';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'portal_topics' => 'portal_topics',
  );
  $handler->display->display_options['filters']['machine_name']['group'] = 1;
  $handler->display->display_options['block_description'] = 'Portal Page Header Block';
  $translatables['portal_topics'] = array(
    t('Master'),
    t('Portal Topics'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('.'),
    t('Frontpage Block'),
    t('Model materials just for you'),
    t('<h3 class="portal-term-info-title">[field_portal_short_title]</h3>
<div class="portal-term-info-description">[field_portal_short_description]</div>'),
    t('Portal Topics Frontpage'),
    t('Portal Page Header Block'),
    t('All'),
  );
  $export['portal_topics'] = $view;

  return $export;
}
