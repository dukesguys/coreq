<?php
/**
 * @file
 * user_uploads.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function user_uploads_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'user_uploards';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'user uploads';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'user uploads';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'views_bootstrap_grid_plugin_style';
  $handler->display->display_options['style_options']['columns'] = '3';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Choose a File to Upload */
  $handler->display->display_options['fields']['field_attached_files_2']['id'] = 'field_attached_files_2';
  $handler->display->display_options['fields']['field_attached_files_2']['table'] = 'field_data_field_attached_files';
  $handler->display->display_options['fields']['field_attached_files_2']['field'] = 'field_attached_files';
  $handler->display->display_options['fields']['field_attached_files_2']['label'] = '';
  $handler->display->display_options['fields']['field_attached_files_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_attached_files_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_attached_files_2']['alter']['text'] = '<i class="fa fa-file-text-o"></i>';
  $handler->display->display_options['fields']['field_attached_files_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_attached_files_2']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_attached_files_2']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_attached_files_2']['type'] = 'media_large_icon';
  /* Field: Content: Endorsed */
  $handler->display->display_options['fields']['field_endorsed_resource']['id'] = 'field_endorsed_resource';
  $handler->display->display_options['fields']['field_endorsed_resource']['table'] = 'field_data_field_endorsed_resource';
  $handler->display->display_options['fields']['field_endorsed_resource']['field'] = 'field_endorsed_resource';
  $handler->display->display_options['fields']['field_endorsed_resource']['label'] = '';
  $handler->display->display_options['fields']['field_endorsed_resource']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_endorsed_resource']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_endorsed_resource']['alter']['text'] = '<i class="fa fa-star-o"></i>';
  $handler->display->display_options['fields']['field_endorsed_resource']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_endorsed_resource']['empty'] = '
<i class="fa fa-fw"></i>
';
  $handler->display->display_options['fields']['field_endorsed_resource']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_endorsed_resource']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_endorsed_resource']['type'] = 'list_key';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '300',
  );
  /* Field: Content: Choose a File to Upload */
  $handler->display->display_options['fields']['field_attached_files']['id'] = 'field_attached_files';
  $handler->display->display_options['fields']['field_attached_files']['table'] = 'field_data_field_attached_files';
  $handler->display->display_options['fields']['field_attached_files']['field'] = 'field_attached_files';
  $handler->display->display_options['fields']['field_attached_files']['label'] = '';
  $handler->display->display_options['fields']['field_attached_files']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_attached_files']['alter']['text'] = 'Preview';
  $handler->display->display_options['fields']['field_attached_files']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_attached_files']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_attached_files']['type'] = 'usaedu_gdoc_field_embedded_doc';
  $handler->display->display_options['fields']['field_attached_files']['settings'] = array(
    'text' => 'Download [file:name]',
  );
  /* Field: Content: Choose a file to attach */
  $handler->display->display_options['fields']['field_endorsement_file']['id'] = 'field_endorsement_file';
  $handler->display->display_options['fields']['field_endorsement_file']['table'] = 'field_data_field_endorsement_file';
  $handler->display->display_options['fields']['field_endorsement_file']['field'] = 'field_endorsement_file';
  $handler->display->display_options['fields']['field_endorsement_file']['label'] = '';
  $handler->display->display_options['fields']['field_endorsement_file']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_endorsement_file']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_endorsement_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_endorsement_file']['type'] = 'file_download_link';
  $handler->display->display_options['fields']['field_endorsement_file']['settings'] = array(
    'text' => 'Download',
  );
  /* Field: download link */
  $handler->display->display_options['fields']['field_attached_files_1']['id'] = 'field_attached_files_1';
  $handler->display->display_options['fields']['field_attached_files_1']['table'] = 'field_data_field_attached_files';
  $handler->display->display_options['fields']['field_attached_files_1']['field'] = 'field_attached_files';
  $handler->display->display_options['fields']['field_attached_files_1']['ui_name'] = 'download link';
  $handler->display->display_options['fields']['field_attached_files_1']['label'] = '';
  $handler->display->display_options['fields']['field_attached_files_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_attached_files_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_attached_files_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_attached_files_1']['type'] = 'file_url_plain';
  $handler->display->display_options['fields']['field_attached_files_1']['settings'] = array(
    'text' => 'Download',
  );
  /* Field: Display */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Display';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="col-sm-12">
<div class="col-sm-6">
[field_attached_files_2]
</div>
<div class="col-sm-6">
<div class="pull-right">
[field_endorsed_resource]
</div>
</div>
</div>
<div class="col-sm-12">
[title]
</div>
<div class="col-sm-12">
[body]
</div>

<div class="col-sm-6">
[field_attached_files]
[field_endorsement_file] 
</div>
<div class="col-sm-6">
<div class="pull-right">
 <a href=[field_attached_files_1] target="_blank">Download</a>

</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Content: Peer rating */
  $handler->display->display_options['fields']['field_peer_rating']['id'] = 'field_peer_rating';
  $handler->display->display_options['fields']['field_peer_rating']['table'] = 'field_data_field_peer_rating';
  $handler->display->display_options['fields']['field_peer_rating']['field'] = 'field_peer_rating';
  $handler->display->display_options['fields']['field_peer_rating']['label'] = '';
  $handler->display->display_options['fields']['field_peer_rating']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_peer_rating']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Author/Creator (field_author_creator) */
  $handler->display->display_options['arguments']['field_author_creator_value']['id'] = 'field_author_creator_value';
  $handler->display->display_options['arguments']['field_author_creator_value']['table'] = 'field_data_field_author_creator';
  $handler->display->display_options['arguments']['field_author_creator_value']['field'] = 'field_author_creator_value';
  $handler->display->display_options['arguments']['field_author_creator_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['field_author_creator_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_author_creator_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_author_creator_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_author_creator_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_author_creator_value']['limit'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'resource' => 'resource',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'user-uploads';
  $translatables['user_uploards'] = array(
    t('Master'),
    t('user uploads'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('<i class="fa fa-file-text-o"></i>'),
    t('<i class="fa fa-star-o"></i>'),
    t('
<i class="fa fa-fw"></i>
'),
    t('Preview'),
    t('<div class="col-sm-12">
<div class="col-sm-6">
[field_attached_files_2]
</div>
<div class="col-sm-6">
<div class="pull-right">
[field_endorsed_resource]
</div>
</div>
</div>
<div class="col-sm-12">
[title]
</div>
<div class="col-sm-12">
[body]
</div>

<div class="col-sm-6">
[field_attached_files]
[field_endorsement_file] 
</div>
<div class="col-sm-6">
<div class="pull-right">
 <a href=[field_attached_files_1] target="_blank">Download</a>

</div>
</div>'),
    t('All'),
    t('Page'),
  );
  $export['user_uploards'] = $view;

  return $export;
}
