<?php
/**
 * @file
 * front_page_blocks.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function front_page_blocks_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'front_page_block';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Front Page Block';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Front Block Link */
  $handler->display->display_options['fields']['field_front_block_link']['id'] = 'field_front_block_link';
  $handler->display->display_options['fields']['field_front_block_link']['table'] = 'field_data_field_front_block_link';
  $handler->display->display_options['fields']['field_front_block_link']['field'] = 'field_front_block_link';
  $handler->display->display_options['fields']['field_front_block_link']['label'] = '';
  $handler->display->display_options['fields']['field_front_block_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_front_block_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_front_block_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_front_block_link']['type'] = 'link_plain';
  /* Field: Content: First Block Background */
  $handler->display->display_options['fields']['field_first_block_background']['id'] = 'field_first_block_background';
  $handler->display->display_options['fields']['field_first_block_background']['table'] = 'field_data_field_first_block_background';
  $handler->display->display_options['fields']['field_first_block_background']['field'] = 'field_first_block_background';
  $handler->display->display_options['fields']['field_first_block_background']['label'] = '';
  $handler->display->display_options['fields']['field_first_block_background']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_block_background']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_first_block_background']['alter']['text'] = '[field_first_block_background]';
  $handler->display->display_options['fields']['field_first_block_background']['alter']['path'] = '[field_front_block_link]';
  $handler->display->display_options['fields']['field_first_block_background']['alter']['link_class'] = 'box-link';
  $handler->display->display_options['fields']['field_first_block_background']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_first_block_background']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_first_block_background']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_first_block_background']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: First Block Icon Class */
  $handler->display->display_options['fields']['field_first_block_icon_class']['id'] = 'field_first_block_icon_class';
  $handler->display->display_options['fields']['field_first_block_icon_class']['table'] = 'field_data_field_first_block_icon_class';
  $handler->display->display_options['fields']['field_first_block_icon_class']['field'] = 'field_first_block_icon_class';
  $handler->display->display_options['fields']['field_first_block_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_first_block_icon_class']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_block_icon_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_first_block_icon_class']['type'] = 'text_plain';
  /* Field: Content: First Block Title */
  $handler->display->display_options['fields']['field_first_block_title']['id'] = 'field_first_block_title';
  $handler->display->display_options['fields']['field_first_block_title']['table'] = 'field_data_field_first_block_title';
  $handler->display->display_options['fields']['field_first_block_title']['field'] = 'field_first_block_title';
  $handler->display->display_options['fields']['field_first_block_title']['label'] = '';
  $handler->display->display_options['fields']['field_first_block_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_block_title']['element_label_colon'] = FALSE;
  /* Field: Content: Second Block Link */
  $handler->display->display_options['fields']['field_second_block_link']['id'] = 'field_second_block_link';
  $handler->display->display_options['fields']['field_second_block_link']['table'] = 'field_data_field_second_block_link';
  $handler->display->display_options['fields']['field_second_block_link']['field'] = 'field_second_block_link';
  $handler->display->display_options['fields']['field_second_block_link']['label'] = '';
  $handler->display->display_options['fields']['field_second_block_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_second_block_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_second_block_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_second_block_link']['type'] = 'link_plain';
  /* Field: Content: Second Block Background */
  $handler->display->display_options['fields']['field_second_block_backgroun']['id'] = 'field_second_block_backgroun';
  $handler->display->display_options['fields']['field_second_block_backgroun']['table'] = 'field_data_field_second_block_backgroun';
  $handler->display->display_options['fields']['field_second_block_backgroun']['field'] = 'field_second_block_backgroun';
  $handler->display->display_options['fields']['field_second_block_backgroun']['label'] = '';
  $handler->display->display_options['fields']['field_second_block_backgroun']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_second_block_backgroun']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_second_block_backgroun']['alter']['text'] = '[field_second_block_backgroun]';
  $handler->display->display_options['fields']['field_second_block_backgroun']['alter']['path'] = '[field_second_block_link] ';
  $handler->display->display_options['fields']['field_second_block_backgroun']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_second_block_backgroun']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_second_block_backgroun']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Second Block Icon Class */
  $handler->display->display_options['fields']['field_second_block_icon_class']['id'] = 'field_second_block_icon_class';
  $handler->display->display_options['fields']['field_second_block_icon_class']['table'] = 'field_data_field_second_block_icon_class';
  $handler->display->display_options['fields']['field_second_block_icon_class']['field'] = 'field_second_block_icon_class';
  $handler->display->display_options['fields']['field_second_block_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_second_block_icon_class']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_second_block_icon_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_second_block_icon_class']['type'] = 'text_plain';
  /* Field: Content: Second Block Title */
  $handler->display->display_options['fields']['field_second_block_title']['id'] = 'field_second_block_title';
  $handler->display->display_options['fields']['field_second_block_title']['table'] = 'field_data_field_second_block_title';
  $handler->display->display_options['fields']['field_second_block_title']['field'] = 'field_second_block_title';
  $handler->display->display_options['fields']['field_second_block_title']['label'] = '';
  $handler->display->display_options['fields']['field_second_block_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_second_block_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_second_block_title']['type'] = 'text_plain';
  /* Field: Content: Third Block Link */
  $handler->display->display_options['fields']['field_third_block_link']['id'] = 'field_third_block_link';
  $handler->display->display_options['fields']['field_third_block_link']['table'] = 'field_data_field_third_block_link';
  $handler->display->display_options['fields']['field_third_block_link']['field'] = 'field_third_block_link';
  $handler->display->display_options['fields']['field_third_block_link']['label'] = '';
  $handler->display->display_options['fields']['field_third_block_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_third_block_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_third_block_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_third_block_link']['type'] = 'link_plain';
  /* Field: Content: Third Block Background */
  $handler->display->display_options['fields']['field_third_block_background']['id'] = 'field_third_block_background';
  $handler->display->display_options['fields']['field_third_block_background']['table'] = 'field_data_field_third_block_background';
  $handler->display->display_options['fields']['field_third_block_background']['field'] = 'field_third_block_background';
  $handler->display->display_options['fields']['field_third_block_background']['label'] = '';
  $handler->display->display_options['fields']['field_third_block_background']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_third_block_background']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_third_block_background']['alter']['text'] = '[field_third_block_background] ';
  $handler->display->display_options['fields']['field_third_block_background']['alter']['path'] = '[field_third_block_link]';
  $handler->display->display_options['fields']['field_third_block_background']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_third_block_background']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_third_block_background']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Third Block Icon Class */
  $handler->display->display_options['fields']['field_third_block_icon_class']['id'] = 'field_third_block_icon_class';
  $handler->display->display_options['fields']['field_third_block_icon_class']['table'] = 'field_data_field_third_block_icon_class';
  $handler->display->display_options['fields']['field_third_block_icon_class']['field'] = 'field_third_block_icon_class';
  $handler->display->display_options['fields']['field_third_block_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_third_block_icon_class']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_third_block_icon_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_third_block_icon_class']['type'] = 'text_plain';
  /* Field: Content: Third Block Title */
  $handler->display->display_options['fields']['field_third_block_title']['id'] = 'field_third_block_title';
  $handler->display->display_options['fields']['field_third_block_title']['table'] = 'field_data_field_third_block_title';
  $handler->display->display_options['fields']['field_third_block_title']['field'] = 'field_third_block_title';
  $handler->display->display_options['fields']['field_third_block_title']['label'] = '';
  $handler->display->display_options['fields']['field_third_block_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_third_block_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_third_block_title']['type'] = 'text_plain';
  /* Field: Display */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Display';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class ="col-sm-12">
[body]
</div>
<div class ="col-sm-12">
<div class ="col-sm-4">
     <a href="[field_front_block_link]" class="box-link">
          [field_first_block_background] 
							<div class="front-block-text">
								<i class=[field_first_block_icon_class] ></i>
								<div class="stile-1">for</div>
								<h2>[field_first_block_title]</h2>
							</div>
     </a>
</div>
<div class ="col-sm-4">
     <a href="[field_second_block_link]"  class="box-link">
          [field_second_block_backgroun] 
							<div class="front-block-text">
								<i class=[field_second_block_icon_class] ></i>
								<div class="stile-1">for</div>
								<h2>[field_second_block_title]</h2>
							</div>
     </a>
</div>
<div class ="col-sm-4">
     <a href="[field_third_block_link]"  class="box-link">
          [field_third_block_background] 
							<div class="front-block-text">
								<i class=[field_third_block_icon_class] ></i>
								<div class="stile-1">for</div>
								<h2>[field_third_block_title]</h2>
							</div>
	</a>
</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'front_page_blocks' => 'front_page_blocks',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['front_page_block'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('[field_first_block_background]'),
    t('[field_second_block_backgroun]'),
    t('[field_third_block_background] '),
    t('<div class ="col-sm-12">
[body]
</div>
<div class ="col-sm-12">
<div class ="col-sm-4">
     <a href="[field_front_block_link]" class="box-link">
          [field_first_block_background] 
							<div class="front-block-text">
								<i class=[field_first_block_icon_class] ></i>
								<div class="stile-1">for</div>
								<h2>[field_first_block_title]</h2>
							</div>
     </a>
</div>
<div class ="col-sm-4">
     <a href="[field_second_block_link]"  class="box-link">
          [field_second_block_backgroun] 
							<div class="front-block-text">
								<i class=[field_second_block_icon_class] ></i>
								<div class="stile-1">for</div>
								<h2>[field_second_block_title]</h2>
							</div>
     </a>
</div>
<div class ="col-sm-4">
     <a href="[field_third_block_link]"  class="box-link">
          [field_third_block_background] 
							<div class="front-block-text">
								<i class=[field_third_block_icon_class] ></i>
								<div class="stile-1">for</div>
								<h2>[field_third_block_title]</h2>
							</div>
	</a>
</div>
</div>'),
    t('Block'),
  );
  $export['front_page_block'] = $view;

  $view = new view();
  $view->name = 'front_page_video';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'file_managed';
  $view->human_name = 'Front Page Video';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Homepage Video';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Poster Image';
  $handler->display->display_options['header']['area']['content'] = '<?php
                      $file_url = \'\';
                      $file_fid = theme_get_setting(\'homepage_poster\');
                      if (!empty($file_fid)) {
                        $file_loaded = file_load($file_fid);
                        if ($file_loaded !== false) {
                          if (!empty($file_loaded->uri)) {
                            $file_url_raw = file_create_url($file_loaded->uri);
                            if ($file_url_raw !== false) {
                              $file_url = $file_url_raw;
                            }
                          }
                        }
                      } else {
                    $file_url_raw = drupal_get_path(\'theme\', $GLOBALS[\'theme\']).\'/files/video-poster.png\';
                }
?>

<div id="videoPoster" class="hidden" data-url="<?php echo $file_url_raw; ?>">
</div>';
  $handler->display->display_options['header']['area']['format'] = 'php_code';
  /* Sort criterion: File: Upload date */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'file_managed';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
  /* Filter criterion: File: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'file_managed';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );
  /* Filter criterion: File: Path */
  $handler->display->display_options['filters']['uri']['id'] = 'uri';
  $handler->display->display_options['filters']['uri']['table'] = 'file_managed';
  $handler->display->display_options['filters']['uri']['field'] = 'uri';
  $handler->display->display_options['filters']['uri']['operator'] = 'contains';
  $handler->display->display_options['filters']['uri']['value'] = 'homepage_video';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['front_page_video'] = array(
    t('Master'),
    t('Homepage Video'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Poster Image'),
    t('<?php
                      $file_url = \'\';
                      $file_fid = theme_get_setting(\'homepage_poster\');
                      if (!empty($file_fid)) {
                        $file_loaded = file_load($file_fid);
                        if ($file_loaded !== false) {
                          if (!empty($file_loaded->uri)) {
                            $file_url_raw = file_create_url($file_loaded->uri);
                            if ($file_url_raw !== false) {
                              $file_url = $file_url_raw;
                            }
                          }
                        }
                      } else {
                    $file_url_raw = drupal_get_path(\'theme\', $GLOBALS[\'theme\']).\'/files/video-poster.png\';
                }
?>

<div id="videoPoster" class="hidden" data-url="<?php echo $file_url_raw; ?>">
</div>'),
    t('Block'),
  );
  $export['front_page_video'] = $view;

  return $export;
}
