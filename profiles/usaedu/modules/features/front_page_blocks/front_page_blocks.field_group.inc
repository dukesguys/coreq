<?php
/**
 * @file
 * front_page_blocks.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function front_page_blocks_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left_block|node|front_page_blocks|form';
  $field_group->group_name = 'group_left_block';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'front_page_blocks';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Left Block',
    'weight' => '3',
    'children' => array(
      0 => 'field_first_block_title',
      1 => 'field_first_block_icon_class',
      2 => 'field_first_block_background',
      3 => 'field_front_block_link',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Left Block',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_left_block|node|front_page_blocks|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_middle_block|node|front_page_blocks|form';
  $field_group->group_name = 'group_middle_block';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'front_page_blocks';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Middle Block',
    'weight' => '4',
    'children' => array(
      0 => 'field_second_block_title',
      1 => 'field_second_block_icon_class',
      2 => 'field_second_block_backgroun',
      3 => 'field_second_block_link',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_middle_block|node|front_page_blocks|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right_block|node|front_page_blocks|form';
  $field_group->group_name = 'group_right_block';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'front_page_blocks';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Right Block',
    'weight' => '5',
    'children' => array(
      0 => 'field_third_block_title',
      1 => 'field_third_block_icon_class',
      2 => 'field_third_block_background',
      3 => 'field_third_block_link',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_right_block|node|front_page_blocks|form'] = $field_group;

  return $export;
}
