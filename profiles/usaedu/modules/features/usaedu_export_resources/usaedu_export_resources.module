<?php
/**
 * @file
 * Code for the USA EDU Export Resources feature.
 */

include_once 'usaedu_export_resources.features.inc';

/**
 * Implements hook_theme_registry_alter().
 */
function usaedu_export_resources_theme_registry_alter(&$theme_registry) {
  $theme_registry['views_view_field']['preprocess functions'][] = 'usaedu_export_resources_theme_views_view_field';
}

/**
 * Custom preprocess function for view fields.
 * @see usaedu_export_resources_theme_registry_alter()
 */
function usaedu_export_resources_theme_views_view_field(&$variables) {
  if ('export_pearson_schoolnet' == $variables['view']->name) {
    $variables['output'] = usaedu_export_resources_alter_pearson_schoolnet_fields($variables['output'], $variables['field']->options['id']);
  }
}

function usaedu_export_resources_alter_pearson_schoolnet_fields($output, $field_id) {
  if ('nothing' == $field_id) { // Academic Benchmark GUID "metadata/standards/guids/guid"
    return usaedu_export_resources_map_academic_benchmark_guids($output);
  }
  if ('nothing_2' == $field_id) { // Academic Benchmark GUID dummies "metadata/standards/guids/guid"
    return usaedu_export_resources_trim_bar($output);
  }
  if ('field_restricted_to_educators' == $field_id) {
    return usaedu_export_resources_format_student_facing($output);
  }
  if ('field_subject_areas' == $field_id) {
    return usaedu_export_resources_map_subjects_string($output);
  }
  if (in_array($field_id, array('field_grade_level_term_1', 'field_grade_level_term_2'))) {
    return usaedu_export_resources_map_single_grade($output);
  }
  if ('field_publisher_provider' == $field_id) {
    return usaedu_export_resources_provider_default($output);
  }
  return $output;
}

function usaedu_export_resources_trim_bar($string) {
  return trim($string, "| \t\n\r\0\x0B");
}

function usaedu_export_resources_format_boolean($bool_as_int) {
  if ('0' == $bool_as_int) {
    return 'False';
  }
  if ('1' == $bool_as_int) {
    return 'True';
  }
  return $bool_as_int;
}

function usaedu_export_resources_format_student_facing($is_restricted) {
  if ('0' == $is_restricted) {
    return 'true';
  }
  if ('1' == $is_restricted) {
    return 'false';
  }
  return $is_restricted;
}

function usaedu_export_resources_map_subjects_string($string) {
  $subjects = explode('|', $string);
  array_walk($subjects, 'usaedu_export_resources_map_single_subject');
  return implode(' | ', $subjects);
}

function usaedu_export_resources_map_single_subject(& $subject) {
  $subject = trim($subject);
  $map = array(
    // From Subjects_RTTT_to_Pearson_RL.xlsx provided by Pearson
    'English Language Arts' => 'English Language and Literature',
    'Mathematics' => 'Mathematics',
    'Science' => 'Life and Physical Sciences',
    'Social Studies' => 'Social Sciences and History',
    'Arts' => 'Fine and Performing Arts',
    'World Languages' => 'Foreign Language and Literature',
    'Health' => 'Health and Safety Education',
    'Physical Education' => 'Physical Education',
    'Agriculture Education' => 'Agriculture and Renewable Natural Resources',
    'Agriculture, Food, and Natural Resources' => 'Agriculture and Renewable Natural Resources',
    'Architecture and Construction' => 'Construction Trades | Drafting',
    'Business and Marketing' => 'Business | Marketing',
    'Business, Finance and Information Technology Education' => 'Business | Computer and Information Sciences',
    'Career Development' => 'General',
    'Communications and Audio/Visual Technology' => 'Mass Communication | Graphic and Printing Communication',
    'Computer and Information Sciences' => 'Computer and Information Sciences',
    'Education' => 'General',
    'Engineering and Technology' => 'Industrial/Technology Education',
    'Entrepreneurship' => 'Business',
    'Family and Consumer Sciences Education' => 'Consumer and Homemaking Education',
    'Health Care Sciences' => 'Health Care Sciences',
    'Hospitality and Tourism' => 'Business | Marketing',
    'Personal Services' => 'General',
    'Library Science' => 'General',
    'Manufacturing' => 'Industrial/Technology Education',
    'Marketing and Entrepreneurship Education' => 'Business | Marketing',
    'Mechanical Operation' => 'Industrial/Technology Education',
    'Military Science' => 'Military Science',
    'Public, Protective, and Government Services' => 'Public, Protective, and Social Services',
    'Religious Education and Theology' => 'Religious Education and Theology',
    'Technology Engineering and Design Education' => 'Industrial/Technology Education',
    'Trade and Industrial Education' => 'Industrial/Technology Education',
    'Transportation, Distribution and Logistics' => 'Energy, Power, and Transportation Technologies',
    'Other Career/Vocational' => 'Vocational Home Economics',
    'Other' => 'General',

    // From http://njcore.org/admin/structure/taxonomy/subject
    // Some subjects with commas in their names got scrambled during site launch, now we have to cater for those taxonomy term names as well.
    // @todo Fix names of these taxonomy terms.
    'Agriculture' /* 'Agriculture, Food, and Natural Resources' */ => 'Agriculture and Renewable Natural Resources',
    'Business' /* 'Business, Finance and Information Technology Education' */ => 'Business | Computer and Information Sciences',
    'Public' /* 'Public, Protective, and Government Services' */ => 'Public, Protective, and Social Services',
    'Transportation' /* 'Transportation, Distribution and Logistics' */ => 'Energy, Power, and Transportation Technologies',
  );
  if (isset($map[$subject])) {
    $subject = $map[$subject];
  }
  // No return value, but change argument by reference because this function is used with array_walk()
}

function usaedu_export_resources_map_single_grade($grade) {
  $map = array(
    'K' => 'KG',

    '1' => '01',
    '2' => '02',
    '3' => '03',
    '4' => '04',
    '5' => '05',
    '6' => '06',
    '7' => '07',
    '8' => '08',
    '9' => '09',

    // As per Evan Linhardt's email from Tue, Sep 23, 2014 at 6:11 PM PDT
    'Algebra I' => '09',
    'Geometry' => '10',
    'Algebra II' => '11',
  );
  return isset($map[$grade]) ? $map[$grade] : $grade;
}

function usaedu_export_resources_map_academic_benchmark_guids($string) {
  $tids = explode('|', $string);
  array_walk($tids, 'usaedu_export_resources_map_single_academic_benchmark_guid');
  $tids = array_filter($tids);
  return implode(' | ', $tids);
}

function usaedu_export_resources_map_single_academic_benchmark_guid(& $tid) {
  $tid = usaedu_export_resources_trim_bar($tid);

  $guid = db_select('field_data_field_academic_benchmark_guid', 'guids')
    ->fields('guids', array('field_academic_benchmark_guid_value'))
    ->condition('entity_id', $tid,'=')
    ->condition('deleted', 0,'=')
    ->condition('entity_type', 'taxonomy_term','=')
    ->execute()
    ->fetchField();

  // No return value, but change argument by reference because this function is used with array_walk()
  // Replace unfound codes with FALSE so that they can be removed with array_filter()
  $tid = (FALSE !== $guid) ? usaedu_export_resources_hyphenize_guid($guid) : FALSE;
}

function usaedu_export_resources_hyphenize_guid($guid) {
  $matches = array();
  if (preg_match('{^([[:xdigit:]]{8})-?([[:xdigit:]]{4})-?([[:xdigit:]]{4})-?([[:xdigit:]]{4})-?([[:xdigit:]]{12})$}', $guid, $matches)) {
    return $matches[1] . '-' . $matches[2] . '-' . $matches[3] . '-' . $matches[4] . '-' . $matches[5];
  }
  return $guid;
}

function usaedu_export_resources_fingerprint($text) {
  return trim(preg_replace('{[^0-9a-zA-Z]+}', ' ', $text));
  // return trim(substr(preg_replace('{[^0-9a-zA-Z]+}', ' ', $text), 0, 100));
}

function usaedu_export_resources_provider_default($output) {
  $output = trim($output);
  return ('' == $output) ? t('General') : $output;
}

/**
 * Implements hook_url_inbound_alter().
 */
function usaedu_export_resources_url_inbound_alter(&$path, $original_path, $path_language) {
  if (preg_match('{^resource/by-uuid/(.*)?}', $path, $matches)) {
    $path = 'uuid/node/' . $matches[1];
  }
}

/**
 * Implements hook_date_formats().
 */
function usaedu_export_resources_date_formats() {
  return array(
    array(
      'type' => 'year_only',
      'format' => 'Y',
      'locales' => array(),
    ),
  );
}

/**
 * Implements hook_date_format_types().
 */
function usaedu_export_resources_date_format_types() {
  // Define the core date format types.
  return array(
    'year_only' => t('Year Only'),
  );
}

/**
 * Implementation of hook_theme().
 */
// function views_data_export_json_theme() {
//   // Make sure that views picks up the preprocess functions.
//   module_load_include('inc', 'usaedu_export_resources', 'theme/usaedu_export_resources.theme');
//   return array();
// }
