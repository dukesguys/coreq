<?php

class UsaeduExportResources_XmlReader {
  // callable usaedu_export_resources_save_mapping(string $standardCode, string $guid);
  // @see http://www.php.net/manual/en/language.types.callable.php
  protected $mappingHandler = NULL;

  // encapsulate xml_parser_create() PHP resource
  protected $xmlParser = NULL;

  // state during stream parsing.
  protected $guid = '';
  protected $standardCodes = array();
  protected $standardDescription = '';
  protected $characterData = '';

  public function __construct($mappingHandler) {
    $this->mappingHandler = $mappingHandler;
    $this->xmlParser = xml_parser_create();
    xml_parser_set_option($this->xmlParser, XML_OPTION_CASE_FOLDING, FALSE);
    xml_set_character_data_handler($this->xmlParser, array($this, 'handleCharacterData'));
    xml_set_element_handler($this->xmlParser, array($this, 'handleElementStart'), array($this, 'handleElementEnd'));
  }
  
  public function __destruct() {
    $this->end();
    xml_parser_free($this->xmlParser);
  }

  public function parse($chunk, $is_final = FALSE) {
    $success = xml_parse($this->xmlParser, $chunk, $is_final);
    if (0 === $success) {
      $error_code = xml_get_error_code($this->xmlParser);
      $error_message = xml_error_string($error_code);
      drush_log(dt('XML Parser Error @error_code: @error_message', array('@error_code' => $error_code, '@error_message' => $error_message)), 'error');
    }
  }

  public function end() {
    $this->parse('', TRUE);
  }

  /**
   * @see xml_set_element_handler()
   */
  public function handleElementStart($parser, $name, array $attribs) {
    switch ($name) {
      case 'LearningStandardItem':
        return $this->handleLearningStandardItemStart($attribs);
        break;
      case 'StatementCode':
        return $this->handleStatementCodeStart($attribs);
        break;
      case 'Statement':
        return $this->handleStatementStart($attribs);
        break;
    }
  }

  /**
   * @see xml_set_element_handler()
   */
  public function handleElementEnd($parser, $name) {
    switch ($name) {
      case 'LearningStandardItem':
        return $this->handleLearningStandardItemEnd();
        break;
      case 'StatementCode':
        return $this->handleStatementCodeEnd();
        break;
      case 'Statement':
        return $this->handleStatementEnd();
        break;
    }
  }

  /**
   * @see xml_set_character_data_handler()
   */
  public function handleCharacterData($parser, $data) {
    $this->characterData .= $data;
  }

  public function handleLearningStandardItemStart(array $attribs) {
    $this->guid = $attribs['RefId'];
    $this->standardCodes = array();
    $this->standardDescription = '';
  }

  public function handleLearningStandardItemEnd() {
    call_user_func_array($this->mappingHandler, array($this->guid, $this->standardDescription, reset($this->standardCodes)));
    $this->guid = '';
    $this->standardCodes = array();
    $this->standardDescription = '';
  }

  public function handleStatementCodeStart(array $attribs) {
    $this->characterData = '';
  }

  public function handleStatementCodeEnd() {
    $this->standardCodes[] = $this->characterData;
  }

  public function handleStatementStart(array $attribs) {
    $this->characterData = '';
  }

  public function handleStatementEnd() {
    $this->standardDescription .= $this->characterData;
  }
}
