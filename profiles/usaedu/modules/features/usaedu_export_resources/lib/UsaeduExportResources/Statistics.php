<?php

class UsaeduExportResources_Statistics {
  public $total = NULL;
  public $nids = NULL;
  public function __construct() {
    $this->total = new UsaeduExportResources_Statistics_Counter();
    $this->nids = new UsaeduExportResources_Statistics_Set();
  }

  public function output() {
    $this->total->output('total');
    $this->nids->output('referencing_nids');
  }

  public function handle($bucketname = '', array $nids) {
    $this->total->inc($bucketname);
    $this->nids->add($bucketname, $nids);
  }
}
