<?php

class UsaeduExportResources_Statistics_Counter {
  protected $buckets = array();

  public function inc($bucketname = '', $step = 1) {
    $this->buckets[$bucketname] = isset($this->buckets[$bucketname]) ? $this->buckets[$bucketname]+$step : $step;
  }

  public function output($heading = '') {
    var_dump($heading, $this->buckets);
  }
}
