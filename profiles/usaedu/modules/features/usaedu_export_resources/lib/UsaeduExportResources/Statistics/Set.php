<?php

class UsaeduExportResources_Statistics_Set {
  protected $buckets = array();

  public function add($bucketname = '', array $new_members) {
    if (isset($this->buckets[$bucketname])) {
      $this->buckets[$bucketname] = array_merge($this->buckets[$bucketname], $new_members);
    }
    else {
      $this->buckets[$bucketname] = $new_members;
    }
  }

  public function output($heading = '') {
    $bucket_counts = array();
    foreach ($this->buckets as $bucketname => $bucket) {
      $bucket_counts[$bucketname] = count(array_unique($bucket));
    }
    var_dump($heading, $bucket_counts);
  }
}
