<?php
/**
 * @file
 * usaedu_export_resources.features.inc
 */

/**
 * Implements hook_views_api().
 */
function usaedu_export_resources_views_api() {
  return array("version" => "3.0");
}
