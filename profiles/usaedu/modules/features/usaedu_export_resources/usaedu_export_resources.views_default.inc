<?php
/**
 * @file
 * usaedu_export_resources.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function usaedu_export_resources_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'export_pearson_schoolnet';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Export for Pearson Schoolnet';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Export for Pearson Schoolnet';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer nodes';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_keywords' => 'field_keywords',
    'field_author_creator' => 'field_author_creator',
    'field_endorsement_file' => 'field_endorsement_file',
    'field_standards_ccss' => 'field_standards_ccss',
    'body' => 'body',
    'field_digital_media_type' => 'field_digital_media_type',
    'field_item_type' => 'field_item_type',
    'field_learning_resource_type' => 'field_learning_resource_type',
    'field_standard_curriculum' => 'field_standard_curriculum',
    'nid' => 'nid',
    'field_standards_state' => 'field_standards_state',
    'field_resource_url' => 'field_resource_url',
    'field_publisher_provider' => 'field_publisher_provider',
    'field_restricted_to_educators' => 'field_restricted_to_educators',
    'field_usage_rights_url' => 'field_usage_rights_url',
    'field_usage_rights_url_na' => 'field_usage_rights_url_na',
    'field_grade_level_term' => 'field_grade_level_term',
    'field_subject_areas' => 'field_subject_areas',
  );
  $handler->display->display_options['style_options']['default'] = 'nid';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_keywords' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_author_creator' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_endorsement_file' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_standards_ccss' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_digital_media_type' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_item_type' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_learning_resource_type' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_standard_curriculum' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_standards_state' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_resource_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_publisher_provider' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_restricted_to_educators' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_usage_rights_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_usage_rights_url_na' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_grade_level_term' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_subject_areas' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No resources found.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Content: Node UUID */
  $handler->display->display_options['fields']['uuid']['id'] = 'uuid';
  $handler->display->display_options['fields']['uuid']['table'] = 'node';
  $handler->display->display_options['fields']['uuid']['field'] = 'uuid';
  $handler->display->display_options['fields']['uuid']['label'] = 'metadata/providerid';
  $handler->display->display_options['fields']['uuid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'metadata/lom/general/title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'metadata/lom/general/description';
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Keywords */
  $handler->display->display_options['fields']['field_keywords']['id'] = 'field_keywords';
  $handler->display->display_options['fields']['field_keywords']['table'] = 'field_data_field_keywords';
  $handler->display->display_options['fields']['field_keywords']['field'] = 'field_keywords';
  $handler->display->display_options['fields']['field_keywords']['label'] = 'metadata/lom/general/keyword';
  $handler->display->display_options['fields']['field_keywords']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_keywords']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_keywords']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_keywords']['delta_offset'] = '0';
  /* Field: Field: Grades */
  $handler->display->display_options['fields']['field_grade_level_term_1']['id'] = 'field_grade_level_term_1';
  $handler->display->display_options['fields']['field_grade_level_term_1']['table'] = 'field_data_field_grade_level_term';
  $handler->display->display_options['fields']['field_grade_level_term_1']['field'] = 'field_grade_level_term';
  $handler->display->display_options['fields']['field_grade_level_term_1']['label'] = 'metadata/grades/from';
  $handler->display->display_options['fields']['field_grade_level_term_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_grade_level_term_1']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_grade_level_term_1']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_grade_level_term_1']['delta_offset'] = '0';
  /* Field: Field: Grades */
  $handler->display->display_options['fields']['field_grade_level_term_2']['id'] = 'field_grade_level_term_2';
  $handler->display->display_options['fields']['field_grade_level_term_2']['table'] = 'field_data_field_grade_level_term';
  $handler->display->display_options['fields']['field_grade_level_term_2']['field'] = 'field_grade_level_term';
  $handler->display->display_options['fields']['field_grade_level_term_2']['label'] = 'metadata/grades/to';
  $handler->display->display_options['fields']['field_grade_level_term_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_grade_level_term_2']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_grade_level_term_2']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_grade_level_term_2']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_grade_level_term_2']['delta_reversed'] = TRUE;
  /* Field: Field: Subjects */
  $handler->display->display_options['fields']['field_subject_areas']['id'] = 'field_subject_areas';
  $handler->display->display_options['fields']['field_subject_areas']['table'] = 'field_data_field_subject_areas';
  $handler->display->display_options['fields']['field_subject_areas']['field'] = 'field_subject_areas';
  $handler->display->display_options['fields']['field_subject_areas']['label'] = 'metadata/subject/text';
  $handler->display->display_options['fields']['field_subject_areas']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_subject_areas']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_subject_areas']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_subject_areas']['separator'] = ' | ';
  /* Field: Content: Common Core Standards */
  $handler->display->display_options['fields']['field_standards_ccss']['id'] = 'field_standards_ccss';
  $handler->display->display_options['fields']['field_standards_ccss']['table'] = 'field_data_field_standards_ccss';
  $handler->display->display_options['fields']['field_standards_ccss']['field'] = 'field_standards_ccss';
  $handler->display->display_options['fields']['field_standards_ccss']['label'] = '';
  $handler->display->display_options['fields']['field_standards_ccss']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_standards_ccss']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_standards_ccss']['alter']['text'] = '[field_standards_ccss-tid]';
  $handler->display->display_options['fields']['field_standards_ccss']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_standards_ccss']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_standards_ccss']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_standards_ccss']['separator'] = ' | ';
  /* Field: Content: New Jersey Model Curriculum */
  $handler->display->display_options['fields']['field_standard_curriculum']['id'] = 'field_standard_curriculum';
  $handler->display->display_options['fields']['field_standard_curriculum']['table'] = 'field_data_field_standard_curriculum';
  $handler->display->display_options['fields']['field_standard_curriculum']['field'] = 'field_standard_curriculum';
  $handler->display->display_options['fields']['field_standard_curriculum']['label'] = '';
  $handler->display->display_options['fields']['field_standard_curriculum']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_standard_curriculum']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_standard_curriculum']['alter']['text'] = '[field_standard_curriculum-tid]';
  $handler->display->display_options['fields']['field_standard_curriculum']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_standard_curriculum']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_standard_curriculum']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_standard_curriculum']['separator'] = ' | ';
  /* Field: Academic Benchmark GUID */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Academic Benchmark GUID';
  $handler->display->display_options['fields']['nothing']['label'] = 'metadata/standards/guids/guid';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_standards_ccss] | [field_standard_curriculum]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: [temp. disabled] Academic Benchmark GUID dummies */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['ui_name'] = '[temp. disabled] Academic Benchmark GUID dummies';
  $handler->display->display_options['fields']['nothing_2']['label'] = 'metadata/standards/guids/guid';
  $handler->display->display_options['fields']['nothing_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = '6A59A66C6EC011DFAB2D366B9DFF4B22 | 6A5A1ADE6EC011DFAB2D366B9DFF4B22';
  $handler->display->display_options['fields']['nothing_2']['element_label_colon'] = FALSE;
  /* Field: Content: Node UUID */
  $handler->display->display_options['fields']['uuid_1']['id'] = 'uuid_1';
  $handler->display->display_options['fields']['uuid_1']['table'] = 'node';
  $handler->display->display_options['fields']['uuid_1']['field'] = 'uuid';
  $handler->display->display_options['fields']['uuid_1']['label'] = 'metadata/attachments/urls/uuid';
  $handler->display->display_options['fields']['uuid_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['uuid_1']['alter']['text'] = 'http://njcore.org/resource/by-uuid/[uuid_1]';
  $handler->display->display_options['fields']['uuid_1']['alter']['path'] = 'resource/by-uuid/[uuid_1]';
  $handler->display->display_options['fields']['uuid_1']['element_label_colon'] = FALSE;
  /* Field: Content: Copyright year */
  $handler->display->display_options['fields']['field_copyright_year']['id'] = 'field_copyright_year';
  $handler->display->display_options['fields']['field_copyright_year']['table'] = 'field_data_field_copyright_year';
  $handler->display->display_options['fields']['field_copyright_year']['field'] = 'field_copyright_year';
  $handler->display->display_options['fields']['field_copyright_year']['label'] = 'metadata/lom/rights/copyright_date';
  $handler->display->display_options['fields']['field_copyright_year']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_copyright_year']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_copyright_year']['settings'] = array(
    'format_type' => 'year_only',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Publisher / Provider */
  $handler->display->display_options['fields']['field_publisher_provider']['id'] = 'field_publisher_provider';
  $handler->display->display_options['fields']['field_publisher_provider']['table'] = 'field_data_field_publisher_provider';
  $handler->display->display_options['fields']['field_publisher_provider']['field'] = 'field_publisher_provider';
  $handler->display->display_options['fields']['field_publisher_provider']['label'] = 'metadata/lom/rights/publisher';
  $handler->display->display_options['fields']['field_publisher_provider']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'metadata/providerproducts/providerproduct';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'USAEdu\\NJCore';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  /* Field: Content: Restricted to Educators */
  $handler->display->display_options['fields']['field_restricted_to_educators']['id'] = 'field_restricted_to_educators';
  $handler->display->display_options['fields']['field_restricted_to_educators']['table'] = 'field_data_field_restricted_to_educators';
  $handler->display->display_options['fields']['field_restricted_to_educators']['field'] = 'field_restricted_to_educators';
  $handler->display->display_options['fields']['field_restricted_to_educators']['label'] = 'metadata/student_facing';
  $handler->display->display_options['fields']['field_restricted_to_educators']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_restricted_to_educators']['type'] = 'list_key';
  /* Field: Content: Digital media type */
  $handler->display->display_options['fields']['field_digital_media_type']['id'] = 'field_digital_media_type';
  $handler->display->display_options['fields']['field_digital_media_type']['table'] = 'field_data_field_digital_media_type';
  $handler->display->display_options['fields']['field_digital_media_type']['field'] = 'field_digital_media_type';
  $handler->display->display_options['fields']['field_digital_media_type']['label'] = 'metadata/resource_format_type';
  $handler->display->display_options['fields']['field_digital_media_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_digital_media_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_digital_media_type']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_digital_media_type']['separator'] = ' | ';
  /* Sort criterion: Content: Nid */
  $handler->display->display_options['sorts']['nid']['id'] = 'nid';
  $handler->display->display_options['sorts']['nid']['table'] = 'node';
  $handler->display->display_options['sorts']['nid']['field'] = 'nid';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'resource' => 'resource',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Math or ELA */
  $handler->display->display_options['filters']['field_subject_areas_tid']['id'] = 'field_subject_areas_tid';
  $handler->display->display_options['filters']['field_subject_areas_tid']['table'] = 'field_data_field_subject_areas';
  $handler->display->display_options['filters']['field_subject_areas_tid']['field'] = 'field_subject_areas_tid';
  $handler->display->display_options['filters']['field_subject_areas_tid']['ui_name'] = 'Math or ELA';
  $handler->display->display_options['filters']['field_subject_areas_tid']['value'] = array(
    74 => '74',
    75 => '75',
  );
  $handler->display->display_options['filters']['field_subject_areas_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['field_subject_areas_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_subject_areas_tid']['vocabulary'] = 'subject';
  $handler->display->display_options['filters']['field_subject_areas_tid']['hierarchy'] = 1;
  /* Filter criterion: Non-empty title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['ui_name'] = 'Non-empty title';
  $handler->display->display_options['filters']['title']['operator'] = 'regular_expression';
  $handler->display->display_options['filters']['title']['value'] = '[^\\s]';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Hint to export button';
  $handler->display->display_options['header']['area']['content'] = '<p style="margin-bottom:2em;">To export a CSV file, please click the small orange "CSV" button in the lower left corner of this page.</p>
';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['path'] = 'admin/content/export-pearson-schoolnet';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Resource Export Pearson Schoolnet';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'data_export');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['style_options']['provide_file'] = 1;
  $handler->display->display_options['style_options']['filename'] = 'njcore-export-for-pearson-schoolnet-%timestamp-full.csv';
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['style_options']['quote'] = 1;
  $handler->display->display_options['style_options']['trim'] = 1;
  $handler->display->display_options['style_options']['replace_newlines'] = 0;
  $handler->display->display_options['style_options']['header'] = 1;
  $handler->display->display_options['style_options']['keep_html'] = 0;
  $handler->display->display_options['style_options']['encoding'] = 'Windows-1252';
  $handler->display->display_options['path'] = 'admin/content/export-pearson-schoolnet/csv';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $handler->display->display_options['use_batch'] = 'batch';
  $translatables['export_pearson_schoolnet'] = array(
    t('Master'),
    t('Export for Pearson Schoolnet'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('No resources found.'),
    t('metadata/providerid'),
    t('metadata/lom/general/title'),
    t('metadata/lom/general/description'),
    t('metadata/lom/general/keyword'),
    t('metadata/grades/from'),
    t('metadata/grades/to'),
    t('metadata/subject/text'),
    t('[field_standards_ccss-tid]'),
    t('[field_standard_curriculum-tid]'),
    t('metadata/standards/guids/guid'),
    t('[field_standards_ccss] | [field_standard_curriculum]'),
    t('6A59A66C6EC011DFAB2D366B9DFF4B22 | 6A5A1ADE6EC011DFAB2D366B9DFF4B22'),
    t('metadata/attachments/urls/uuid'),
    t('http://njcore.org/resource/by-uuid/[uuid_1]'),
    t('metadata/lom/rights/copyright_date'),
    t('metadata/lom/rights/publisher'),
    t('metadata/providerproducts/providerproduct'),
    t('USAEdu\NJCore'),
    t('metadata/student_facing'),
    t('metadata/resource_format_type'),
    t('Page'),
    t('Hint to export button'),
    t('<p style="margin-bottom:2em;">To export a CSV file, please click the small orange "CSV" button in the lower left corner of this page.</p>
'),
    t('Data export'),
  );
  $export['export_pearson_schoolnet'] = $view;

  return $export;
}
