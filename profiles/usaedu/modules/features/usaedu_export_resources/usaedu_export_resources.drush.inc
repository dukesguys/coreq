<?php

// Don't include these classes via .info file because they are only needed for this drush file.
include_once dirname(__FILE__) . '/lib/UsaeduExportResources/XmlReader.php';
include_once dirname(__FILE__) . '/lib/UsaeduExportResources/Statistics/Counter.php';
include_once dirname(__FILE__) . '/lib/UsaeduExportResources/Statistics/Set.php';
include_once dirname(__FILE__) . '/lib/UsaeduExportResources/Statistics.php';

/**
 * Implements hook_drush_command().
 */
function usaedu_export_resources_drush_command() {
  $items = array();
  $items['usaedu-export-resources-prepare-academic-benchmark-mapping'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Extract a "Standards Codes to Academic Benchmark GUID" mapping from XML files.'),
  );
  $items['usaedu-export-resources-map-to-taxonomy'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Extract a "Standards Codes to Academic Benchmark GUID" mapping from XML files.'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function usaedu_export_resources_drush_help($section) {
  switch ($section) {
    case 'drush:usaedu-export-resources-prepare-academic-benchmark-mapping':
      return dt('Prepare exporting resources into Pearson Schoolnet: Extract a "Standards Codes to Academic Benchmark GUID" mapping from XML files.');
  }
}

function drush_usaedu_export_resources_prepare_academic_benchmark_mapping() {
  drush_log(dt('Start reading Academic Benchmark XML files.'), 'status');
  $xml_dirname = dirname(__FILE__) . '/academic_benchmark_guids';
  drush_log(dt('Read from directory "@dir".', array('@dir' => $xml_dirname)), 'status');
  $files = file_scan_directory($xml_dirname, '/\.xml/i');
  foreach ($files as $file) {
    $filename = $file->uri;
    $short_filename = $file->filename;
    drush_log(dt('Reading Academic Benchmark XML file "@file".', array('@file' => $short_filename)), 'status');
    usaedu_export_resources_read_xml($filename);
  }
  drush_log(dt('Done reading Academic Benchmark XML files.'), 'status');
}

function usaedu_export_resources_read_xml($filename) {
  $file_content = file_get_contents($filename);
  $reader = new UsaeduExportResources_XmlReader('usaedu_export_resources_save_mapping');
  // TODO make this streaming
  $reader->parse($file_content);
  $reader->end();
}

/**
 * callback for UsaeduExportResources_XmlReader
 */
function usaedu_export_resources_save_mapping($guid, $standardDescription, $standardCode) {
  $countQuery = db_select('usaedu_export_resources_academic_benchmark_map', 'map')
    ->fields('map')
    ->condition('academic_benchmark_guid', $guid,'=')
    ->countQuery();
  $count = (int) $countQuery->execute()->fetchField();

  if (0 < $count) {
    drush_log(dt('Already in DB: @guid', array('@guid' => $guid)), 'warning');
    return;
  }
  
  db_insert('usaedu_export_resources_academic_benchmark_map')
    ->fields(array(
      'academic_benchmark_guid' => $guid,
      'standard_description' => usaedu_export_resources_fingerprint($standardDescription),
      'standard_code' => $standardCode,
    ))
    ->execute();
}

function drush_usaedu_export_resources_map_to_taxonomy() {
  drush_log(dt('Start mapping Academic Benchmark to NJDOE taxonomy data.'), 'status');
  $tids = usaedu_export_resources_get_referenced_tids();

  $statistics = new UsaeduExportResources_Statistics();
  $statistics->total->inc('referenced_terms', count($tids));

  $done = 0;
  $total = count($tids);
  foreach ($tids as $tid) {
    usaedu_export_resources_match_ab_guid_with_tid($tid, $statistics);
    if (0 === (++$done % 10)) {
      drush_log(dt('Processed @done out of @total taxonomy terms.', array('@done' => $done, '@total' => $total)), 'status');
    }
  }

  $statistics->output();

  drush_log(dt('Done mapping Academic Benchmark to NJDOE taxonomy data.'), 'status');
}

function usaedu_export_resources_match_ab_guid_with_tid($tid, & $statistics) {
  $term = taxonomy_term_load($tid);
  $nids = usaedu_export_resources_get_referencing_resource_nids($tid);

  $success = usaedu_export_resources_match_via_fingerprint($term, $nids, $statistics);

  if (! $success) {
    $success = usaedu_export_resources_match_via_code($term, $nids, $statistics);
  }
}

function usaedu_export_resources_match_via_fingerprint($term, array $nids, & $statistics) {
  $term_fingerprint = usaedu_export_resources_fingerprint($term->description);
  $guids = usaedu_export_resources_find_ab_guids_by_fingerprint($term_fingerprint);
  if (1 === count($guids)) {
    $statistics->handle('found/fingerprint', $nids);
    usaedu_export_resources_save_guid_to_term($term, reset($guids));
    return TRUE;
  }
  else if (0 === count($guids)) {
    $statistics->handle('not_found/fingerprint', $nids);
    return FALSE;
  }
  else if (1 < count($guids)) {
    $statistics->handle('found_multiple/fingerprint', $nids);
    return FALSE;
  }
}

function usaedu_export_resources_match_via_code($term, array $nids, & $statistics) {
  $term_code = $term->name;
  $guids = usaedu_export_resources_find_ab_guids_by_code($term_code);
  if (1 === count($guids)) {
    $statistics->handle('found/code', $nids);
    usaedu_export_resources_save_guid_to_term($term, reset($guids));
    return TRUE;
  }
  else if (0 === count($guids)) {
    $statistics->handle('not_found/code', $nids);
    return FALSE;
  }
  else if (1 < count($guids)) {
    $statistics->handle('found_multiple/code', $nids);
    return FALSE;
  }
}

function usaedu_export_resources_save_guid_to_term($term, $guid) {
  $guids_already_on_this_term = field_get_items('taxonomy_term', $term, 'field_academic_benchmark_guid');
  if ((FALSE === $guids_already_on_this_term) || (0 === count($guids_already_on_this_term))) {
    $guid = usaedu_export_resources_hyphenize_guid($guid);
    $term->field_academic_benchmark_guid = array(LANGUAGE_NONE => array(array('value' => $guid)));
    taxonomy_term_save($term);
  }
}

function usaedu_export_resources_get_referenced_tids() {
  return array_unique(array_merge(
    usaedu_export_resources_get_referenced_ccss_tids(),
    usaedu_export_resources_get_referenced_curriculum_tids()
  ));
}

function usaedu_export_resources_get_referenced_ccss_tids() {
  $sql = "
    SELECT DISTINCT
      field_standards_ccss_tid AS tid
    FROM {field_data_field_standards_ccss} AS ccss
    WHERE
      entity_type = 'node'
      AND
      bundle = 'resource'
      AND
      deleted = 0
  ";
  return db_query($sql)->fetchCol();
}

function usaedu_export_resources_get_referenced_curriculum_tids() {
  $sql = "
    SELECT DISTINCT
      field_standard_curriculum_tid AS tid
    FROM {field_data_field_standard_curriculum} AS curriculum
    WHERE
      entity_type = 'node'
      AND
      bundle = 'resource'
      AND
      deleted = 0
  ";
  return db_query($sql)->fetchCol();
}

function usaedu_export_resources_find_ab_guids_by_fingerprint($fingerprint) {
  $sql = "
    SELECT
      academic_benchmark_guid AS guid
    FROM {usaedu_export_resources_academic_benchmark_map} AS map
    WHERE standard_description = :fingerprint
  ";
  return db_query($sql, array(':fingerprint' => $fingerprint))->fetchCol();
}

function usaedu_export_resources_find_ab_guids_by_code($code) {
  $sql = "
    SELECT
      academic_benchmark_guid AS guid
    FROM {usaedu_export_resources_academic_benchmark_map} AS map
    WHERE standard_code = :code
  ";
  return db_query($sql, array(':code' => $code))->fetchCol();
}

function usaedu_export_resources_get_referencing_resource_nids($tid) {
  return array_unique(array_merge(
    usaedu_export_resources_get_referencing_resource_nids_ccss($tid),
    usaedu_export_resources_get_referencing_resource_nids_curriculum($tid)
  ));
}

function usaedu_export_resources_get_referencing_resource_nids_ccss($tid) {
  $sql = "
    SELECT DISTINCT
      entity_id AS nid
    FROM {field_data_field_standards_ccss} AS ccss
    WHERE
      field_standards_ccss_tid = :tid
      AND
      entity_type = 'node'
      AND
      bundle = 'resource'
      AND
      deleted = 0
  ";
  return db_query($sql, array(':tid' => $tid))->fetchCol();
}

function usaedu_export_resources_get_referencing_resource_nids_curriculum($tid) {
  $sql = "
    SELECT DISTINCT
      entity_id AS nid
    FROM {field_data_field_standard_curriculum} AS curriculum
    WHERE
      field_standard_curriculum_tid = :tid
      AND
      entity_type = 'node'
      AND
      bundle = 'resource'
      AND
      deleted = 0
  ";
  return db_query($sql, array(':tid' => $tid))->fetchCol();
}

