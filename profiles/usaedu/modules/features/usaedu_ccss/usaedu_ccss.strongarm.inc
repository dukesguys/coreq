<?php
/**
 * @file
 * usaedu_ccss.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function usaedu_ccss_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_taxonomy_term__ccss';
  $strongarm->value = array(
    'view_modes' => array(
      'mini_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'child_taxonomy_term_page' => array(
        'custom_settings' => TRUE,
      ),
      'narrow_teaser' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'path' => array(
          'weight' => '9',
        ),
        'name' => array(
          'weight' => '0',
        ),
        'description' => array(
          'weight' => '1',
        ),
      ),
      'display' => array(
        'description' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'mini_teaser' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'child_taxonomy_term_page' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'usaedu_term_downstream_node_count' => array(
          'child_taxonomy_term_page' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
          'mini_teaser' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_taxonomy_term__ccss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_ccss_pattern';
  $strongarm->value = 'ccss/[term:name]';
  $export['pathauto_taxonomy_term_ccss_pattern'] = $strongarm;

  return $export;
}
