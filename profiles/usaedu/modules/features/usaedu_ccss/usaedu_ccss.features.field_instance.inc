<?php
/**
 * @file
 * usaedu_ccss.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function usaedu_ccss_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-ccss-field_academic_benchmark_guid'
  $field_instances['taxonomy_term-ccss-field_academic_benchmark_guid'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The Academic Bechmark GUIDs which correspond to this standard.',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 10,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_academic_benchmark_guid',
    'label' => 'Academic Benchmark GUID',
    'placeholder' => '',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'taxonomy_term-ccss-field_ccss_domain'
  $field_instances['taxonomy_term-ccss-field_ccss_domain'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A domain is a group of related standards. Standards from different domains may sometimes be closely related.',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_ccss_domain',
    'label' => 'Domain (Mathematics) / Strand (ELA)',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 0,
      ),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-ccss-field_grade_level'
  $field_instances['taxonomy_term-ccss-field_grade_level'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select all grades relevant to this item.',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_grade_level',
    'label' => 'Grade level',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'taxonomy_term-ccss-field_source_url'
  $field_instances['taxonomy_term-ccss-field_source_url'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_source_url',
    'label' => 'Source URL',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => FALSE,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 0,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'html5_tools',
      'settings' => array(),
      'type' => 'urlwidget',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'taxonomy_term-ccss-field_standard_short_code'
  $field_instances['taxonomy_term-ccss-field_standard_short_code'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_standard_short_code',
    'label' => 'Standard short code',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-ccss-field_standards_cluster'
  $field_instances['taxonomy_term-ccss-field_standards_cluster'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_standards_cluster',
    'label' => 'Cluster',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'taxonomy_term-ccss-field_term_page_content'
  $field_instances['taxonomy_term-ccss-field_term_page_content'] = array(
    'bundle' => 'ccss',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'child_taxonomy_term_page' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_page_content',
    'label' => 'Term Page Content',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A domain is a group of related standards. Standards from different domains may sometimes be closely related.');
  t('Academic Benchmark GUID');
  t('Cluster');
  t('Domain (Mathematics) / Strand (ELA)');
  t('Grade level');
  t('Select all grades relevant to this item.');
  t('Source URL');
  t('Standard short code');
  t('Term Page Content');
  t('The Academic Bechmark GUIDs which correspond to this standard.');

  return $field_instances;
}
