<?php
/**
 * @file
 * usaedu_ccss.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_ccss_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'ccss_term_page';
  $context->description = 'Term page or map page in CCSS standard';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'standards/ccss/*' => 'standards/ccss/*',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'ccss' => 'ccss',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'standards/ccss',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Term page or map page in CCSS standard');
  $export['ccss_term_page'] = $context;

  return $export;
}
