<?php
/**
 * @file
 * usaedu_ccss.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function usaedu_ccss_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_academic_benchmark_guid'
  $field_bases['field_academic_benchmark_guid'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_academic_benchmark_guid',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 36,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_ccss_domain'
  $field_bases['field_ccss_domain'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ccss_domain',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Counting & Cardinality' => 'Domain: Counting & Cardinality',
        'Operations & Algebraic Thinking' => 'Domain: Operations & Algebraic Thinking',
        'Number & Operations in Base Ten' => 'Domain: Number & Operations in Base Ten',
        'Number & Operations—Fractions' => 'Domain: Number & Operations—Fractions',
        'Measurement & Data' => 'Domain: Measurement & Data',
        'Geometry' => 'Domain: Geometry',
        'Ratios & Proportional Relationships' => 'Domain: Ratios & Proportional Relationships',
        'The Number System' => 'Domain: The Number System',
        'Expressions & Equations' => 'Domain: Expressions & Equations',
        'Functions' => 'Domain: Functions',
        'Statistics & Probability' => 'Domain: Statistics & Probability',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_level'
  $field_bases['field_grade_level'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_level',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'P' => 'P',
        'K' => 'K',
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
        12 => 12,
        'Novice-Mid' => 'Novice-Mid',
        'Novice-High' => 'Novice-High',
        'Intermediate-Low' => 'Intermediate-Low',
        'Intermediate-Mid' => 'Intermediate-Mid',
        'Intermediate-High' => 'Intermediate-High',
        'Advanced-Low' => 'Advanced-Low',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_source_url'
  $field_bases['field_source_url'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_source_url',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_standard_short_code'
  $field_bases['field_standard_short_code'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_standard_short_code',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 128,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_standards_cluster'
  $field_bases['field_standards_cluster'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_standards_cluster',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'standards_cluster',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_term_page_content'
  $field_bases['field_term_page_content'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_term_page_content',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
