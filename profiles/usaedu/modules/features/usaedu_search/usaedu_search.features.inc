<?php
/**
 * @file
 * usaedu_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function usaedu_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "current_search" && $api == "current_search") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function usaedu_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_autocomplete_search().
 */
function usaedu_search_default_search_api_autocomplete_search() {
  $items = array();
  $items['search_api_views_resources_search'] = entity_import('search_api_autocomplete_search', '{
    "machine_name" : "search_api_views_resources_search",
    "name" : "Resources search",
    "index_id" : "resources_search",
    "type" : "search_api_views",
    "enabled" : "1",
    "options" : { "result count" : true, "fields" : [] },
    "rdf_mapping" : []
  }');
  $items['search_api_views_standards_search'] = entity_import('search_api_autocomplete_search', '{
    "machine_name" : "search_api_views_standards_search",
    "name" : "Standards search",
    "index_id" : "standards",
    "type" : "search_api_views",
    "enabled" : "1",
    "options" : { "result count" : true, "fields" : [] },
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function usaedu_search_default_search_api_index() {
  $items = array();
  $items['resources_search'] = entity_import('search_api_index', '{
    "name" : "Resources search",
    "machine_name" : "resources_search",
    "description" : null,
    "server" : "nj_usaedu_solr",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_alignment_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_alignment_type_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_author_creator" : { "type" : "text" },
        "field_copyright_holder" : { "type" : "text" },
        "field_digital_media_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_digital_media_type_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_educational_use" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_educational_use_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_end_user" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_end_user_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_endorsed_resource" : { "type" : "boolean" },
        "field_featured_portal_content" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_grade_level_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_grade_level_term" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_interactivity_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_item_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_item_type_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_keywords" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_language" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_learning_resource_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_peer_rating" : { "type" : "decimal" },
        "field_resource_type_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_revised_blooms" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_standard_curriculum" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_standards_ccss" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_standards_state" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_subject_area_other" : { "type" : "list\\u003Ctext\\u003E" },
        "field_subject_areas" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_attachments_alter_settings" : {
          "status" : 1,
          "weight" : "0",
          "settings" : {
            "excluded_extensions" : "aif art avi bmp gif ico mov oga ogv png psd ra ram rgb",
            "excluded_private" : 0
          }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "body:value" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['standards'] = entity_import('search_api_index', '{
    "name" : "Standards",
    "machine_name" : "standards",
    "description" : null,
    "server" : "nj_usaedu_solr",
    "item_type" : "taxonomy_term",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "description" : { "type" : "text", "boost" : "5.0" },
        "name" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "vocabulary" : { "type" : "integer", "entity_type" : "taxonomy_vocabulary" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : {
            "default" : "0",
            "bundles" : {
              "ccss" : "ccss",
              "curriculum" : "curriculum",
              "statestand" : "statestand"
            }
          }
        },
        "search_api_attachments_alter_settings" : {
          "status" : 0,
          "weight" : "0",
          "settings" : {
            "excluded_extensions" : "aif art avi bmp gif ico mov oga ogv png psd ra ram rgb",
            "excluded_private" : 0
          }
        },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "parents_all:parent" : "parents_all:parent" } }
        },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "name" : true, "description" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "description" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "name" : true, "description" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "name" : true, "description" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function usaedu_search_default_search_api_server() {
  $items = array();
  $items['nj_usaedu_solr'] = entity_import('search_api_server', '{
    "name" : "NJ USAEDU",
    "machine_name" : "nj_usaedu_solr",
    "description" : "[found Solr config in settings.php] ",
    "class" : "search_api_solr_service",
    "options" : {
      "scheme" : "http",
      "host" : "localhost",
      "port" : "8080",
      "path" : "\\/solr3\\/njcore",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_sort().
 */
function usaedu_search_default_search_api_sort() {
  $items = array();
  $items['resources_search__changed'] = entity_import('search_api_sort', '{
    "index_id" : "resources_search",
    "field" : "changed",
    "name" : "Date changed",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "resources_search__changed",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Date changed" },
    "rdf_mapping" : []
  }');
  $items['resources_search__created'] = entity_import('search_api_sort', '{
    "index_id" : "resources_search",
    "field" : "created",
    "name" : "Most recently added",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "resources_search__created",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Date created" },
    "rdf_mapping" : []
  }');
  $items['resources_search__field_endorsed_resource'] = entity_import('search_api_sort', '{
    "index_id" : "resources_search",
    "field" : "field_endorsed_resource",
    "name" : "Endorsed resource",
    "enabled" : "0",
    "weight" : "-100",
    "identifier" : "resources_search__field_endorsed_resource",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Endorsed" },
    "rdf_mapping" : []
  }');
  $items['resources_search__field_peer_rating'] = entity_import('search_api_sort', '{
    "index_id" : "resources_search",
    "field" : "field_peer_rating",
    "name" : "Evaluation score",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "resources_search__field_peer_rating",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Peer rating" },
    "rdf_mapping" : []
  }');
  $items['resources_search__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "resources_search",
    "field" : "search_api_relevance",
    "name" : "Relevance",
    "enabled" : "0",
    "weight" : "-99",
    "identifier" : "resources_search__search_api_relevance",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" },
    "rdf_mapping" : []
  }');
  return $items;
}
