<?php
/**
 * @file
 * usaedu_search.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function usaedu_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use search_api_autocomplete for search_api_views_resources_search'.
  $permissions['use search_api_autocomplete for search_api_views_resources_search'] = array(
    'name' => 'use search_api_autocomplete for search_api_views_resources_search',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'non-authenticated' => 'non-authenticated',
    ),
    'module' => 'search_api_autocomplete',
  );

  // Exported permission: 'use search_api_autocomplete for search_api_views_standards_search'.
  $permissions['use search_api_autocomplete for search_api_views_standards_search'] = array(
    'name' => 'use search_api_autocomplete for search_api_views_standards_search',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'non-authenticated' => 'non-authenticated',
    ),
    'module' => 'search_api_autocomplete',
  );

  return $permissions;
}
