<?php
/**
 * @file
 * usaedu_search.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function usaedu_search_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi:block_cache:search_api@resources_search';
  $strongarm->value = -1;
  $export['facetapi:block_cache:search_api@resources_search'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi_pretty_paths_searcher_search_api@resources_search';
  $strongarm->value = 1;
  $export['facetapi_pretty_paths_searcher_search_api@resources_search'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi_pretty_paths_searcher_search_api@resources_search_options';
  $strongarm->value = array(
    'sort_path_segments' => 0,
  );
  $export['facetapi_pretty_paths_searcher_search_api@resources_search_options'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_alignment_type';
  $strongarm->value = 'alignment_type';
  $export['usaedu_pretty_paths_alias_alignment_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_curriculum';
  $strongarm->value = 'unit';
  $export['usaedu_pretty_paths_alias_curriculum'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_digital_media_type';
  $strongarm->value = 'media_type';
  $export['usaedu_pretty_paths_alias_digital_media_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_educational_use';
  $strongarm->value = 'educational_use';
  $export['usaedu_pretty_paths_alias_educational_use'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_end_user';
  $strongarm->value = 'end_user';
  $export['usaedu_pretty_paths_alias_end_user'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_grade_level';
  $strongarm->value = 'grade';
  $export['usaedu_pretty_paths_alias_grade_level'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_interactivity_type';
  $strongarm->value = 'interactivity_type';
  $export['usaedu_pretty_paths_alias_interactivity_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_item_type';
  $strongarm->value = 'item_type';
  $export['usaedu_pretty_paths_alias_item_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_keywords';
  $strongarm->value = 'keywords';
  $export['usaedu_pretty_paths_alias_keywords'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_language';
  $strongarm->value = 'language';
  $export['usaedu_pretty_paths_alias_language'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_learning_resource_type';
  $strongarm->value = 'resource_type';
  $export['usaedu_pretty_paths_alias_learning_resource_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_revised_bloom_taxonomy';
  $strongarm->value = 'blooms';
  $export['usaedu_pretty_paths_alias_revised_bloom_taxonomy'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'usaedu_pretty_paths_alias_subject';
  $strongarm->value = 'subject';
  $export['usaedu_pretty_paths_alias_subject'] = $strongarm;

  return $export;
}
