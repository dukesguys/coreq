/**
 * For each link pointing to a recent search, display on a tooltip the terms from that search.
 *
 * The breakdown of the URL for the Recent Search can be broken down and parsed to get the Terms to display in the list.
 * Basically you can remove "/resources" and anything after the "?" in the URL, and then split on "/" to bust apart the URL into an array.
 * Then create a term of each of the even numbered items in the array. The array will be comprised of:
 *
 *   "/vocabulary-we-don't-want/term-we-want/vocabulary-we-don't-want/term-we-want/vocabulary-we-don't-want/term-we-want/etc..."
 *
 * You then need to process each term for display; replace "-" with spaces and convert to title case.
 *
 * Examples: /resources/resource_type/assessment-item/educational_use/assessment will look like:
 *
 *    Assessment Item
 *    Assessment
 *
 */
(function ($) {
  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  Drupal.behaviors.tooltipKeywords = {
    attach: function (context, settings) {
      $('#block-usaedu-search-my-recent-searches > .block-content a', context).once('tooltip-keywords', function () {
        var path = this.getAttribute('href');
        if (typeof path !== undefined && path !== null) {
          // remove query from url
          var pathArray = path.split('?');
          path = pathArray[0];

          // split the path and ignore the domain, also the minimun valid url will have a least 4 parts (domain, "resources", vocabulary, term)
          pathArray = path.split('/');
          if (pathArray[1] === 'resources' && pathArray.length >= 4) {
            var i = 3,
                terms = '';

            while (i < pathArray.length) {
              var term = pathArray[i].replace(/-/g, ' ');
              // treat special cases
              if (term === 'curriculuminstruction') {
                term = 'Curriculum/Instruction';
              }
              else if (isNumber(term)) {
                term = 'Grade ' + term;
              }
              terms += '<span class="tootip-term">' + term + '</span> ';
              i += 2;
            }

            $(this).popover({
              html: true,
              trigger: 'hover',
              placement: 'top',
              content: terms
            });
          }
        }
      });
    }
  };
})(jQuery);