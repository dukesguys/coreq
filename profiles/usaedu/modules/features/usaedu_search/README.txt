Search API / Solr configuration
-------------------------------

Special $conf values to connect with a Solr server have to be included in the
settings.php file like so:

$conf['search_api_server_options'] = array(
  'SEARCH_API_SERVER_DEFINITION_MACHINE_NAME' => array(
    'host' => 'SOLR_HOSTNAME', // Usually "localhost".
    'port' => 'SOLR_PORT', // Usually "8983".
    'path' => 'SOLR_PATH', // Usually "/solr".
    'http_user' => '', // Usually "", you can leave that out if you wish.
    'http_pass' => '', // Usually "", you can leave that out if you wish.
  ),
);

Please replace all the UPPER_CASE parts with the real values from your
configuration.

At the time of this writing, the only Search API server definition in this
Drupal install profile has the machine name nj_usaedu_solr.

For your development environment, here is what most likely goes at the end of
settings.php for a local Solr install with out-of-the-box default values:

$conf['search_api_server_options'] = array(
  'nj_usaedu_solr' => array(
    'host' => 'localhost',
    'port' => '8983',
    'path' => '/solr',
  ),
);

Reasoning:
----------

In order for Search API to work properly (and not bring the whole site down with
a fatal error) all Search API servers referenced by featurized Search API
indexes should be featurized as well.

For Solr servers, that featurization includes the server credentials.

These credentials are most likely different in different environments
(like your local, and on the dev, stage and production servers), but the
featurization originally would force the credentials to be equal across all of
the environments.

Fortunately Search API offers a hook_default_search_api_server_alter() which we
use here to let site admins define their own server credentials in settings.php
(as described above), thereby giving them a means to overwrite the featurized
defaults.
