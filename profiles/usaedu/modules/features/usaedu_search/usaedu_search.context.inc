<?php
/**
 * @file
 * usaedu_search.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function usaedu_search_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'resources_search';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'resources_search:page' => 'resources_search:page',
        'resources_search:portal_page' => 'resources_search:portal_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'usaedu_portals-portal_page_header_block' => array(
          'module' => 'usaedu_portals',
          'delta' => 'portal_page_header_block',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views--exp-resources_search-page' => array(
          'module' => 'views',
          'delta' => '-exp-resources_search-page',
          'region' => 'content_preface',
          'weight' => '-10',
        ),
        'usaedu_search-search_resources' => array(
          'module' => 'usaedu_search',
          'delta' => 'search_resources',
          'region' => 'content_preface',
          'weight' => '-9',
        ),
        'current_search-current_resources_search_terms' => array(
          'module' => 'current_search',
          'delta' => 'current_resources_search_terms',
          'region' => 'content',
          'weight' => '-20',
        ),
        'usaedu_search-my_recent_searches' => array(
          'module' => 'usaedu_search',
          'delta' => 'my_recent_searches',
          'region' => 'sidebar_second',
          'weight' => '-47',
        ),
        'usaedu_tool-tools' => array(
          'module' => 'usaedu_tool',
          'delta' => 'tools',
          'region' => 'sidebar_second',
          'weight' => '-46',
        ),
        'usaedu_search-resource_search_facet_header' => array(
          'module' => 'usaedu_search',
          'delta' => 'resource_search_facet_header',
          'region' => 'sidebar_second',
          'weight' => '-45',
        ),
        'facetapi-7d3jDURVVQUqaxnVYIPEXsW3kly2eb06' => array(
          'module' => 'facetapi',
          'delta' => '7d3jDURVVQUqaxnVYIPEXsW3kly2eb06',
          'region' => 'sidebar_second',
          'weight' => '-43',
        ),
        'facetapi-TP3gmFx8K1Zbka4HsImS4J7QZqumGtjt' => array(
          'module' => 'facetapi',
          'delta' => 'TP3gmFx8K1Zbka4HsImS4J7QZqumGtjt',
          'region' => 'sidebar_second',
          'weight' => '-42',
        ),
        'facetapi-7b9gOE82ST3l61elXXPr9MWAEsBMHnBr' => array(
          'module' => 'facetapi',
          'delta' => '7b9gOE82ST3l61elXXPr9MWAEsBMHnBr',
          'region' => 'sidebar_second',
          'weight' => '-41',
        ),
        'facetapi-vR9B07j5ub1ydgfRlixhZbZ7ztPTIXEq' => array(
          'module' => 'facetapi',
          'delta' => 'vR9B07j5ub1ydgfRlixhZbZ7ztPTIXEq',
          'region' => 'sidebar_second',
          'weight' => '-40',
        ),
        'facetapi-lhNPfu0UR5kxPAa771xSdZ65evmKq0lL' => array(
          'module' => 'facetapi',
          'delta' => 'lhNPfu0UR5kxPAa771xSdZ65evmKq0lL',
          'region' => 'sidebar_second',
          'weight' => '-39',
        ),
        'facetapi-aHnk4rpxnqH80DLtnrFTxEcXr8FlcYRQ' => array(
          'module' => 'facetapi',
          'delta' => 'aHnk4rpxnqH80DLtnrFTxEcXr8FlcYRQ',
          'region' => 'sidebar_second',
          'weight' => '-38',
        ),
      ),
    ),
    'breadcrumb' => 'resources',
  );
  $context->condition_mode = 0;
  $export['resources_search'] = $context;

  return $export;
}
