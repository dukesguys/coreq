<?php
/**
 * @file
 * usaedu_search.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function usaedu_search_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'resources_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_resources_search';
  $view->human_name = 'Resources search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Find resources';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Go';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<strong>Sorry, we couldn\'t find any resources that match this search. </strong>
<br />The resource exchange is still growing... for now we\'d suggest trying a different search or removing search filters.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  /* Sort criterion: Indexed Node: Date created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Most recently added';
  /* Sort criterion: Indexed Node: Peer rating */
  $handler->display->display_options['sorts']['field_peer_rating']['id'] = 'field_peer_rating';
  $handler->display->display_options['sorts']['field_peer_rating']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['sorts']['field_peer_rating']['field'] = 'field_peer_rating';
  $handler->display->display_options['sorts']['field_peer_rating']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_peer_rating']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_peer_rating']['expose']['label'] = 'Evaluation score';
  /* Sort criterion: Search: Relevance */
  $handler->display->display_options['sorts']['search_api_relevance']['id'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['sorts']['search_api_relevance']['field'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['order'] = 'DESC';
  /* Filter criterion: Indexed Node: Content type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'resource' => 'resource',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    3 => 0,
    6 => 0,
    4 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '<div class="view-result-summary">Displaying @start - @end of @total Results</div>';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['path'] = 'resources';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Resource Exchange';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Portal Page */
  $handler = $view->new_display('page', 'Portal Page', 'portal_page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Find resources (Portal)';
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '<div class="view-result-summary">Displaying @start - @end of @total Results</div>';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Indexed Node: Featured Portal Content */
  $handler->display->display_options['arguments']['field_featured_portal_content']['id'] = 'field_featured_portal_content';
  $handler->display->display_options['arguments']['field_featured_portal_content']['table'] = 'search_api_index_resources_search';
  $handler->display->display_options['arguments']['field_featured_portal_content']['field'] = 'field_featured_portal_content';
  $handler->display->display_options['arguments']['field_featured_portal_content']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_featured_portal_content']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['field_featured_portal_content']['default_argument_options']['index'] = '2';
  $handler->display->display_options['arguments']['field_featured_portal_content']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_featured_portal_content']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_featured_portal_content']['not'] = 0;
  $handler->display->display_options['path'] = 'resources/portal/%';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Resource Exchange';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['resources_search'] = array(
    t('Master'),
    t('Find resources'),
    t('more'),
    t('Go'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('<strong>Sorry, we couldn\'t find any resources that match this search. </strong>
<br />The resource exchange is still growing... for now we\'d suggest trying a different search or removing search filters.'),
    t('Node ID'),
    t('.'),
    t(','),
    t('Most recently added'),
    t('Evaluation score'),
    t('Page'),
    t('<div class="view-result-summary">Displaying @start - @end of @total Results</div>'),
    t('Portal Page'),
    t('Find resources (Portal)'),
    t('All'),
  );
  $export['resources_search'] = $view;

  $view = new view();
  $view->name = 'standards_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_standards';
  $view->human_name = 'Standards search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'plain_text';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'mini_teaser';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '<div class="view-result-summary">Displaying @start - @end of @total Results</div>';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area_1']['id'] = 'area_1';
  $handler->display->display_options['empty']['area_1']['table'] = 'views';
  $handler->display->display_options['empty']['area_1']['field'] = 'area';
  $handler->display->display_options['empty']['area_1']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_1']['content'] = '<p>No search results found.</p>';
  $handler->display->display_options['empty']['area_1']['format'] = 'full_html';
  /* Field: Indexed Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'search_api_index_standards';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  /* Contextual filter: Indexed Taxonomy term: Vocabulary */
  $handler->display->display_options['arguments']['vocabulary']['id'] = 'vocabulary';
  $handler->display->display_options['arguments']['vocabulary']['table'] = 'search_api_index_standards';
  $handler->display->display_options['arguments']['vocabulary']['field'] = 'vocabulary';
  $handler->display->display_options['arguments']['vocabulary']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['vocabulary']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['vocabulary']['title'] = 'Search: %1';
  $handler->display->display_options['arguments']['vocabulary']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['vocabulary']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['vocabulary']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['vocabulary']['validate']['type'] = 'taxonomy_vocabulary';
  $handler->display->display_options['arguments']['vocabulary']['validate_options']['vocabularies'] = array(
    'ccss' => 'ccss',
    'statestand' => 'statestand',
    'curriculum' => 'curriculum',
  );
  $handler->display->display_options['arguments']['vocabulary']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['vocabulary']['not'] = 0;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_standards';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['operator'] = 'OR';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Or search for a standard';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    3 => 0,
    6 => 0,
    4 => 0,
  );

  /* Display: Standards search */
  $handler = $view->new_display('page', 'Standards search', 'standards_search_page');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['path'] = 'standards/%/search';
  $translatables['standards_search'] = array(
    t('Master'),
    t('more'),
    t('Search'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Select any filter and click on Apply to see results'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('<div class="view-result-summary">Displaying @start - @end of @total Results</div>'),
    t('<p>No search results found.</p>'),
    t('Term ID'),
    t('.'),
    t(','),
    t('All'),
    t('Search: %1'),
    t('Or search for a standard'),
    t('Standards search'),
  );
  $export['standards_search'] = $view;

  return $export;
}
