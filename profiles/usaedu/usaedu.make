; Install the initial drupal codebase using this drush make command

api = 2
core = 7.x

; Build Kit drupal-org.make (May 30 2011)

; Lets use buildkit HEAD for the time being:
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make

; Overrides for Build Kit
projects[devel][subdir] = development
projects[tao][subdir] = contrib
projects[rubik][subdir] = contrib
projects[context][version] = 3.1

; ---------------------------------
; usaedu contrib
; ---------------------------------

projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][subdir] = contrib

projects[aurora][version] = 3.1
projects[aurora][subdir] = contrib

projects[backup_migrate][version] = 2.7
projects[backup_migrate][subdir] = contrib

projects[brb][version] = 1.0
projects[brb][subdir] = contrib

projects[captcha][version] = 1.0
projects[captcha][subdir] = contrib

projects[chosen][version] = 2.x-dev
projects[chosen][subdir] = contrib
projects[chosen][revision] = f731b4269dc6770616b6a21a2328ec10a9f841c9

projects[colorbox][version] = 2.4
projects[colorbox][subdir] = contrib

projects[css3pie][version] = 2.1
projects[css3pie][subdir] = contrib

projects[date][version] = 2.6
projects[date][subdir] = contrib

projects[dialog][version] = 1.x-dev
projects[dialog][subdir] = contrib
projects[dialog][revision] = 2ff76c67f7b3b951746e404adf6cec5373bd3308
projects[dialog][patch][1032038] = http://drupal.org/files/no-overwrite-assets-D7-1032038-comment7.patch
projects[dialog][patch][1994676] = http://drupal.org/files/dialog_sorting_css_and_js.patch

projects[ds][version] = 2.4
projects[ds][subdir] = contrib

projects[elements][version] = 1.4
projects[elements][subdir] = contrib

projects[email][version] = 1.2
projects[email][subdir] = contrib

projects[entity][type] = module
projects[entity][version] = 1.2
projects[entity][subdir] = contrib

projects[facetapi][version] = 1.3
projects[facetapi][subdir] = contrib

projects[facetapi_bonus][version] = 1.1
projects[facetapi_bonus][subdir] = contrib

projects[facetapi_collapsible][version] = 1.1
projects[facetapi_collapsible][subdir] = contrib

projects[facetapi_pretty_paths][version] = 1.0
projects[facetapi_pretty_paths][subdir] = contrib

projects[fences][version] = 1.0
projects[fences][subdir] = contrib

projects[field_group][version] = 1.1
projects[field_group][subdir] = contrib

projects[field_placeholder][version] = 1.0
projects[field_placeholder][subdir] = contrib

projects[file_entity][version] = 2.0-unstable7
projects[file_entity][subdir] = contrib

projects[fitvids][version] = 1.14
projects[fitvids][subdir] = contrib

projects[flag][version] = 3.1
projects[flag][subdir] = contrib

projects[flexslider][version] = 2.0-alpha3
projects[flexslider][subdir] = contrib

projects[google_analytics][version] = 1.3
projects[google_analytics][subdir] = contrib

projects[hierarchical_select][version] = 3.0-alpha5
projects[hierarchical_select][subdir] = contrib
projects[hierarchical_select][patch][1331812] = http://drupal.org/files/null_term_load_check-1331812-4.patch
projects[hierarchical_select][patch][1587570] = http://drupal.org/files/hierarchical_select-php_54-1587570-29.patch
projects[hierarchical_select][patch][1847974] = http://drupal.org/files/hierarchical_select_1847974_1_unloaded_common_inc.patch

projects[hide_submit][version] = 2.0
projects[hide_submit][subdir] = contrib

projects[html5_tools][version] = 1.2
projects[html5_tools][subdir] = contrib

projects[jquery_update][version] = 2.x-dev
projects[jquery_update][subdir] = contrib
projects[jquery_update][revision] = 65eecb0f1fc69cf6831a66440f72e33a1effb1f3
projects[jquery_update][patch][2123973-3] = http://drupal.org/files/issues/jquery_effects_missing-2123973-3_0.patch

projects[legal][version] = 1.4
projects[legal][subdir] = contrib

projects[libraries][version] = 2.1
projects[libraries][subdir] = contrib

projects[link][version] = 1.1
projects[link][subdir] = contrib

projects[login_destination][version] = 1.1
projects[login_destination][subdir] = contrib

projects[logintoboggan][version] = 1.3
projects[logintoboggan][subdir] = contrib

projects[magic][version] = 1.4
projects[magic][subdir] = contrib

projects[media][version] = 2.0-unstable7
projects[media][subdir] = contrib

projects[media_vimeo][version] = 2.x-dev
projects[media_vimeo][subdir] = contrib

projects[media_youtube][version] = 2.0-rc2
projects[media_youtube][subdir] = contrib

projects[menu_attributes][version] = 1.0-rc2
projects[menu_attributes][subdir] = contrib
projects[menu_attributes][patch][1488960-25] = http://www.drupal.org/files/menu_attributes-attributes_for_li-1488960-25.patch

projects[menu_block][version] = 2.3
projects[menu_block][subdir] = contrib

projects[migrate][subdir] = contrib
projects[migrate][version] = 2.5

projects[module_filter][version] = 1.7
projects[module_filter][subdir] = contrib

projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib

projects[quicktabs][version] = 3.6
projects[quicktabs][subdir] = contrib

projects[references][version] = 2.1
projects[references][subdir] = contrib

projects[rules][version] = 2.3
projects[rules][subdir] = contrib

projects[search_api][version] = 1.7
projects[search_api][subdir] = contrib

projects[search_api_attachments][version] = 1.2
projects[search_api_attachments][subdir] = contrib

projects[search_api_autocomplete][version] = 1.0
projects[search_api_autocomplete][subdir] = contrib

projects[search_api_solr][version] = 1.0
projects[search_api_solr][subdir] = contrib

projects[search_api_saved_searches][version] = 1.1
projects[search_api_saved_searches][subdir] = contrib

projects[search_api_sorts][version] = 1.4
projects[search_api_sorts][subdir] = contrib

projects[sharethis][version] = 2.5
projects[sharethis][subdir] = contrib

projects[similarterms][version] = 2.3
projects[similarterms][subdir] = contrib
projects[similarterms][patch][1851754] = http://drupal.org/files/0001-similarterm-options-were-not-being-saved-due-to-wron.patch

projects[special_menu_items][version] = 2.0
projects[special_menu_items][subdir] = contrib

projects[taxonomy_csv][version] = 5.10
projects[taxonomy_csv][subdir] = contrib

projects[taxonomy_revision][version] = 1.0
projects[taxonomy_revision][subdir] = contrib

projects[term_reference_filter_by_views][version] = 2.0-beta2
projects[term_reference_filter_by_views][subdir] = contrib

projects[token][version] = 1.5
projects[token][subdir] = contrib

projects[uuid][version] = 1.x-dev
projects[uuid][subdir] = contrib
projects[uuid][revision] = 4f00a9db2ec2168f43fa96a0408a97b57bca62bc

projects[variable][version] = 2.2
projects[variable][subdir] = contrib

projects[views_bulk_operations][version] = 3.1
projects[views_bulk_operations][subdir] = contrib

projects[views_data_export][version] = 3.0-beta7
projects[views_data_export][subdir] = contrib

projects[workflow][version] = 1.2
projects[workflow][subdir] = contrib
projects[workflow][patch][2044199] = http://drupal.org/files/workflow-transition-features-2044199-1.patch

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2

projects[xmlsitemap][version] = 2.0-rc2
projects[xmlsitemap][subdir] = contrib

projects[zen][version] = 5.4
projects[zen][subdir] = contrib

; Testing and bug tracking

projects[browscap][version] = 2.0
projects[browscap][subdir] = contrib

projects[feedback][version] = 2.x-dev
projects[feedback][subdir] = contrib

projects[unfuddle_api][version] = 1.x-dev
projects[unfuddle_api][subdir] = contrib

projects[unfuddle_feedback][version] = 1.x-dev
projects[unfuddle_feedback][subdir] = contrib

; Production specific modules

; Damien says:
; for your guys local devs, if you start using memcache you will see a hella improvement in speed (even while logged in)
; you need the pecl memcache extension in your PHP though, which might be a pain on osx
; not to be confused with the -other- memcached extension (you want the older one with no "d" at the end on the PHP side, and memcached with the D is the server it talks to)
;
; also, add this to settings.php:
; /* Memcache */
; $conf['cache_backends'][] = 'profiles/usaedu/modules/contrib/memcache/memcache.inc';
; $conf['cache_default_class'] = 'MemCacheDrupal';
; $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
; $conf['memcache_key_prefix'] = sha1(serialize($databases['default']['default']));
;
projects[memcache][version] = 1.x-dev
projects[memcache][subdir] = contrib
projects[memcache][revision] = cf758f792cd5ad1efdfc0f722741ff01f1dda7fd

projects[varnish][version] = 1.0-beta2
projects[varnish][subdir] = contrib

projects[webpurify][version] = 1.01
projects[webpurify][subdir] = contrib
projects[webpurify][patch][2120743-1] = https://drupal.org/files/undefined_webpurify_get_node_body_fail_mode-2120743-1.patch

; ---------------------------------
; libraries
; ---------------------------------

; Chosen
libraries[chosen][download][type] = "get"
libraries[chosen][download][url] = "http://github.com/harvesthq/chosen/releases/download/1.0.0/chosen_v1.0.0.zip"
libraries[chosen][directory_name] = "chosen"

; Ckeditor
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.5.2/ckeditor_3.5.2.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"

; Colorbox
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"
libraries[colorbox][directory_name] = "colorbox"

; CSS3PIE
libraries[css3pie][download][type] = get
libraries[css3pie][download][url] = http://css3pie.com/download-latest-1.x
libraries[css3pie][directory_name] = PIE

; FitVids
libraries[fitvids][download][type] = get
libraries[fitvids][download][url] = https://github.com/davatron5000/FitVids.js/archive/master.zip
libraries[fitvids][directory_name] = fitvids

; FlexSlider
libraries[flexslider][download][type] = get
libraries[flexslider][download][url] = https://github.com/woothemes/FlexSlider/archive/master.zip
libraries[flexslider][directory_name] = flexslider

; scrollTo
libraries[scrollto][download][type] = "get"
libraries[scrollto][download][url] = "http://github.com/flesler/jquery.scrollTo/archive/master.zip"
libraries[scrollto][directory_name] = "scrollto"

; SolrPhpClient
libraries[solrphpclient][download][type] = get
libraries[solrphpclient][download][url] = http://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.zip
libraries[solrphpclient][directory_name] = SolrPhpClient
libraries[solrphpclient][destination] = modules/contrib/search_api_solr
