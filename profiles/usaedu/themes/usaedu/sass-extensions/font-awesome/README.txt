This is the Font Awesome Library: http://fortawesome.github.io/Font-Awesome/

HOW TO UPDATE THE LIBRARY
-------------------------
Replace the current "font-awesome" folder at the same root level as this file with the new "font-awesome" folder.

HOW TO UPDATE VARIABLES
-------------------------
Do the variables updates on the _font-awesome.scss file.