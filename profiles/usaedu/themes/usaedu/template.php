<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   usaedu_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: usaedu_field()
 *
 *   where usaedu is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function usaedu_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  usaedu_preprocess_html($variables, $hook);
  usaedu_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function usaedu_preprocess_html(&$variables, $hook) {
  // Add Google font Cabin
  if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $base = 'https://';
  }
  else {
    $base = 'http://';
  }
  $google_font = array(
    '#tag' => 'link', // The #tag is the html tag - <link />
    '#attributes' => array( // Set up an array of attributes inside the tag
      'href' => $base . 'fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700',
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
  );
  drupal_add_html_head($google_font, 'google_font_open_sans');

  // Formalize
  drupal_add_js(
    // Since Drupal relies on jquery v1.7, we can't use the latest version of formalize.
    drupal_get_path('theme', 'usaedu') . '/sass-extensions/formalize/js/jquery.formalize.legacy.min.js',
    array(
      'type' => 'file',
      'group' => CSS_THEME,
      'weight' => 100,
    )
  );

  // Add a class for the 'Resource search' page
  if (arg(0) == 'resources' && !arg(1)) {
    $variables['classes_array'][] = 'page-resources-search';
  }
  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function usaedu_preprocess_page(&$variables, $hook) {
  global $user;

  // Get rid of the page title and tabs on the front page.
  if (drupal_is_front_page()) {
    $variables['title'] = '';
    unset($variables['tabs']);
  }
  else {
    //override page title for legal module
    if(arg(0) === 'legal') {
      drupal_set_title('Terms of Use for Educator Resource Exchange');
    }
  }
  // Hide all tabs for users that are not:
  // - UID 1
  // - 'Manager' (= user role) users
  if ($user->uid != 1 && !in_array('Manager', $user->roles)) {
    unset($variables['tabs']);
  }
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function usaedu_preprocess_node(&$variables, $hook) {
  // Run node-type-specific preprocess functions, like
  // usaedu_preprocess_node_page() or usaedu_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}

/**
 * Override or insert variables into the resource node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function usaedu_preprocess_node_resource(&$variables, $hook) {

  switch ($variables['view_mode']) {
    case 'full':
      if (
        isset($variables['content']['group_main_content']['field_attached_files'][0]['#bundle'])
        &&
        ('video' == $variables['content']['group_main_content']['field_attached_files'][0]['#bundle'])
      ) {
        /**
         * fitvids.js responsive relative container sizing breaks if we leave in
         * the float:left; for field items in fields with inlined labels.
         * ( @see /modules/field/theme/field.css ) Just hide the label instead,
         * which gets rid of the float:left; as well.
         */
        $variables['content']['group_main_content']['field_attached_files']['#label_display'] = 'hidden';
      }
      else
        if (isset($variables['content']['group_main_content']['field_attached_files']['#title'])) {
        // Otherwise, use a custom label.
        $variables['content']['group_main_content']['field_attached_files']['#title'] = t('Download');
      }

      // Create some custom labels
      if (isset($variables['content']['group_main_content']['field_resource_url']['#title'])) {
        $variables['content']['group_main_content']['field_resource_url']['#title'] = t('Click to open');
      }
      break;
  }
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function usaedu_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function usaedu_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function usaedu_preprocess_block(&$variables, $hook) {
  // Add a class when a views block is empty.
  if ($variables['block']->module == 'views') {
    $delta = $variables['block']->delta;

    // Copied from views_block_view()
    if (strlen($delta) == 32) {
      $hashes = variable_get('views_block_hashes', array());
      if (!empty($hashes[$delta])) {
        $delta = $hashes[$delta];
      }
    }

    $explode_delta = explode('-', $delta);
    $view_result = views_get_view_result($explode_delta[0], $explode_delta[1]);
    if (empty($view_result)) {
      $variables['classes_array'][] = 'block-views-empty';
    }
  }
}

/**
 * Returns HTML for an inactive facet item.
 *
 * @param $variables
 *   An associative array containing the keys 'text', 'path', and 'options'. See
 *   the l() function for information about these variables.
 *
 * @see l()
 *
 * @ingroup themeable
 */
function usaedu_facetapi_link_active($variables) {

  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  // Builds link, passes through t() which gives us the ability to change the
  // position of the widget on a per-language basis.
  $replacements = array(
    '!facetapi_deactivate_widget' => $link_text,
    '!facetapi_accessible_markup' => theme('facetapi_accessible_markup', $accessible_vars),
  );
  $variables['text'] = t('!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements);
  $variables['options']['html'] = TRUE;

  return theme_link($variables);
}

function usaedu_preprocess_form_element(&$variables) {
  if (isset($variables['element']['#required']) && $variables['element']['#required']) {
    $variables['element']['#field_suffix'] = theme('form_required_marker', array('element' => $variables['element']));
  }
}

/**
 * Formats a link field widget.
 *
 * The only reason becasue this functions has been implemented is so we can add the "placeholder" attribute.
 */
function usaedu_link_field($vars) {
  // 'value' was the 'key' used in hook_field_placeholder_info()
  if (isset($vars['element']['value']['#attributes'])) {
    $vars['element']['url']['#attributes'] = array_merge($vars['element']['url']['#attributes'], $vars['element']['value']['#attributes']);
  }
  return theme_link_field($vars);
}

/*
 * Override output of the "Endorsed resource?" Boolean field. If unchecked, display no markup at all.
 */
function usaedu_field__field_endorsed_resource($vars) {
  if ($vars['items']['0']['#markup'] == 'Not endorsed') {
    // Return empty field content.
    return ' ';
  }
  return theme_field($vars);
}

/*
 * Override output of the "Standard CCSS" field.
 */
function usaedu_field__field_standards_ccss($vars) {
  $output = '';
  if ($vars['element']['#view_mode'] == 'teaser') {
    // In teaser mode display the short name of the term
    for ($i = 0; $i < sizeof($vars['items']); $i++) {
      $item = $vars['items'][$i];
      $object = entity_metadata_wrapper('taxonomy_term', $item['#options']['entity']);
      $item['#title'] = $object->field_standard_short_code->value();
      $vars['items'][$i] = $item;
    }
  }
  if (!empty($vars['ds-config'])) {
    $output =  theme_ds_field_expert($vars);
  }
  else {
    $output = theme_field($vars);
  }
  return $output;
}

/*
 * Override output of the "Peer Rating" decimal field.
 */
function usaedu_field__field_peer_rating($vars) {
  $output = '';
  $vars['items']['0']['#markup'] = decimal_to_stars($vars['element']['#items'][0]['value']);
  if (isset($vars['ds-config'])) {
    $output =  theme_ds_field_expert($vars);
  }
  else {
    $output = theme_field($vars);
  }
  return $output;
}

/*
 * Override output of the "Expert Peer Rating" decimal field.
 */
function usaedu_field__expert__field_peer_rating($vars){
  $output = '';
  $vars['items']['0']['#markup'] = decimal_to_stars($vars['element']['#items'][0]['value'], $vars['items']['0']['#markup']);
  if (isset($vars['ds-config'])) {
    $output =  theme_ds_field_expert($vars);
  }
  else {
    $output = theme_field($vars);
  }
  return $output;
}

/*
 * Convert a decimal rating into a stars representation
 *
 * @param value the float number to be use for the rating
 * @param suffix (optional) to be added after the starts
 * @return an string conteining the final html code
 */
function decimal_to_stars($value, $suffix = NULL) {
  $value = (float)$value; // make sure we get the right type
  $output = array();
  $star_empty = '<i class="icon-star-empty"></i>';

  if ($value == 0) {
    $output = array($star_empty, $star_empty, $star_empty, $star_empty);
  }
  else {
    $star_half = '<i class="icon-star-half-empty"></i>';
    $star_full = '<i class="icon-star"></i>';
    $integer = floor($value);
    $fraction = $value - $integer;

    for ($i = 0; $i < $integer; $i++) {
      $output[] = $star_full;
    }

    if ($fraction > 0) {
      $output[] = $star_half;
    }

    $size = sizeof($output);
    for ($i = $size + 1; $i <= 4; $i++) {
      $output[] = $star_empty;
    }
  }

  if (!empty($suffix)) {
    $output[] = $suffix;
  }
  return '<div class ="stars-rate">' . implode(' ', $output) . '</div>';
}

/*
 * Override output of facet titles, specifically the "Endorsed Resource" facet
 */
function usaedu_facetapi_title($vars) {
  switch ($vars['title']) {
    case 'Endorsed':
      return t('Endorsement');
    case 'Digital Media Type':
      return t('Media Type');
    case 'Learning Resource Type':
      return t('Resource Type');
    default:
      return $vars['title'];
  }
}

/*
 * Override Zen's breadcrumb theming.
 */
function usaedu_breadcrumb($variables) {

  $breadcrumb = $variables['breadcrumb'];
  $output = '';

  // Do not show any breadcrumb on the home page.
  if (drupal_is_front_page()) { return; }

  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('zen_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('zen_breadcrumb_home');
    if (!$show_breadcrumb_home && strip_tags($breadcrumb[0]) == 'Home') {
      array_shift($breadcrumb);
    }

    // Get the other theme breadcrumb settings: separator, etc.
    $breadcrumb_separator = theme_get_setting('zen_breadcrumb_separator');
    $trailing_separator = $title = '';
    if (theme_get_setting('zen_breadcrumb_title')) {
      $item = menu_get_item();
      if (!empty($item['tab_parent'])) {
        // If we are on a non-default tab, use the tab's title.
        $breadcrumb[] = check_plain($item['title']);
      }
      // Only add the current page at the end if it isn't already there.
      else {
        $title = drupal_get_title();
        $lastcrumb = end($breadcrumb);
        if (strip_tags($lastcrumb) != $title && $title != 'Find resources') {
          $breadcrumb[] = drupal_get_title();
        }
      }
    }
    elseif (theme_get_setting('zen_breadcrumb_trailing')) {
      $trailing_separator = $breadcrumb_separator;
    }

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users.
    if (empty($variables['title'])) {
      $variables['title'] = t('You are here');
    }
    // Unless overridden by a preprocess function, make the heading invisible.
    if (!isset($variables['title_attributes_array']['class'])) {
      $variables['title_attributes_array']['class'][] = 'element-invisible';
    }

    // Wrap in markup.
    $output = '<nav class="breadcrumb" role="navigation">';
    $output .= '<h2' . drupal_attributes($variables['title_attributes_array']) . '>' . $variables['title'] . '</h2>';
    $output .= '<ol><li>' . implode($breadcrumb_separator . '</li><li>', $breadcrumb) . $trailing_separator . '</li></ol>';
    $output .= '</nav>';
  }

  return $output;
}

/**
* Theme the accept terms and conditions label.
*
* @param $variables
*   An associative array of variables for themeing, containing:
*    - link: Whether or not the label contains a link to the legal page.
*
* @ingroup themeable
*/

function usaedu_legal_accept_label($variables) {

  if ($variables['link']) {
    return t("I agree to the terms as specified on the site's <a href='@terms'>Terms of Use</a> page.", array('@terms' => url('legal')));
  }
  else {
    return t('<strong>Accept</strong> Terms & Conditions of Use');
  }
}


/**
 * Implements hook_form_FORM_ID_alter().
 */
function usaedu_form_user_profile_form_alter(&$form, $form_state) {
  if ($form['#id'] === 'profile-tabs-group-myfeeds' && $form['legal']['legal_accept']['#value']) {
    hide($form['legal']);
  }
}