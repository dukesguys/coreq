(function ($) {

  Drupal.behaviors.ieHelper = {
    attach: function (context, settings) {
      if (($.browser.msie) && ($.browser.version >= 9.0)) {
        $('body').addClass('gt-ie8');
      }
      if (($.browser.msie) && ($.browser.version == 8.0)) {
        $('body').prepend('<div id="ie8-warning">This site is currently in Beta and Internet Explorer 8 and below are not supported.</div>');
      }
    }
  };

})(jQuery);