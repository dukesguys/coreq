/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  var layoutNarrowMaxW = 840, //max-width for narrow layout
      layoutLargeMinW = 1114; //min-width for large layout

  $.fn.slideFadeToggle = function(speed, easing, callback) {
    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
  };

  var _getTallestHeight = function(tallest, item) {
    // First remove inline style by setting property to inherit, then get the height.
    var thisHeight = $(item).css({'height': 'inherit', 'min-height': 'inherit'}).outerHeight();
    if (thisHeight > tallest) {
      tallest = thisHeight;
    }
    return tallest;
  };

  /*
   * @param items array of css selectors
   */
  var usaeduEqualHeight = function(items) {
    var tallest = 0, i;
    // find the tallest
    for (i = 0; i < items.length; i++) {
      // Does the css selector target more than one item?
      if ($(items[i]).length > 1) {
        $(items[i]).each(function() {
          tallest = _getTallestHeight(tallest, this);
        });
      }
      else if ($(items[i]).length == 1){
        tallest = _getTallestHeight(tallest, items[i]);
      }
    }
    // set the height
    for (i = 0; i < items.length; i++) {
      $(items[i]).css('min-height', tallest);
    }
  };

  /*
   * Responsive menu
   */
  var usaeduResponsiveMenu = function(layoutW) {
    if (!$('#block-menu-block-1 .btn-menu').length) {
      $('#block-menu-block-1 .menu-name-main-menu').before('<a href="#" class="btn-menu"><i class="icon-reorder icon-2x"></i></a>');

      $('.btn-menu')
        .unbind('click.usaedu')
        .bind('click.usaedu', function() {
          $('#block-menu-block-1 .menu-name-main-menu > ul').slideToggle();
        });
    }

    if (layoutW <= layoutNarrowMaxW) {
      $('#block-usaedu-users-logged-in-user-menu .block-content, #block-menu-block-1 .menu-name-main-menu > ul').hide();
    }
    else {
      $('#block-menu-block-1 .menu-name-main-menu > ul').show();
    }

    // Expand submenu when a ".no-link" item is clicked
    $('#block-menu-block-1 .menu-name-main-menu .no-link').each(function() {
      if ($('.active', this).length == 0) {
        $(this).removeClass('expanded');
      }
      $(this)
        .unbind('click.usaedu')
        .bind('click.usaedu', function(e) {
          $(this).toggleClass('expanded');
        });
    });
  };

  /*
   * Auto close menus
   */
  var autoCloseMenus = function() {
    $('html').unbind('click.usaedu').bind('click.usaedu', function(e) {
      if ($(e.target).parent().attr('id') !== 'block-usaedu-users-logged-in-user-menu' && $('#block-usaedu-users-logged-in-user-menu .block-content').is(':visible')) {
        $('#block-usaedu-users-logged-in-user-menu .block-content').slideToggle();
      }
      if ($(window).width() <= layoutNarrowMaxW) {
        if ($(e.target).hasClass('nolink') === false && $(e.target).hasClass('btn-menu') === false && $(e.target).parent().hasClass('btn-menu') === false && $('#block-menu-block-1 .menu-name-main-menu > ul').is(':visible')) {
          $('#block-menu-block-1 .menu-name-main-menu > ul').slideToggle();
        }
      }
    });
  };

  /*
   * Get scrollbar width
   */
  var getScrollBarWidth = function() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild (inner);

    document.body.appendChild (outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild (outer);

    return (w1 - w2);
  };

  /*
   * Sidebar independent scroll
   */
  var usaeduSidebarScroll = function(layoutW) {
    if (layoutW <= layoutNarrowMaxW) {
      // Remove scroll on narrow layout
      if ($('.region-sidebar-first > .usaedu-scroll').length) {
        $('.region-sidebar-first > .usaedu-scroll').children().unwrap();
      }
    }
    else {
      if (!$('.region-sidebar-first > .usaedu-scroll').length) {
        $('.region-sidebar-first').wrapInner('<div class="usaedu-scroll" />');
      }

      var sidebarW = $('.region-sidebar-first').width(),
          sidebarOuterW = $('.region-sidebar-first').outerWidth(),
          scrollW = getScrollBarWidth();

      $('.region-sidebar-first > .usaedu-scroll')
        .css({
          'height' : $(window).height(),
          'width'  : (1 * sidebarOuterW + scrollW) + 'px'
        })
        .children()
          .css({'width': sidebarW + 'px'});
    }
  };

  /*
   * Show/hide authenticated user menu
   */
  var usaeduToggleAuthUserMenu = function() {
    $('#block-usaedu-users-logged-in-user-menu .block-title')
      .unbind('click.usaedu')
      .bind('click.usaedu', function() {
        $('#block-usaedu-users-logged-in-user-menu .block-content').slideToggle();
      });
  };

  /*
   * Show/hide "My recent searches"
   */
  var usaeduToggleMyRecentSearches = function() {
    $('#block-usaedu-search-my-recent-searches .block-title')
      .unbind('click.usaedu')
      .bind('click.usaedu', function() {
        $('#block-usaedu-search-my-recent-searches .block-content').slideToggle(function(){
          $('#block-usaedu-search-my-recent-searches .block-title').toggleClass('expanded');
        });
      });
  };

  /*
   * Toggle evaluation results on resource node.
   */
  var usaeduToggleEvaluationResults = function(context) {
    $('#peer-rating', context).append('<div id="toggle-eval-results"></div>');
    $('.group-evaluation-results', context).append('<div id="close-eval-results">Close</div>');
    $('#toggle-eval-results', context).click(function(event) {
      event.preventDefault();
      if ($(this).hasClass('is-expanded')) {
        $('.group-evaluation-results').slideFadeToggle('fast', function() {
          $('#toggle-eval-results').removeClass('toggled');
        });
        $('#toggle-eval-results').animate({
          paddingBottom: 0
        }, 'fast');
        $(this).removeClass('is-expanded');
      }
      else {
        $('.group-evaluation-results').slideFadeToggle('fast');
        $('#toggle-eval-results').animate({
          paddingBottom: '10px'
        }, 'fast');
        $(this).addClass('is-expanded');
        $(this).addClass('toggled');
      }
    });
    $('#close-eval-results', context).click(function() {
      $('.group-evaluation-results').slideFadeToggle('fast', function() {
        $('#toggle-eval-results').removeClass('toggled');
      });
      $('#toggle-eval-results').animate({
        paddingBottom: 0
      }, 'fast');
      $('#toggle-eval-results').removeClass('is-expanded');
    });
  };

  /*
   * Toggle search facets
   */
  var usaeduToggleSearchFacets = function() {
    $('#block-usaedu-search-resource-search-facet-header').click(function() {
      $(this).siblings('.block-facetapi').slideFadeToggle();
      if ($(this).children('.block-content').text() == "Show Search Filters") {
        $(this).children('.block-content').text("Hide Search Filters");
      }
      else {
        $(this).children('.block-content').text("Show Search Filters");
      }
      $(this).children('.block-content').toggleClass('hidden');
    });
  };

  /*
   * Keyword search on resources search page.
   */
  var clearSearchField = function(searchField, submitButton, formId) {
    $(searchField).keypress(function() {
      $(this).addClass('active');
      $(submitButton).addClass('active');
      $('#clear-keyword').show();
    });
    if ($(searchField).val()) {
      $(searchField).addClass('active');
      $('#clear-keyword').show();
    }
    // 2. Remove icon and submit form when clearing field.
    $('#clear-keyword').click(function(event){
      event.preventDefault();
      $(searchField).val('').focus().removeClass('active');
      $(submitButton).removeClass('active');
      $('#clear-keyword').hide();
      $(formId).submit();
    });
  };

  /*
   * Bootstrap helper
   */
  var usaeduBootstrapHelper = function() {
    // Popover for reporting tool
    $('#block-usaedu-base-resource-tools #report').popover({
      content: '<div class="tool-popover">Is there something wrong with this content?<br/>Report it to a site administrator by clicking this link.</div>',
      html: true,
      placement: 'bottom',
      trigger:'hover'
    });

    // Display form field descriptions in Bootstrap popover().
    $("#dialog").on("dialogopen", function(event, ui) {
      $('.toggle-field-desc').popover({trigger:'hover'});
    });

    // Make sure Bootstrap popovers close when clicking outside of them.
    // http://mattlockyer.com/2013/04/08/close-a-twitter-bootstrap-popover-when-clicking-outside
    $('div:not(#anything)').on('click', function (event) {
      $('.toggle-field-desc').each(function () {
        if (!$(this).is(event.target) && $(this).has(event.target).length === 0 && $('.popover').has(event.target).length === 0) {
          $(this).popover('hide');
          return;
        }
      });
    });
  };

  /*
   * Window resize
   */
  var usaeduWindowResize = function() {
    var layoutW = $(window).width();

    if (layoutW < layoutLargeMinW) {
      $('body').removeClass('large-layout');
    }
    else {
      $('body').addClass('large-layout');
    }

    usaeduResponsiveMenu(layoutW);
    usaeduSidebarScroll(layoutW);

    // Set equal heights for different sets of blocks.
    var recentResources = [
          'body.front #block-views-recent-resources-block .view-content',
          'body.front #block-views-endorsed-resources-block .view-content'
        ],
        recentVideos = [
          'body.front #block-views-recent-video-block .view-content',
          'body.front #block-views-recent-video-block-1 .view-content'
        ],
        howToUseSection = [
          'body.front #block-usaedu-base-how-to-use-search',
          'body.front #block-usaedu-base-how-to-use-collect',
          'body.front #block-usaedu-base-how-to-use-browse'
        ],
        howToFindSection_content = [
          'body.front #block-usaedu-base-browse-ccss .browse-text',
          'body.front #block-usaedu-base-browse-njss .browse-text',
          'body.front #block-usaedu-base-browse-njmc .browse-text'
        ],
        howToFindSection_title = [
          'body.front #block-usaedu-base-browse-ccss .block-title',
          'body.front #block-usaedu-base-browse-njss .block-title',
          'body.front #block-usaedu-base-browse-njmc .block-title'
        ],
        dashboardBlocks = [
          // '.page-dashboard .region-content-preface .block-content .view > [class^=.view-]'
          /*'#block-views-my-collections-block .view-content',
          '#block-views-recent-resources-block-1 .view-content',
          '#block-views-recent-resources-block-3 .view-content',
          '#block-views-endorsed-resources-block-1 .view-content',
          '#block-views-user-feed-block-1 .view-content',
          '#block-views-my-collections-block .view-empty',
          '#block-views-recent-resources-block-1 .view-empty',
          '#block-views-recent-resources-block-3 .view-empty',
          '#block-views-endorsed-resources-block-1 .view-empty',
          '#block-views-user-feed-block-1 .view-empty'*/
          'body.page-dashboard #content > .block'
        ];
    usaeduEqualHeight(recentResources);
    usaeduEqualHeight(recentVideos);
    usaeduEqualHeight(howToUseSection);
    usaeduEqualHeight(howToFindSection_content);
    usaeduEqualHeight(howToFindSection_title);
    usaeduEqualHeight(dashboardBlocks);
  };

  // Returns a random integer between min and max
  // Using Math.round() will give you a non-uniform distribution!
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /*
   * Front page rotatory banner
   */
  var frontPageRotatoryBanner = function() {
    var banner = 'body.front #header';
    if ($(banner).length) {
      var path = "/profiles/usaedu/themes/usaedu/images/banners/homepage/NJDOE_Home_Main_Image_0";

      //var bgImg = $(banner).css('background-image').replace('url(','').replace(')',''),

      $(banner).css("background-image", "url('" + path + getRandomInt(1, 4) + ".jpg')");
    }
  };

  Drupal.behaviors.usaeduTheme = {
    attach: function (context, settings) {
      frontPageRotatoryBanner();
      autoCloseMenus();
      usaeduWindowResize();

      $(window).resize(function() {
        usaeduWindowResize();
      });

      usaeduToggleAuthUserMenu();
      usaeduToggleMyRecentSearches();

      $('#peer-rating', context).once('usaedu-theme', function () {
        // Apply the usaedu-theme effect to the elements only once.
        usaeduToggleEvaluationResults(context);
      });

      usaeduToggleSearchFacets();

      clearSearchField('#edit-search-api-views-fulltext', '#edit-submit-resources-search', '#views-exposed-form-resources-search-page');
      clearSearchField('#edit-search-api-views-fulltext', '#edit-submit-standards-search', '#views-exposed-form-standards-search-standards-search-page');

      usaeduBootstrapHelper();
    }
  };

})(jQuery, Drupal, this, this.document);
