<?php
  $link_content = $fields['field_portal_bg_img_block']->content;
  $link_content .= $fields['field_portal_short_description']->wrapper_prefix;
  $link_content .= $fields['field_portal_short_description']->content;
  $link_content .= $fields['field_portal_short_description']->wrapper_suffix;

  print l($link_content, 'resources/portal/' . $fields['tid']->raw, array('html' => TRUE));
