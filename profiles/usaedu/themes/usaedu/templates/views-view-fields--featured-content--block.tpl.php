<?php
  // Sorry, I couldn't figure out how to get a preprocess-function going for this views-view-field template.
  // @TODO Stick this into a proper preprocess function.
  $has_link = isset($row->field_field_fc_link[0]['rendered']['#element']['url']);
  if ($has_link) {
    $link_url = $row->field_field_fc_link[0]['rendered']['#element']['url'];
  }
?>
<div class="slide-inner-wrapper slide-type-<?php print strtolower($row->field_field_fc_type[0]['rendered']['#markup']); ?>">
  <?php if ($has_link): ?><a href="<?php print $link_url; ?>"><?php else: ?><span class="no-link"><?php endif; ?>
    <?php foreach ($fields as $id => $field): ?>
      <?php if (!empty($field->separator)): ?>
        <?php print $field->separator; ?>
      <?php endif; ?>
      <?php print $field->wrapper_prefix; ?>
        <?php print $field->label_html; ?>
        <?php print $field->content; ?>
      <?php print $field->wrapper_suffix; ?>
    <?php endforeach; ?>
  <?php if ($has_link): ?></a><?php else: ?></span><?php endif; ?>
</div>