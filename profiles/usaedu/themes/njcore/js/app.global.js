var app = app || {},
    $ = jQuery;

app.global = {

    config : {},

    // Video Block Configuration
    video : {
        obj : '.view-front-page-video video', // Video object
        blockIds : ['videoBoxes'], // Blocks that overlay video (hidden when playing)
        playBtn : '<span class="video-controls paused startstop" />', // Play btn overlay on video stopped state
        useMaxHeightCSS: false, // $(window).height() - $('#header').height() when false
        maxHeight : '', // Set variable for when video is toggled
        requestFullScreen : true, // Sets fullpage video onclick playBtn
        playerName : 'homepageVideo', // Name of the video player
        setPoster : true, // True :: sets the initial page poster image
        posterDiv : '#videoPoster' // The hidden div that contains the URL to the poster -- gets data-url
    },
    videoEvents : { // Event listeners added to video:obj
        'onpause' : 'stoppedVideo()',
        'onended' : 'stoppedVideo()',
        'onplay'  : 'playingVideo()'
    },

    init: function()
    {
        app.global.pageInit();
        app.global.initNavDrop();
        app.global.initHandlers();

    },
    pageInit:function() {
        app.global.initHomePageVideo();
        app.global.userRegistration();
    },
    initHandlers:function() {
        // Navigation/Header
        $('.login-link a').on('click', this.handlers.showLoginOptions);
        $('span.account').on('click', this.handlers.showLoginForm);
        $('.search-button .icon-search').on('click', this.handlers.showSearch);
        $('#site-search input').keypress(this.handlers.submitSearchForm);
        $('#site-search button').on('click', this.handlers.checkFormSubmit);

        // Homepage Video
        $('.video-controls.startstop').on('click', this.handlers.setPlayback);

        // Password Strength
        $('.form-type-password').bind('input', this.handlers.checkStrength);

    },

    handlers : {
        showLoginOptions:function(e) {
            e.preventDefault();
                //$('a.nav-title').parents('#block-user-login').toggleClass('active');
        },
        showSearch:function() {
            $(this).parents('.search-wrapper').toggleClass('active');
        },
        submitSearchForm:function() {
            if (event.which == 13) {
                event.preventDefault();
                $("#site-search").submit();
            }
        },
        checkFormSubmit:function() {
            var $form = $(this).parents('form'),
                $input = $form.find('#edit-search');

            if($input.get(0).value != '') {
                $form.submit();
            }
        },
        showLoginForm:function(e) {
            e.preventDefault();

            var $this = $(this),
                $hiddenElm = $this.parents('.login-menu').find('.hidden'),
                $activeElm = $this.parents('.login-menu').find('.active');

            $hiddenElm.removeClass('hidden').addClass('active');
            $activeElm.removeClass('active').addClass('hidden');
        },
        setPlayback:function() {
            var $video = $(app.global.video.obj);

            if($video[0].paused) {
                $video[0].play();

                if(app.global.video.requestFullScreen == true)
                    app.global.requestFullScreen();
            }
            else {
                $video[0].pause();
            }
            return false;

        },
        stoppedVideo:function() {
            var $video = $(app.global.video.obj),
                blocks = app.global.video.blockIds,
                maxHeight = app.global.video.maxHeight;

            $video.removeAttr('controls');
            $video.parent().css('max-height', maxHeight);

            // Show play overlay
            $('.video-controls').show();

            // Show lower blocks
            $.each(blocks, function(i, v) {
                $('#'+v+'').show();
            });
        },
        playingVideo:function() {
            var $video = $(app.global.video.obj),
                blocks = app.global.video.blockIds;

            $video.attr('controls', 'controls');
            $video.parent().css('max-height', 'none');

            // Hide play overlay
            $('.video-controls').hide();

            // Hide lower blocks
            $.each(blocks, function(i, v) {
                $('#'+v+'').hide();
            });
        },
        checkStrength:function() {
            var strengthBar = $(this).parent().find('.password-indicator > .indicator'),
                barWidth = strengthBar.width(),
                parentWidth = strengthBar.parent().width(),
                percent = 100*barWidth/parentWidth;

            if(percent >= 75){
                strengthBar.addClass('strong');
            } else {
                strengthBar.removeClass('strong');
            }
        }
    },
    initHomePageVideo:function() {
        var $video = $(app.global.video.obj),
            playBtn = app.global.video.playBtn,
            playerName = app.global.video.playerName,
            poster = app.global.video.posterDiv,
            posterImage = $(poster).data('url');

        $video.attr('id', playerName);
        $video.parent().prepend(playBtn);

        // Set video poster
        if($video.length != 0)
            document.getElementById(playerName).setAttribute('poster', posterImage);

        // Set max-height or get from CSS
        app.global.video.maxHeight = $(window).height()-$('#header').height();
        if(app.global.video.useMaxHeightCSS == true)
            app.global.video.maxHeight = $video.parent().css('max-height');

        // Call Video Stopped State
        app.global.handlers.stoppedVideo();

        // Set media event listeners
        $.each(app.global.videoEvents, function(i, v) {
            $video.attr(i, 'app.global.handlers.'+v);
        });
    },
    userRegistration:function() {
        // Move the password field on the user registration form @todo: find the right hook for this
        var passStrength = $('.password-parent > .password-strength');
        if(passStrength.length != 0)
            $('.password-parent > .password-strength:first').appendTo('.pass-indicator');
    },
    requestFullScreen:function() {
        var playerName = app.global.video.playerName,
            elem = document.getElementById(playerName);

        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen();
        }
    },
    initNavDrop:function() {
        var mq = window.matchMedia('(min-width: 768px)');
        if (mq.matches) {
            $('ul.navbar-nav > li').addClass('hovernav');
        } else {
            $('ul.navbar-nav > li').removeClass('hovernav');
        }

        /*
         The addClass/removeClass also needs to be triggered
         on page resize <=> 768px
         */
        if (matchMedia) {
            var mq = window.matchMedia('(min-width: 768px)');
            mq.addListener(WidthChange);
            WidthChange(mq);
        }

        function WidthChange(mq) {
            if (mq.matches) {
                $('ul.navbar-nav > li').addClass('hovernav');
                // Restore "clickable parent links" in navbar
                $('.hovernav a').click(function () {
                    window.location = this.href;
                });
            } else {
                $('ul.navbar-nav > li').removeClass('hovernav');
            }
        }

        // Restore "clickable parent links" in navbar
        $('.hovernav a').click(function () {
            window.location = this.href;
        });
    }
};

$(document).ready(function(){
    app.global.init();
});
