<?php

/**
 * @file
 * template.php
 */

/**
 * Implements hook_theme().
 */
function njcore_theme(&$existing, $type, $theme, $path) {
    $hooks['_usaedu_search_block_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'njcore') . '/templates',
    'template' => 'usaedu-search-form',
    );

    $hooks['_usaedu_main_search_block_form'] = array(
        'render element' => 'form',
        'path' => drupal_get_path('theme', 'njcore') . '/templates',
        'template' => 'usaedu-main-search-form',
    );

    $hooks['user_login_block'] = array(
    'template' => 'templates/user-login-block',
    'render element' => 'form',
    );
    $hooks['user_register_form'] = array(
        'render element' => 'form',
        'path' => drupal_get_path('theme', 'njcore') . '/templates',
        'template' => 'user-register-form',
        /*'preprocess functions' => array(
            'njcore_preprocess_user_register_form'
        ),*/
    );
  return $hooks;
}

function njcore_preprocess_region(&$variables) {
    // Add class to navigation region
    if($variables['region'] == 'navigation') {
        $variables['classes_array'][] = 'navbar-right';
    }
}

function njcore_menu_tree($tree) {
    return '<ul class="menu nav navbar-nav fullWidth">' . $tree['tree'] .'</ul>';
}

function njcore_preprocess_page(&$variables, $hook) {
    global $user;

    // Get rid of the page title and tabs on the front page.
    if (drupal_is_front_page()) {
        $variables['title'] = '';
        unset($variables['tabs']);
    }
    else {
        //override page title for legal module
        if(arg(0) === 'legal') {
            drupal_set_title('Terms of Use for Educator Resource Exchange');
        }
    }
    // Hide all tabs for users that are not:
    // - UID 1
    // - 'Manager' (= user role) users
    if ($user->uid != 1 && !in_array('Manager', $user->roles)) {
        unset($variables['tabs']);
    }

    // Unset title from user registration page [handled by block]
    if(arg(0) === 'user' && arg(1) === 'register') {
        drupal_set_title('');
    }

    // Add information about the number of sidebars.
    if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
        $variables['content_column_class'] = ' class="col-sm-6"';
    }
    elseif (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
        $variables['content_column_class'] = ' class="col-sm-9"';
    }
    /*else {
        $variables['content_column_class'] = ' class="col-sm-12 container"';
    }*/

    // @todo: find the proper hook
    unset($variables['page']['navigation']['#theme_wrappers']);
    $variables['page']['navigation']['usaedu_search_find_resources_search']['#theme_wrappers'] = array('form');
    $variables['page']['navigation']['usaedu_search_find_resources_search']['#id'] = 'site-search';
    $variables['page']['navigation']['usaedu_search_find_resources_search']['#attributes']['class'][] = 'search-form navbar-form navbar-left';
    unset($variables['page']['navigation']['usaedu_search_find_resources_search']['search']['#title']);
    unset($variables['page']['navigation']['usaedu_search_find_resources_search']['search']['#theme_wrappers']);
    $variables['page']['navigation']['usaedu_search_find_resources_search']['search']['#prefix'] = '<div class="form-group search-holder">';
    $variables['page']['navigation']['usaedu_search_find_resources_search']['search']['#suffix'] = '</div>';
    $variables['page']['navigation']['usaedu_search_find_resources_search']['search']['#attributes']['class'][] = 'form-control';
    $variables['page']['navigation']['usaedu_search_find_resources_search']['search']['#attributes']['placeholder'][] = 'Enter Search Term';
    $variables['page']['navigation']['usaedu_search_find_resources_search']['submit']['#value'] = '<i class="icon-search"></i>';
    $variables['page']['navigation']['usaedu_search_find_resources_search']['submit']['#attributes']['class'][] = 'btn btn-warning';
    //kpr($variables['page']['navigation']['usaedu_search_find_resources_search']);

    //kpr($variables['page']['content']);

    //$variables['page']['header'][] = $variables['page']['content_preface']['views_tweet_feed-block'];
    unset($variables['page']['content_preface']['views_tweet_feed-block']);

}

/**
 * Implements hook_preprocess_block().
 */
function njcore_preprocess_block(&$vars) {
    $delta = $vars['block']->delta;

    // Setup the contact form in a block
    $module = 'block';
    $deltaNum = 2;
      if ($vars['block']->module == $module && $vars['block']->delta == $deltaNum) {
          module_load_include('inc', 'contact', 'contact.pages');
          $contact_form = drupal_get_form('contact_site_form');
          // Adjust the form before render
          $title = '<h2>WE\'D LOVE TO HEAR YOUR FEEDBACK</h2>';

          $contact_form['#attributes']['class'][] = 'feedback-form';
          $contact_form['title'] = array(
              '#markup' => '<div class="col-sm-12">' . $title. '</div>',
              '#weight' => -5,
          );

          $contact_form['name']['#attributes']['placeholder'] = 'Your Name';
          $contact_form['name']['#prefix'] = '<div class="col-sm-6">';
          $contact_form['name']['#suffix'] = '</div>';
          $contact_form['mail']['#attributes']['placeholder'] = 'Email Address';
          $contact_form['mail']['#prefix'] = '<div class="col-sm-6">';
          $contact_form['mail']['#suffix'] = '</div>';
          $contact_form['message']['#attributes']['placeholder'] = 'Please give us your feedback. We look forward to hearing from you.';
          $contact_form['message']['#prefix'] = '<div class="col-sm-12">';
          $contact_form['message']['#suffix'] = '</div>';
          $contact_form['actions']['submit']['#value'] = 'Submit';
          $contact_form['actions']['submit']['#prefix'] = '<div class="button-holder">';
          $contact_form['actions']['submit']['#suffix'] = '</div>';
          $contact_form['actions']['submit']['#attributes']['class'] = array('btn btn-success');
          unset($contact_form['subject']);
          unset($contact_form['copy']);

          $vars['content'] = render($contact_form);
      }

}

/**
 * Implements hook_preprocess_html
 */
function njcore_preprocess_html(&$vars) {
    $path = drupal_get_path_alias();
    $aliases = explode('/', $path);

    foreach($aliases as $alias) {
        $vars['classes_array'][] = drupal_clean_css_identifier($alias); // Add arg() path to <html class>
    }

}


/**
 * Implements hook_menu_link().
 */
function njcore_menu_link__main_menu(&$variables) {
    $element = $variables['element'];

    // Inject the user_login_block into this menu item
    if($element['#href'] == 'user/login') {
        $element['#attributes'][] = 'login-link';
        $markup = getMarkup('login');
        $output = l($element['#title'], $element['#href'], $element['#localized_options']);

        return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $markup . "</li>\n";
    }

    // Rewrites the dashboard link
    if($element['#href'] == 'user/edit') {
        if(user_is_logged_in()) {
            $variables['element']['#href'] = 'dashboard';
            return bootstrap_menu_link($variables);
        } else {
            return '';
        }
    }

    // Return a normal menu item through BS
    return bootstrap_menu_link($variables);

}


/**
 * Implements hook_user_register_form()
 */
function njcore_preprocess_user_register_form(&$vars) {

    // Setup shortcuts to field groups
    $personal_info = $vars['form']['group_myaccount']['group_personal_info'];
    $smid_info = $vars['form']['group_myaccount']['group_smid'];
    $account = $personal_info['account'];

    // Adjust some data before render
    $smid_text = t('If you do not know your exact SMID #, start typing the name of the school, district or country with which your SMID is associated.');
    $smid_bottom = $smid_info['field_smid']['und']['#description'];

    $vars['smid_top'] = $smid_text;
    $vars['smid_bottom'] = render($smid_bottom);

    $vars['form']['actions']['cancel']['#markup'] = '<button class="btn btn-danger" type="submit">Cancel</button>';
    $vars['form']['actions']['submit']['#attributes']['class'] = array('btn btn-warning');


    // Unset fields so they're not rendered twice
    unset($smid_info['field_schools']['und']['#description']);
    unset($account['mail']['#description']);
    unset($vars['form']['actions']['description']);
    unset($smid_info['field_smid']['und'][0]['value']['#description']);


    // Set variables for frontend template
    $vars['first_name'] = render($personal_info['field_first_name']);
    $vars['last_name'] = render($personal_info['field_last_name']);
    $vars['username'] = render($account['name']);
    $vars['email'] = render($account['mail']);
    $vars['email_confirm'] = render($account['conf_mail']);
    $vars['password'] = render($account['pass']['pass1']);
    $vars['password_confirm'] = render($account['pass']['pass2']);
    $vars['position'] = render($smid_info['field_position_title']);
    $vars['school'] = render($smid_info['field_schools']);
    $vars['smid'] = render($smid_info['field_smid']);
    $vars['captcha'] = render($vars['form']['captcha']);
    $vars['legal'] = render($vars['form']['legal']);

    $vars['submit'] = render($vars['form']['actions']);

    // Call drupal_render_children
    $vars['extra'] = drupal_render_children($vars['form']);
}

/**
 * Implements hook_legal_accept_label()
 */
function njcore_legal_accept_label($variables) {
    if ($variables['link']) {
        return t("I accept the <a href='@terms'>Terms &amp; Conditions</a> of Use", array('@terms' => url('legal')));
    }
    else {
        return t('<strong>Accept</strong> Terms & Conditions of Use');
    }
}

/**
 * Implements hook_preprocess_contact_site_form().
 */
function njcore_preprocess__usaedu_search_block_form(&$variables) {
  // Shorten the form variable name for easier access.
  $form = $variables['form'];

  // Adds wrapper class from psd2html and injects jcf variables
  $form['search']['#prefix'] = '<i class="icon-search"></i>';
  $form['search']['#attributes']['placeholder'] = $form['search']['#title'];
  $form['search']['#title'] = '';
  unset($form['search']['#theme_wrappers']); // Removes extra divs around input box

  $form['end_user']['#prefix'] = '<div class="col-select">';
  $form['end_user']['#title'] = 'I am a';
  $form['end_user']['#suffix'] = '</div>';
  $form['end_user']['#attributes']['class'][] = 'sel-1 sel-2';

  $form['subject']['#prefix'] = '<div class="col-select">';
  $form['subject']['#title'] = 'Interested in';
  $form['subject']['#suffix'] = '</div>';
  $form['subject']['#attributes']['class'][] = 'sel-1';
  $form['subject']['#attributes']['multiple'] = 'true';
  $form['subject']['#attributes']['data-jcf'] = '{"wrapNative": false, "wrapNativeOnMobile": false, "useCustomScroll": false, "multipleCompactStyle": true}';

  $form['grade_level']['#prefix'] = '<div class="col-select">';
  $form['grade_level']['#title'] = 'Matieral for';
  $form['grade_level']['#suffix'] = '</div>';
  $form['grade_level']['#attributes']['class'][] = 'sel-1';
  $form['grade_level']['#attributes']['multiple'] = 'true';
  $form['grade_level']['#attributes']['data-jcf'] = '{"wrapNative": false, "wrapNativeOnMobile": false, "useCustomScroll": false, "multipleCompactStyle": true}';

  // Create variables for individual elements.
  $variables['search'] = render($form['search']);
  $variables['submit'] = render($form['submit']);
  $variables['end_user'] = render($form['end_user']);
  $variables['subject'] = render($form['subject']);
  $variables['grade_level'] = render($form['grade_level']);
  // Be sure to print the remaining rendered form items.
  $variables['children'] = drupal_render_children($form);
}

/**
 * Implements hook_preprocess_user_login_block().
 */
function njcore_preprocess_user_login_block(&$vars) {

    // Sets form data
    $vars['form']['#block']->subject = '<a href="login" class="nav-title">login/sign up</a>';

    $vars['form']['name']['#attributes']['placeholder'] = t('School Email Address');
    $vars['form']['name']['#title'] = '<none>';

    $vars['form']['pass']['#attributes']['placeholder'] = t('Password');
    $vars['form']['pass']['#title'] = '<none>';

    $vars['form']['actions']['submit']['#value'] = t('Sign In >>');

    unset($vars['form']['links']); // Remove links as template file contains them

    // Sets vars used in template tpl
    $vars['name'] = render($vars['form']['name']);
    $vars['pass'] = render($vars['form']['pass']);
    $vars['submit'] = render($vars['form']['actions']['submit']);
    $vars['rendered'] = drupal_render_children($vars['form']);

}


/**
 * Implements hook_form_alter()
 */
function njcore_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'search_block_form') {
        $form['search_block_form']['#attributes']['placeholder'] = t('Enter search term');
        unset($form['search_block_form']['#theme_wrappers']); // Unsets the search glyph icon
    }
}

/**
 * Implements hook_css_alter().
 */
function njcore_css_alter(&$css) {
    // Custom bootstrap CSS is imported through the theme.info file -- remove CDN bootstrap
    if($css['//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css']) {
        unset($css['//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css']);
    }
}

/*
 * Override breadcrumb theming.
 */
function njcore_breadcrumb($variables) {

    $breadcrumb = $variables['breadcrumb'];
    $output = '';

    // Do not show any breadcrumb on the home page.
    if (drupal_is_front_page()) { return; }

    // Determine if we are to display the breadcrumb.
    $show_breadcrumb = theme_get_setting('zen_breadcrumb');
    if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

        // Optionally get rid of the homepage link.
        $show_breadcrumb_home = theme_get_setting('zen_breadcrumb_home');
        if (!$show_breadcrumb_home && strip_tags($breadcrumb[0]) == 'Home') {
            array_shift($breadcrumb);
        }

        // Get the other theme breadcrumb settings: separator, etc.
        $breadcrumb_separator = theme_get_setting('zen_breadcrumb_separator');
        $trailing_separator = $title = '';
        if (theme_get_setting('zen_breadcrumb_title')) {
            $item = menu_get_item();
            if (!empty($item['tab_parent'])) {
                // If we are on a non-default tab, use the tab's title.
                $breadcrumb[] = check_plain($item['title']);
            }
            // Only add the current page at the end if it isn't already there.
            else {
                $title = drupal_get_title();
                $lastcrumb = end($breadcrumb);
                if (strip_tags($lastcrumb) != $title && $title != 'Find resources') {
                    $breadcrumb[] = drupal_get_title();
                }
            }
        }
        elseif (theme_get_setting('zen_breadcrumb_trailing')) {
            $trailing_separator = $breadcrumb_separator;
        }

        // Provide a navigational heading to give context for breadcrumb links to
        // screen-reader users.
        if (empty($variables['title'])) {
            $variables['title'] = t('You are here');
        }
        // Unless overridden by a preprocess function, make the heading invisible.
        if (!isset($variables['title_attributes_array']['class'])) {
            $variables['title_attributes_array']['class'][] = 'element-invisible';
        }

        // Wrap in markup.
        $output = '<nav class="breadcrumb" role="navigation">';
        $output .= '<h2' . drupal_attributes($variables['title_attributes_array']) . '>' . $variables['title'] . '</h2>';
        $output .= '<ol><li>' . implode($breadcrumb_separator . '</li><li>', $breadcrumb) . $trailing_separator . '</li></ol>';
        $output .= '</nav>';
    }

    return $output;
}


// Adds Font-awesome icon fonts.
drupal_add_css("//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css", array('type' => 'external'));

/**
 * Uses the menu active trail to find the correct color scheme
 */
function getMarkup($element) {
    $markup = '';

    switch ($element) {

        case 'login' :
            $markup = '<div class="dropdown-menu" role="menu"><div class="drop-hold login-menu">
    <div class="default-view active login">
        <div class="sing"><a href="/user/register">SIGN UP!</a></div>
        <span class="account">Already have an ACCOUNT?</span>
    </div><!-- .default-view -->'. render(drupal_get_form("user_login_block")).'</div></div>';
            break;

    }

    return $markup;
}