<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function njcore_form_system_theme_settings_alter(&$form, &$form_state) {

    // Create Vertical Tab Group
    $form['theme'] = array(
        '#type' => 'vertical_tabs',
        '#weight' => -30,
        '#prefix' => '<h2><small>Site/Theme Specific Settings</small></h2>',
    );

    // Create Page settings
    $form['page'] = array(
        '#type' => 'fieldset',
        '#title' => t('Page Assets'),
        '#weight' => 5,
        '#collapsible' => true,
        '#collapsed' => false,
        '#group' => 'theme',
    );

    $form['page']['homepage'] = array(
        '#type' => 'fieldset',
        '#title' => t('Homepage'),
        '#collapsible' => true,
        '#collapsed' => false,
    );
    $form['page']['homepage']['homepage_video'] = array(
        '#title' => t('Homepage Video'),
        '#description' => t('Upload a video, allowed extensions: mp4'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://homepage_video/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('mp4'),
        ),
        '#default_value' => theme_get_setting('homepage_video'),
        '#group' => 'theme',
    );
    $form['page']['homepage']['homepage_poster'] = array(
        '#title' => t('Homepage Video Poster'),
        '#description' => t('Upload an image to be displayed when the Front Page Video is not playing'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://homepage_video/poster/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#default_value' => theme_get_setting('homepage_poster'),
        '#group' => 'theme',
    );

    $form['page']['registration'] = array(
        '#type' => 'fieldset',
        '#title' => t('Registration'),
        '#collapsible' => true,
        '#collapsed' => true,
    );
    $form['page']['registration']['registration_image'] = array(
        '#title' => t('Registration Image'),
        '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://page_images/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#default_value' => theme_get_setting('registration_image'),
        '#group' => 'theme',
    );

    $form['page']['global'] = array(
        '#type' => 'fieldset',
        '#title' => t('Misc Pages'),
        '#collapsible' => true,
        '#collapsed' => true,
    );
    $form['page']['global']['admin_toolkit_image'] = array(
        '#title' => t('Admin Toolkit Image'),
        '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://page_images/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#default_value' => theme_get_setting('admin_toolkit_image'),
        '#group' => 'theme',
    );
    $form['page']['global']['breadcrumb_image'] = array(
        '#title' => t('Breadcrumb Image'),
        '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://page_images/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#default_value' => theme_get_setting('breadcrumb_image'),
        '#group' => 'theme',
    );


    // Create footer settings
    $form['footer'] = array(
        '#type' => 'fieldset',
        '#title' => t('Footer Assets'),
        '#weight' => 5,
        '#collapsible' => true,
        '#collapsed' => false,
        '#group' => 'theme',
    );
  $form['footer']['footer_logo'] = array(
    '#title' => t('Footer logo'),
    '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://footer_logo/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
    '#default_value' => theme_get_setting('footer_logo'),
    '#group' => 'theme',
  );
  $form['footer']['client_logo'] = array(
    '#title' => t('Client logo'),
    '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://footer_logo/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
    '#default_value' => theme_get_setting('client_logo'),
    '#group' => 'theme',
  );
  $form['#submit'][] = 'njcore_settings_form_submit';

// Get all themes.
  $themes = list_themes();
// Get the current theme
  $active_theme = $GLOBALS['theme_key'];
  $form_state['build_info']['files'][] = str_replace("/$active_theme.info", '', $themes[$active_theme]->filename) . '/theme-settings.php';
}

function njcore_settings_form_submit(&$form, $form_state) {
    // Save Page Data
    $client_fid = $form_state['values']['homepage_video'];
    $client_video = file_load($client_fid);
    saveManagedFile($client_video);

    $client_fid = $form_state['values']['homepage_poster'];
    $client_video = file_load($client_fid);
    saveManagedFile($client_video);

    $client_fid = $form_state['values']['registration_image'];
    $reg_image = file_load($client_fid);
    saveManagedFile($reg_image);

    $client_fid = $form_state['values']['admin_toolkit_image'];
    $toolkit_image = file_load($client_fid);
    saveManagedFile($toolkit_image);

    $client_fid = $form_state['values']['breadcrumb_image'];
    $crumb_image = file_load($client_fid);
    saveManagedFile($crumb_image);

    // Save Footer Data
  $image_fid = $form_state['values']['footer_logo'];
  $footer_image = file_load($image_fid);
  saveManagedFile($footer_image);

  $client_fid = $form_state['values']['client_logo'];
  $client_logo = file_load($client_fid);
  saveManagedFile($client_logo);

}

/**
 * @param $object
 * Saves a managed file entity type
 */
function saveManagedFile($object) {
    if (is_object($object)) {
        // Check to make sure that the file is set to be permanent.
        if ($object->status == 0) {
            // Update the status.
            $object->status = FILE_STATUS_PERMANENT;
            // Save the update.
            file_save($object);
            // Add a reference to prevent warnings.
            file_usage_add($object, 'njcore', 'theme', 1);
        }
    }
}