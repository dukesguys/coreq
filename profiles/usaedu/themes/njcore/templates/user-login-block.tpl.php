

<div class="login-view hidden">
    <div class="top-box">

        <div class="header">
            <h2 class="white caps">login</h2>
            <h3><?php print $submit; ?></h3>
        </div>

        <div class="login-name">
            <?php print $name; ?>
            <a href="/user/password" class="forgot-link">Forgot Email Address?</a>
        </div>
        <div class="password">
            <?php print $pass; ?>
            <a href="/user/password" class="forgot-link">Forgot Password?</a>
        </div>

        <?php print $rendered; // Print remaining elements ?>

    </div>
    <div class="bottom-box">
        <a class="white caps" href="/user/register">don't have an account?</a>
    </div>

</div><!--.login-view-->

