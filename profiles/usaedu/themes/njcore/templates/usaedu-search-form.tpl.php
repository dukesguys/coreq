<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


module_load_include('module', 'nf_twitter');

//$tweets = nf_twitter_fetch();
//var_dump($tweets);
//die();

$tweets = array(); // Fix this to pull tweets -- setting to an empty array for now
?>

<div class="col sameheight">
    <form action="#" class="filter-form">
        <fieldset>
            <div class="row-input">
                <?php print $search; ?>
                <?php print $submit; ?>
            </div>
            <div class="row-select">
                <?php
                    print $end_user;
                    print $subject;
                    print $grade_level;
                    print $children;
                ?>
            </div>
        </fieldset>
    </form>
</div>



<div class="col sameheight">
    <div class="cycle-gallery-1">
        <div class="mask">
            <div class="slideset">


                <?php foreach($tweets as $tweet) : ?>

                    <div class="slide">
                        <h1>Social Media</h1>
                        <div class="box">
                            <a href="#" class="ico"><i class="icon-twitter"></i></a>
                            <div class="text">
                                <div class="text-hold">
                                    <p>#RecentPost Lorem Ipsum</p>
                                </div>
                                <div class="box-time">
                                    <div class="text-hold">
                                        <p>21 hours ago</p>
                                    </div>
                                    <p><a href="#">See post</a></p>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>

                <div class="slide">
                    <h1>Social Media</h1>
                    <div class="box">
                        <a href="#" class="ico"><i class="icon-twitter"></i></a>
                        <div class="text">
                            <div class="text-hold">
                                <p>#RecentPost Lorem Ipsum</p>
                            </div>
                            <div class="box-time">
                                <div class="text-hold">
                                    <p>21 hours ago</p>
                                </div>
                                <p><a href="#">See post</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <h1>Social Media</h1>
                    <div class="box">
                        <a href="#" class="ico"><i class="icon-twitter"></i></a>
                        <div class="text">
                            <div class="text-hold">
                                <p>#RecentPost Lorem Ipsum</p>
                            </div>
                            <div class="box-time">
                                <div class="text-hold">
                                    <p>21 hours ago</p>
                                </div>
                                <p><a href="#">See post</a></p>
                            </div>
                        </div>
                    </div>
                </div>




            </div>
        </div>
        <a class="btn-prev" href="#">Previous</a>
        <a class="btn-next" href="#">Next</a>
    </div>
</div>