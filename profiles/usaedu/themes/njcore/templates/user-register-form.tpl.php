<?php

/**
 * Available Fields in this template file:
 * Generated from hook_preprocess_user_register_form();
 *
 * $first_name = the first name input box
 * $last_name = the last name input box
 *
 * $position = the position title
 * $school = the school option select box
 *
 * $username = the account username
 * $email = email address field
 * $email_confirm = the confirm email field
 * $password = the user password field
 * $password_confirm = the confirm password field
 * $password_strength = the password strength box
 *
 * $smid_top = smid #description field from school option (moved into text field so BS is not adding popover)
 * $captcha = the user captcha
 * $legal = the terms agreement
 *
 * $submit = submit and cancel buttons
 *
 * $extra = rest of the form data
 *
 */
?>

<fieldset class="register-form">
    <div class="form-holder">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $username; ?>
                    <span class="input-text">This will show up when you upload a document.</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $email; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $email_confirm; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $password; ?>
                </div>
                <div class="pass-indicator">
                    <?php // moved with jQuery // ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $password_confirm; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-holder">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $first_name; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $last_name; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $position; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $school; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-holder">
        <div class="text-holder">
            <p><?php print $smid_top; ?></p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php print $smid; ?>
                </div>
                <div class="text">
                    <?php print $smid_bottom; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-holder">
        <div class="row">
            <div class="col-sm-12">
                <div class="captcha">
                    <?php print $captcha; ?>
                </div>
                <div class="input-row">
                    <?php print $legal; ?>
                </div>
                <div class="box-button">
                    <?php print $submit; ?>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<div class="hidden">
    <?php print $extra; ?>
</div>