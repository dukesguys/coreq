<div class="container-fluid">

    <?php if (!empty($title)): ?>
        <div class="title-accordion">
            <h1><?php print $title ?></h1>
        </div>
    <?php endif ?>

    <!-- accordion -->
    <div id="views-bootstrap-accordion-<?php print $id ?>" class=" accordion-box">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php foreach ($rows as $key => $row): ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="id-1">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#id-<?php print $id . '-' . $key ?>" aria-expanded="false" aria-controls="id-<?php print $id . '-' . $key ?>"><?php print $titles[$key] ?> <i class="ico icon-arrow-down"></i></a>
                        </h4>
                    </div>
                    <div id="id-<?php print $id . '-' . $key ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="id-<?php print $id; ?>" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <?php print $row ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
