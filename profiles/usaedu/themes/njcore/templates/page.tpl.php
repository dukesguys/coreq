<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<div id="wrapper">
    <header id="header">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- page logo -->
                <?php if ($logo): ?>
                    <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                    </a>
                <?php endif; ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse noPaddingRight" id="bs-example-navbar-collapse-1">
                <div class="nav-holder">
                    <!-- additional navigation -->
                    <?php if (!empty($primary_nav)): ?>
                        <?php print render($primary_nav); ?>
                    <?php endif; ?>
                    <!-- search form -->
                    <?php if (!empty($page['navigation'])): ?>
                        <?php print render($page['navigation']); ?>
                    <?php endif; ?>
                </div>
            </div>
        </nav>
    </header>

    <main id="main" role="main">

        <?php if(!empty($page['sub_nav'])) : ?>
            <nav class="navbar navbar-default">
                <div class="collapse navbar-collapse noPaddingRight" id="sub-nav">
                    <?php print render($page['sub_nav']); ?>
                </div>
            </nav>
        <?php endif; ?>

        <?php if(!empty($page['content_preface'])) : ?>
            <div id="preface">
                <?php print render($page['content_preface']); ?>
            </div>
        <?php endif; ?>

        <div class="container-fluid">

        </div><!-- .container-fluid -->

        <div class="container-link">

        </div>

        <?php if(!empty($page['header'])) : ?>
        <section id="videoBoxes" class="filter-box sameheight-box">
            <?php print render($page['header']); ?>
        </section>
        <?php endif; ?>


    </main>

    <div class="main-container container-fluid">


            <?php if (!empty($page['sidebar_first'])): ?>
              <aside class="col-sm-3" role="complementary">
                  <?php print render($page['sidebar_first']); ?>
              </aside>  <!-- /#sidebar-first -->
            <?php endif; ?>

            <section>
                    <?php if (!empty($page['highlighted'])): ?>
                      <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
                    <?php endif; ?>
                    <?php if (!empty($breadcrumb)): print $breadcrumb;
                    endif; ?>
                    <a id="main-content"></a>
                    <?php print render($title_prefix); ?>
                    <?php if (!empty($title)): ?>
                      <h1 class="page-header"><?php print $title; ?></h1>
                    <?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php print $messages; ?>
                    <?php if (!empty($tabs)): ?>
                      <?php print render($tabs); ?>
                    <?php endif; ?>
                    <?php if (!empty($page['help'])): ?>
                      <?php print render($page['help']); ?>
                    <?php endif; ?>
                    <?php if (!empty($action_links)): ?>
                      <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>
                    <?php print render($page['content']); ?>
            </section>

                <?php if (!empty($page['sidebar_second'])): ?>
              <aside class="col-sm-3" role="complementary">
              <?php print render($page['sidebar_second']); ?>
              </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

    </div>




    <footer id="footer" class="footer container large">
        <div class="<?php print $classes; ?> center-holder">
            <div class="col">
                <?php if ($page['footer']): ?>
                  <div class="<?php print $classes; ?>">

                      <?php
                      $file_url = '';
                      $file_fid = theme_get_setting('footer_logo');
                      if (!empty($file_fid)) {
                        $file_loaded = file_load($file_fid);
                        if ($file_loaded !== false) {
                          if (!empty($file_loaded->uri)) {
                            $file_url_raw = file_create_url($file_loaded->uri);
                            if ($file_url_raw !== false) {
                              $file_url = $file_url_raw;
                            }
                          }
                        }
                      } else {
                          $file_url_raw = drupal_get_path('theme', $GLOBALS['theme']).'/files/footer-logo.png';
                      }
                      ?>
                      <div class="logo-footer">
                          <img src="<?php print $file_url_raw; ?>" />
                      </div>

                      <?php print render($page['footer']['menu_menu-footer-menu']); ?>
                  </div>
                <?php endif; ?>
            </div>
            <div class="col">
                <?php print render($page['footer']['usaedu_base_in_collaboration']); ?>

                <?php
                $file_url = '';
                $file_fid = theme_get_setting('client_logo');
                if (!empty($file_fid)) {
                  $file_loaded = file_load($file_fid);
                  if ($file_loaded !== false) {
                    if (!empty($file_loaded->uri)) {
                      $file_url_raw = file_create_url($file_loaded->uri);
                      if ($file_url_raw !== false) {
                        $file_url = $file_url_raw;
                      }
                    }
                  }
                } else {
                    $file_url_raw = drupal_get_path('theme', $GLOBALS['theme']).'/files/collab-logo.png';
                }

                ?>

                <div class="logo-client">
                    <img src="<?php print $file_url_raw; ?>" />
                </div>
                    <span class="text">
                        Copyright &copy; <?php print date('Y'); ?> <? print $site_name; ?>
                    </span>
                    <span class="text">
                        Designed by <a class="designed-by" href="http://nickelfish.com/" title="Nickelfish IDM.">Nickelfish</a>, IDM.
                    </span>

            </div>
        </div>
    </footer>
</div><!-- #wrapper -->