<footer id="footer">
	<div class="center-holder">
		<div class="col">
			<!-- page logo -->
			<div class="logo-footer">
				<a href="#"><img src="images/logo-footer.png" height="103" width="404" alt="new jersey collaborative online resource exchange"></a>
			</div>
			<!-- additional navigation -->
			<nav class="nav">
				<ul>
					<li><a href="#">STATE STANDARDS</a></li>
					<li><a href="#">ABOUT US</a></li>
					<li><a href="#">Toolkits</a></li>
					<li><a href="#">Dashboard</a></li>
				</ul>
			</nav>
		</div>
		<div class="col">
			<span class="title-holder">IN COLLABORATION WITH</span>
			<!-- page logo -->
			<div class="logo-client">
				<a href="#"><img src="images/logo-client.png" height="161" width="424" alt="new jersey collaborative online resource exchange"></a>
			</div>
			<span class="text">Copyright ©2015, New Jersey Educator Resource Exchange</span>
			<span class="text">Designed by <a href="#">Nickelfish</a>, IDM.</span>
		</div>
	</div>
</footer>