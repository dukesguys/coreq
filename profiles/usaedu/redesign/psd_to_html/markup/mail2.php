<?php
	//****************************************
	//edit here
	$senderName = 'WEB';
	$senderEmail = 'site@example.com';
	$targetEmail = 'admin@example.com';
	$messageSubject = 'Message from web-site';
	$redirectToReferer = true;
	$redirectURL = 'thankyou.html';
	//****************************************

	// mail content
	$uname = $_POST['uname'];
	$umail = $_POST['umail'];
	$umail_confirm = $_POST['umail_confirm'];
	$upassword = $_POST['upassword'];
	$upassword_confirm = $_POST['upassword_confirm'];
	$ufname = $_POST['ufname'];
	$ulname = $_POST['ulname'];
	$uposition = $_POST['uposition'];
	$select_position = $_POST['select_position'];
	$usm = $_POST['usm'];

	// prepare message text
	$messageText =	'Name: '.$uname."\n".
					'Your Email Address: '.$umail."\n".
					'Confirm Your Email Address: '.$umail_confirm."\n".
					'Password: '.$upassword."\n".
					'Re-Enter Password: '.$upassword_confirm."\n".
					'First Name: '.$ufname."\n".
					'Last Name: '.$ulname."\n".
					'Position Title: '.$uposition."\n".
					'Selected Title: '.$select_position."\n".
					'Enter your SM: '.$usm."\n";

	// send email
	$senderName = "=?UTF-8?B?" . base64_encode($senderName) . "?=";
	$messageSubject = "=?UTF-8?B?" . base64_encode($messageSubject) . "?=";
	$messageHeaders = "From: " . $senderName . " <" . $senderEmail . ">\r\n"
				. "MIME-Version: 1.0" . "\r\n"
				. "Content-type: text/plain; charset=UTF-8" . "\r\n";

	if (preg_match('/^[_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,4}$/',$targetEmail,$matches))
	mail($targetEmail, $messageSubject, $messageText, $messageHeaders);

	// redirect
	if($redirectToReferer) {
		header("Location: ".@$_SERVER['HTTP_REFERER'].'#sent');
	} else {
		header("Location: ".$redirectURL);
	}
?>