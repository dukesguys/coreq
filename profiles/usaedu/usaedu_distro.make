; run it like this: drush make --working-copy --yes --verbose --debug usaedu_distro.make .
; or so, without "verbose" and "debug" flags set: drush make --working-copy --yes usaedu_distro.make .

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7.24"

projects[usaedu][type] = profile
projects[usaedu][download][type] = git
projects[usaedu][download][url] = http://stash.nf-labs.com/scm/njcor/usaedu.git
projects[usaedu][download][branch] = development
