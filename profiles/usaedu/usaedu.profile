<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function usaedu_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks().
 *
 * Add additional tasks to the install profile install process.
 */
function usaedu_install_tasks($install_state) {
  $tasks = array();

  $tasks['set_themes'] = array(
    'display_name' => st('Setting default theme'),
    'display' => TRUE,
    'type' => 'batch',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'usaedu_set_themes',
  );

  $tasks['set_modules'] = array(
    'display_name' => st('Setting modules status'),
    'display' => TRUE,
    'type' => 'batch',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'usaedu_set_modules',
  );

  $tasks['add_taxonomy_terms'] = array(
    'display_name' => st('Add taxonomy terms'),
    'display' => TRUE,
    'type' => 'batch',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'usaedu_add_taxo_terms',
  );

  $tasks['configure_login_destination'] = array(
    'display_name' => st('Configure login destination'),
    'display' => TRUE,
    'type' => 'batch',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'usaedu_configure_login_destination',
  );

  /* Uncomment this after we add user roles.
  $tasks['create_testing_users'] = array(
    'display_name' => st('Creating test users'),
    'display' => TRUE,
    'type' => 'batch',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'usaedu_create_testing_users',
  );
  */

  return $tasks;
}

/**
 * Install task - Sets default themes.
 */
function usaedu_set_themes() {
  // Any themes without keys here will get numeric keys and so will be enabled,
  // but not placed into variables.
  $enable = array(
    'theme_default' => 'usaedu',
    'admin_theme' => 'seven',
  );
  theme_enable($enable);

  foreach ($enable as $var => $theme) {
    if (!is_numeric($var)) {
      variable_set($var, $theme);
    }
  }

  // Disable the default Bartik theme
  theme_disable(array('bartik'));
}


/**
 * Install task - Enable/Disable modules.
 *
 * This are modules that you may turn on/off but are not required for
 * the project to run.
 */
function usaedu_set_modules() {
  module_disable(
    array(
      'admin',
      'toolbar'),
    FALSE
  );

  module_enable(
    array(
      'admin_menu',
      'admin_menu_toolbar',
      'module_filter',
      'elements',
      'html5_tools',
      'magic'
    ),
    FALSE
  );
}

/**
 * Install task - Creates and populates taxonomy on install.
 */
function usaedu_add_taxo_terms() {
  $vocabularies = array_merge(usaedu_get_taxonomy_terms(), usaedu_get_taxonomy_terms_from_csv());
  $batch_size = 10;

  // Process results and prepare batch operations.
  foreach ($vocabularies as $vocabulary => $terms) {
    $offset = 0;
    while (count($batch = array_slice($terms, $offset, $batch_size))) {
      $offset = $offset + count($batch);
      $params = array($vocabulary, $batch);
      $operations[] = array('usaedu_add_term_process', $params);
    }
  }

  $definition = array(
    'operations' => $operations,
    'finished' => 'usaedu_taxonomy_terms_addition_batch_finished',
    'title' => t('Processing taxonomy terms export'),
    'init_message' => t('Taxonomy terms addition is starting.'),
    'progress_message' => t('Processed @percentage percent'),
    'error_message' => t('Taxonomy terms addition has encountered an error.'),
  );

  return $definition;
}

/**
 * Process to be run by batch API
 */
function usaedu_add_term_process($vocabulary_name, $terms, &$context) {
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);
  if ($vocabulary) {
    foreach ($terms as $t) {
      usaedu_traverse_taxonomy_terms_and_create($vocabulary, $t['name'], $t['subterm']);
    }
  }
}

/**
 * Returns an array with all the taxonomy terms grouped by vocubulary
 *
 * @return array() The array of terms grouped by vocabulary as follow:
 *   array(
 *     'vocabulary 1' => array(
 *       array('name' => 'term name', 'subterm' => 'term description'),
 *       ...,
 *       array('name' => 'term name', 'subterm' => 'term description'),
 *     ),
 *     'vocabulary 2' => array(...),
 *  )
 *
 */
function usaedu_get_taxonomy_terms() {
  $terms = array(
    //'vocabulary name' => array(
    //  array(
    //    'name' => '',
    //    'subterm' => '',
    //  ),
    //),
  );

  return $terms;
}


/**
 * Returns an array with all the taxonomy terms grouped by vocubulary
 *
 * The terms are read from a csv file with a file name matching the vocabulary machine name and the with a ("Name", "Description") schema.
 * For example for a my_vocabulary.csv file the content would be something like:
 *
 *   Name, Description
 *   Term 1, "This is my description."
 *   Term 2, "This is my description."
 */
function usaedu_get_taxonomy_terms_from_csv() {
  $terms = array();
  $path = 'profiles/usaedu/resources/taxonomy_terms/';

  if ($handle = opendir($path)) {
    while (false !== ($entry = readdir($handle))) {
      if ($entry != "." && $entry != "..") {
        $file_info = explode('.', $entry, 2);
        if ($file_info[1] === 'csv') {
          $row = 0;
          if (($file = fopen("$path/$entry", "r")) !== FALSE) {
            while (($data = fgetcsv($file, 1000, ',', '"', '\\')) !== FALSE) {
              if (!$row++) continue; // Skipping the header row.

              // data[0] = name; data[1] = description
              if (!empty($data[0])) {
                $terms[$file_info[0]][] = array(
                  'name' => $data[0],
                  'subterm' => (isset($data[1])) ? $data[1] : "",
                );
              }
            }
            fclose($file);
          }
        }
      }
    }
    closedir($handle);
  }

  return $terms;
}

/**
 * Add terms into the database
 */
function usaedu_traverse_taxonomy_terms_and_create($vocabulary, $name = '', $subterms = '', $parent_tid = 0) {
  //set_time_limit(60);  // To be used in case of running into timeouts
  static $weight = 1;
  if (!isset($name)) return;
  if (!isset($vocabulary)) return;

  $description = (!is_array($subterms)) ? $subterms : '';

  $term = new stdClass();
  $term->vocabulary_machine_name = $vocabulary->machine_name;
  $term->vid = $vocabulary->vid;
  $term->name = $name;
  $term->description = $description;
  if (isset($parent_tid) && $parent_tid != 0) {
    $term->parent = $parent_tid;
  }
  $term->weight = $weight;
  $weight++;
  $status = taxonomy_term_save($term);
  if (is_array($subterms)) {
    foreach ($subterms as $sub_term_name => $sub_term_subterms) {
      usaedu_traverse_taxonomy_terms_and_create($vocabulary, $sub_term_name, $sub_term_subterms, $term->tid);
    }
  }
  //set_time_limit(30);
  return;
}

function usaedu_taxonomy_terms_addition_batch_finished () {

}

/**
 * Install task - Configure non-featurizable modules.
 */
function usaedu_configure_login_destination() {
  $result = db_insert('login_destination')
    ->fields(array(
      'id' => 1,
      'triggers' => 'a:1:{s:5:"login";s:5:"login";}',
      'roles' => 'a:0:{}',
      'pages_type' => 0,
      'pages' => '',
      'destination_type' => 0,
      'destination' => 'dashboard',
      'weight' => 0,
    ))
    ->execute();
}

/**
 * Install task - Creates some testing user accounts.
 */
function usaedu_create_testing_users() {
  $roles = array_flip(user_roles(TRUE));

  $base_user = new stdclass();
  $base_user->pass = 'affinity';
  $base_user->roles = array(
    $roles['authenticated user'] => $roles['authenticated user']
  );
  $base_user->status = TRUE;

  $authenticated_user = clone $base_user;
  $authenticated_user->name = 'authenticated';
  $authenticated_user->mail = 'robin+usaedu-authenticated@affinitybridge.com';
  user_save(NULL, (array) $authenticated_user);

  $administrator_user = clone $base_user;
  $administrator_user->name = 'administrator';
  $administrator_user->mail = 'robin+usaedu-administrator@affinitybridge.com';
  $administrator_user->roles += array(
    $roles['Administrator'] => $roles['Administrator']
  );
  user_save(NULL, (array) $administrator_user);
}
