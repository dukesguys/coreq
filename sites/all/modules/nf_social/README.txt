This module does not integrate tightly with Drupal.

The main reason/case scenario is when using Domain Access with a single DB -- but needing different social feeds based upon themes/domains.


######
## To use
######
#
# Call the following function within the template.tpl.php file to render the appropriate feed
#  Twitter: nf_twitter_fetch();
##      nf_twitter_fetch() can accept an array of auth args:
##  :   CONSUMER_KEY
##  :   CONSUMER_SECRET
##  :   access_token
##  :   access_secret

